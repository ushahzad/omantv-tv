//
//  LiveChannelsVC.m
//  OmanTV
//
//  Created by Curiologix on 24/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "LiveChannelsVC.h"
#import "TVChannelShowsVC.h"
#import "RadioSubCategoriesVC.h"
@interface LiveChannelsVC ()

@end

@implementation LiveChannelsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"channels"];
    
    self.contentList = [NSMutableArray new];

    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    self.view_parent.hidden = true;
    
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadTVChannelsFromServer];
    
    [self.indicator startAnimating];
    
    [self setupChannelsTypesController];
    [self getChannelBg];
}

-(void)setupChannelsTypesController
{
    ChannelsTypeVC *vc_channelsTypeVC = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    
    CGRect frame = self.view.frame;
    CGFloat height = 70;
    
    [self addChildViewController:vc_channelsTypeVC];
    
    [vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_channelsTypes addSubview:vc_channelsTypeVC.view];
    
    [vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:vc_channelsTypeVC.view];
    
    vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info) {
        weakSelf.isRadio = isRadio;
        [self resetContent];
        [self loadTVChannelsFromServer];
    };
}

-(void)resetContent
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

-(void)getChannelBg {
    NSString *path = [NSString stringWithFormat:@"%@/plus/settings?user_id=168&key=49e55ab68365d827e87a3fb857c5aa87&smartTV=1",BaseURL];
    
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"channel"];
}

-(void)loadTVChannelsFromServer
{
    //[self resetControllerData];
    
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/plus/live_channels?user_id=%@&key=%@&&json=1&is_radio=%d&mixed=yes&info=1&mplayer=true&need_live=yes&need_next=yes&next_limit=2&need_playback=yes",BaseURL,BaseUID,BaseKEY,self.isRadio];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:self.isRadio ? @"radio" : @"tv"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        self.view_parent.hidden = false;
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         self.view_parent.hidden = false;
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        NSString *selectedType = self.isRadio ? @"radio" : @"tv";
        if ([callType containsString:selectedType])
        {
            for (NSDictionary *chDic in responseObject)
            {
                NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
                if ([enableLive isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
                {
                    continue;
                }
                
                if (chDic[@"catchup"] && [chDic[@"catchup"] intValue] == 0)
                {
                    continue;
                }
                
//                if ([chDic[@"catchup"] intValue] != 1)
//                {
//                    continue;
//                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:chDic[@"id"]];
                info.playbackUrl = [StaticData checkStringNull:chDic[@"playbackURL"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
                info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
                
//                if (info.thumbnailPath.length == 0) {
//                    info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
//                }
                info.thumbnail = [StaticData checkStringNull:chDic[@"cover"]];
                
                
                if ([StaticData isVideoBlockInMyCountry:chDic isShowGeoStatus:false])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData checkIsScueduleGeoBlockOnLiveChannel:chDic])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData isLiveChannelDigitalRightsDisable:chDic])
                {
                    info.isLiveShowDigitalRightDisable = true;
                }
                
                if ([selectedType isEqualToString:@"radio"]) {
                    info.isRadio = true;
                }
                
                [_contentList addObject:info];
                
            }
            [self reloadCollectionView];
        }
        else if ([callType isEqualToString:@"channel"]) {
            NSString *thumbnail = responseObject[@"apple_tv_ch_bg"];
            NSString *encodedCover = [thumbnail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
            [self.img_bgChannel sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                     if (!self.tempImage) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                 } else {
                 }
             }];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)checkRecordFound
{
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGFloat width = (self.view.frame.size.width - 120 ) / 5;
    if (self.contentList.count < 5) {
        self.collectionViewWidth.constant = (width * self.contentList.count) + 60;
    }
    else {
        self.collectionViewWidth.constant = self.view.frame.size.width - 120;
    }
    return CGSizeMake(width, 200.0);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.contentList[indexPath.row];
    cell.lbl_title.text = info.title;
    NSString *path = info.thumbnailPath;
    
    if (info.isSelected) {
        cell.view_blur.alpha = 1;
        cell.view_blur.backgroundColor = [UIColor whiteColor];
    } else {
        cell.view_blur.alpha = 0.7;
        cell.view_blur.backgroundColor = [StaticData colorFromHexString:@"#262626"];
    }
//
//    cell.alpha = 0.2;
//    cell.contentView.alpha = 0.2;
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:path];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [cell.indicator startAnimating];
        
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.layer.cornerRadius = 16.0;
    cell.clipsToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    TVChannelShowsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TVChannelShowsVC"];
    vc.selectedChannel = info;
    [self.navigationController pushViewController:vc animated:true];
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}

-(void)startSliderTimer
{
//    if ([self.sliderTimer isValid]) {
//        [self.sliderTimer invalidate];
//    }
//
//    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollToNextSliderItem) userInfo:nil repeats:true];
}


-(IBAction)playVideo:(id)sender
{
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        cell.view_blur.backgroundColor = [StaticData colorFromHexString:@"#262626"];
        
        cell.view_blur.alpha = 0.7;
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        cell.view_blur.backgroundColor = [UIColor whiteColor];
        
        cell.view_blur.alpha = 1;
    }
}

/*+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}*/

/*-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }
            
        }];
    }
    
    if (context.nextFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }
        }];
    }
}*/

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}
@end
