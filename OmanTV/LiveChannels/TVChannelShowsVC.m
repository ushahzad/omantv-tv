//
//  TVChannelShowsVC.m
//  OmanTV
//
//  Created by Curiologix on 24/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "TVChannelShowsVC.h"

@interface TVChannelShowsVC ()

@end

@implementation TVChannelShowsVC

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"channels"];
    
    self.contentList = [NSMutableArray new];

    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.pageNumber = 1;
    self.lbl_noRecordsFound.text = NoRecordFound;
    [StaticData scaleToArabic:self.collectionView];
    
    self.lbl_title.text = self.selectedChannel.title;
    
    [self loadDataFromServer];
}

-(void)loadDataFromServer
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&channel_id=%@&cat_id=&genre_id=&p=%d&ended=no&limit=30&is_radio=%d&need_show_times=no&need_channel=yes&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedChannel.ID,self.pageNumber,self.selectedChannel.isRadio,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"SubCategory"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"SubCategory"])
        {
            NSInteger totalResults = self.contentList.count;
            
            for (NSDictionary *dicObj in responseObject[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"app_thumbnail"]];
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            self.isNeedToLoadMorePrograms = true;
            if (totalResults == self.contentList.count)
            {
                self.isNeedToLoadMorePrograms = false;
            }
            
            if (_contentList.count == 0) {
                self.lbl_noRecordsFound.hidden = false;
            }
            [self reloadCollectionView];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 180) / 6;
    CGFloat height = width;
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    return CGSizeMake(width, height+ 50);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     if (image)
                     {
                         //[StaticData imageLoaded:image withCell:cell];
                     }
                     
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.layer.cornerRadius = 12.0;
    cell.clipsToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    if (self.selectedChannel.isRadio)
    {
        [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
    } else
    {
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}



-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

/*+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}*/

/*-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }
            
        }];
    }
    
    if (context.nextFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }
        }];
    }
}*/

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.contentList.count > 0) {
            for (int i = 0; i < self.contentList.count; i++)
            {
                MediaInfo *channel = self.contentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight-50)
        {
            // then we are at the end
            if (self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = false;
                _pageNumber += 1;
                
                [self loadDataFromServer];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

@end
