//
//  DubaiFontLabel.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 28/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DubaiFontLabel : UILabel

@end

NS_ASSUME_NONNULL_END
