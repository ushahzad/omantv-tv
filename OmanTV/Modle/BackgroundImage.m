//
//  BackgroundImage.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 10/11/2020.
//  Copyright © 2020 dotcom. All rights reserved.
//

#import "BackgroundImage.h"

@implementation BackgroundImage

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.image = nil;
    self.backgroundColor = [StaticData colorFromHexString:@"#1F2022"];
}

@end
