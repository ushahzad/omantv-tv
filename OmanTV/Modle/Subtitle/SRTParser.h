//
//  SRTParser.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 02/09/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SDSRTParserError)
{
    SDSRTMissingIndexError,
    SDSRTCarriageReturnIndexError,
    SDSRTInvalidTimeError,
    SDSRTMissingTimeError,
    SDSRTMissingTimeBoundariesError
};

@interface SRTParser : NSObject

//@property (nonatomic, copy) NSArray *subtitles;
@property (nonatomic, strong) NSMutableArray *videoThumbnails;

+(SRTParser *)shared;
- (NSMutableArray *)loadVideoThumbnailsWithUrl:(NSString *)strFilePath;
@end

NS_ASSUME_NONNULL_END
