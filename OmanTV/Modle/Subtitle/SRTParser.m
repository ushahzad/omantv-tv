//
//  SRTParser.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 02/09/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "SRTParser.h"
#import "SDSubtitle.h"
#import "ASBSubtitle.h"

@implementation SRTParser

static SRTParser *sharedInstance;
+(SRTParser *)shared
{
    if (!sharedInstance) {
        sharedInstance = [SRTParser new];
    }
    return sharedInstance;
}

- (NSMutableArray *)loadVideoThumbnailsWithUrl:(NSString *)strFilePath
{
    NSURL *url = [NSURL URLWithString:strFilePath];
    NSError *localError;
    NSString *text;
    
    text = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&localError];
    if (localError == nil)
    {
        return [self loadVideoThumbnail:text error:&localError];
    }
    
    if(localError != NULL)
    {
        
    }
    return [NSMutableArray new];
}

- (void)loadFromString:(NSString *)subtitlesString error:(NSError **)error
{
    //_subtitles = [SRTParser parseString:subtitlesString error:error];
}

- (NSMutableArray *)loadVideoThumbnail:(NSString *)string error:(NSError **)error
{
    NSScanner *scanner;
    
    scanner = [NSScanner scannerWithString:string];
    self.videoThumbnails = [NSMutableArray new];
    
    while (!scanner.isAtEnd)
    {
        NSInteger index;
        NSString *startString;
        NSString *endString;
        NSString *text = @"";
        NSString *line;
        BOOL endScanningText;
        
        scanner.charactersToBeSkipped = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        //[scanner scanInteger:&index];
        [scanner scanUpToString:@"-->" intoString:&startString];
        [scanner scanString:@"-->" intoString:NULL];
        [scanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&endString];
        scanner.charactersToBeSkipped = nil;
        [scanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:NULL];
        do {
            endScanningText = ![scanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&line];
            if(!endScanningText)
            {
                line = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                text = [text stringByAppendingFormat:@"%@%@", (text.length > 0?@"\n":@""), line];
                [scanner scanUpToString:@"\n" intoString:NULL];
                [scanner scanString:@"\n" intoString:NULL];
            }
        } while (!endScanningText);
        
        MediaInfo *info = [MediaInfo new];
        info.thumbnail = text;
        NSArray *thumbPositions = [[[text componentsSeparatedByString:@"xywh="] lastObject] componentsSeparatedByString:@","];
        if (thumbPositions.count >= 4) {
            info.x = [thumbPositions[0] integerValue];
            info.y = [thumbPositions[1] integerValue];
            info.w = [thumbPositions[2] integerValue];
            info.h = [thumbPositions[3] integerValue];
        } else {
            continue;
        }
        info.thumbnailPosition = thumbPositions;
        info.startTime = [self videoTimeFromString:startString];
        info.endTime = [self videoTimeFromString:endString];
        info.thumbStartTime = [self durationFromString:startString];
        info.thumbStopTime = [self durationFromString:endString];
        
        [_videoThumbnails addObject:info];
    }
    
    if(error != NULL)
    {
        *error = nil;
    }
    //[self setupTimeObserver];
    return _videoThumbnails;
}

- (NSString *)videoTimeFromString:(NSString *)timeString
{
    NSScanner *scanner;
    NSInteger hours = 0;
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    NSInteger milliseconds = 0;
    
    scanner = [NSScanner scannerWithString:timeString];
    
    [scanner scanInteger:&hours];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&minutes];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&seconds];
    [scanner scanString:@"," intoString:NULL];
    [scanner scanInteger:&milliseconds];
    
    //if (hours > 0) {
        NSString *hoursString = hours < 10 ? [NSString stringWithFormat:@"0%d", (int)hours] : [NSString stringWithFormat:@"%d", (int)hours];
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", (int)(int)minutes] : [NSString stringWithFormat:@"%d", (int)minutes];
        NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", (int)seconds] : [NSString stringWithFormat:@"%d", (int)seconds];
        return [NSString stringWithFormat:@"%@:%@:%@",hoursString,minsString, secsString];
    //}
    
    /*NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", (int)minutes] : [NSString stringWithFormat:@"%d", (int)minutes];
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", (int)seconds] : [NSString stringWithFormat:@"%d", (int)seconds];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];*/
}

- (NSTimeInterval)durationFromString:(NSString *)timeString
{
    NSScanner *scanner;
    NSInteger hours = 0;
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    NSInteger milliseconds = 0;
    NSTimeInterval time;
    
    scanner = [NSScanner scannerWithString:timeString];
    
    [scanner scanInteger:&hours];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&minutes];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&seconds];
    [scanner scanString:@"," intoString:NULL];
    [scanner scanInteger:&milliseconds];
    
    time = hours*3600 + minutes*60 + seconds + milliseconds/1000.0;
    
    return time;
}

+ (NSArray *)parseString:(NSString *)string error:(NSError **)error
{
    NSMutableArray *subtitles = [[NSMutableArray alloc] init];
    NSScanner *scanner = [NSScanner scannerWithString:string];
    scanner.charactersToBeSkipped = [NSCharacterSet whitespaceCharacterSet];
    NSUInteger line = 1;
    
    while (![scanner isAtEnd])
    {
        //@autoreleasepool
        {
            // Skip garbage lines if any
            [scanner scanCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:NULL];
            NSString *cr;
            while ([scanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&cr])
            {
                line += cr.length;
            }
            
            if ([scanner isAtEnd])
            {
                break;
            }
            
            NSInteger index;
            if (![scanner scanInteger:&index])
            {
                if (error) *error = [self errorWithDescription:@"Missing index" type:SDSRTMissingIndexError line:line];
                return nil;
            }
            
            if (![scanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&cr])
            {
                if (error) *error = [self errorWithDescription:@"Missing return after index" type:SDSRTCarriageReturnIndexError line:line];
                return nil;
            }
            line += cr.length;
            
            NSTimeInterval startTime = [self scanTime:scanner];
            if (startTime == -1)
            {
                if (error) *error = [self errorWithDescription:@"Invalid start time" type:SDSRTInvalidTimeError line:line];
                return nil;
            }
            
            if (![scanner scanString:@"-->" intoString:NULL])
            {
                if (error) *error = [self errorWithDescription:@"Missing or invalid time boundaries separator" type:SDSRTMissingTimeBoundariesError line:line];
                return nil;
            }
            
            NSTimeInterval endTime = [self scanTime:scanner];
            if (endTime == -1)
            {
                if (error) *error = [self errorWithDescription:@"Invalid end time" type:SDSRTInvalidTimeError line:line];
                return nil;
            }
            
            if (![scanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&cr])
            {
                if (error) *error = [self errorWithDescription:@"Missing return after time boundaries" type:SDSRTCarriageReturnIndexError line:line];
                return nil;
            }
            line += cr.length;
            
            NSMutableString *content = [[NSMutableString alloc] init]
            ;
            NSString *lineString;
            while ([scanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&lineString])
            {
                [content appendString:lineString];
                [scanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&cr];
                line += cr.length;
                if (cr.length > 1)
                {
                    break;
                }
            }
            [subtitles addObject:[[SDSubtitle alloc] initWithIndex:index startTime:startTime endTime:endTime content:content]];
        }
    }
    
    return [subtitles copy];
}

+ (NSTimeInterval)scanTime:(NSScanner *)scanner
{
    NSInteger hours, minutes, seconds, milliseconds;
    if (![scanner scanInteger:&hours]) return -1;
    if (![scanner scanString:@":" intoString:NULL]) return -1;
    if (![scanner scanInteger:&minutes]) return -1;
    if (![scanner scanString:@":" intoString:NULL]) return -1;
    if (![scanner scanInteger:&seconds]) return -1;
    if (![scanner scanString:@"," intoString:NULL]) return -1;
    if (![scanner scanInteger:&milliseconds]) return -1;
    
    if (hours < 0  || minutes < 0  || seconds <  0 || milliseconds < 0 ||
        hours > 60 || minutes > 60 || seconds > 60 || milliseconds > 999)
    {
        return -1;
    }
    
    return (hours * 60 * 60) + (minutes * 60) + seconds + (milliseconds / 1000.0);
}

+ (NSError *)errorWithDescription:(NSString *)description type:(SDSRTParserError)type line:(NSUInteger)line
{
    description = [description stringByAppendingFormat:@" at %d line", (uint)line + 1];
    return [NSError errorWithDomain:@"SDSRTParser" code:type userInfo:@
            {
            NSLocalizedDescriptionKey: description,
                @"line": @(line)
            }];
}

@end
