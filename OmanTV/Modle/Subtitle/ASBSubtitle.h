//
//  ASBSubtitle.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 02/09/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ASBSubtitle : NSObject
@property (nonatomic, assign) NSTimeInterval startTime;
@property (nonatomic, assign) NSTimeInterval stopTime;
@property (nonatomic, retain) NSString *thumbnail;
@property (nonatomic, retain) NSArray *thumbnailPosition;
@property (nonatomic, assign) NSInteger x;
@property (nonatomic, assign) NSInteger y;
@property (nonatomic, assign) NSInteger w;
@property (nonatomic, assign) NSInteger h;
@end

NS_ASSUME_NONNULL_END
