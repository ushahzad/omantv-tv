//
//  LabelClass.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 17/10/2020.
//  Copyright © 2020 dotcom. All rights reserved.
//

#import "LabelClass.h"

@implementation LabelClass

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UIFont *font = self.font;
    UIFont *f = [UIFont fontWithName:[StaticData AppFont:self.tag] size:font.pointSize + 4];
    self.font = f;
    
    NSTextAlignment alignment = self.textAlignment;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineHeightMultiple = 0.85;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.text.length)];

    self.attributedText = attributedString;
    self.textAlignment = alignment;
}

//- (void)drawTextInRect:(CGRect)rect {
//    if (self.tag == 101 || self.tag == 102) {
//        [super drawTextInRect:rect];
//    } else {
//        UIEdgeInsets myLabelInsets = {0, 0, 0, 0};
//        if (![StaticData isLanguageEnglish]) {
//            [super drawTextInRect:UIEdgeInsetsInsetRect(rect, myLabelInsets)];
//        } else {
//            [super drawTextInRect:rect];
//        }
//    }
//}

@end
