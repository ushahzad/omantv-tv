//
//  CollectionCell.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 08/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CollectionCell : UICollectionViewCell
@property void (^DeleteCallBack) (CollectionCell *cell,MediaInfo *info);
@property void (^ViewCallBack) (CollectionCell *cell,MediaInfo *info);
@property (nonatomic, weak) IBOutlet MarqueeLabel *lbl_titleMarquee;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_subTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbl_time;
@property (nonatomic, weak) IBOutlet UILabel *lbl_date;
@property (nonatomic, weak) IBOutlet UILabel *lbl_now;
@property (nonatomic, weak) IBOutlet UILabel *lbl_pg;
@property (nonatomic, weak) IBOutlet UILabel *lbl_seasons;
@property (nonatomic, weak) IBOutlet UILabel *lbl_videoDropDownTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbl_episode;
@property (nonatomic, weak) IBOutlet UILabel *lbl_rating;
@property (nonatomic, weak) IBOutlet UIView *view_blur;
@property (nonatomic, weak) IBOutlet UIView *view_channelParent;
@property (nonatomic, weak) IBOutlet UIView *view_info;
@property (nonatomic, weak) IBOutlet UIView *view_play;
@property (nonatomic, weak) IBOutlet UIView *view_border;
@property (nonatomic, weak) IBOutlet UIView *view_time;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, weak) IBOutlet UILabel *lbl_weekDays;
@property (nonatomic, weak) IBOutlet UITextView *txv_info;
@property (nonatomic, weak) IBOutlet UIButton *btn_play;
@property (nonatomic, weak) IBOutlet UIButton *btn_add;
@property (nonatomic, weak) IBOutlet UIButton *btn_like;
@property (nonatomic, weak) IBOutlet UIButton *btn_dislike;
@property (nonatomic, weak) IBOutlet UIButton *btn_info;
@property (nonatomic, weak) IBOutlet UIButton *btn_dropDown;
@property (nonatomic, weak) IBOutlet UIButton *btn_watch;
@property (nonatomic, weak) IBOutlet UIButton *btn_listenNow;
@property (nonatomic, weak) IBOutlet UIButton *btn_notification;
@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UIImageView *borderLine;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *channelLogo;
@property (nonatomic, weak) IBOutlet UIImageView *signLanguageLogo;
@property (nonatomic, weak) IBOutlet UIImageView *img_channel;
@property (nonatomic, weak) IBOutlet UIImageView *img_gradient;
@property (nonatomic, weak) IBOutlet UIImageView *img_play;
@property (nonatomic, weak) IBOutlet UIImageView *img_highlight;
@property (nonatomic, weak) IBOutlet UIImageView *img_checked;
@property (nonatomic, weak) IBOutlet UIImageView *img_exclusive;
@property (nonatomic, weak) IBOutlet UIView *view_pg;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *title_centerConstraint;
@property (strong,nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (nonatomic, retain) MediaInfo *info;

@end

NS_ASSUME_NONNULL_END
