//
//  ButtonClass.m
//  OmanTV
//
//  Created by Curiologix on 27/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "ButtonClass.h"

@implementation ButtonClass

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UIFont *font = self.titleLabel.font;
    // tag grater than 0 mean Bold Font apply
    self.titleLabel.font = [UIFont fontWithName:[StaticData AppFont:self.tag] size:font.pointSize];
}

@end
