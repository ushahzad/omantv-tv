//
//  TextFieldClass.m


#import "TextFieldClass.h"

@implementation TextFieldClass

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UIFont *font = self.font;
    
    // tag grater than 0 mean Bold Font apply
    self.font = [UIFont fontWithName:[StaticData AppFont:self.tag] size:font.pointSize];
}

@end
