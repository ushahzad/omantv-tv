//
//  MenuBar.h
//  AwaanAppleTV-New
//
//  Created by MacUser on 21/10/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MenuBar : UICollectionView <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, retain) NSMutableArray *menuList;
@end

NS_ASSUME_NONNULL_END
