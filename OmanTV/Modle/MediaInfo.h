//
//  MediaInfo.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 08/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MediaInfo : NSObject

@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *channelID;
@property (nonatomic, retain) NSString *profileID;
@property (nonatomic, retain) NSString *premium;
@property (nonatomic, retain) NSString *seasonID;
@property (nonatomic, retain) NSString *showID;
@property (nonatomic, retain) NSString *isShowHide;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *src;
@property (nonatomic, retain) NSString *srclang;
@property (nonatomic, retain) NSString *orderType;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *carousalTitle;
@property (nonatomic, retain) NSString *title_ar;
@property (nonatomic, retain) NSString *title_en;
@property (nonatomic, retain) NSString *season_title;
@property (nonatomic, retain) NSString *showTitle;
@property (nonatomic, retain) NSString *subTitle;
@property (nonatomic, retain) NSString *thumbnailPath;
@property (nonatomic, retain) NSString *hoverImage;
@property (nonatomic, retain) NSString *audioChannelPath;
@property (nonatomic, retain) NSString *channelLogoPath;
@property (nonatomic, retain) NSString *signLogoPath;
@property (nonatomic, retain) NSString *img_logo;
@property (nonatomic, retain) NSString *descriptions;
@property (nonatomic, assign) CGFloat widthConstraint;
@property (nonatomic, assign) CGFloat heightConstraint;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *time;
@property (nonatomic, retain) NSString *startTime;
@property (nonatomic, retain) NSString *endTime;
@property (nonatomic, retain) NSString *stopTime;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *birthDay;
@property (nonatomic, retain) NSString *day;
@property (nonatomic, retain) NSString *trailerID;
@property (nonatomic, retain) NSString *trID;
@property (nonatomic, retain) NSString *shortDate;
@property (nonatomic, retain) NSString *streamingPlaybackUrl;
@property (nonatomic, retain) NSString *position;
@property (nonatomic, retain) NSString *playbackUrl;
@property (nonatomic, retain) NSString *episodeCount;
@property (nonatomic, retain) NSString *tags;
@property (nonatomic, retain) NSString *cellType;
@property (nonatomic, retain) NSDictionary *dicInfo;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *countryName;
@property (nonatomic, retain) NSString *countryCode;
@property (nonatomic, retain) NSString *phoneCode;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSString *cast_color;
@property (nonatomic, retain) NSString *defaultThemeColor;
@property (nonatomic, retain) NSString *atvImagPath;
@property (nonatomic, retain) NSString *transparentImagPath;
@property (nonatomic, retain) NSString *reviews;
@property (nonatomic, retain) NSString *itemType;
@property (nonatomic, retain) NSString *exclusive;
@property (nonatomic, retain) NSString *carouselType;
@property (nonatomic, retain) id details;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isPlayingAudio;
@property (nonatomic, assign) BOOL isVideoPremium;
@property (nonatomic, assign) BOOL isVideoExclusive;
@property (nonatomic, assign) BOOL isVideoVIP;
@property (nonatomic, assign) BOOL isRadioChannel;
@property (nonatomic, assign) BOOL isLiveChannel;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic, assign) BOOL isRadioShow;
@property (nonatomic, assign) BOOL isIcon;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int count;
@property (nonatomic,assign)  NSInteger intType;
@property (nonatomic, assign) NSInteger audioPosition;
@property (nonatomic, retain) MediaInfo *content;
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, assign) NSTimeInterval thumbStartTime;
@property (nonatomic, assign) NSTimeInterval thumbStopTime;
@property (nonatomic, retain) NSString *thumbnail;
@property (nonatomic, retain) NSString *imgShowLogoPath;
@property (nonatomic, retain) NSArray *thumbnailPosition;
@property (nonatomic, assign) NSInteger x;
@property (nonatomic, assign) NSInteger y;
@property (nonatomic, assign) NSInteger w;
@property (nonatomic, assign) NSInteger h;
@property (nonatomic, assign) BOOL isLiveShowBlockedInMyCountry;
@property (nonatomic, assign) BOOL isLiveShowDigitalRightDisable;
@property (nonatomic, assign) BOOL isNeedToHideDate;
@property (nonatomic, assign) BOOL isVideoNew;
@property (nonatomic, assign) BOOL isRadioAudio;
@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, assign) BOOL isfav;
@property (nonatomic, assign) int pageType;
@property (nonatomic, assign) CGFloat rating;
@property (nonatomic, retain) NSString *imagePath;
@property (nonatomic, assign) int resumeVideoPosition;


@end

NS_ASSUME_NONNULL_END
