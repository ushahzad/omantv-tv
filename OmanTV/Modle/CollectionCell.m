//
//  CollectionCell.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 08/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView setTransform:CGAffineTransformMakeScale(-1, 1)];
    
    if (self.thumbnail.tag != 101) {
        self.thumbnail.layer.cornerRadius = CellRounded;
        self.thumbnail.clipsToBounds = true;
    }
    
    [StaticData scaleToArabic:self.contentView];
    //[StaticData scaleToArabic:self.view_info];
}

-(IBAction)deleteItem:(id)sender
{
    if (self.info)
    {
        if (self.DeleteCallBack) {
            self.DeleteCallBack(self,self.info);
        }
    }
}

-(IBAction)viewItem:(id)sender
{
    if (self.info)
    {
        if (self.ViewCallBack) {
            self.ViewCallBack(self,self.info);
        }
    }
}

@end
