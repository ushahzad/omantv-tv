//
//  BackgroundImage.h
//  AwaanAppleTV-New
//
//  Created by MacUser on 10/11/2020.
//  Copyright © 2020 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BackgroundImage : UIImageView

@end

NS_ASSUME_NONNULL_END
