//
//  DubaiFontLabel.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 28/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "DubaiFontLabel.h"

@implementation DubaiFontLabel

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {30, 0, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
