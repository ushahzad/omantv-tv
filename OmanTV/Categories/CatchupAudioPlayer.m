//
//  CatchupAudioPlayer.m
//  Awan
//
//  Created by Curiologix on 17/10/2017.
//  Copyright © 2017 Ali. All rights reserved.
//

#import "CatchupAudioPlayer.h"

@interface CatchupAudioPlayer ()

@end

@implementation CatchupAudioPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    _lbl_title.marqueeType = MLRightLeft;
    _lbl_title.rate = 60.0f;
    
    self.view_parent.layer.cornerRadius = 12.0;
    self.view_parent.clipsToBounds = true;
    
    self.view_parent.backgroundColor = [StaticData colorFromHexString:@"#211E16"];
    
    [StaticData shared].isSearching = false;
    
//    if (self.type == 1) {
//        [StaticData blurEffectsOnView:self.view_parent withEffectStyle:UIBlurEffectStyleDark];
//    } else {
//        [StaticData blurEffectsOnView:self.view_parent withEffectStyle:UIBlurEffectStyleLight];
//    }
    
    self.img_audio.layer.cornerRadius = self.img_audio.frame.size.height / 2.0;
    self.img_audio.clipsToBounds = true;
    
    [self setupAudioImage];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.viewIsActive = true;
}

-(void)viewWillAppear:(BOOL)animated {
    [StaticData shared].isSearching = false;
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (self.isMovingFromParentViewController)
    {
        
    }
    
    if (![StaticData shared].isSearching) {
        self.viewIsActive = false;
        [self removeAudioPlayer];

        [StaticData shared].isSearching = false;
    }
}

-(void)removeAudioPlayer
{
    if (self)
    {
        [self resetPlayer];
        [self willMoveToParentViewController:self];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
}

-(void)setupAudioImage
{
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:self.playerInfo.imagePath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.img_audio.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.img_audio_indicator startAnimating];
        [self.img_audio sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            if (image)
            {
            }
            [self.img_audio_indicator stopAnimating];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)setResumePositionOfVideo
{
    self.viewIsActive = false;
    
    [self executeApiToPostVideoResume:[NSString stringWithFormat:@"%@/endpoint/channel_users/set_audio_resume_position?user_id=%@&key=%@&X-API-KEY=%@",BaseURL,BaseUID,BaseKEY,[StaticData findLogedUserToken]] parameters:[self setResumePostParms] callType:@"setResume"];
}

-(NSDictionary *)setResumePostParms
{
    @try {
        
        //here we will update the episode video position in episodes array that we take from previous screen
        
        Float64 currentSeconds = CMTimeGetSeconds(self.player.currentItem.currentTime);
        int videotime = currentSeconds;
        self.playerInfo.position = [@(videotime) stringValue];
      
        
        if ([StaticData shared].userInfo) {
            return @{@"audio_id":self.playerInfo.ID,@"position":[@(videotime) stringValue],@"channel_userid":[StaticData shared].userInfo.ID,@"device_id":@""};
        }
        return @{@"audio_id":self.playerInfo.ID,@"position":[@(videotime) stringValue],@"channel_userid":@""};
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return @{@"":@""};
}


-(void)executeApiToPostVideoResume:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"%@",responseObject);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Error: %@", error.description);
     }];
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    if ([type isEqualToString:@"setResume"])
    {
        NSLog(@"Set Resume: %@",response);
    }
}

-(void)resetPlayer
{
    _btn_playRadio.tag = 0;
    [_btn_playRadio setImage:[UIImage imageNamed:@"audio_play_icon"] forState:UIControlStateNormal];
    [_player pause];
    //_player = nil;
    //_str_playbackURL = @"";
}

-(void)setupGestureActionsOnPlayer
{
    UITapGestureRecognizer *gestureAwaan = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonClickAction:)];
    gestureAwaan.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [_btn_playRadio addGestureRecognizer:gestureAwaan];
    
    UISwipeGestureRecognizer *rightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forwardVideo:)];
    rightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [_btn_progressFocuse addGestureRecognizer:rightGesture];
    
    UISwipeGestureRecognizer *leftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forwardBackVideo:)];
    leftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [_btn_progressFocuse addGestureRecognizer:leftGesture];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(haldePlayAndPause:)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];
}

-(void)buttonClickAction:(UITapGestureRecognizer *)gesture
{
    if (gesture.view == _btn_playRadio)
    {
        [self playRadio];
    }
}

-(void)haldePlayAndPause:(UITapGestureRecognizer *)gesture
{
    if (_btn_playRadio.tag == 0)
    {
        self.btn_playRadio.tag = 1;
        [self playRadioNow];
    } else
    {
        self.btn_playRadio.tag = 0;
        [self pauseRadioNow];
    }
}

#pragma mark setup audio player
-(void)playRadio
{
    if (_str_playbackURL.length == 0)return;
    
    if (_btn_playRadio.tag == 0)
    {
        [self playRadioNow];
    } else
    {
        [self pauseRadioNow];
    }
}

-(void)playRadioNow
{
    _btn_playRadio.tag = 1;
    [_btn_playRadio setImage:[UIImage imageNamed:@"audio_play_paus"] forState:UIControlStateNormal];
    if (!_player) {
        [self setupAudioPlayer];
    } else {
        [_player play];
    }
}

-(void)pauseRadioNow
{
    _btn_playRadio.tag = 0;
    [_btn_playRadio setImage:[UIImage imageNamed:@"audio_play_icon"] forState:UIControlStateNormal];
    [_player pause];
}

-(void)setupAudioPlayer
{
    NSURL *soundFileURL = [NSURL URLWithString:_str_playbackURL];
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:soundFileURL];
    _player = [AVPlayer playerWithPlayerItem:item];
    [_player play];
    
    NSLog(@"%d",self.resumeVideoPosition);
    if (self.resumeVideoPosition > 0) {
        CMTime resumeTimer = CMTimeMake(self.resumeVideoPosition, 1);
        
        [self.player seekToTime:resumeTimer toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        self.resumeVideoPosition = 0;
    }
    
    CMTime interval = CMTimeMake(33, 1000);
    __weak __typeof(self) weakself = self;
    self.playbackObserver = [self.player addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        if (weakself.viewIsActive)
        {
            CMTime endTime = CMTimeConvertScale (weakself.player.currentItem.asset.duration, weakself.player.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
            if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
                double normalizedTime = (double) weakself.player.currentTime.value / (double) endTime.value;
                weakself.progressView.progress = normalizedTime;
            }
            
            NSString *currentTime = [weakself getStringFromCMTime:weakself.player.currentTime];
            //NSString *remaingTime = [weakself getStringFromCMTime:subtract];
            
            weakself.lbl_currentTime.text = currentTime;
            weakself.lbl_totalTime.text = [weakself getTotalVideoTime];
            
//            CMTime subtract = CMTimeSubtract(weakself.player.currentItem.asset.duration,weakself.player.currentTime);
//
//            weakself.lbl_hours.text = [weakself getStringFromCMTime:subtract withTimeType:1];
//            weakself.lbl_minutes.text = [weakself getStringFromCMTime:subtract withTimeType:2];
//            weakself.lbl_seconds.text = [weakself getStringFromCMTime:subtract withTimeType:3];
        } else {
            [weakself.player pause];
        }
    }];
    
    //self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
}

-(NSString*)getStringFromCMTime:(CMTime)time
{
    Float64 currentSeconds = CMTimeGetSeconds(time);
    
    //int mins = currentSeconds/60.0;
    //int secs = fmodf(currentSeconds, 60.0);
    int totalSeconds = (int)currentSeconds;
    
    if (totalSeconds < 0) {
        return @"00:00";
    }
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        NSString *hoursString = hours < 10 ? [NSString stringWithFormat:@"0%d", hours] : [NSString stringWithFormat:@"%d", hours];
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
        return [NSString stringWithFormat:@"%@:%@:%@",hoursString,minsString, secsString];
    }
    
    NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}

-(NSString *)getTotalVideoTime
{
    int totalSeconds = (int) CMTimeGetSeconds(self.player.currentItem.duration);
    
    if (totalSeconds < 0) {
        return @"00:00";
    }
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%d:%@:%@",hours, minsString, secsString];
    }
    
    if (minutes > 0) {
        return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
    }
    
    return [NSString stringWithFormat:@"%@", secsString];
}

-(void)forwardVideo:(UITapGestureRecognizer *)gesture
{
    NSLog(@"forward video");
    
    NSLog(@"slider value: %f",self.progressView.progress);
    [self progressBarChanged:0.008000];
}

-(void)forwardBackVideo:(UITapGestureRecognizer *)gesture
{
    
    NSLog(@"back video");
    NSLog(@"slider value: %f",self.progressView.progress);
    [self progressBarChanged:-0.008000];
}

-(void)progressBarChanged:(float)movement
{
    //[self.videoPlayer pause];
    self.progressView.progress = self.progressView.progress + movement;
    NSLog(@"slider value: %f",self.progressView.progress);
    CMTime seekTime = CMTimeMakeWithSeconds(self.progressView.progress * (double)self.player.currentItem.asset.duration.value/(double)self.player.currentItem.asset.duration.timescale, self.player.currentTime.timescale);
    [self.player seekToTime:seekTime];
}

-(NSString*)getStringFromCMTime:(CMTime)time withTimeType:(int)type
{
    int currentSeconds = CMTimeGetSeconds(time);
    int hurs = currentSeconds/3600;
    int mins = (currentSeconds / 60) % 60;
    int secs = fmodf(currentSeconds, 60.0);
    
    if (secs < 0) {
        return @"00 | 00 | 00";
    }
    
    NSString *hursString = hurs < 10 ? [NSString stringWithFormat:@"0%d", hurs] : [NSString stringWithFormat:@"%d", hurs];
    NSString *minsString = mins < 10 ? [NSString stringWithFormat:@"0%d", mins] : [NSString stringWithFormat:@"%d", mins];
    NSString *secsString = secs < 10 ? [NSString stringWithFormat:@"0%d", secs] : [NSString stringWithFormat:@"%d", secs];
    
    if (type == 1) {
        return [NSString stringWithFormat:@"%@ HR",hursString];
    } else if (type == 2) {
        return [NSString stringWithFormat:@"%@ MIN",minsString];
    } else if (type == 3) {
        return [NSString stringWithFormat:@"%@ SEC",secsString];
    }
    
    
    return @"";
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.nextFocusedView == self.btn_progressFocuse) {
        self.progressView.trackTintColor = [UIColor clearColor];
    }
    
    if (context.previouslyFocusedView == self.btn_progressFocuse) {
        self.progressView.trackTintColor = [UIColor clearColor];
    }
    
    if (context.nextFocusedView == self.btn_playRadio) {
        context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
    }
    
    if (context.previouslyFocusedView == self.btn_playRadio) {
        context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
}

-(BOOL)ignoreMenu:(NSSet*)presses
{
    if (((UIPress *)[presses anyObject]).type == UIPressTypeMenu) {
        return YES;
    }
    return false;
}

-(void)pressesBegan:(NSSet*)presses withEvent:(UIPressesEvent *)event
{
    if ([self ignoreMenu:presses])
    {
        [self.player pause];
        self.player = [AVPlayer new];
        return;
    }
    
    [super pressesBegan:presses withEvent:event];
    // my code
}










@end
