//
//  TVSubCategoriesVC.h
//  OmanTV
//
//  Created by Curiologix on 23/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TVSubCategoriesVC : UIViewController
<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_categories;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noRecordsFound;

@property (nonatomic, weak) IBOutlet UIView *view_dropdown;
@property (nonatomic, weak) IBOutlet UILabel *lbl_dropdown;
@property (nonatomic, weak) IBOutlet UIButton *btn_dropdown;
@property (nonatomic, weak) IBOutlet UIImageView *img_dropdown;

@property (nonatomic, retain) NSMutableArray *categoryList;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) MediaInfo *selectedCategory;
@property (nonatomic, assign) BOOL isNeedToReloadCateogries;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isRadio;
@end

NS_ASSUME_NONNULL_END
