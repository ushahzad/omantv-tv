//
//  TVSubCategoriesVC.m
//  OmanTV
//
//  Created by Curiologix on 23/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "TVSubCategoriesVC.h"
#import "RadioShowDetailsVC.h"

@interface TVSubCategoriesVC ()

@end

@implementation TVSubCategoriesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentList = [NSMutableArray new];

    [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@""];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.view_dropdown.layer.cornerRadius = 35.0;
    self.view_dropdown.clipsToBounds = true;
    
    self.lbl_dropdown.textColor = [UIColor whiteColor];
    self.img_dropdown.image = [StaticData changeImageColor:[UIImage imageNamed:@"dropdown"] withColor:[StaticData colorFromHexString:@"#ffffff"]];
    self.view_dropdown.backgroundColor = [UIColor clearColor];
    self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
    self.view_dropdown.layer.borderWidth = 1.0;

    
    self.lbl_noRecordsFound.text = NoRecordFound;
    
    [StaticData scaleToArabic:self.collectionView];
    [StaticData scaleToArabic:self.cv_categories];
    
    [self setupSelectedCategory:self.selectedCategory];
    
    [self setupTriggerActionOnButtons];
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_dropdown];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_dropdown)
        {
            [self openCategories];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)openCategories
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (MediaInfo *info in self.categoryList)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:info.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self setupSelectedCategory:info];
        }];
        [as addAction:action];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:Cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [as addAction:cancel];
    
    [self presentViewController:as animated:TRUE completion:nil];
}

-(void)loadDataFromServer
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&&channel_id=&cat_id=%@&genre_id=&p=%d&ended=no&limit=30&is_radio=%d&need_show_times=no&need_channel=yes&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedCategory.ID,self.pageNumber,self.isRadio,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"SubCategory"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"SubCategory"])
        {
            NSInteger totalResults = self.contentList.count;
            for (NSDictionary *dicObj in responseObject[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"app_thumbnail"]];
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            self.isNeedToLoadMorePrograms = true;
            if (totalResults == self.contentList.count)
            {
                self.isNeedToLoadMorePrograms = false;
            }
            
            if (_contentList.count == 0) {
                self.lbl_noRecordsFound.hidden = false;
            }
            [self reloadCollectionView];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)reloadCateogriesCollectionView
{
    [StaticData reloadCollectionView:self.cv_categories];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_categories) {
        return CGSizeMake(266.0, 150.0);
    }
    
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 80) / 5;
    CGFloat height = width;
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    return CGSizeMake(width, height+ 50);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == self.cv_categories) {
        return self.categoryList.count;
    }
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_categories)
    {
        return [self categoriesCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    }
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     if (image)
                     {
                         //[StaticData imageLoaded:image withCell:cell];
                     }
                     
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.layer.cornerRadius = 12.0;
    cell.clipsToBounds = true;
    
    return cell;
}

-(UICollectionViewCell *)categoriesCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.categoryList[indexPath.row];
    cell.lbl_title.text = info.title;
    
    cell.view_info.hidden = true;
    if (info.isSelected) {
        cell.view_info.backgroundColor = LightWidthColor;
        cell.view_info.hidden = false;
    }
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 cell.thumbnail.image = [StaticData changeImageColor:image withColor:[UIColor whiteColor]];
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (image)
                     {
                         cell.thumbnail.image = [StaticData changeImageColor:image withColor:[UIColor whiteColor]];
                     }
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
        
        cell.img_highlight.contentMode = UIViewContentModeScaleAspectFit;
        [cell.img_highlight sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.img_highlight withUrl:imageURL completed:^(UIImage *image) {
                     if (image)
                     {
                     }
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.backgroundColor = [StaticData colorFromHexString:@"#262626"];
    cell.layer.cornerRadius = 12.0;
    cell.clipsToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_categories)
    {
        MediaInfo *info = self.categoryList[indexPath.row];
        [self setupSelectedCategory:info];
    } else
    {
        MediaInfo *info = self.contentList[indexPath.row];
        info.isRadio = self.isRadio;
        if (self.isRadio)
        {
            [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
        } else
        {
            [StaticData openShowDetailsPageWith:self withMediaInfo:info];
        }
    }
}

-(void)setupSelectedCategory:(MediaInfo *)info
{
    self.pageNumber = true;
    
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
    
    for (MediaInfo *info in self.categoryList) {
        info.isSelected = false;
    }
    
    self.lbl_dropdown.text = info.title;
    info.isSelected = true;
    self.selectedCategory = info;
    [self loadDataFromServer];
    
    
    [self reloadCateogriesCollectionView];
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}



-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_categories)
    {
        self.isNeedToReloadCateogries = true;
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            cell.view_info.hidden = true;
            
            MediaInfo *info = self.categoryList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected)
            {
                cell.view_info.hidden = false;
                cell.view_info.backgroundColor = LightWidthColor;
            }
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.view_info.backgroundColor = [UIColor whiteColor];
            cell.view_info.hidden = false;
        }
    } else
    {
        if (self.isNeedToReloadCateogries) {
            self.isNeedToReloadCateogries = false;
            [self reloadCateogriesCollectionView];
        }
        
        [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_dropdown)
        {
            self.view_dropdown.layer.borderColor = AppColor.CGColor;
            self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
         if (sender == self.btn_dropdown) {
             self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
             self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
}

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.contentList.count > 0) {
            for (int i = 0; i < self.contentList.count; i++)
            {
                MediaInfo *channel = self.contentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight-50)
        {
            // then we are at the end
            if (self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = false;
                _pageNumber += 1;
                
                [self loadDataFromServer];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

@end
