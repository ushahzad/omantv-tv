//
//  CategoriesVC.h
//  OmanTV
//
//  Created by Curiologix on 23/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CategoriesVC : UIViewController
<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
@end

NS_ASSUME_NONNULL_END
