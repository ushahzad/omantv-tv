//
//  RadioShowDetailsVC.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 23/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CatchupAudioPlayer.h"
NS_ASSUME_NONNULL_BEGIN

@interface RadioShowDetailsVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UIImageView *img_show;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;

@property (nonatomic, weak) IBOutlet UICollectionView *cv_showVideos;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) NSMutableArray *showVideos;

@property (nonatomic, retain) id category;
@property (nonatomic, retain) IBOutlet UIView *view_navBarParent;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, retain) MediaInfo *selectedCatgoryShow;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) MediaInfo *infoAudio;
@property (nonatomic, retain) UIImage *tempImage;

@property (nonatomic, strong) NSTimer *updateOlineTimer;

@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) NSMutableString *sessionID;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) int totalVideos;
@property (nonatomic, assign) int resumeVideoPosition;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isUserPlayVideo;



@end

NS_ASSUME_NONNULL_END
