//
//  RadioShowDetailsVC.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 23/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "RadioShowDetailsVC.h"

@interface RadioShowDetailsVC ()

@end

@implementation RadioShowDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@""];
    
    self.showVideos = [NSMutableArray new];

    self.view_parent.hidden = true;

    _pageNumber = 1;
    
    self.lbl_title.text = self.selectedShow.title;
    
    [self executeApiCallForShows:@""];

    [self setupTriggerActionOnButtons];
    
    [StaticData scaleToArabic:self.cv_showVideos];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playPauseChanne:)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];
}

-(void)playPauseChanne:(UITapGestureRecognizer *)gesture
{
    if (self.radioPlayer) {
        [self.radioPlayer playRadio];
    }
}

-(void)setupTriggerActionOnButtons
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuButtonPressed:)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeMenu], nil];
    [self.view addGestureRecognizer:gesture];
}

-(void)menuButtonPressed:(UITapGestureRecognizer *)gesture
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)buttonActionTrigger:(UIButton *)sender
{
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isUserPlayVideo)
    {
        self.isUserPlayVideo = false;
    }
}

#pragma mark Load Sub Categories
-(void)executeApiCallForShows:(NSString *)session
{
    @try {
        [self.loading_indicator startAnimating];
        
        NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=show&show_id=%@&season=%@&p=%d&limit=50&cast=yes&limit_also=3&channel_userid=&need_avg_rating=yes&need_ordered_audios=yes&all_fav_types_in_show_audios=yes&lang=ar",BaseURL,BaseUID,BaseKEY,_selectedShow.ID,session.length > 0 ? session:@"",_pageNumber];
        [self executePostApiToGetResponse:path parameters:nil callType:@"ShowsInfo"];
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

#pragma mark - Load Categories Request
// make web service api call
-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RadioPlayBackUrl"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            if ([type isEqualToString:@"ShorterUrl"])
            {
                responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            }
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    }
}

-(void)responseReceived:(id)response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"ShowsInfo"])
        {
            self.view_parent.hidden = false;
            
            [self.loading_indicator stopAnimating];
            [self setupAllVideos:response];
            
            _totalVideos = [response[@"videos_count"] intValue];
            
            self.isNeedToLoadMorePrograms = false;
            
            if(_totalVideos == _showVideos.count) {
                self.isNeedToLoadMorePrograms = true;
            }
            [self reloadShows];
        } else if ([type isEqualToString:@"RadioPlayBackUrl"])
        {
            NSLog(@"%@",response);
            NSDictionary *resumeDic = [StaticData checkDictionaryNull:response[@"audio"]];
            NSDictionary *resume_position = [StaticData checkDictionaryNull:resumeDic[@"resume_position"]];
            
            if (resume_position && [resume_position count] > 0)
            {
                if (resume_position[@"position"])
                {
                    self.resumeVideoPosition = [[StaticData checkStringNull:resume_position[@"position"]] intValue];
                }
            }
            
            _str_playbackURL = response[@"playback_url"];
            [self setupAuidoPlayer];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupAllVideos:(id)response
{
    for (NSDictionary *videoDic in response[@"audio"])
    {
        if ([videoDic[@"publish"] isEqualToString:@"yes"])
        {
            MediaInfo *info = [MediaInfo new];
            info.ID = videoDic[@"id"];
            info.title = [StaticData checkStringNull:videoDic[@"title_ar"]];
            info.imagePath = [StaticData checkStringNull:videoDic[@"cat_thumbnail"]];
            info.time = [StaticData checkStringNull:[self getVideoTime:[videoDic[@"duration"] intValue]]];
            info.position = [StaticData checkStringNull:videoDic[@"completion_position"]];
            info.duration = [StaticData checkStringNull:videoDic[@"duration"]];
            info.date = [self getAudioFormatedDate:[StaticData checkStringNull:videoDic[@"create_time"]]];
            if (![StaticData isVideoBlockInMyCountry:videoDic isShowGeoStatus:false])
            {
                [self.showVideos addObject:info];
            }
        }
    }
}

-(void)setupRelatedShowObject
{
    for (MediaInfo *info in self.showVideos)
    {
        if ([info.cellType isEqualToString:@"related"]) {
            [self.showVideos removeObject:info];
        }
    }
}

-(NSString *)getAudioFormatedDate:(NSString *)time
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [df dateFromString:time];
    [df setDateFormat:@"dd/MM/yyyy"];
    return [df stringFromDate:date];
}

-(NSString *)getVideoTime:(int)totalSeconds
{
    if (totalSeconds < 0) {
        return @"";
    }
    
    //int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        if (hours == 1) {
            return [NSString stringWithFormat:@"%d hr %d min",hours,minutes];
        } else {
            return [NSString stringWithFormat:@"%d hrs %d mins",hours,minutes];
        }
    }
    return [NSString stringWithFormat:@"%d mins", minutes];
}


-(void)reloadShows
{
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.cv_showVideos reloadData];
    });
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = self.cv_showVideos.frame;
    CGFloat width = frame.size.width;
    
    return CGSizeMake(width, 80);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.showVideos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = _showVideos[indexPath.row];

    cell.info = info;
    
    cell.lbl_titleMarquee.text = info.title;
    cell.lbl_time.text = info.time;
    cell.lbl_date.text = info.date;
    
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    cell.view_blur.hidden = true;
    cell.view_blur.layer.cornerRadius = 16.0;
    cell.view_blur.clipsToBounds = true;
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:info.imagePath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        
        [cell.indicator startAnimating];
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            if (image)
            {
                if (!self.tempImage) {
                }
            } else {
                [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                    if (!self.tempImage && image) {
                        self.tempImage = image;
                    }
                }];
            }
            [cell.indicator stopAnimating];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.thumbnail.layer.cornerRadius = cell.thumbnail.frame.size.height / 2.0;
    cell.thumbnail.clipsToBounds = true;
    //[StaticData scaleToArabic:cell.view_info];
    //[StaticData scaleToArabic:cell.thumbnail];
    
    //    cell.thumbnail.layer.cornerRadius = 8.0;
    //    cell.thumbnail.clipsToBounds = true;
    //    cell.layer.cornerRadius = 8.0;
    //    cell.clipsToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.showVideos.count == 0) return;
    MediaInfo *info = self.showVideos[indexPath.row];
    self.infoAudio = info;
    [self getPlayBackUrl:info];
}

-(void)getPlayBackUrl:(MediaInfo *)info
{
    [self.loading_indicator startAnimating];
    
    _selectedShow = info;
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=%@&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,_selectedShow.ID,[StaticData findeLogedUserID]];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
    
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    self.radioPlayer = [self.storyboard instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    
    [self addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    self.radioPlayer.playerInfo = self.selectedShow;
    self.radioPlayer.resumeVideoPosition = self.resumeVideoPosition;
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = _selectedShow.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

-(void)Updaterequest
{
    @try {
        NSString *URLsettings = [NSString stringWithFormat:@"%@?/crosssitestats/UpdateOnline",BaseURL];
        
        NSData* userIdData = [BaseUID dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64EncodedUserID = [userIdData base64EncodedStringWithOptions:0];
        if (_selectedShow == nil) return;
        NSLog(@"Catchup ID = %@",_selectedShow.ID);
        
        NSDictionary *parametrs = @{@"userid":base64EncodedUserID,@"browserOS":@"appletv",@"videoid":_selectedShow.ID,@"channelid":@"",@"sessionid":self.sessionID,@"domain":@"",@"device":@"7"};
        
        [self executePostApiToGetResponse:URLsettings parameters:parametrs callType:@"UpDateOnline"];
    } @catch (NSException *exception) {
        NSLog(@"Update online Request = %@",exception.description);
    } @finally {
        
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView canFocusItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = _showVideos[indexPath.row];
    if ([info.cellType isEqualToString:@"related"]) {
        return false;
    }
    return true;
}

- (void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == self.cv_showVideos)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [cell.lbl_titleMarquee pauseLabel];
            cell.view_blur.hidden = true;
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == self.cv_showVideos)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            [cell.lbl_titleMarquee unpauseLabel];
            
            cell.view_blur.hidden = false;
            if (cell.view_blur.tag == 0) {
                cell.view_blur.tag = 1;
                [StaticData blurEffectsOnView:cell.view_blur withEffectStyle:UIBlurEffectStyleDark];
            }
        }
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[UITextView class]])
    {
        [UIView animateWithDuration:0.1 animations:^{
            context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[UITextView class]])
    {
        [UIView animateWithDuration:0.1 animations:^{
            context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
    
    context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]] || [context.nextFocusedView isKindOfClass:[UIButton class]])
    {
    }
}

-(void)updateFocuseOfPopover
{
    [self setNeedsFocusUpdate];
    [self updateFocusIfNeeded];
}

/*- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight+10)
        {
            // then we are at the end
            if (!self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = true;
                _pageNumber += 1;
                
                //[self setupTableFooterView];
                
                if (_selectedSessionID.length > 0)
                {
                    [self executeApiCallForShows:_selectedSessionID];
                } else {
                    [self executeApiCallForShows:@""];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}*/


-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    //MediaInfo *info = _showVideos[indexPath.row];
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
//    MediaInfo *info = _showVideos[indexPath.row];
//    if ([info.cellType isEqualToString:@"related"]) {
//        [self removeViewControllerFromParentViewController];
//    }
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}
@end
