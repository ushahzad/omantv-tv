//
//  CatchupAudioPlayer.h
//  Awan
//
//  Created by Curiologix on 17/10/2017.
//  Copyright © 2017 Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatchupAudioPlayer : UIViewController

@property (nonatomic, weak) IBOutlet UIButton *btn_playRadio;
@property (nonatomic, weak) IBOutlet UIButton *btn_progressFocuse;

@property (nonatomic, retain) IBOutlet MarqueeLabel *lbl_title;

@property (nonatomic, weak) IBOutlet UIImageView *img_audio;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *img_audio_indicator;
@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UILabel *lbl_currentTime;
@property (nonatomic, weak) IBOutlet UILabel *lbl_totalTime;
@property (nonatomic, weak) IBOutlet UILabel *lbl_hours;
@property (nonatomic, weak) IBOutlet UILabel *lbl_minutes;
@property (nonatomic, weak) IBOutlet UILabel *lbl_seconds   ;
@property (nonatomic, weak) IBOutlet UILabel *lbl_time   ;

@property (nonatomic, weak) IBOutlet UIProgressView *progressView;

@property(nonatomic, strong) id playbackObserver;
@property(nonatomic, strong) AVPlayer *player;
@property (nonatomic, retain) NSTimer *audioTimer;

@property (nonatomic, retain) NSString *str_playbackURL;
@property(nonatomic, retain) MediaInfo *playerInfo;
@property (nonatomic, assign) BOOL viewIsActive;
@property (nonatomic, assign) BOOL isAudioPaused;
@property (nonatomic, assign) int resumeVideoPosition;
@property (nonatomic, assign) int type;

-(void)setResumePositionOfVideo;
-(void)setupGestureActionsOnPlayer;
-(void)removeAudioPlayer;
-(void)resetPlayer;
-(void)playRadio;
-(void)playRadioNow;
-(void)pauseRadioNow;
@end
