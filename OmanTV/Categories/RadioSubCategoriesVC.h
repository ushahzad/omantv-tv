//
//  RadioSubCategoriesVC.h
//  OmanTV
//
//  Created by Curiologix on 24/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioSubCategoriesVC : UIViewController
<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewChannels;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noRecordsFound;
@property (nonatomic, retain) MediaInfo *selectedChannel;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *radioChannelList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) MediaInfo *selectedCategory;
@end

NS_ASSUME_NONNULL_END
