//
//  MangoMoloAnalytics.m
//  OmanTV
//
//  Created by MacUser on 25/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "MangoMoloAnalytics.h"

#if __has_feature(objc_arc)
  #define MDLog(format, ...) CFShow((__bridge CFStringRef)[NSString stringWithFormat:format, ## __VA_ARGS__]);
#else
  #define MDLog(format, ...) CFShow([NSString stringWithFormat:format, ## __VA_ARGS__]);
#endif

@implementation MangoMoloAnalytics

static MangoMoloAnalytics *shared;

+(MangoMoloAnalytics *)shared
{
    if (!shared) {
        shared = [MangoMoloAnalytics new];
    }
    return shared;
}

+(void)updateOnlineWithChannelID:(NSString *)channelID withSessionID:(NSString *)sessionID withVideoID:(NSString *)videoID
{
    @try {
        
        NSString *URLsettings = [NSString stringWithFormat:@"%@/crosssitestats/UpdateOnline",BaseURL];
        
        NSData* userIdData = [BaseUID dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64EncodedUserID = [userIdData base64EncodedStringWithOptions:0];
        
        NSData* channelIdData = [channelID dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64EncodedChannelID = [channelIdData base64EncodedStringWithOptions:0];
        
        NSDictionary *parametrs = @{@"userid":base64EncodedUserID,@"browserOS":BrowserOS,@"videoid":videoID,@"channelid":base64EncodedChannelID,@"sessionid":sessionID,@"domain":@"",@"device":DeviceType,@"app_id":AppID,@"channel_user_id":[StaticData findeLogedUserID]};
        [self executePostApiToGetResponse:URLsettings parameters:parametrs callType:@"UpDateOnline"];
        
        MDLog(@"MangoMoloAnalytics URL = %@",URLsettings);
        MDLog(@"MangoMoloAnalytics Prams = %@",parametrs);
        
        MDLog(@"MangoMoloAnalytics BaseUserId =  %@----%@",BaseUID,base64EncodedUserID);
        MDLog(@"MangoMoloAnalytics Channel id =  %@----%@",channelID,base64EncodedChannelID);
        MDLog(@"MangoMoloAnalytics Session id =  %@",sessionID);
        MDLog(@"MangoMoloAnalytics Video id =  %@",videoID);
        MDLog(@"MangoMoloAnalytics Device Type =  %@",DeviceType);
        MDLog(@"MangoMoloAnalytics Logged User ID =  %@",[StaticData findeLogedUserID]);
        MDLog(@"MangoMoloAnalytics BrowserOS =  %@",BrowserOS);
        
    } @catch (NSException *exception) {
        NSLog(@"Update online Request = %@",exception.description);
    } @finally {
        
    }
}

+(void)setVideoComplitionRateWithVideoDuration:(int)videoDuration withVideoPlayedTime:(int)videoPlayedSeconds withVideoDetails:(NSDictionary *)videoDetails
{
    @try
    {
        if (videoDetails != nil)
        {
            if (videoPlayedSeconds < 0 || videoDuration < 0) {
                return;
            }
            
            NSString *userid = [StaticData findeLogedUserID];
            if ([StaticData findeLogedUserID].length == 0)
            {
                userid = @"";
            }
            
            NSString *isVideoComplete = @"No";
            if (videoPlayedSeconds >= videoDuration)
            {
                isVideoComplete = @"Yes";
                
            }
            
            NSString *URLsettings = [NSString stringWithFormat:@"%@/nand/?scope=player&action=completionrate&key=%@&user_id=%@&app_id=%@",BaseURL,BaseKEY,BaseUID,AppID];
            
            NSString *country = videoDetails[@"tracking"][@"country"][@"name"];
            NSString *ip = videoDetails[@"tracking"][@"ip"];
            
            if (country.length == 0) {
               country = [StaticData shared].myCountryName;
            }
            
            if (country.length == 0) {
                country = [StaticData getMyCountryCode];
            }
            
            if (ip.length == 0)
            {
                if ([StaticData shared].myIP.length > 0) {
                    ip = [StaticData shared].myIP;
                } else {
                    ip = @"";
                }
            }
            
            
            NSString *channelID = [StaticData checkStringNull:videoDetails[@"channel_id"]];
            
            NSDictionary *parametrs = @{@"videoid":[StaticData checkStringNull:videoDetails[@"id"]],@"channelid":channelID,@"duration":[@(videoDuration) stringValue],@"country":country,@"ip":ip,@"time":[StaticData getCurrentTime],@"is_completed":isVideoComplete,@"paused":[@(videoPlayedSeconds) stringValue],@"platform":BrowserOS,@"parent_url":@"",@"channel_userid":[StaticData findeLogedUserID],@"user_id":BaseUID};
            
            [self executePostApiToGetResponse:URLsettings parameters:parametrs callType:@"setVideoComplitionRate"];
            
            MDLog(@"MangoMoloAnalytics URL = %@",URLsettings);
            MDLog(@"MangoMoloAnalytics Prams = %@",parametrs);
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}


#pragma mark - Api Call Request
// make web service api call
+(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    if ([type isEqualToString:@"setVideoComplitionRate"])
    {
        [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    } else
    {
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        if ([type isEqualToString:@"setVideoComplitionRate"])
        {
            responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        }
        
        NSLog(@"Mangomolo Analytics Response Received = %@",responseObject);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Api Error: %@",error.description);
        @try {
            NSError* jsonError;
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSLog(@"%@",errResponse);
            if (!jsonError && errResponse)
            {
            }
        } @catch (NSException *exception) {
            NSLog(@"%@",exception.description);
        } @finally {
        }
        
     }];
}

@end
