//
//  MangoMoloAnalytics.h
//  OmanTV
//
//  Created by MacUser on 25/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DeviceType @"7"
#define BrowserOS @"appletv"

NS_ASSUME_NONNULL_BEGIN

@interface MangoMoloAnalytics : NSObject
+(MangoMoloAnalytics *)shared;
+(void)updateOnlineWithChannelID:(NSString *)channelID withSessionID:(NSString *)sessionID withVideoID:(NSString *)videoID;
+(void)setVideoComplitionRateWithVideoDuration:(int)videoDuration withVideoPlayedTime:(int)videoPlayedSeconds withVideoDetails:(NSDictionary *)videoDetails;
@end

NS_ASSUME_NONNULL_END
