//
//  StaticData.h
//  AlarabyNew
//
//  Created by Dotcomlb on 14/01/2017.
//  Copyright © 2017 dotcomlb. All rights reserved.
//

#ifndef StaticClass_h
#define StaticClass_h

typedef enum { PlayButton, PauseButton,PlayerPauseDuringOtherActivityWithPlayer } PlayButtonType;

// Dev URL
//#define BaseURL @"https://dev.admin.mangomolo.com/analytics/index.php"
//#define BaseImageURL @"https://dev.admin.mangomolo.com/analytics/"
//#define BaseAuthURL @"http://dev.admin.mangomolo.com/analytics/index.php/endpoint/auth"

// Live URL
#define BaseURL @"https://admin.mangomolo.com/analytics/index.php"
#define BaseImageURL @"https://admango.cdn.mangomolo.com/analytics/"
#define BaseAuthURL @"https://admin.mangomolo.com/analytics/index.php/endpoint/auth"

#define WideImageUrl @"https://admin.mangomolo.com/analytics/"
#define OmanTVBaseURL @"http://ayn.om"
#define OmanTVWebSiteURL @"https://ayn.om/".

#define BaseKEY @"49e55ab68365d827e87a3fb857c5aa87"
#define BaseUID @"168"
#define AppID @"17"
#define ExcludeChannelId @"233"
#define AudioBookChannelId @"162"

#define ChannelID @"197"

#define TheMixArabicLight @"TheMixArabic-Light"
#define TheMixArabicBold @"TheMixArabic-Bold"
#define TheMixArabicBlack @"TheMixArabic-Black"
#define TheMixArabicExtraBlod @"TheMixArabic-ExtraBold"


#define LoraBold @"Lora-Bold"
#define LoraRegular @"Lora-Regular"

#define PFDinTextArabicRegular @"Cairo-Regular"
#define PFDinTextArabicBold @"Cairo-Bold"
#define PFDinTextArabicMedium @"Cairo-Bold"

#define MovieCategoryID @"212272"

#define GenreIDShow @"52"
#define GenreIDSeries @"53"

#define AudioPlayerHeight 100.0
#define AudioPlayerSuperViewPadding 8.0;
#define RoundeCorner 12.0
#define TopCarouselSpace 0.0

#define Notify [NSNotificationCenter defaultCenter]
#define isIpad [StaticData isIpadDevice]

#define HomeLiveChannelNOTI @"HomeLiveChannelNOTI"
#define LiveChannelNOTI @"LiveChannelNOTI"
#define VideoPlayerNOTI @"VideoPlayerNOTI"
#define UpdateProfileNOTI @"UpdateProfileNOTI"

#define RemovePlayerObserveNOTI @"RemoveObserveNOTI"


#define AboutUS @"https://majid.dotcomlb.com/about-us"
#define ContactUS @"https://majid.dotcomlb.com/contact-us"
#define PrivacyPolicy @"https://majid.dotcomlb.com/privacy-policy"
#define TermsConditions @"https://majid.dotcomlb.com/terms-and-conditions"
#define FacebookURL @""
#define TwitterURL @""
#define InstagramURL @""
#define YoutubeURL @""

#define CellVideoInfoViewHeight 135.0
#define CellShowInfoViewHeight 80.0
#define CellInfoViewHeight 115.0
#define Cell1InfoViewHeight 30.0
#define CellRounded 12.0
#define CellBorderWidth 5.0

#define CellHeight 120
#define CarouselHeight 150.0

#define VideoChannelID @"174"

#define RedThemeName @"red"
#define LightBlueThemeName @"lign_blue"
#define DarkBlueThemeName @"dark_blue"
#define PinkThemeName @"pink"
#define GreenThemeName @"green"

#define RedColor [StaticData colorFromHexString:@"#B81C19"]
#define BlueColor [StaticData colorFromHexString:@"#234B97"]
#define PinkColor [StaticData colorFromHexString:@"#CF7DAE"]
#define GreeColor [StaticData colorFromHexString:@"#4AAFA9"]

#define LightWidthColor [StaticData colorFromHexString:@"#E3D8D8"];

#define RedColorCodeImage @"#E6231F"
#define GreenColorCodeImage @"#4AAFA9"
#define DarkBlueColorCodeImage @"#2A5BB6"
#define LightBlueColorCodeImage @"#77ADEF"

#define AppDubaiMediaFontBold @"AbuDhabiMedia-Bold"
#define AppDubaiMediaFontRegular @"AbuDhabiMedia-Regular"

#define NoResultFound @"No results found"
#define PasswordNotMatched @"كلمة المرور وتأكيد كلمة المرور لا تتطابق"
#define EmailNotExist @"This email does not exist"
#define UnknownError @"Unknown error found please try again"
#define FavouriteShowAdded @"Added successfully"
#define FavouriteShowRemoved @"Deleted successfully"
#define ForgetPasswordEmailSent @"A request has been sent to your email address, please check to change your password"
#define YourEmailSent @"Your email is sent!"

#define ShowNotAvailableInYourCountry @"هذا المحتوى غير متوفر في بلدك"
#define DigitalRightIsDiabel @"هذا المحتوى غير متاح للبث على الأنترنت, الرجاء مشاهدة القناة على التلفاز"
#define LimitExceededSubscribedNow @"عذراً! لقد تجاوزت الحد المسموح به للأجهزة المخوّلة بالعمل عبر ميزة العرض المتزامن. إذا كنت ترغب بزيادة عدد الأجهزة يرجى ترقية حسابك"

#define AppColor [StaticData colorFromHexString:[StaticData appColorCode]]
#define AppSelectedColor [StaticData colorFromHexString:[StaticData appSelectedColorCode]]
#define SelectedColor [StaticData colorFromHexString:[StaticData appColorCode]]
#define RamadanAppColor [StaticData colorFromHexString:[StaticData ramadanAppColorCode]]

#define RedColorTheme [StaticData colorFromHexString:@"#E6231F"]
#define BlueColorTheme [StaticData colorFromHexString:@"#2B5BB6"]
#define LightBlueColorTheme [StaticData colorFromHexString:@"#77ADEF"]
#define PinkColorTheme [StaticData colorFromHexString:@"#F997D2"]
#define GreeColorTheme [StaticData colorFromHexString:@"#59D3CC"]
#define BlueColorLightTheme [StaticData colorFromHexString:@"#5AB0EF"]

#define NoRecordFound @"لا توجد سجلات"
#define NoAudioFound @"No Audio Found"

#define PleaseSignInToContinueWatching @"الرجاء تسجيل الدخول لمتابعة المشاهدة!"
#define Continue @"متابعة"
#define FreeContentForTodayIsOverPleaseSignIn @"الرجاء تسجيل الدخول لمتابعة المشاهدة! المحتوى المجاني لهذا اليوم انتهى!"
#define FreeVideoWatchingCount @"FreeVideoWatchingCount"
#define FreeVideoCount 5
#define PleaseSubscribeToSeeThisContnet @"الرجاء الاشتراك بالباقة لمشاهدة المحتوى المميز!"
#define YouCannotCreateNewAccountPleaseSubscribe @"لا يمكنك إنشاء حساب آخر إذا لم تكن مشتركاً!"
#define FreeVideoWatchingCountOverPleaseSignIn @"الرجاء تسجيل الدخول لمتابعة المشاهدة! المحتوى المجاني لهذا اليوم انتهى!"
#define ExceededLimitOfDevicesAllowed @"لقد تجاوزت عدد الأجهزة المسموح بها للعرض المتزامن"
#define YouHaveBeenLogOutPleaseLoginAgain @"لقد تم تسجيل خروجك, الرجاء تسجيل الدخول من جديد"

#define LoginFirstError @"يرجى تسجيل الدخول أولاً"
#define PleaseLoginFirst @"الرجاء تسجيل الدخول لمتابعة المشاهدة!"
#define InvalidEmail @"الرجاء ادخال بريد إلكتروني صحيح"
#define EmailInvalid @"الرجاء إدخال بريد صحيح"
#define EmailIsAlreadyUsed @"هذا البريد قيد الإستخدام. الرجاء إدخال بريد أخر."
#define EmailChanged @"لقد تم تغيير البريد بنجاح"
#define PasswordIsIncorrect @"حدث خطأ في تسجيل الدخول, البريد الألكتروني أو كلمة المرور غير صحيحة"
#define PasswordChanged @"لقد تم تغيير كلمة السر بنجاح"
#define PleaseEnterPassword @"يرجى إدخال كلمة المرور"
#define PleaseEnterNewPassword @"يرجى إدخال كلمة المرور الجديدة"
#define OldPasswordWrong @"كلمة السر الحالية غير صحيحة "
#define PhoneNumberChanged @"لقد تم تغيير رقم الهاتف بنجاح"
#define CommentPosted @"لقد تم اضافة التعليق بنجاح"
#define EnterEmail @"الرجاء ادخال البريد الالكتروني"
#define EnterOldPassword @"الرجاء ادخال كلمة السر الحالية"
#define EnterNewPassword @"الرجاء ادخال كلمة سر جديدة"
#define EnterConfirmPassword @"أدخل تأكيد كلمة المرور"
#define EnterPhoneNumber @"الرجاء ادخال رقم الجوال"
#define AllFieldRequird @"الرجاء ملء الحقول"
#define UserNotExist @"الرجاء التأكد من البريد أو كلمة السر"
#define CheckUserAndPassword @"الرجاء التأكد من اسم المستخدم أو كلمة السر"
#define VerificationCodeSent @"تم إرسال الشيفرة الى بريدك الالكتروني"
#define PasswordChangeSuccessfuly @"تم تحديث كلمة السر بنجاح"
#define ChangeSuccessfuly @"تم التغيير بنجاح"
#define UserAlreadyExist @"هذا المستخدم موجود"
#define EnterCorrectVerificationCode @"كلمة السر الحالية غير مطابقة"
#define PasswordRequiredMoreChar @"يجب أن يكون طول كلمة المرور على الأقل 8 حروف."

#define Ok isEnglishLang ? @"Ok" : @"حسناً"
#define NoRecordFound @"لا توجد سجلات"
#define PasswordNotMatchedAR @"كلمة المرور وتأكيد كلمة المرور لا تتطابق"
#define SaveSuccessfully @"حفظ بنجاح"
#define PleaseAcceptTerms @"يرجى قبول الشروط والأحكام"

#define Cancel @"إلغاء"

#define InstagramLink @"https://www.instagram.com/majidkids"
#define FacebookLink @"https://www.facebook.com/majidkids"
#define TwitterLink @"https://twitter.com/MajidKids"

#define isEnglishLang [StaticData isLanguageEnglish]

#endif /* StaticClass_h */
