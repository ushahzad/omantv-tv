//
//  StaticData.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 08/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "StaticData.h"
#import "HomeParentVC.h"
#import "ShowDetailsParentVC.h"
#import "RadioShowDetailsVC.h"

@implementation StaticData

static StaticData *shared;

+(StaticData *)shared
{
    if (!shared) {
        shared = [StaticData new];
    }
    return shared;
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+(void)downloadImageIfNotLoadinedBySdImage:(UIImageView *)imagevc withUrl:(NSURL *)imgURL
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imgURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError) {
            UIImage *img = [[UIImage alloc] initWithData:data];
            imagevc.image = img;
            // pass the img to your imageview
        }else{
            NSLog(@"%@",connectionError);
        }
    }];
}

+(NSString *)getTimeStamp
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSString *strTimeStamp = [NSString stringWithFormat:@"%f",timeStamp];
    return strTimeStamp;
}

+(NSString *)getCurrentTime
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDateTime = [df stringFromDate:[NSDate date]];
    return strDateTime;
}

+ (NSArray *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSArray *integers = [text componentsSeparatedByCharactersInSet:nonDigitCharacterSet];
    
    NSArray *noEmptyStrings = [integers filteredArrayUsingPredicate:
                               [NSPredicate predicateWithFormat:@"length > 0"]];
    
    
    return noEmptyStrings;
}

+(void)textFieldPadding:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    textField.rightView = paddingView;
    textField.rightViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [self textFieldPlaceHolderColor:textField];
}

+(void)textFieldPlaceHolderColor:(UITextField *)textField
{
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

+(void)saveDataInPrefs:(NSString *) data withKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:data forKey:key];
    [userDefault synchronize];
}

+(NSString *)getSaveDataFromPrefs:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault valueForKey:key];
}

+(NSString *)appSelectedColorCode
{
    return @"#F997D2";
}

+(void)saveArrayDataInPrefs:(NSMutableArray *) array withPath:(NSString *)pathName
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:array forKey:pathName];
    [userDefault synchronize];
}

+(NSMutableArray *)getArrayDataFromPrefs:(NSString *)pathName
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault valueForKey:pathName];
}

+(void)scaleToArabic:(UIView *)object
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [object setTransform:CGAffineTransformMakeScale(-1, 1)];
    });
}

+(void)log:(NSString *)message
{
    NSLog(@"%@",message);
}

+(void)downloadImageIfNotLoadinedBySdImage:(UIImageView *)imagevc withUrl:(NSURL *)imgURL completed:(ImageLoadingCompletionBlock)completedBlock
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imgURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        if (!connectionError) {
            UIImage *img = [[UIImage alloc] initWithData:data];
            if (!img){
                img = [StaticData placeHolder];
            }
            
            imagevc.image = img;
            completedBlock(img);
            // pass the img to your imageview
        }else{
            completedBlock([StaticData placeHolder]);
            NSLog(@"%@",connectionError);
        }
    }];
}

+(UIImage *)placeHolder
{
    UIImage *placeHolder = [UIImage imageNamed:@"default_image"];
    
    return placeHolder;
}

+(UIImage *)radioPlaceHolder
{
    UIImage *placeHolder = [UIImage imageNamed:@"default_logo_audio"];
    
    return placeHolder;
}

+(void)reloadCollectionView:(UICollectionView *)collView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [collView reloadData];
//        [collView layoutIfNeeded];
//        [collView reloadInputViews];
    });
}

+(void)addConstraingWithParent:(UIView *)parent onChild:(UIView *)child
{
    [parent addSubview:child];
    
    child.translatesAutoresizingMaskIntoConstraints = NO;
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:parent attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:child attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:parent attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:child attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:parent attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:child attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:0.0f]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:parent attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:child attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    //}
}

+(void)saveDataPref:(id)object withFileName:(NSString *)fileName
{
    @try {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSData *ecodedData = [NSKeyedArchiver archivedDataWithRootObject:object];
        [userDefault setObject:ecodedData forKey:fileName];
        [userDefault synchronize];
    } @catch (NSException *exception) {
    } @finally {
    }
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *cacheDirectory = paths[0];
//    NSString *filePath = [cacheDirectory stringByAppendingPathComponent:fileName];
//    [NSKeyedArchiver archiveRootObject:object toFile:filePath];
}

+(BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(void)reloadTableView:(UITableView *)tableView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [tableView reloadData];
    });
}

+(NSString *)findeLogedUserID
{
    if ([StaticData shared].userInfo) {
        return [StaticData shared].userInfo.ID;
    }
    return @"";
}

+(NSString *)findLogedUserToken
{
    NSString *token = @"";
    UserInfo *info = [StaticData shared].userInfo;
    if (info) {
        token = info.token;
    }
    return token;
}

+(NSString *)findSelectedProfileToken
{
    @try {
        NSString *token = @"";
        NSDictionary *profileInfo = [StaticData getDataPrefWithFileName:@"ProfileInfo"];
        if (profileInfo)
        {
            token = [StaticData checkStringNull:profileInfo[@"token"]];
        }
        return token;
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

+(NSString *)findSelectedProfileID
{
    @try {
        if ([StaticData isUserLogins])
        {
            NSDictionary *profileInfo = [StaticData getDataPrefWithFileName:@"ProfileInfo"];
            if (profileInfo)
            {
                NSString *profileID = @"";
                if ([profileInfo[@"id"] isKindOfClass:[NSNumber class]]) {
                    profileID = [@([StaticData checkNumberNull:profileInfo[@"id"]]) stringValue];
                } else if ([profileInfo[@"id"] isKindOfClass:[NSString class]]) {
                    profileID = [StaticData checkStringNull:profileInfo[@"id"]];
                }
                return profileID;
            }
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

+(BOOL)isUserLogins
{
    if ([StaticData shared].userInfo) {
        return true;
    }
    return false;
}

+(BOOL)isUserSubscribed
{
    @try {
//        if ([StaticData shared].userInfo.isUserSubscribed) {
//            return true;
//        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return false;
}

+(NSString *)checkStringNull:(NSString *)string
{
    @try {
        if ([string isEqual:[NSNull null]] || string.length == 0) {
            return @"";
        }
        return string;
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"(Checking String Error) String = %@,\nError = %@",string,exception.description]];
        return @"";
    } @finally {
    }
    return string;
}

+(NSArray *)checkArrayNull:(id)dic
{
    @try {
        if ([dic isEqual:[NSNull null]] || dic == (id)[NSNull null] || [dic isKindOfClass:[NSNull class]] || ![dic isKindOfClass:[NSArray class]])
        {
            return [NSArray new];
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"(Checking Array Error) String = %@,\nError = %@",dic,exception.description]];
        return nil;
    } @finally {
    }
    
    return dic;
}

+(NSDictionary *)checkDictionaryNull:(NSDictionary *)dic
{
    @try {
        if ([dic isEqual:[NSNull null]] || dic == (id)[NSNull null] || [dic isKindOfClass:[NSNull class]] || [dic count] == 0 || ![dic isKindOfClass:[NSDictionary class]])
        {
            return nil;
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"(Checking Dictionary Error) String = %@,\nError = %@",dic,exception.description]];
        return nil;
    } @finally {
    }
    
    return dic;
}

+ (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%d:%d:%d",hours, minutes, seconds];
    }
    return [NSString stringWithFormat:@"%d:%d", minutes, seconds];
}

+(id)getDataPrefWithFileName:(NSString *)fileName
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefault objectForKey:fileName];
    id cachedObject = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *cacheDirectory = paths[0];
//    NSString *filePath = [cacheDirectory stringByAppendingPathComponent:fileName];
//    id cachedObject = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    return cachedObject;
}

+(BOOL)isLanguageEnglish
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:@"IsEnglishLanguage"];
}

+(BOOL)isVideoBlockInMyCountry:(NSDictionary *)response isShowGeoStatus:(BOOL)istrue
{
    @try {
        
        NSString *countryCode = [StaticData shared].myCountryCode;
        if (countryCode.length == 0) {
            countryCode = [StaticData getMyCountryCode];
        }
        
        // in case if country code not able to find from server then we use thiss
        if (countryCode.length == 0) {
            NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
            countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        }
        
        NSString *geoStatus = @"";
        NSString *geoCountry = @"";
        if (istrue) {
            geoStatus = response[@"show_geo_status"];
            geoCountry = response[@"show_geo_countries"];
        } else {
            geoStatus = response[@"geo_status"];
            geoCountry = response[@"geo_countries"];
        }
        
        if ((geoStatus != (id)[NSNull null] && geoStatus.length > 0) &&(geoCountry != (id)[NSNull null] && geoCountry.length > 0))
        {
            if ([geoStatus isEqualToString:@"allowed"] && ![geoCountry containsString:countryCode])
            {
                return true;
            } else if ([geoStatus isEqualToString:@"not_allowed"] && (countryCode != (id)[NSNull null] && [geoCountry containsString:countryCode]))
            {
                return true;
            } else
            {
                return false;
            }
        }
        
        return false;
    } @catch (NSException *exception) {
        return false;
    } @finally {
    }
    
    return false;
}

+ (NSString *)getMyCountryCode
{
    @try {
        NSString *url = [NSString stringWithFormat:@"%@/plus/settings?key=%@&user_id=%@&app_id=%@",BaseURL,BaseKEY,BaseUID,AppID];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:url]];

        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // handle request error
            if (error) {
                NSLog(@"%@",error.description);
                return;
            }
            
            NSError *myError;
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&myError]];
            NSString *countryCode = dict[@"geo"][@"country"][@"code"];
            NSString *countryName = dict[@"geo"][@"country"][@"name"];
            NSString *ip = dict[@"geo"][@"ip"];
            [StaticData shared].appSettingsInfo = dict;
            [StaticData shared].myCountryCode = [StaticData checkStringNull:countryCode];
            [StaticData shared].myCountryName = [StaticData checkStringNull:countryName];
            [StaticData shared].myIP = [StaticData checkStringNull:ip];
        }]resume] ;
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
    return @"";
}

+(NSString *)myCountryCode
{
    NSString *countryCode = [StaticData shared].myCountryCode;
    if (countryCode.length == 0) {
        countryCode = [StaticData getMyCountryCode];
    }
    
    // in case if country code not able to find from server then we use thiss
    if (countryCode.length == 0) {
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
        countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    }
    
    return countryCode;
}

+(BOOL)checkIsScueduleGeoBlockOnLiveChannel:(NSDictionary *)chDic
{
    @try {
        NSArray *schedule = [StaticData checkArrayNull:chDic[@"live"]];
        for (NSDictionary *sgeoDic in schedule)
        {
            NSString *scheduleGeo = [StaticData checkStringNull:sgeoDic[@"schedule_geo"]];
            NSString *myCountryCode = [StaticData myCountryCode];
            if (scheduleGeo.length > 0 && [scheduleGeo containsString:myCountryCode])
            {
                return true;
            }
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"(Checking ScueduleGeo Error) String = %@,\nError = %@",chDic,exception.description]];
    } @finally {
    }
    
    return false;
}

+(BOOL)isLiveChannelDigitalRightsDisable:(NSDictionary *)response
{
    @try {
        NSArray *schedule = response[@"live"];
        for (NSDictionary *sgeoDic in schedule)
        {
            // 0 mean not watch channel
            int rights = [sgeoDic[@"digital_rights"] intValue];
            if (rights == 0)
            {
                return true;
            }
        }
    } @catch (NSException *exception) {
        return false;
    } @finally {
    }
    
    return false;
}

+ (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

+(void)buttonUnderLineTitle:(UIButton *)button
{
    @try {
        NSDictionary *attrDict = @{NSFontAttributeName : button.titleLabel.font,NSForegroundColorAttributeName : [UIColor whiteColor]};
        NSMutableAttributedString *title =[[NSMutableAttributedString alloc] initWithString:button.titleLabel.text attributes: attrDict];
        [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0,[button.titleLabel.text length])];
        [button setAttributedTitle:title forState:UIControlStateNormal];
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(void)buttonUnderLineTitle:(UIButton *)button withColor:(UIColor *)color
{
    @try {
        NSDictionary *attrDict = @{NSFontAttributeName : button.titleLabel.font,NSForegroundColorAttributeName : color};
        NSMutableAttributedString *title =[[NSMutableAttributedString alloc] initWithString:button.titleLabel.text attributes: attrDict];
        [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0,[button.titleLabel.text length])];
        [button setAttributedTitle:title forState:UIControlStateNormal];
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(int)checkNumberNull:(id)number
{
    @try {
        if (![number isKindOfClass:[NSNumber class]] || [number isEqual:[NSNull null]] ) {
            return 0;
        }
        return [number intValue];
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"(Checking Number Error) String = %@,\nError = %@",number,exception.description]];
        return 0;
    } @finally {
    }
    return 0;
}

+(void)removeUnderLineTitle:(UIButton *)button
{
    @try {
        NSDictionary *attrDict = @{NSFontAttributeName : button.titleLabel.font,NSForegroundColorAttributeName : button.titleLabel.textColor};
        NSMutableAttributedString *title =[[NSMutableAttributedString alloc] initWithString:button.titleLabel.text attributes: attrDict];
        [title removeAttribute:NSUnderlineStyleAttributeName range:NSMakeRange(0,[button.titleLabel.text length])];
        [button setAttributedTitle:title forState:UIControlStateNormal];
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(void)imageLoaded:(UIImage *)image withCell:(CollectionCell *)cell
{
    [StaticData roundImageView:image wihtImageView:cell.thumbnail];
}

+(void)roundImageView:(UIImage *)image wihtImageView:(UIImageView *)thumbnail
{
    if (image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *rounded =  [StaticData imageWithRoundedCornersSize:CellRounded usingImage:image withImageView:thumbnail];
            //UIImage *rounded =  [StaticData makeRoundedImage:image radius:CellRounded];
            thumbnail.image = rounded;
        });
        
    }
}

+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original withImageView:(UIImageView *)imageView
{
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 1.0);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:imageView.bounds
                                cornerRadius:cornerRadius] addClip];
    // Draw your image
    [original drawInRect:imageView.bounds];
    
    // Get the image, here setting the UIImageView image
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return imageView.image;
}

+(BOOL)isVideoScheduleGeoBlocking:(NSDictionary *)response
{
    @try {
        NSArray *schedule = response[@"live"];
        for (NSDictionary *sgeoDic in schedule)
        {
            NSString *countryCode = [StaticData shared].myCountryCode;
            if (countryCode.length == 0) {
                countryCode = [StaticData getMyCountryCode];
            }
            
            // in case if country code not able to find from server then we use thiss
            if (countryCode.length == 0) {
                NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
                countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
            }
            
            NSString *scheduleGeo = [StaticData checkStringNull:sgeoDic[@"schedule_geo"]];
            if (scheduleGeo.length > 0 && [scheduleGeo containsString:countryCode])
            {
                return true;
            }
        }
    } @catch (NSException *exception) {
        return false;
    } @finally {
    }
    
    return false;
}

+(void)showAlertView:(NSString *)message withvc:(UIViewController *)vc
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"حسناً" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action];
    [vc presentViewController:alert animated:true completion:nil];
}

+(BOOL)isContentPlus:(NSDictionary *)response isContnetShow:(BOOL)isShow
{
    @try {
        NSString *channelID = @"";
        if (isShow) {
            channelID = [StaticData checkStringNull:response[@"ch_id"]];
        } else {
           channelID = [StaticData checkStringNull:response[@"channel_id"]];
        }
        
//        if ([channelID isEqualToString:PlusContentChannelID])
//        {
//            return true;
//        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return false;
}

+(NSDate *)getDateFromCalendar:(int)day
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    
    comps.day = day;
    
    NSDate *date = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    return date;
}

+(NSArray *)readJsobFileWithName:(NSString *)fileName
{
    NSError *error;
    NSString *filePath = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"txt"] encoding:NSUTF8StringEncoding error:&error];
    NSData *fileData = [filePath dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *arrayJson = [NSJSONSerialization JSONObjectWithData:fileData options:NSJSONReadingMutableLeaves error:&error];
    return arrayJson;
}

+(NSString *)findArabicWeekDayByDate:(NSString *)serverDate
{
    NSDateFormatter *df = [NSDateFormatter new];
    
    [df setDateFormat:@"YYYY-MM-dd"];
    NSDate *date = [df dateFromString:serverDate];
    
    [df setDateFormat:@"EEEE"];
    NSString *wDay = [df stringFromDate:date];
    
    NSArray *weekDays = [StaticData readJsobFileWithName:@"WeekendDays"];
    
    for (NSDictionary *weeks in weekDays)
    {
        if ([weeks[@"EName"] isEqualToString:wDay])
        {
            return weeks[@"AName"];
        }
    }
    return @"";
}

+(NSString *)getForamttedDate:(NSString *)time
{
    @try {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"YYYY-MM-dd"];
        NSDate *catchupDate = [df dateFromString:time];
        
        [df setDateFormat:@"YYYY-dd-MM"];
        
        NSString *time = [df stringFromDate:catchupDate];
        
        return time;
    } @catch (NSException *exception) {
        [StaticData log:exception.description];
    } @finally {
    }
}

+(NSString *)getArabicWeekDay:(NSString *)wDay
{
    @try {
        NSArray *weekDays = [StaticData readJsobFileWithName:@"WeekendDays"];
        for (NSDictionary *weeks in weekDays)
        {
            if ([weeks[@"EName"] isEqualToString:wDay])
            {
                return weeks[@"AName"];
                break;
            }
        }
        
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

+(NSString *) stringByStrippingHTML:(NSString *)string {
    NSRange r;
    while ((r = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:r withString:@""];
    return string;
}

+(void)blurEffectsOnView:(UIView *)blurAbleView withEffectStyle:(UIBlurEffectStyle)style
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:style];

    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.alpha = 0.5;
    
    visualEffectView.frame = blurAbleView.bounds;
    [blurAbleView addSubview:visualEffectView];
    [blurAbleView sendSubviewToBack:visualEffectView];
}

+(void)findeDigitFromString:(UILabel *)lable
{
    if (lable.text.length == 0) return;
    
    NSArray *allDegits = [[self NumberFromText:lable.text] componentsSeparatedByString:@" "];
    NSString *displayText = lable.text;
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:displayText];;
    for (NSString *degit in allDegits)
    {
        if (degit.length > 0)
        {
            NSRange range = [lable.text rangeOfString:degit];
            [attrString beginEditing];
            [attrString addAttribute:NSFontAttributeName
                               value:[UIFont systemFontOfSize:[lable.font pointSize]]
                               range:range];
        }
    }
    if (attrString) {
        [lable setAttributedText:attrString];
    }
}

+ (NSString *)NumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@" "];
}

+(void)setupMarqureeLabel:(MarqueeLabel *)lbl_title
{
    lbl_title.marqueeType = MLRightLeft;
    lbl_title.rate = 60.0f;
    [lbl_title pauseLabel];
    lbl_title.animationCurve = UIViewAnimationOptionCurveEaseInOut;
}

+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]])
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
        cell.thumbnail.layer.borderWidth = 0.0;
        cell.thumbnail.clipsToBounds = true;
        
        [UIView animateWithDuration:0.1 animations:^{
            //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        cell.thumbnail.layer.borderColor = SelectedColor.CGColor;
        cell.thumbnail.layer.borderWidth = 10.0;
        cell.thumbnail.clipsToBounds = true;
        
        [UIView animateWithDuration:0.1 animations:^{
            //context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}

+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        cell.lbl_title.textColor = [UIColor whiteColor];
        cell.lbl_subTitle.textColor = [UIColor whiteColor];
        //cell.lbl_time.textColor = [UIColor whiteColor];
        
//        cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
//        cell.thumbnail.layer.borderWidth = 0.0;
//        cell.thumbnail.clipsToBounds = true;
        
        [UIView animateWithDuration:0.1 animations:^{
            cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            //cell.thumbnail.superview.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        cell.lbl_title.textColor = [UIColor redColor];
        cell.lbl_subTitle.textColor = [UIColor redColor];
        //cell.lbl_time.textColor = [UIColor redColor];
        
//        cell.thumbnail.layer.borderColor = SelectedColor.CGColor;
//        cell.thumbnail.layer.borderWidth = 15.0;
//        cell.thumbnail.clipsToBounds = true;
        
        [UIView animateWithDuration:0.1 animations:^{
            cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            //cell.thumbnail.superview.transform = CGAffineTransformMakeScale(1.2, 1.2);
        }];
    }
}

+(void)setupButtonFocusState:(UIButton *)button withContext:(UIFocusUpdateContext *)context
{
    @try {
        if ([context.nextFocusedView isKindOfClass:[UIButton class]] && context.nextFocusedView == button) {
            [UIView animateWithDuration:0.1 animations:^{
                button.backgroundColor = [UIColor whiteColor];
                [button setBackgroundImage:[UIImage imageNamed:@"rectangle_gradient_bg_white"] forState:UIControlStateNormal];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //self.btn_moreVideos.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }];
        }
        
        if ([context.previouslyFocusedView isKindOfClass:[UIButton class]] && context.previouslyFocusedView == button) {
            [UIView animateWithDuration:0.1 animations:^{
                button.backgroundColor = [UIColor clearColor];
                [button awakeFromNib];
                //[button setBackgroundImage:[UIImage imageNamed:@"rectangle_gradient_bg_ad"] forState:UIControlStateNormal];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}



+(NSString *)appColorCode
{
    return @"#ED2B5C";;
}

+(NSString *)title:(NSDictionary *)dicObj
{
    @try {
        NSString *title = [StaticData checkStringNull:dicObj[@"title_ar"]];
        if (title.length == 0) {
            title = [StaticData checkStringNull:dicObj[@"title_en"]];
        }
        return title;
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

+(NSMutableArray *)getCateogryList
{
    NSMutableArray *categories = [NSMutableArray new];
    MediaInfo *info = [MediaInfo new];
    info.ID = @"212654";
    info.title = @"فنون";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212326";
    info.title = @"ترفيهي";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212293";
    info.title = @"موضة وأزياء";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212290";
    info.title = @"كوميدي";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212280";
    info.title = @"دراما";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212282";
    info.title = @"ﺗﺸﻮﻳﻖ ﻭﺇﺛﺎﺭﺓ";
    [categories addObject:info];
    
    info = [MediaInfo new];
    info.ID = @"212296";
    info.title = @"فني";
    [categories addObject:info];
    return categories;
}

+(NSString *)getPlainTextFromHTMLString:(NSString *)string
{
    NSString *htmlString = string;
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:htmlString];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        htmlString = [htmlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    htmlString = [htmlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return [self stringByStrippingHTML:htmlString];
}

+(void)changeNavigationAndTabbarColor:(UIViewController *)vc navbar:(UIView *)nav_bar safeAreaView:(UIView *)safeareaView color:(NSString *)colorCode
{
    safeareaView.backgroundColor = [StaticData colorFromHexString:colorCode];
    nav_bar.backgroundColor = [StaticData colorFromHexString:colorCode];
    vc.view.backgroundColor = [StaticData colorFromHexString:colorCode];
    
    [vc.tabBarController.tabBar setBarTintColor:[StaticData colorFromHexString:colorCode]];
}

+(NavigationBar *)addNavBarToView:(UIView *)navView bgColor:(UIColor *)bgColor withBackButton:(BOOL) isTrue
{
    NavigationBar *nav = [[NSBundle mainBundle] loadNibNamed:isIpad ? @"NavigationBarIpad" : @"NavigationBar" owner:self options:nil][0];
    if (isTrue) {
        nav = [[NSBundle mainBundle] loadNibNamed:isIpad ? @"NavigationBarIpad" : @"NavigationBar" owner:self options:nil][1];
    }
    [navView addSubview:nav];
    nav.backgroundColor = bgColor;
    [StaticData addConstraingWithParent:navView onChild:nav];
    return nav;
}

+(NavigationBar *)addNavBarToView:(UIView *)navView withMenuButton:(BOOL) isShow
{
    NavigationBar *nav = [[NSBundle mainBundle] loadNibNamed:@"NavigationBar" owner:self options:nil][0];
    if (isShow) {
        nav = [[NSBundle mainBundle] loadNibNamed:@"NavigationBar" owner:self options:nil][1];
    }
    [navView addSubview:nav];
    [StaticData addConstraingWithParent:navView onChild:nav];
    return nav;
}

+(BOOL)isIphoneX
{
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
            case 2436:
                printf("iPhone X, XS");
                return true;
                break;
                
            case 2688:
                printf("iPhone XS Max");
                return true;
                break;
                
            case 1792:
                printf("iPhone XR");
                return true;
                break;
                
            default:
                printf("Unknown");
                return false;
                break;
        }
    }
    
    return false;
    
    /*CGRect frame = [UIScreen mainScreen].bounds;
    if (frame.size.height == 812)
    {
        return true;
    }
    return false;*/
}

+(NSAttributedString *)removeHTMLFromContent:(NSString *)content
{
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [content dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    return attributedString;
}

+(BOOL) isJailbroken
{
    @try {
        #if !(TARGET_IPHONE_SIMULATOR)

           if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"] ||
               [[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/MobileSubstrate.dylib"] ||
               [[NSFileManager defaultManager] fileExistsAtPath:@"/bin/bash"] ||
               [[NSFileManager defaultManager] fileExistsAtPath:@"/usr/sbin/sshd"] ||
               [[NSFileManager defaultManager] fileExistsAtPath:@"/etc/apt"] ||
               [[NSFileManager defaultManager] fileExistsAtPath:@"/private/var/lib/apt/"] ||
               [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.example.package"]])  {
                 return YES;
           }

           FILE *f = NULL ;
           if ((f = fopen("/bin/bash", "r")) ||
              (f = fopen("/Applications/Cydia.app", "r")) ||
              (f = fopen("/Library/MobileSubstrate/MobileSubstrate.dylib", "r")) ||
              (f = fopen("/usr/sbin/sshd", "r")) ||
              (f = fopen("/etc/apt", "r")))  {
                 fclose(f);
                 return YES;
           }
           fclose(f);

           NSError *error;
           NSString *stringToBeWritten = @"This is a test.";
           [stringToBeWritten writeToFile:@"/private/jailbreak.txt" atomically:YES encoding:NSUTF8StringEncoding error:&error];
           [[NSFileManager defaultManager] removeItemAtPath:@"/private/jailbreak.txt" error:nil];
           if(error == nil)
           {
              return YES;
           }

        #endif
    } @catch (NSException *exception) {
    } @finally {
    }
   return NO;
}

+(void)setupExternalPlayer:(AVPlayer *)avplayer
{
    [[StaticData shared] allowExternalPlayer:false];
    [StaticData shared].externalVideoPlayer = avplayer;
    [[StaticData shared] allowExternalPlayer:true];
}

-(void)allowExternalPlayer:(BOOL)isTrue
{
    if (self.externalVideoPlayer) {
        [[StaticData shared].externalVideoPlayer setAllowsExternalPlayback:isTrue];
    self.externalVideoPlayer.usesExternalPlaybackWhileExternalScreenIsActive = isTrue;
    }
}

+(NSString *)removeSpaceFromString:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"%" withString:@"-"];
    
    string = [string stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"!" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"(" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@")" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"." withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"#" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"*" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"&" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"$" withString:@"-"];
              
    string = [string stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"=" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@";" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"?" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"," withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"<" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@">" withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"-" withString:@"-"];
              

    return string;
}

//+(void)removePlayerObserve
//{
//    @try {
//        [Notify postNotificationName:RemovePlayerObserveNOTI object:nil];
//
//        RootContainerViewController *rootContainerVC;
//        rootContainerVC =
//        (RootContainerViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//        NSArray *childs = rootContainerVC.childViewControllers;
//        if (childs.count == 0) {
//            return;
//        }
//
//        TabBarController *tabbarController = (TabBarController *)childs[0];
//        if (tabbarController)
//        {
//            for (UINavigationController *nav in tabbarController.viewControllers)
//            {
//                for (UIViewController *vc in nav.viewControllers)
//                {
//                    if ([vc isKindOfClass:[MainVC class]])
//                    {
//                        MainVC *home = (MainVC *)vc;
//                        if (home.vc_homeLiveChannel && home.vc_homeLiveChannel.videoPlayer && home.vc_homeLiveChannel.observingMediaPlayer)
//                        {
//                            [home.vc_homeLiveChannel removePlayerObserve];
//                        }
//                    }
//                }
//            }
//        }
//    } @catch (NSException *exception) {
//        NSLog(@"%@",exception.description);
//    } @finally {
//    }
//}

+(void)setTextFieldPlaceHolderColor:(UITextField *)textField withColor:(UIColor *)color
{
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

+(void)setupLogedUserProfileInfo
{
    NSDictionary *userDic = [self getSavedLoginUserInfo];
    if (userDic)
    {
        UserInfo *user = [UserInfo new];
        if ([userDic[@"id"] isKindOfClass:[NSNumber class]])
        {
            int number = [userDic[@"id"] intValue];
            user.ID = [@(number) stringValue];
        } else if ([userDic[@"id"] isKindOfClass:[NSString class]])
        {
            user.ID = [StaticData checkStringNull:userDic[@"id"]];
        } else {
            user.ID = [StaticData checkStringNull:userDic[@"id"]];
        }
        user.token = [StaticData checkStringNull:userDic[@"token"]];
        user.firstName = [StaticData checkStringNull:userDic[@"firstname"]];
        
        [StaticData shared].userInfo = user;
    }
}

+(NSDictionary *)getSavedLoginUserInfo
{
    @try {
        NSError * error;
        NSUserDefaults *userDefault = [NSUserDefaults new];
        NSString *encrypted = [userDefault objectForKey:@"LoginUserInfo"];
        if (encrypted.length > 0)
        {
            NSString *decrypt = [SecurityUtils decrypt:encrypted error:&error];
            NSData *data = [decrypt dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (json) {
                NSLog(@"%@",json);
                return json;
            }
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return nil;
}

+(void)saveMyRememberInfo:(NSDictionary *)userInfo
{
    @try {
        NSUserDefaults *userDefault = [NSUserDefaults new];
        [userDefault removeObjectForKey:@"RememberLoginInfo"];
        [userDefault synchronize];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
        NSString * plainText = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        NSLog(@"%@",plainText);
        
        NSError * error;
        NSString *encrypted = [SecurityUtils encrypt:plainText error:&error];
        
        [userDefault setObject:encrypted forKey:@"RememberLoginInfo"];
        [userDefault synchronize];
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(void)changeLanguageToEnglish:(BOOL)isEnglish
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (isEnglish)
    {
        [userDefault setBool:true forKey:@"IsEnglishLanguage"];
    } else
    {
        [userDefault setBool:false forKey:@"IsEnglishLanguage"];
    }
}

+(NSString *)AppDubaiFont:(NSInteger)tag
{
    switch (tag) {
        case 0:
            return AppDubaiMediaFontRegular;
            break;
        case 1:
            return AppDubaiMediaFontBold;
        break;
        default:
            break;
    }
    return AppDubaiMediaFontRegular;
}

+(BOOL)checkIsVideoPrimium:(NSDictionary *)videoDicInfo
{
    @try {
        NSString *isPremium = [StaticData checkStringNull:videoDicInfo[@"premium"]];
        
        if ([isPremium isEqualToString:@"1"]) {
            return true;
        }
        return false;
    } @catch (NSException *exception) {
    } @finally {
    }
    return false;
}

+(BOOL)checkIsVideoExclusive:(NSDictionary *)videoDicInfo
{
    @try {
        NSString *isPremium = [StaticData checkStringNull:videoDicInfo[@"exclusive"]];
        
        if ([isPremium isEqualToString:@"yes"]) {
            return true;
        }
        return false;
    } @catch (NSException *exception) {
    } @finally {
    }
    return false;
}

+(BOOL)checkIsVideoVIP:(NSDictionary *)videoDicInfo
{
    @try {
        NSString *isPremium = [StaticData checkStringNull:videoDicInfo[@"vip"]];
        
        if ([isPremium isEqualToString:@"1"]) {
            return true;
        }
        return false;
    } @catch (NSException *exception) {
    } @finally {
    }
    return false;
}

+(void)setFreeNumberOfWatchingVideoCount:(NSInteger)count
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setInteger:count forKey:FreeVideoWatchingCount];
    [userDefault synchronize];
}

+(NSInteger)getFreeNumberOfWatchingVideoCount
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault integerForKey:FreeVideoWatchingCount];
}

+(BOOL)isIpadDevice
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return YES; /* Device is iPad */
    }
    return false;
}

+(UIStoryboard *)getStoryboard:(int) number
{
    if (number == 1) {
        return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else if (number == 2) {
        return [UIStoryboard storyboardWithName:@"Catchup" bundle:nil];
    } else if (number == 3) {
        return [UIStoryboard storyboardWithName:@"ShowDetails" bundle:nil];
    } else if (number == 4) {
        return [UIStoryboard storyboardWithName:@"Live" bundle:nil];
    } else if (number == 5) {
        return [UIStoryboard storyboardWithName:@"Categories" bundle:nil];
    } else if (number == 6) {
        return [UIStoryboard storyboardWithName:@"LiveChannels" bundle:nil];
    } else if (number == 7) {
        return [UIStoryboard storyboardWithName:@"Search" bundle:nil];
    } else if (number == 8) {
        return [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    } else if (number == 9) {
        return [UIStoryboard storyboardWithName:@"Auth" bundle:nil];
    }
    return nil;
}

+(NSString*)getVideoDuration:(int)currentSeconds
{
    int totalSeconds = (int)currentSeconds;
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60);
    int hours = 0;//totalSeconds / 3600;
    
    if (hours > 0) {
        NSString *hoursString = hours < 10 ? [NSString stringWithFormat:@"%02d", hours] : [NSString stringWithFormat:@"%d", hours];
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        return [NSString stringWithFormat:@"%@:%@",hoursString,minsString];
    } else if (minutes > 0) {
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"%02d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"%02d", seconds] : [NSString stringWithFormat:@"%d", seconds];
        return [NSString stringWithFormat:@"%@:%@",minsString,secsString];
    }
    
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"%02d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    
    /*if (seconds >= 30) {
        minutes += 1;
    }
    
    if (hours > 0) {
        NSString *hoursString = hours < 10 ? [NSString stringWithFormat:@"%d", hours] : [NSString stringWithFormat:@"%d", hours];
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        return [NSString stringWithFormat:@"%@:%@",hoursString,minsString];
    } else if (minutes > 0) {
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        return [NSString stringWithFormat:@"%@",minsString];
    }
    
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"%d", seconds] : [NSString stringWithFormat:@"%d", seconds];*/
    return [NSString stringWithFormat:@"%@", secsString];
}

+(BOOL)checkStringIsAlphabets:(NSString *)myString
{
    @try {
        NSRegularExpression *regex = [[NSRegularExpression alloc]
          initWithPattern:@"[a-zA-Z]" options:0 error:NULL];

        // Assuming you have some NSString `myString`.
        NSUInteger matches = [regex numberOfMatchesInString:myString options:0
          range:NSMakeRange(0, [myString length])];

        if (matches > 0) {
          // `myString` contains at least one English letter.
            return true;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return false;
}

+(void)changeSelectedTabbarImage:(UITabBarController *)tabBarController imageName:(NSString *)selectedImageName
{
    UITabBarItem *tabBarItem = [tabBarController.tabBar.items objectAtIndex:tabBarController.selectedIndex];
    [tabBarItem setSelectedImage:[[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

+(NSDictionary *)getLoginErrorMessageInfoWithMessage:(NSString *)message
{
     NSDictionary *dicInfo = @{@"message":message,@"type":@"login",@"button_title_1":Continue};
    return dicInfo;
}

+(NSDictionary *)getSubscriptionErrorMessageInfoWithMessage:(NSString *)message
{
     NSDictionary *dicInfo = @{@"message":message,@"type":@"subscribeNow",@"button_title_1":Continue};
    return dicInfo;
}

+(NSDictionary *)getAlertErrorMessageInfoWithMessage:(NSString *)message
{
     NSDictionary *dicInfo = @{@"message":message,@"type":@"alert",@"button_title_1":@""};
    return dicInfo;
}

+(void)setRootViewController
{
    @try {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *root = [sb instantiateViewControllerWithIdentifier:@"Root"];
        appDelegate.window.rootViewController = root;
    } @catch (NSException *exception) {
        NSLog(@"exception found = %@",exception.description);
    } @finally {
    }
    
}

+(UIImage *)changeImageColor:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
    CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));

    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    
    return coloredImg;
}

+(void)logOutUser
{
    [StaticData shared].userInfo = nil;
    [StaticData saveDataPref:nil withFileName:@"UserInfo"];
    [StaticData saveDataPref:nil withFileName:@"ProfileInfo"];
    [StaticData setRootViewController];
}

+(void)updateProfileInLocal:(id)responseObject
{
    NSDictionary *savedUserInfo = [StaticData getDataPrefWithFileName:@"UserInfo"];
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:savedUserInfo];
    
    NSMutableArray *savedPofileList = [NSMutableArray arrayWithArray:userInfo[@"profiles"]];
    
    NSMutableArray *serverProfileList = [NSMutableArray new];
    for (NSDictionary *serverProfile in responseObject[@"data"][@"profiles"])
    {
        for (NSDictionary *saveProfile in savedPofileList)
        {
//            if ([[StaticData checkStringNull:saveProfile[@"id"]] isEqualToString:[StaticData checkStringNull:serverProfile[@"id"]]])
//            {
//                NSMutableDictionary *mutableServerProfile = [NSMutableDictionary dictionaryWithDictionary:serverProfile];
//                [mutableServerProfile setObject:saveProfile[@"token"] forKey:@"token"];
//                [serverProfileList addObject:mutableServerProfile];
//            }
            NSDictionary *profileInfo = [StaticData getDataPrefWithFileName:@"ProfileInfo"];
            
            if (profileInfo && [[StaticData checkStringNull:profileInfo[@"id"]] isEqualToString:[StaticData checkStringNull:serverProfile[@"id"]]])
            {
                [StaticData saveDataPref:serverProfile withFileName:@"ProfileInfo"];
            }
        }
    }
    
    [userInfo setObject:serverProfileList forKey:@"profiles"];
    [StaticData saveDataPref:userInfo withFileName:@"UserInfo"];
}

+(NSDictionary *)getCreateNewProfileErrorMessageInfoWithMessage:(NSString *)message
{
     NSDictionary *dicInfo = @{@"message":message,@"type":@"subscribeNow",@"button_title_1":@"اشترك"};
    return dicInfo;
}

+(void)changeImageViewColor:(UIImageView *)imageView WithColor:(NSString *)colorCode
{
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:[StaticData colorFromHexString:colorCode]];
}

+(void)addFocusBoderOnItem:(id)imageView withCornerRadius:(NSInteger)radius isFocus:(BOOL)isFocus
{
    if (isFocus) {
        [imageView layer].borderWidth = 4.0;
        [imageView layer].borderColor = [UIColor whiteColor].CGColor;
        [imageView layer].cornerRadius = radius;
    } else {
        [imageView layer].borderWidth = 0.0;
        [imageView layer].borderColor = [UIColor whiteColor].CGColor;
        [imageView layer].cornerRadius = 0.0;
    }
}

+(void)scaleCellObjects:(CollectionCell *)cell
{
    [StaticData scaleToArabic:cell.thumbnail];
    [StaticData scaleToArabic:cell.view_info];
}

+(void)addShadowAroundCell:(CollectionCell *)cell
{
    /*cell.view_content_child.layer.cornerRadius = CellRounded;
    cell.view_content_child.layer.masksToBounds = true;
    
    if (cell.view_content_parent.tag == 0) {
        cell.view_content_parent.tag = 1011;
        cell.view_content_parent.layer.shadowRadius  = 1.5f;
        cell.view_content_parent.layer.shadowColor   = [StaticData colorFromHexString:@"#000000"].CGColor;
        cell.view_content_parent.layer.shadowOffset  = CGSizeZero;
        cell.view_content_parent.layer.shadowOpacity = 1.0f;
        cell.view_content_parent.layer.masksToBounds = NO;

//        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
//        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.view_content_parent.bounds, shadowInsets)];
//        cell.view_content_parent.layer.shadowPath    = shadowPath.CGPath;
    }*/
}
+(UIViewController *)getSelectedViewController
{
    UINavigationController *rootVC = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([rootVC isKindOfClass:[UINavigationController class]])
    {
        UIViewController *activeController = [rootVC.viewControllers lastObject];
        return activeController;
    }
    
    return nil;
}

+(void)checkUserAccess
{
    if ([StaticData isUserLogins])
    {
        [StaticData checkIsUserSubscribedWithToken:[StaticData checkStringNull:[StaticData findLogedUserToken]] WithComplition:^(BOOL isTrue)
        {}];
    }
}

+(BOOL)checkIsVideoNewVideoByDate:(NSString *)publishDate
{
    @try {
        if (publishDate.length == 0) {
            return false;
        }
        
        //2020-06-05 22:40:24
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSDate *videoDate = [df dateFromString:publishDate];
        
        if (!videoDate) {
            return false;
        }
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterFullStyle];
        [formatter setTimeStyle:NSDateFormatterFullStyle];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *today = [NSDate date];
        NSString *todayDateStr = [formatter stringFromDate:today];
        NSDate *todayDate = [df dateFromString:todayDateStr];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitHour
                fromDate: videoDate toDate: todayDate options: 0];
        NSInteger hours = [components hour];
        //NSLog(@"Hours = %ld",(long)hours);
        if (hours <= 48) {
            return true;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return false;
}

+(NSString *)AppFont:(NSInteger)tag
{
//    if (isEnglishLang) {
//        switch (tag) {
//            case 0:
//                return TheMixArabicLight;
//                break;
//            case 1:
//                return TheMixArabicBold;
//            break;
//            case 2:
//                return TheMixArabicBlack;
//            case 3:
//                return TheMixArabicExtraBlod;
//            break;
//            default:
//                break;
//        }
//    } else {
        switch (tag) {
            case 0:
                return PFDinTextArabicRegular;
                break;
            case 1:
                return PFDinTextArabicBold;
            break;
            case 2:
                return PFDinTextArabicMedium;
            break;
            case 3:
                return PFDinTextArabicBold;
            break;
            default:
                break;
        }
    //}
    
    return TheMixArabicLight;
}

+(void)UpdateHorizontalCarouselFocusInContext:(UICollectionViewFocusUpdateContext *)context withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [cell.lbl_titleMarquee pauseLabel];
            cell.view_info.hidden = true;
            cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
            cell.thumbnail.layer.borderWidth = 0.0;
            
            [UIView animateWithDuration:0.1 animations:^{
                //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            [cell.lbl_titleMarquee unpauseLabel];
            cell.view_info.hidden = true;
            
            cell.clipsToBounds = true;
            cell.thumbnail.layer.borderColor = AppColor.CGColor;
            cell.thumbnail.layer.borderWidth = CellBorderWidth;
            cell.thumbnail.layer.cornerRadius = CellRounded;
            
            [UIView animateWithDuration:0.1 animations:^{
                //context.nextFocusedView.transform = CGAffineTransformMakeScale(1.12, 1.12);
            }];
            
            NSIndexPath *indexPath = context.nextFocusedIndexPath;
            if (indexPath)
            {
                //[self updateCollectionViewCells:indexPath withCollectionView:collectionView];
            }
        }
    }
}

+(void)openVideoPage:(MediaInfo *)info withVC:(UIViewController *)activeVC
{
    PlayVideos *vc = [[self getStoryboard:1] instantiateViewControllerWithIdentifier:@"PlayVideos"];
    vc.show_id = info.ID;
    [activeVC presentViewController:vc animated:true completion:nil];
}

+(void)openShowDetailsPageWith:(UIViewController *)activeVC withMediaInfo:(MediaInfo *)info
{
    ShowDetailsParentVC *vc = [[self getStoryboard:3] instantiateViewControllerWithIdentifier:@"ShowDetailsParentVC"];
    vc.selectedShow = info;
    [activeVC.navigationController pushViewController:vc animated:true];
}

+(void)openRadioShowDetailsPageWith:(UIViewController *)activeVC withMediaInfo:(MediaInfo *)info
{
//    RadioShowDetailsVC *vc = [[self getStoryboard:5] instantiateViewControllerWithIdentifier:@"RadioShowDetailsVC"];
//    vc.selectedShow = info;
//    [activeVC.navigationController pushViewController:vc animated:true];
    
    [StaticData openShowDetailsPageWith:activeVC withMediaInfo:info];
}

+(TabbarController *)setupTabbarControllerWithVC:(UIViewController *)vc withChildView:(UIView *)parent withPageType:(NSString *)pageType
{
    TabbarController *vc_menuVC = [[self getStoryboard:1] instantiateViewControllerWithIdentifier:@"TabbarController"];
    vc_menuVC.parentVC = vc;
    vc_menuVC.pageType = pageType;
    CGRect frame = vc.view.frame;
    CGFloat height = 180;
    
    [vc addChildViewController:vc_menuVC];
    
    [parent setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [vc.view addSubview:vc_menuVC.view];
    
    [vc_menuVC didMoveToParentViewController:vc];
    
    [StaticData addConstraingWithParent:parent onChild:vc_menuVC.view];
    
    for (NSLayoutConstraint *constraint in parent.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            vc_menuVC.parentHeightConstraint = constraint;
            break;
        }
    }
    
    return vc_menuVC;
}

+(void)setSSLCertificate:(AFHTTPSessionManager *)manager {
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
}

+(MenuVC *)setupMenuWithVC:(UIViewController *)vc
{
    MenuVC *vc_menuVC = [[self getStoryboard:1] instantiateViewControllerWithIdentifier:@"MenuVC"];
    vc_menuVC.parentVC = vc;
    CGRect frame = vc.view.frame;
    CGFloat height = frame.size.height;
    
    [vc addChildViewController:vc_menuVC];
    
    [vc_menuVC.view setFrame:CGRectMake(0, -(height-150), frame.size.width, height)];
    
    [vc.view addSubview:vc_menuVC.view];
    
    [vc_menuVC didMoveToParentViewController:vc];
    return vc_menuVC;
//    __weak typeof(self) weakSelf = self;
//    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
//        weakSelf.vc_homeBanner.heightConstraint = heightConstraint;
//        [weakSelf viewDidLayoutSubviews];
//    }];
//
//    self.vc_homeBanner.HomeBannerSwipeUpCallBack = ^{
//    };
//
//    self.vc_homeBanner.UpdateContentSize = ^{
//       [weakSelf updateScrollViewContentSize];
//    };
}

+(NSDictionary *)getMyRememberInfo
{
    @try {
        NSError * error;
        NSUserDefaults *userDefault = [NSUserDefaults new];
        NSString *encrypted = [userDefault objectForKey:@"RememberLoginInfo"];
        NSString *decrypt = [SecurityUtils decrypt:encrypted error:&error];
        NSData *data = [decrypt dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (json) {
            NSLog(@"%@",json);
            return json;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return nil;
}

+(void)saveLoginUserInfo:(NSDictionary *)userInfo
{
    @try {
        NSError *err;
        NSData *jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
        NSString *plainText = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        NSLog(@"%@",plainText);
        
        NSError *error;
        NSString *encrypted = [SecurityUtils encrypt:plainText error:&error];
        
        NSUserDefaults *userDefault = [NSUserDefaults new];
        [userDefault setObject:encrypted forKey:@"LoginUserInfo"];
        [userDefault synchronize];
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(void)logOutMe
{
    NSUserDefaults *userDefault = [NSUserDefaults new];
    [userDefault setObject:nil forKey:@"LoginUserInfo"];
    [userDefault synchronize];
}

+(void)setupLineSpacing:(id)textField
{
    @try {
        if (isEnglishLang) return;
        
        if ([textField isKindOfClass:[UITextView class]])
        {
            
            UITextView *textview = (UITextView *)textField;
            UIColor *color = textview.textColor;
            
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:textview.text];
            NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
            style.alignment = NSTextAlignmentJustified;
            style.baseWritingDirection = NSWritingDirectionRightToLeft;
            style.lineBreakMode = NSLineBreakByWordWrapping;
            style.lineSpacing = 7.0;
            [text addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, textview.text.length)];
            textview.attributedText = text;
            
            textview.textColor = color;
            [textview awakeFromNib];
        } else if ([textField isKindOfClass:[UILabel class]])
        {
            UILabel *textview = (UILabel *)textField;
            
            UIColor *color = textview.textColor;
            
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:textview.text];
            NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
            style.alignment = NSTextAlignmentJustified;
            style.baseWritingDirection = NSWritingDirectionRightToLeft;
            style.lineBreakMode = NSLineBreakByWordWrapping;
            style.lineSpacing = 20.0;
            [text addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, textview.text.length)];
            textview.attributedText = text;
            textview.textColor = color;
            [textview awakeFromNib];
            
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

+(NSMutableArray *)menuItems
{
    NSMutableArray *menuItems = [NSMutableArray new];
    MediaInfo *info = [MediaInfo new];
    
    info = [MediaInfo new];
    info.itemType = @"logo";
    info.title_ar = @"";
    info.thumbnailPath = @"app_logo";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"home";
    info.title_ar = @"الرئيسية";
    info.thumbnailPath = @"menu_home";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"programs";
    info.title_ar = @"البرامج";
    info.thumbnailPath = @"menu_program";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"channels";
    info.title_ar = @"القنوات";
    info.thumbnailPath = @"menu_channel";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"categories";
    info.title_ar = @"الفئات";
    info.thumbnailPath = @"menu_categories";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"schedule";
    info.title_ar = @"لا يفوتك";
    info.thumbnailPath = @"menu_don't_miss_it";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"live";
    info.title_ar = @"البث المباشر";
    info.thumbnailPath = @"menu_live_broadcast";
    info.intType = 1;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"";
    info.title_ar = @"";
    info.thumbnailPath = @"";
    info.intType = 2;
    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"login";
    info.title_ar = @"حسابي";
    info.thumbnailPath = @"menu_user_login";
    info.intType = 1;
    [menuItems addObject:info];
    
//    info = [MediaInfo new];
//    info.itemType = @"";
//    info.title_ar = @"إعدادات";
//    info.thumbnailPath = @"menu_settings";
//    info.intType = 1;
//    [menuItems addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"search";
    info.title_ar = @"بحث";
    info.thumbnailPath = @"menu_search";
    info.intType = 1;
    [menuItems addObject:info];
    
//    info = [MediaInfo new];
//    info.itemType = @"";
//    info.title_ar = @"اللغات";
//    info.thumbnailPath = @"menu_language";
//    info.intType = 1;
//    [menuItems addObject:info];
    
//    info = [MediaInfo new];
//    info.itemType = @"";
//    info.title_ar = @"من نحن";
//    info.thumbnailPath = @"men_who_are_we";
//    info.intType = 1;
//    [menuItems addObject:info];

    
    return menuItems;
}

+(void)isExclusive:(NSString *)isExclusive exclusiveImg:(UIImageView *)exclusiveImg {
    if ([isExclusive isEqualToString:@"yes"]) {
        [exclusiveImg setHidden:false];
    }
    else {
        [exclusiveImg setHidden:true];
    }
}

+(void)addSpaceInLabelText:(UILabel *)lblText {
    UIColor *color = lblText.textColor;
    UIFont *lblFont = [lblText.font copy];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:lblText.text];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentRight;
//        style.baseWritingDirection = NSWritingDirectionRightToLeft;
//        style.lineBreakMode = NSLineBreakByWordWrapping;
    style.lineSpacing = 7;
    [text addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, lblText.text.length)];
    lblText.attributedText = text;
    [lblText setFont:lblFont];
    lblText.textColor = color;
//        [cell.lbl_title awakeFromNib];
}

+(NSString *)getBlockThumbnailWithAspectRatioType:(NSString *)imageAspectRatio WithCarousel:(NSDictionary *)subDic
{
    @try {
        NSString *thumbnailPath = @"";
        if ([imageAspectRatio isEqualToString:@"h"])
        {
            thumbnailPath = [StaticData checkStringNull:subDic[@"h_image"]];
            
            if (thumbnailPath.length == 0) {
                thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
            }
        } else if ([imageAspectRatio isEqualToString:@"v"])
        {
            thumbnailPath = [StaticData checkStringNull:subDic[@"v_image"]];
            
            if (thumbnailPath.length == 0)
            {
                thumbnailPath = [StaticData checkStringNull:subDic[@"atv_thumbnail"]];
            }
            
            if (thumbnailPath.length == 0)
            {
                thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
            }
        }
        return thumbnailPath;
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
    
}

@end
