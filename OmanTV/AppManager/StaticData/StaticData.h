//
//  StaticData.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 08/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFURLSessionManager.h"
#import "AFHTTPSessionManager.h"
#import "UIImageView+WebCache.h"
#import "MediaInfo.h"
#import "UserInfo.h"
#import "MarqueeLabel.h"
#import "NavigationBar.h"
//#import "PopUpVC.h"
////#import "CustomAlertVC.h"
////#import "TabbarVC.h"
#import "CollectionCell.h"
#import "MenuVC.h"
#import "TabbarController.h"
#import "SecurityUtils.h"
#import "CatchupAudioPlayer.h"

@interface StaticData : NSObject

typedef void(^ImageLoadingCompletionBlock)(UIImage *image);
typedef void(^ConstraintCompletionBlock)(NSLayoutConstraint *heightConstraint);
typedef void(^HomeConstraintCompletionBlock)(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint);
typedef void(^ConstraintOFScrollChildCompletionBlock)(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint);
typedef void(^CheckVideoAbleToWatchCompletionBlock)(BOOL isTrue);
typedef void(^ProfileCompletionBlock)(id josn);

@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) NSString *myCountryCode;
@property (nonatomic, retain) NSString *myCountryName;
@property (nonatomic, retain) NSString *myIP;
@property (nonatomic, strong) UserInfo *userInfo;
@property(nonatomic, retain) NSString *deviceID;
@property (nonatomic, retain) NSMutableArray *liveChannels;
@property (nonatomic, retain) AVPlayer *externalVideoPlayer;
@property (nonatomic, retain) NSDictionary *notificationReceived;
@property (nonatomic, retain) NSDictionary *ramadanScheduleDic;
@property (nonatomic, assign) BOOL isUserJustSignUpSoTakeToLogin;
@property (nonatomic, assign) BOOL isSearching;

+(StaticData *)shared;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+(void)scaleToArabic:(UIView *)object;
+(void)showStatusBar:(BOOL)show;
+(void)buttonUnderLineTitle:(UIButton *)button;
+(void)removeUnderLineTitle:(UIButton *)button;
+(void)log:(NSString *)message;
+(BOOL)validateEmailWithString:(NSString*)email;
+(BOOL)checkIsScueduleGeoBlockOnLiveChannel:(NSDictionary *)chDic;
+(NSDictionary *)checkDictionaryNull:(NSDictionary *)dic;
+(NSArray *)checkArrayNull:(id)dic;
+(NSString *)checkStringNull:(NSString *)string;
+(void)reloadCollectionView:(UICollectionView *)collView;
+(void)downloadImageIfNotLoadinedBySdImage:(UIImageView *)imagevc withUrl:(NSURL *)imgURL completed:(ImageLoadingCompletionBlock)completedBlock;
+(void)roundImageView:(UIImage *)image wihtImageView:(UIImageView *)thumbnail;
+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original withImageView:(UIImageView *)imageView;
+(BOOL)isVideoBlockInMyCountry:(NSDictionary *)response isShowGeoStatus:(BOOL)istrue;
+(BOOL)isVideoScheduleGeoBlocking:(NSDictionary *)response;
+ (NSString *)getMyCountryCode;
+(BOOL)isLanguageEnglish;
+(void)saveDataPref:(id)object withFileName:(NSString *)fileName;
+(id)getDataPrefWithFileName:(NSString *)fileName;
+(NSString *)findeLogedUserID;
+(NSString *)findSelectedProfileToken;
+(NSString *)findLogedUserToken;
+(BOOL)isUserLogins;
+(BOOL)isContentPlus:(NSDictionary *)response isContnetShow:(BOOL)isShow;
+(NSDate *)getDateFromCalendar:(int)day;
+(void)textFieldPadding:(UITextField *)textField;
+(void)textFieldPlaceHolderColor:(UITextField *)textField;
+(NSArray *)readJsobFileWithName:(NSString *)fileName;
+(NSString *)findArabicWeekDayByDate:(NSString *)serverDate;
+(NSString *)getForamttedDate:(NSString *)time;
+(CGFloat)getTabbarHeight;
+(NSString *) stringByStrippingHTML:(NSString *)string;
+(void)updateBottomAnchorWhenAudioPlayingWithConstraint:(NSLayoutConstraint *)parentViewBottomAnchor;
+(void)blurEffectsOnView:(UIView *)blurAbleView withEffectStyle:(UIBlurEffectStyle)style;
+(void)findeDigitFromString:(UILabel *)lable;
+(NSString *)getCurrentTime;
+(void)showAlertView:(NSString *)message withvc:(UIViewController *)vc;
+(void)setSSLCertificate:(AFHTTPSessionManager *)manager;
+(void)setupMarqureeLabel:(MarqueeLabel *)lbl_title;
+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator;
+(NSString *)channelID;
+(NSString *)title:(NSDictionary *)dic;
+(NSMutableArray *)getCateogryList;
+(UIImage *)placeHolder;
+(NSString *)getArabicWeekDay:(NSString *)wDay;
+(void)setupButtonFocusState:(UIButton *)button withContext:(UIFocusUpdateContext *)context;
+(void)saveDataInPrefs:(NSString *) data withKey:(NSString *)key;
+(NSString *)getSaveDataFromPrefs:(NSString *)pakeythName;
+(void)openStartVCAsAViewController;
+(NSString *)appColorCode;
+(NSString *)getPlainTextFromHTMLString:(NSString *)string;
+(void)setStartViewController:(UIViewController *)vc;
+(NavigationBar *)addNavBarToView:(UIView *)navView withMenuButton:(BOOL) isShow;
+(BOOL)isIphoneX;
+(NSString *)appSelectedColorCode;
+(BOOL)isViewLandscapeMode;
+(NSString *)channelBaseURL;
+(NSAttributedString *)removeHTMLFromContent:(NSString *)content;
+(BOOL)isLiveChannelDigitalRightsDisable:(NSDictionary *)response;
+(BOOL) isJailbroken;
+(void)setupExternalPlayer:(AVPlayer *)avplayer;
-(void)allowExternalPlayer:(BOOL)isTrue;
+(NSString *)removeSpaceFromString:(NSString *)string;
+(int)checkNumberNull:(id)number;
+(NSString *)ramadanAppColorCode;
+(void)removePlayerObserve;

+(void)setTextFieldPlaceHolderColor:(UITextField *)textField withColor:(UIColor *)color;
+(void)setupLogedUserProfileInfo;
+(void)setRootViewController;
+(void)changeLanguageToEnglish:(BOOL)isEnglish;
+(NSString *)AppDubaiFont:(NSInteger)tag;

+(NavigationBar *)addNavBarToView:(UIView *)navView bgColor:(UIColor *)bgColor withBackButton:(BOOL) isTrue;
+(void)changeNavigationAndTabbarColor:(UIViewController *)vc navbar:(UIView *)nav_bar safeAreaView:(UIView *)safeareaView color:(NSString *)colorCode;
+(BOOL)checkIsVideoPrimium:(NSDictionary *)videoDicInfo;
+(BOOL)checkIsVideoExclusive:(NSDictionary *)videoDicInfo;
//+(CustomAlertVC *)showSignInAlertViewWithVC:(UIViewController *)vc;
//+(CustomAlertVC *)freeContentForTodayIsOverPleaseSignInNowWithVC:(UIViewController *)vc;
+(void)setFreeNumberOfWatchingVideoCount:(NSInteger)count;
+(NSInteger)getFreeNumberOfWatchingVideoCount;
//+(CustomAlertVC *)showSubscribeAlertViewWithVC:(UIViewController *)activeVC;
+(void)checkIsUserSubscribedToWatchThisVideoWithVideoID:(NSString *)videoID withComplition:(CheckVideoAbleToWatchCompletionBlock) completion;
+(BOOL)isIpadDevice;
+(void)openShowDetailsPageWith:(UIViewController *)activeVC withMediaInfo:(MediaInfo *)info;
+(void)openRadioShowDetailsPageWith:(UIViewController *)activeVC withMediaInfo:(MediaInfo *)info;
+(BOOL)checkIsVideoVIP:(NSDictionary *)videoDicInfo;
+(NSString*)getVideoDuration:(int)currentSeconds;
+(BOOL)checkStringIsAlphabets:(NSString *)myString;
+(void)checkIsUserSubscribedWithToken:(NSString *)token WithComplition:(CheckVideoAbleToWatchCompletionBlock) completion;
+(void)changeSelectedTabbarImage:(UITabBarController *)tabBarController imageName:(NSString *)selectedImageName;

+(void)showPopUpVC:(UIViewController *)activeVC mainTitle:(NSString *)mainTitle messageTitle:(NSString *)msgTitle;
///+(CustomAlertVC *)showCustomAlertVC:(UIViewController *)activeController withAlertInfo:(NSDictionary *)alertInfo;
+(NSDictionary *)getLoginErrorMessageInfoWithMessage:(NSString *)message;
+(NSDictionary *)getSubscriptionErrorMessageInfoWithMessage:(NSString *)message;
+(NSDictionary *)getAlertErrorMessageInfoWithMessage:(NSString *)message;
///+(TabbarVC *)setupOfTabbarVCWithViewController:(UIViewController *)controller withParentView:(UIView *)tabbarView;
+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView;
+(UIImage *)changeImageColor:(UIImage *)image withColor:(UIColor *)color;
+(void)getMyProfileInfoWithComplition:(ProfileCompletionBlock) completion;
+(NSDictionary *)getCreateNewProfileErrorMessageInfoWithMessage:(NSString *)message;
+(void)updateProfileInLocal:(id)responseObject;
+(void)logOutUser;
+(void)changeImageViewColor:(UIImageView *)imageView WithColor:(NSString *)colorCode;
+(BOOL)isUserSubscribed;
+(void)addShadowAroundCell:(CollectionCell *)cell;
+(void)setupPremiumContent:(CollectionCell *)cell withContentInfo:(MediaInfo *)info;
+(void)addFocusBoderOnItem:(id)imageView withCornerRadius:(NSInteger)radius isFocus:(BOOL)isFocus;
+(void)scaleCellObjects:(CollectionCell *)cell;
+(UIStoryboard *)getStoryboard:(int) number;
+(void)checkUnSubscribesUserWatchingVideoStatusWithComplition:(CheckVideoAbleToWatchCompletionBlock) completion;
+(void)checkUserAccess;
+(void)checkInternetAvailable;
+(NSString *)findSelectedProfileID;
+(NSString *)getIPAddress;
+(BOOL)checkIsVideoNewVideoByDate:(NSString *)publishDate;
+(void)imageLoaded:(UIImage *)image withCell:(CollectionCell *)cell;
+(NSString *)AppFont:(NSInteger)tag;
+(void)UpdateHorizontalCarouselFocusInContext:(UICollectionViewFocusUpdateContext *)context withCollectionView:(UICollectionView *)collectionView;
+(TabbarController *)setupTabbarControllerWithVC:(UIViewController *)vc withChildView:(UIView *)parent withPageType:(NSString *)pageType;
+(MenuVC *)setupMenuWithVC:(UIViewController *)vc;
+(void)addConstraingWithParent:(UIView *)parent onChild:(UIView *)child;
+(void)openVideoPage:(MediaInfo *)info withVC:(UIViewController *)activeVC;
+(NSDictionary *)getMyRememberInfo;
+(void)buttonUnderLineTitle:(UIButton *)button withColor:(UIColor *)color;
+(void)saveLoginUserInfo:(NSDictionary *)userInfo;
+(void)saveMyRememberInfo:(NSDictionary *)userInfo;
+(void)logOutMe;
+(void)setupLineSpacing:(id)textField;
@property (nonatomic, retain) NSMutableDictionary *appSettingsInfo;
+(NSMutableArray *)menuItems;
+(UIImage *)radioPlaceHolder;
+(void)isExclusive:(NSString *)isExclusive exclusiveImg:(UIImageView *)exclusiveImg;
+(void)addSpaceInLabelText:(UILabel *)lblText;
+(NSString *)getBlockThumbnailWithAspectRatioType:(NSString *)imageAspectRatio WithCarousel:(NSDictionary *)subDic;
@end
