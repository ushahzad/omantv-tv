//
//  UnRegisterPopoverVC.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 09/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "UnRegisterPopoverVC.h"

@interface UnRegisterPopoverVC ()

@end

@implementation UnRegisterPopoverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.view_content.alpha = 0.9;
    
    if(self.isVideoPlaying) {
       self.lbl_title1.text = @"عرض الشاشة الكاملة متوفر فقط لأعضاء تطبيق أوان.";
    }
    
    if ([StaticData isLanguageEnglish]) {
        if(self.isVideoPlaying) {
            self.lbl_title1.text = @"Full Screen mode is only available for registered users.";
        } else {
            self.lbl_title1.text = @"This feature is only available for registered users.";
        }
        self.lbl_title2.text = @"New to Awaan? Please sign up.";
        self.lbl_signup.text = @"Sign up";
    }
    
    // Do any additional setup after loading the view.
    [self.btn_subscribe addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
}


-(void)buttonActionTrigger:(UIButton *)sender
{
    if (self.SubscribeButtonCallBack) {
        self.SubscribeButtonCallBack();
    }
    //[StaticData goToLoginPage];
}

-(void)updateFocuse
{
    [self preferredFocusEnvironments];
    [self updateFocusIfNeeded];
}

-(NSArray*)preferredFocusEnvironments
{
    return @[self.btn_subscribe];
}

-(IBAction)openSubscriptionPage:(id)sender
{
    
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]] || [context.nextFocusedView isKindOfClass:[UIButton class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[UIButton class]])
        {
            [UIView animateWithDuration:0.1 animations:^{
                
                context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
        
        if ([context.nextFocusedView isKindOfClass:[UIButton class]])
        {
            [UIView animateWithDuration:0.1 animations:^{
                
                context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based ap
 plication, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
