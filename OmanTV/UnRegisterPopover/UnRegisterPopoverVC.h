//
//  UnRegisterPopoverVC.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 09/08/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UnRegisterPopoverVC : UIViewController
@property void (^SubscribeButtonCallBack)(void);
@property (nonatomic, weak) IBOutlet UILabel *lbl_title1;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title2;
@property (nonatomic, weak) IBOutlet UILabel *lbl_signup;
@property (nonatomic, weak) IBOutlet UIButton *btn_subscribe;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, assign) BOOL isVideoPlaying;
-(void)updateFocuse;
@end

NS_ASSUME_NONNULL_END
