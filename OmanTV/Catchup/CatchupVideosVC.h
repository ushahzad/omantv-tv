//
//  CatchupVideosVC.h
//  OmanTV
//
//  Created by Curiologix on 22/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CatchupVideosVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^RadioCatchupClickCallBack)(MediaInfo *info);
@property void(^ChannelClickCallBack)(MediaInfo *info);
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UIView *view_dropdown;
@property (nonatomic, weak) IBOutlet UILabel *lbl_dropdown;
@property (nonatomic, weak) IBOutlet UIButton *btn_dropdown;
@property (nonatomic, weak) IBOutlet UIImageView *img_dropdown;
@property (nonatomic, weak) IBOutlet UIViewController *parentVc;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *channelList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) MediaInfo *selectedCatchup;
@property (nonatomic, retain) id scheduleResponse;
@property (nonatomic, assign) BOOL isNeedToOpenDropdown;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic, assign) int pageNumber;
@end

NS_ASSUME_NONNULL_END
