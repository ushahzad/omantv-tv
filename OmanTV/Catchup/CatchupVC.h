//
//  CatchupVC.h
//  OmanTV
//
//  Created by MacUser on 20/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CatchupVideosVC.h"
#import "CatchupAudioPlayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface CatchupVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, retain) NavigationBar *navBar;
@property (nonatomic, weak) IBOutlet UIView *view_navBarParent;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewChannels;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *parentViewBottomAnchor;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noRecordsFound;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_contentParent;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *scrollBottomConstraint;

@property (nonatomic, retain) NSMutableArray *channelList;
@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) MediaInfo *selectedChannel;
@property (nonatomic, retain) MediaInfo *infoAudio;
@property (nonatomic, retain) NSMutableString *sessionID;
@property (nonatomic, strong) NSTimer *updateOlineTimer;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isRadio;

@end

NS_ASSUME_NONNULL_END
