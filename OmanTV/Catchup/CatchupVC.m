//
//  CatchupVC.m
//  OmanTV
//
//  Created by MacUser on 20/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "CatchupVC.h"

@interface CatchupVC ()

@end

@implementation CatchupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.channelList = [NSMutableArray new];

    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"catchup"];
    self.vc_tabbarVC.cv_tabbar.remembersLastFocusedIndexPath = false;
    self.vc_tabbarVC.ChooseChannelTypeCallBack = ^(BOOL isRadio, MediaInfo * _Nonnull info) {
        self.isRadio = isRadio;
        [self removeAudioPlayer];
        [self loadTVChannelsFromServer];
    };
    
    [self setupChannelsTypesController];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionViewChannels.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionViewChannels setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [StaticData scaleToArabic:self.collectionViewChannels];
    
    self.lbl_noRecordsFound.text = NoRecordFound;

    self.scroll_view.hidden = true;
    
    [self loadTVChannelsFromServer];
}

-(void)resetContent
{
    [_channelList removeAllObjects];
    [self reloadCollectionView];
    [self removeAllChilds];
}

-(void)setupChannelsTypesController
{
    ChannelsTypeVC *vc_channelsTypeVC = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    
    CGRect frame = self.view.frame;
    CGFloat height = 70;
    
    [self addChildViewController:vc_channelsTypeVC];
    
    [vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_channelsTypes addSubview:vc_channelsTypeVC.view];
    
    [vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:vc_channelsTypeVC.view];
    
    vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info) {
        weakSelf.isRadio = isRadio;
        [self removeAudioPlayer];
        [self loadTVChannelsFromServer];
    };
}

-(void)loadTVChannelsFromServer
{
    [self resetContent];
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/plus/live_channels?user_id=%@&key=%@&json=1&is_radio=%d&mixed=yes&info=1&mplayer=true&need_live=yes&need_next=yes&next_limit=2",BaseURL,BaseUID,BaseKEY,self.isRadio];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"LiveChannels"];
}

-(void)loadTVProgramsByChannel
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=awaan&action=ChannelCatchup&t=1589188694&channel_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedChannel.ID];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[NSString stringWithFormat:@"ContnetList/%@",self.selectedChannel.ID]];
}

-(void)loadRadioTVProgramsByChannel
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=channel_catchup&custom_icon=yes&t=1589188549&channel_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedChannel.ID];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[NSString stringWithFormat:@"ContnetList/%@",self.selectedChannel.ID]];
}

-(void)getPlayBackUrl:(MediaInfo *)info
{
    self.infoAudio = info;
    [self.indicator startAnimating];
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,info.ID];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        self.scroll_view.hidden = false;
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         self.scroll_view.hidden = false;
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"LiveChannels"])
        {
            for (NSDictionary *dicObj in responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if (dicObj[@"catchup"] && [dicObj[@"catchup"] intValue] == 0)
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"icon"]];
                
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_channelList addObject:info];
                }
            }
            
            if (_channelList.count > 0)
            {
                [self loadSelectedChannel:_channelList[0]];
            }
            
            [StaticData reloadCollectionView:self.collectionViewChannels];
        } else if ([callType containsString:self.selectedChannel.ID])
        {
            NSMutableArray *dayList = [NSMutableArray new];
            MediaInfo *info = [MediaInfo new];
            info.title = isEnglishLang ? @"Today" : @"اليوم";
            info.isSelected = true;
            info.type = 1;
            [self setupContentData:@"today_catch" withInfo:info withResponse:responseObject];
            [dayList addObject:info];
            
            info = [MediaInfo new];
            info.title = isEnglishLang ? @"Yesterday" : @"أمس";
            info.type = 2;
            [self setupContentData:@"yesterday" withInfo:info withResponse:responseObject];
            [dayList addObject:info];

            info = [MediaInfo new];
            info.title = isEnglishLang ? @"Before Yesterday" : @"قبل يومين";
            info.type = 3;
            [self setupContentData:@"b4yesterday" withInfo:info withResponse:responseObject];
            [dayList addObject:info];
            
            self.selectedChannel.items = [dayList mutableCopy];
            
            [self loadSelectedChannelCatchupData];
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)setupContentData:(NSString *)key withInfo:(MediaInfo *)info withResponse:(NSDictionary *)response
{
    NSMutableArray *catchupList = [NSMutableArray new];
    for (NSDictionary *dicObj in response[key])
    {
        if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
        {
            continue;
        }
         
        MediaInfo *info = [MediaInfo new];
        
        info.ID = [StaticData checkStringNull:dicObj[@"id"]];
        info.title = [StaticData title:dicObj];
        
        info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
        
        info.duration = [StaticData checkStringNull:dicObj[@"duration"]];
        info.time = [StaticData getVideoDuration:[info.duration intValue]];
        info.startTime = [self getStartTime:[StaticData checkStringNull:dicObj[@"first_start"]]];
        info.stopTime = [self getStartTime:[StaticData checkStringNull:dicObj[@"stop_time"]]];
        
        int totalReviews = [dicObj[@"today_views"] intValue];
        info.reviews = [NSString stringWithFormat:@"%d ",totalReviews];
        info.isRadio = self.isRadio;
        if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
        {
            [catchupList addObject:info];
        }
    }
    
    info.items = catchupList;
}

-(NSString *)getStartTime:(NSString *)time
{
    @try {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"HH:mm:ss"];
        NSDate *catchupDate = [df dateFromString:time];
        
        [df setDateFormat:@"HH:mm"];
        
        NSString *time = [df stringFromDate:catchupDate];
        
        return time;
    } @catch (NSException *exception) {
        [StaticData log:exception.description];
    } @finally {
    }
}

-(void)loadSelectedChannelCatchupData
{
    [self removeAllChilds];
    
    BOOL resultsFound = false;
    BOOL isDropdDownOpen = true;
    for (MediaInfo *item in self.selectedChannel.items)
    {
        if (item.items.count > 0)
        {
            resultsFound = true;
            [self setupCatchupVideos:item isNeedToShowDropDown:isDropdDownOpen];
            isDropdDownOpen = false;
        }
    }
    
    if (!resultsFound)
    {
        self.lbl_noRecordsFound.hidden = false;
    }
}

-(void)removeAllChilds
{
    self.lbl_noRecordsFound.hidden = true;
    [self.view_content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self viewDidLayoutSubviews];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionViewChannels];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewChannels) {
        return CGSizeMake(265, 150);
    }
    
    CGRect bounds = collectionView.bounds;
    CGFloat width = bounds.size.width;
    CGFloat height = 200.0;
    
    if (self.tempImage) {
        CGFloat thumbWidth = width - 90;
        CGSize size = self.tempImage.size;
        CGFloat ration = size.height / size.width;
        CGFloat height = (thumbWidth * ration) + 60;
    
        return CGSizeMake(width, height);
    }
    return CGSizeMake(width, 124);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.channelList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
 
    if (self.channelList.count == 0) {
        [cell.indicator startAnimating];
        return cell;
    }
    MediaInfo *info = self.channelList[indexPath.row];
    if (info.isSelected) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [StaticData colorFromHexString:@"#262626"];
    }
    
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 /*if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }*/
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     /*if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }*/
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.layer.cornerRadius = CellRounded;
    cell.clipsToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewChannels) {
        MediaInfo *info = self.channelList[indexPath.row];
        [self loadSelectedChannel:info];
    }
}

-(void)loadSelectedChannel:(MediaInfo *)channelInfo
{
    for (MediaInfo *info in self.channelList)
    {
        info.isSelected = false;
    }
    
    channelInfo.isSelected = true;
    
    self.lbl_noRecordsFound.hidden = true;
    [self.view_content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self viewDidLayoutSubviews];
    
    self.selectedChannel = channelInfo;
    
    if (self.selectedChannel.items.count > 0)
    {
        [self loadSelectedChannelCatchupData];
    } else
    {
        if (self.isRadio)
        {
            [self loadRadioTVProgramsByChannel];
        } else
        {
            [self loadTVProgramsByChannel];
        }
    }
    
    [StaticData reloadCollectionView:self.collectionViewChannels];
}

-(void)setupCatchupVideos:(MediaInfo *)info isNeedToShowDropDown:(BOOL)isDropdown
{
    CatchupVideosVC *vc_catchupVideos = [self.storyboard instantiateViewControllerWithIdentifier:@"CatchupVideosVC"];
    vc_catchupVideos.parentVc = self;
    vc_catchupVideos.isNeedToOpenDropdown = isDropdown;
    vc_catchupVideos.selectedCatchup = info;
    vc_catchupVideos.channelList = _channelList;
    CGRect frame = vc_catchupVideos.view.frame;
    CGFloat height = 300;
    
    [self addChildViewController:vc_catchupVideos];
    
    [vc_catchupVideos.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:vc_catchupVideos.view];
    
    [vc_catchupVideos didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
        vc_catchupVideos.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    vc_catchupVideos.collectionView.remembersLastFocusedIndexPath = true;
    
    vc_catchupVideos.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    vc_catchupVideos.RadioCatchupClickCallBack = ^(MediaInfo * _Nonnull info) {
        [self getPlayBackUrl:info];
    };
    
    vc_catchupVideos.ChannelClickCallBack = ^(MediaInfo * _Nonnull info) {
        [self loadSelectedChannel:info];
    };
}
	
-(void)updateScrollViewContentSize
{
    [self viewDidLayoutSubviews];
    //[self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
}

/*- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight-50)
        {
            // then we are at the end
            if (self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = false;
                _pageNumber += 1;
                
                [self loadProgramsByChannel];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}*/

-(NSString *)getScheduleTime:(NSString *)time
{
    @try {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        NSDate *catchupDate = [df dateFromString:time];
        
        [df setDateFormat:@"HH:mm a"];
        
        NSString *time = [df stringFromDate:catchupDate];
        
        return time;
    } @catch (NSException *exception) {
        [StaticData log:exception.description];
    } @finally {
    }
}

-(void)makeVideoFavNowWithID:(NSString *)videoID
{
    @try {
        if([[StaticData findeLogedUserID] length] == 0){
            [StaticData showAlertView:PleaseLoginFirst withvc:self];
            return;
        }
        
        [HUD showUIBlockingIndicator];
        
        NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_videos",BaseURL];
        
        NSDictionary *prams = @{@"key":BaseKEY,@"user_id":BaseUID,@"video_id":videoID,@"fav_type":@"1",@"X-API-KEY":[StaticData findLogedUserToken]};
        
        [self executePostApiToGetResponse:path parameters:prams callType:@"AddToFavoriteVideo"];
    } @catch (NSException *exception) {
    } @finally {
    }
}

#pragma mark - Api Call Request
// make web service api call
-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     
    if ([type isEqualToString:@"setVideoComplitionRate"])
    {
        [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    }else if ([type isEqualToString:@"ShorterUrl"])
    {
        //manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    } else if ([type isEqualToString:@"AddToFavoriteVideo"] || [type isEqualToString:@"AddVideoToList"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    } else if ([type isEqualToString:@"RadioPlayBackUrl"])
    {
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    } else {
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [HUD hideUIBlockingIndicator];
        
         if ([type isEqualToString:@"ShorterUrl"] || [type isEqualToString:@"setVideoComplitionRate"])
         {
             responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
         }
        NSLog(@"Response Received = %@",responseObject);
        [self responseReceived:responseObject callType:type];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Api Error: %@",error.description);
        [HUD hideUIBlockingIndicator];
        @try {
            NSError* jsonError;
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSLog(@"%@",errResponse);
            if (!jsonError && errResponse)
            {
            }
        } @catch (NSException *exception) {
            NSLog(@"%@",exception.description);
        } @finally {
        }
        
     }];
}

-(void)makeVideoAddToListNowWithID:(NSString *)videoID
{
    @try {
        if (![StaticData isUserLogins]) {
            [StaticData showAlertView:PleaseLoginFirst withvc:self];
            return;
        }
        
        [self.indicator startAnimating];
        NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_videos?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
        NSDictionary *parms = @{@"video_id":videoID,@"fav_type":@"0",@"X-API-KEY":[StaticData findLogedUserToken],@"ignore_editing":@"yes"};
        [self executePostApiToGetResponse:path parameters:parms callType:@"AddVideoToList"];
    } @catch (NSException *exception) {
    } @finally {
    }
}

#pragma mark response received
-(void)responseReceived:(id) responseObject callType:(NSString *)callType
{
    @try {
        NSLog(@"Update Online Request = %@",responseObject);
        if ([callType isEqualToString:@"AddToFavoriteVideo"])
        {
        } else if ([callType isEqualToString:@"RadioPlayBackUrl"])
        {
            _str_playbackURL = responseObject[@"playback_url"];
            [self setupAuidoPlayer];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception found........:) %@",exception.description);
    } @finally {
    }
}

-(void)viewDidLayoutSubviews
{
    @try {
        UIView *view = [[[self.scroll_view.subviews objectAtIndex:0] subviews] lastObject];
        CGFloat contentSize = [view frame].origin.y + [view frame].size.height + 50;
        [self.scroll_view setContentSize:CGSizeMake(self.scroll_view.frame.size.width, contentSize)];
        //self.contentViewBottomConstraint.constant = contentSize;
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    } @finally {
        
    }
}

-(CGFloat)yOfLastChildFromScrollView
{
    @try {
        UIView *subView = [[self.scroll_view.subviews[0] subviews] lastObject];
        
        CGFloat yOfLastObject = 0;
        
        if (subView) {
            //yOfLastObject = subView.frame.size.width + subView.frame.origin.y;
        }
        return yOfLastObject;
    } @catch (NSException *exception) {
    } @finally {
    }
    return 0.0;
}

-(void)addConstraingOnScrollChilds:(UIView *)parent withFixedHeight:(CGFloat)fixedheight withTop:(CGFloat)topY completed:(ConstraintCompletionBlock)completedBlock
{
    @try {
        NSArray *childs = [self.scroll_view.subviews[0] subviews];
        UIView *subView = [childs lastObject];
        UIView *secondLastSubView = nil;
        if (childs.count > 1) {
            secondLastSubView = childs[childs.count-2];
        }
        
        
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Trailing
        NSLayoutConstraint *trailing =[NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Leading
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Bottom
        NSLayoutConstraint *top =[NSLayoutConstraint
                                  constraintWithItem:subView
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:parent
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:topY];
        //Height to be fixed for SubView same as AdHeight
        NSLayoutConstraint *height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeHeight
                                      multiplier:0
                                      constant:fixedheight];
        
        //Add constraints to the Parent
        [parent addConstraint:trailing];
        if (secondLastSubView) {
            top =[NSLayoutConstraint
                  constraintWithItem:subView
                  attribute:NSLayoutAttributeTop
                  relatedBy:NSLayoutRelationEqual
                  toItem:secondLastSubView
                  attribute:NSLayoutAttributeBottom
                  multiplier:1
                  constant:topY];
            [parent addConstraint:top];
        } else {
            [parent addConstraint:top];
        }
        [parent addConstraint:leading];
        
        //Add height constraint to the subview, as subview owns it.
        [subView addConstraint:height];
        
        completedBlock(height);
    } @catch (NSException *exception) {
    } @finally {
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    @try {
        
        CollectionCell *cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.nextFocusedIndexPath];
        if (cell)
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        
        cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.previouslyFocusedIndexPath];
        if (cell)
        {
            cell.backgroundColor = [StaticData colorFromHexString:@"#262626"];;
            MediaInfo *info = self.channelList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.backgroundColor = [UIColor whiteColor];
            }
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    
    self.scrollBottomConstraint.constant = 100.0;
    self.radioPlayer = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    self.radioPlayer.playerInfo = self.infoAudio;
    
    [self addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = self.infoAudio.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

-(void)Updaterequest
{
    @try {
        NSString *URLsettings = [NSString stringWithFormat:@"%@?/crosssitestats/UpdateOnline",BaseURL];
        
        NSData* userIdData = [BaseUID dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64EncodedUserID = [userIdData base64EncodedStringWithOptions:0];
        if (self.infoAudio == nil) return;
        NSLog(@"Catchup ID = %@",self.infoAudio.ID);
        
        NSDictionary *parametrs = @{@"userid":base64EncodedUserID,@"browserOS":@"appletv",@"videoid":self.infoAudio.ID,@"channelid":@"",@"sessionid":self.sessionID,@"domain":@"",@"device":@"7"};
        
        [self executePostApiToGetResponse:URLsettings parameters:parametrs callType:@"UpDateOnline"];
    } @catch (NSException *exception) {
        NSLog(@"Update online Request = %@",exception.description);
    } @finally {
        
    }
}

-(void)removeAudioPlayer
{
    if (self.radioPlayer)
    {
        self.scrollBottomConstraint.constant = 0.0;
        [self.radioPlayer resetPlayer];
        [self.radioPlayer willMoveToParentViewController:self];
        [self.radioPlayer.view removeFromSuperview];
        [self.radioPlayer removeFromParentViewController];
        self.radioPlayer = nil;
    }
    
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}


@end
