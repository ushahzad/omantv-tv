//
//  MainVC.m
//  Majid
//
//  Created by MacUser on 31/05/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "HomeParentVC.h"
#import "MenuVC.h"
@interface HomeParentVC ()

@end

@implementation HomeParentVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.customCarouselListVC = [NSMutableArray new];
    
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"home"];
    
    if (@available(tvOS 11.0, *)) {
        self.scroll_view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.scroll_view setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    
    //[self setupAudioBooksTabController];
    //[self setupAudioBooksController];
    
    //[self setupLiveChannelsController];

    //[self setupNowOnTVController];

    //[self setupProgramsController];

    /// old [self setupOfLatestEpisodesVC];

    //[self setupMostWatchedController];

    [self setupOfHomeBannerVC];
//    [self setupCustomCarousel];
    
    [self loadHomeConfigDataFromServer];

    [self takeScrollViewAtBottom];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.bottomConstraint.constant = 0.0;
}

-(void)setupResumeWatchingCarousel
{
    if ([StaticData isUserLogins]) {
        [self setupOfResumeWatchingVC];
    }
}

-(void)takeScrollViewAtBottom
{
    [UIView animateWithDuration:0.5
    animations:^{
        CGRect frame = self.view.frame;
        CGFloat topInsets = frame.size.height - 250.0;
        self.scrollTopConstraint.constant = topInsets;
        [self.scroll_view setContentOffset:CGPointZero animated:YES];
        //[self.view layoutIfNeeded]; // Called on parent view
    } completion:^(BOOL finished) {
        self.scroll_view.hidden = false;
        //[self.scroll_view setContentOffset:CGPointZero animated:YES];
        [self updateScrollViewContentSize];
    }];
}

-(void)setupActiveChannelShowInfo:(MediaInfo *)show
{
//    if (show.isShow || show.isVideo) {
//        [self setupFeatureShowInfo:show];
//        return;
//    }
    
    if (show.pageType != 1) {
        [self takeScrollViewAtTop];
        return;
    }
    
    if (show)
    {
        [self takeScrollViewAtBottom];
        
        [self removeAllChildPlayerFromTvDisplayBeforeAddingNewOne:^(BOOL isCompleted)
        {
            self.activeShow = show;
            
            [self setupFeatureShowInfo:show];
        }];
    }
}

-(void)takeScrollViewAtTop
{
    if (self.scrollTopConstraint.constant > 140 /*|| self.img_homeBgPattren.hidden*/)
    {
        self.scroll_view.scrollEnabled = true;
        [UIView animateWithDuration:0.5
        animations:^{
            self.scrollTopConstraint.constant = 140.0;
            [self.view layoutIfNeeded];
            
            /*UIImage * toImage = self.img_homeBgPattren.image;
            [UIView transitionWithView:self.img_homeBgPattren
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                              self.liveChannelStreaming.img_channelPreview.image = toImage;
                self.img_homeBgPattren.image = toImage;
            } completion:^(BOOL finished) {
            }];*/
            
        } completion:^(BOOL finished) {
            [self removeAllChildOnTVDisplay];
            [self updateScrollViewContentSize];
        }];
    }
}

-(void)removeAllChildPlayerFromTvDisplayBeforeAddingNewOne:(AnimationCompletionBlock)completedBlock
{
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL isAnyChildFound = false;
        
        if (self.vc_featureShowsInfo){
            isAnyChildFound = true;
            completedBlock(true);
        }
        
        if (!isAnyChildFound) {
            completedBlock(true);
        }
    });
}

-(void)setupFeatureShowInfo:(MediaInfo *)info
{
    if (self.vc_featureShowsInfo) {
        [self.vc_featureShowsInfo setupShowDetails:info];
        return;
    }
    
    //[self removeAllChildOnTVDisplay];
    
//    if (![self.vc_featureShows.selectedChannel.ID isEqualToString:info.ID]) {
//        info = self.vc_featureShows.selectedChannel;
//    }
    
    self.vc_featureShowsInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"FeaturedShowsHomeInfo"];
    self.vc_featureShowsInfo.isPlayerPause = false;
    [self addChildViewController:self.vc_featureShowsInfo];
    
    CGRect frame = self.view.frame;
    
    self.vc_featureShowsInfo.view.frame = frame;
    
    [self.view_tvDisplay addSubview:self.vc_featureShowsInfo.view];
    [self.vc_featureShowsInfo didMoveToParentViewController:self];
    
    [self.vc_featureShowsInfo setupShowDetails:info];
    
    __weak typeof(self) weakSelf = self;
}

-(void)removeAllChildOnTVDisplay
{
    @try {
        if (self.view_tvDisplay.subviews.count > 0) {
            [self.view_tvDisplay.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
        
        NSArray *viewsToRemove = [self.view_tvDisplay subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }
        
        [self.vc_featureShowsInfo willMoveToParentViewController:nil];
        [self.vc_featureShowsInfo removeFromParentViewController];
        [self.vc_featureShowsInfo.view removeFromSuperview];
        self.vc_featureShowsInfo = nil;
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)setupOfHomeBannerVC
{
    self.vc_homeBanner = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeBanner"];
    CGRect frame = self.vc_homeBanner.view.frame;
    CGFloat height = 700;
    
    [self addChildViewController:self.vc_homeBanner];
    
    [self.vc_homeBanner.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeBanner.view];
    
    [self.vc_homeBanner didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeBanner.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeBanner.HomeBannerSwipeUpCallBack = ^{
    };
    
    self.vc_homeBanner.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_homeBanner.HomeBannerSwipeUpCallBack = ^{
        weakSelf.isSwipeUpCallOnHomeBanner = true;
        [weakSelf setNeedsFocusUpdate];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //weakSelf.vc_homeBanner.btn_openShow.userInteractionEnabled = true;
        });
    };
    
    self.vc_homeBanner.FocuseUpdateCallBack = ^(MediaInfo *info) {
        [weakSelf setupActiveChannelShowInfo:info];
        [weakSelf.indicator stopAnimating];
        
        weakSelf.scroll_view.scrollEnabled = false;
        [weakSelf takeScrollViewAtBottom];
    };
    
    self.vc_homeBanner.LoadFirstItemCallBack = ^(MediaInfo * _Nonnull info) {
        [weakSelf setupActiveChannelShowInfo:info];
        [weakSelf.indicator stopAnimating];
        
        [weakSelf takeScrollViewAtBottom];
        
        [weakSelf performSelector:@selector(updateFocus) withObject:nil afterDelay:0.5];
    };
    
    self.vc_homeBanner.FeaturedShowsNotFound = ^{
        weakSelf.featuredViewHeightAnchor.constant = 0.0;
        [weakSelf updateScrollViewContentSize];
        [weakSelf.indicator stopAnimating];
    };
}

-(void)updateFocus
{
    [self preferredFocusEnvironments];
    [self updateFocusIfNeeded];
}

-(void)setupNowOnTVController
{
    self.vc_nowOnTVVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NowOnTVVC"];
    self.vc_nowOnTVVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_nowOnTVVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_nowOnTVVC];
    
    [self.vc_nowOnTVVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_nowOnTVVC.view];
    
    [self.vc_nowOnTVVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_nowOnTVVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_nowOnTVVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_nowOnTVVC.AudioClickCallBack = ^{
        weakSelf.bottomConstraint.constant = 100.0;
    };
    
    self.vc_nowOnTVVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupAudioBooksTabController {
    self.vc_homeAudioBooksTabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeAudioBooksTabVC"];
    
    CGRect frame = self.vc_homeAudioBooksTabVC.view.frame;
    CGFloat height = 180;
    
    [self addChildViewController:self.vc_homeAudioBooksTabVC];
    
    [self.vc_homeAudioBooksTabVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeAudioBooksTabVC.view];
    
    [self.vc_homeAudioBooksTabVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeAudioBooksTabVC.heightConstraint = heightConstraint;
//        weakSelf.vc_homeAudioBooksTabVC.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeAudioBooksTabVC.UpdateContentSize = ^{
        [weakSelf performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };
    
    self.vc_homeAudioBooksTabVC.ChooseAudioBookCallBack = ^(MediaInfo *info) {
        if ([info.title_ar isEqualToString:@"الكل"]) {
            weakSelf.tabBarController.selectedIndex = 2;
        }
        else {
            [weakSelf.vc_homeAudioBooksVC loadDataFromServer:info.audioChannelPath];
        }
    };
    
//    self.vc_homeAudioBooksTabVC.UpdateContentSize = ^{
//       [weakSelf updateScrollViewContentSize];
//    };
    
//    self.vc_homeAudioBooksTabVC.ChooseAudioBookCallBack = ^{
//        weakSelf.bottomConstraint.constant = 100.0;
//    };
    
    self.vc_homeAudioBooksTabVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupAudioBooksController {
    self.vc_homeAudioBooksVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeAudioBooksVC"];
    
    CGRect frame = self.vc_homeAudioBooksVC.view.frame;
    CGFloat height = 300;
    
    [self addChildViewController:self.vc_homeAudioBooksVC];
    
    [self.vc_homeAudioBooksVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeAudioBooksVC.view];
    
    [self.vc_homeAudioBooksVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeAudioBooksVC.heightConstraint = heightConstraint;
//        weakSelf.vc_homeAudioBooksVC.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeAudioBooksVC.UpdateContentSize = ^{
        [weakSelf performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };
    
    self.vc_homeAudioBooksVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupRamadanController
{
    self.vc_homeRamadanVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeRamadanVC"];
    self.vc_homeRamadanVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_homeRamadanVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_homeRamadanVC];
    
    [self.vc_homeRamadanVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeRamadanVC.view];
    
    [self.vc_homeRamadanVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeRamadanVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeRamadanVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_homeRamadanVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupProgramsController
{
    self.vc_homeProgramsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeProgramsVC"];
    self.vc_homeProgramsVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_homeProgramsVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_homeProgramsVC];
    
    [self.vc_homeProgramsVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeProgramsVC.view];
    
    [self.vc_homeProgramsVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeProgramsVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeProgramsVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_homeProgramsVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupLatestPodcastsController
{
    self.vc_latestPodcastsVC = [self.storyboard instantiateViewControllerWithIdentifier:isEnglishLang ? @"LatestPodcastsVC_EN" : @"LatestPodcastsVC_AR"];
    self.vc_latestPodcastsVC.isRadio = self.isHomeRadio;
    
    CGRect frame = self.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_latestPodcastsVC];
    
    [self.vc_latestPodcastsVC.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_latestPodcastsVC.view];
    
    [self.vc_latestPodcastsVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeProgramsVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_latestPodcastsVC.UpdateContentSize = ^{
        [weakSelf performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };
}

-(void)setupOfLatestEpisodesVC
{
    self.vc_latestEpisodesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LatestEpisodesVC"];
    CGRect frame = self.vc_latestEpisodesVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_latestEpisodesVC];
    
    [self.vc_latestEpisodesVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_latestEpisodesVC.view];
    
    [self.vc_latestEpisodesVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_latestEpisodesVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_latestEpisodesVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_latestEpisodesVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupOfRecomendationShowsVC
{
    self.vc_recomendtationShowsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RecomendtationShowsVC"];
    self.vc_recomendtationShowsVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_recomendtationShowsVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_recomendtationShowsVC];
    
    [self.vc_recomendtationShowsVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_recomendtationShowsVC.view];
    
    [self.vc_recomendtationShowsVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_recomendtationShowsVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_recomendtationShowsVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_recomendtationShowsVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupMostWatchedController
{
    self.vc_mostWatchedVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MostWatchedVC"];
    self.vc_mostWatchedVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_mostWatchedVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_mostWatchedVC];
    
    [self.vc_mostWatchedVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_mostWatchedVC.view];
    
    [self.vc_mostWatchedVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_mostWatchedVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_mostWatchedVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_mostWatchedVC.AudioClickCallBack = ^{
        weakSelf.bottomConstraint.constant = 100.0;
    };
    
    self.vc_mostWatchedVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupOfResumeWatchingVC
{
    self.vc_resumeWatchingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ResumeWatchingVC"];
    CGRect frame = self.vc_resumeWatchingVC.view.frame;
    CGFloat height = 500;
    
    [self addChildViewController:self.vc_resumeWatchingVC];
    
    [self.vc_resumeWatchingVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_resumeWatchingVC.view];
    
    [self.vc_resumeWatchingVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:30.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_resumeWatchingVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_resumeWatchingVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_resumeWatchingVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupChannelsTypeTabController
{
    self.vc_homeChannelsTypeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeChannelsTypeVC"];
    self.vc_homeChannelsTypeVC.isRadio = self.isHomeRadio;
    CGRect frame = self.vc_homeChannelsTypeVC.view.frame;
    CGFloat height = 150;
    [self addChildViewController:self.vc_homeChannelsTypeVC];
    
    [self.vc_homeChannelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_homeChannelsTypeVC.view];
    
    [self.vc_homeChannelsTypeVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:30.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeChannelsTypeVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_homeChannelsTypeVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
    
    self.vc_homeChannelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info)
    {
        [weakSelf removeControllerOnTabClicked];
        
        weakSelf.isHomeRadio = false;
        if (isRadio) {
            weakSelf.isHomeRadio = true;
        }
        
        [weakSelf loadHomeConfigDataFromServer];
    };
    
    self.vc_homeChannelsTypeVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)setupLiveChannelsController
{
    self.vc_homeLiveVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeLiveVC"];
    self.vc_homeLiveVC.isRadio = self.isHomeRadio;
    CGRect frame
    = self.vc_homeLiveVC.view.frame;
    CGFloat height = 500;

    [self addChildViewController:self.vc_homeLiveVC];

    [self.vc_homeLiveVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];

    [self.view_content addSubview:self.vc_homeLiveVC.view];

    [self.vc_homeLiveVC didMoveToParentViewController:self];

    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        weakSelf.vc_homeLiveVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];

    self.vc_homeLiveVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };

    self.vc_homeLiveVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info)
    {
        [weakSelf removeControllerOnTabClicked];

        weakSelf.isHomeRadio = false;
        if (isRadio) {
            weakSelf.isHomeRadio = true;
        }

        [weakSelf loadHomeConfigDataFromServer];

        return;

        if (weakSelf.vc_nowOnTVVC)
        {
            weakSelf.vc_nowOnTVVC.isRadio = isRadio;
            [weakSelf.vc_nowOnTVVC loadDataFromServer];
        }

        if (isRadio) {
            self.isHomeRadio = true;
            if (weakSelf.vc_homeProgramsVC) {
                weakSelf.vc_homeProgramsVC.isRadio = true;
                [weakSelf.vc_homeProgramsVC loadTVAndRadioDataFromServer];
            }
        } else {
            if (weakSelf.vc_homeProgramsVC) {
                weakSelf.vc_homeProgramsVC.isRadio = false;
                [weakSelf.vc_homeProgramsVC loadTVAndRadioDataFromServer];
            }
        }

        if (isRadio) {
            if (weakSelf.vc_homeRamadanVC) {
                weakSelf.vc_homeRamadanVC.isRadio = true;
                [weakSelf.vc_homeRamadanVC loadTVAndRadioDataFromServer];
            }
        } else {
            if (weakSelf.vc_homeRamadanVC) {
                weakSelf.vc_homeRamadanVC.isRadio = false;
                [weakSelf.vc_homeRamadanVC loadTVAndRadioDataFromServer];
            }
        }

        if (isRadio) {

            if (weakSelf.vc_mostWatchedVC) {
                weakSelf.vc_mostWatchedVC.isRadio = true;
                [weakSelf.vc_mostWatchedVC loadTVAndRadioMostWatched];
            }
        } else {
            if (weakSelf.vc_mostWatchedVC) {
                weakSelf.vc_mostWatchedVC.isRadio = false;
                [weakSelf.vc_mostWatchedVC loadTVAndRadioMostWatched];
            }
        }

        if (isRadio) {

            if (weakSelf.vc_recomendtationShowsVC) {
                weakSelf.vc_recomendtationShowsVC.isRadio = true;
                [weakSelf.vc_recomendtationShowsVC loadTVAndRadioDataFromServer];
            }
        } else {
            if (weakSelf.vc_recomendtationShowsVC) {
                weakSelf.vc_recomendtationShowsVC.isRadio = false;
                [weakSelf.vc_recomendtationShowsVC loadTVAndRadioDataFromServer];
            }
        }

        if (isRadio) {
            if (weakSelf.vc_resumeWatchingVC) {
                weakSelf.vc_resumeWatchingVC.isRadio = true;
                weakSelf.vc_resumeWatchingVC.view.hidden = true;
                weakSelf.vc_resumeWatchingVC.heightConstraint.constant = 0.0;
                weakSelf.vc_resumeWatchingVC.topConstraint.constant = 0.0;
            }
        } else {
            if (weakSelf.vc_resumeWatchingVC) {
                weakSelf.vc_resumeWatchingVC.isRadio = false;
                weakSelf.vc_resumeWatchingVC.view.hidden = false;
                weakSelf.vc_resumeWatchingVC.heightConstraint.constant = 500;
                weakSelf.vc_resumeWatchingVC.topConstraint.constant = 10.0;
                [weakSelf.vc_resumeWatchingVC getcontentList];
            }
        }
    };

    self.vc_homeLiveVC.FocuseUpdateCallBack = ^{
        [weakSelf takeScrollViewAtTop];
    };
}

-(void)removeControllerOnTabClicked
{
    for (UIView *v in self.view_content.subviews)
    {
        [v removeFromSuperview];
    }
    
    [self.scroll_view setContentSize:CGSizeMake(self.scroll_view.frame.size.width, 0)];
}

-(void)updateScrollViewContentSize
{
    [self viewDidLayoutSubviews];
    [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
}

-(void)viewDidLayoutSubviews
{
    @try {
        UIView *view = [[[self.scroll_view.subviews objectAtIndex:0] subviews] lastObject];
        CGFloat contentSize = [view frame].origin.y + [view frame].size.height + 50;
        [self.scroll_view setContentSize:CGSizeMake(self.scroll_view.frame.size.width, contentSize)];
        //self.contentViewBottomConstraint.constant = contentSize;
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    } @finally {
        
    }
}

-(CGFloat)yOfLastChildFromScrollView
{
    @try {
        UIView *subView = [[self.scroll_view.subviews[0] subviews] lastObject];
        CGFloat yOfLastObject = 0;
        
        if (subView) {
            //yOfLastObject = subView.frame.size.width + subView.frame.origin.y;
        }
        return yOfLastObject;
    } @catch (NSException *exception) {
    } @finally {
    }
    return 0.0;
}

-(void)addConstraingOnScrollChilds:(UIView *)parent withFixedHeight:(CGFloat)fixedheight withTop:(CGFloat)topY completed:(HomeConstraintCompletionBlock)completedBlock
{
    @try {
        NSArray *childs = [self.scroll_view.subviews[0] subviews];
        UIView *subView = [childs lastObject];
        UIView *secondLastSubView = nil;
        if (childs.count > 1) {
            secondLastSubView = childs[childs.count-2];
        }
        
        
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Trailing
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Leading
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Bottom
        NSLayoutConstraint *top =[NSLayoutConstraint
                                  constraintWithItem:subView
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:parent
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:topY];
        //Height to be fixed for SubView same as AdHeight
        NSLayoutConstraint *height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeHeight
                                      multiplier:0
                                      constant:fixedheight];
        
        //Add constraints to the Parent
        [parent addConstraint:trailing];
        if (secondLastSubView) {
            top =[NSLayoutConstraint
                  constraintWithItem:subView
                  attribute:NSLayoutAttributeTop
                  relatedBy:NSLayoutRelationEqual
                  toItem:secondLastSubView
                  attribute:NSLayoutAttributeBottom
                  multiplier:1
                  constant:topY];
            [parent addConstraint:top];
        } else {
            [parent addConstraint:top];
        }
        [parent addConstraint:leading];
        
        //Add height constraint to the subview, as subview owns it.
        [subView addConstraint:height];
        
        completedBlock(height,top);
    } @catch (NSException *exception) {
    } @finally {
    }
    
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}

-(void)loadHomeConfigDataFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=opt&action=home_config",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"HomeConfig"];
}

//-(void)setupCustomCarousel
//{
//    [self.indicator startAnimating];
//
//    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&p=1&limit=10&scope=admc&action=custom_widgets_carousels&type=widgets_1",BaseURL,BaseUID,BaseKEY];
//    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"CustomCarousel"];
//}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"HomeConfig"])
        {
            [self removeControllerOnTabClicked];
            
            [self setupOfHomeBannerVC];
            
            [self setNeedsFocusUpdate];
            
            self.homeConfig = responseObject;
            for (NSDictionary *value in responseObject[@"data"][@"value"])
            {
                if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"audiobooks"])
                {
                    [self setupAudioBooksTabController];
                    [self setupAudioBooksController];
                }
                if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"radio_tv_tab"])
                {
//                    [self setupChannelsTypeTabController];
                    for (NSDictionary *data in value[@"data"])
                    {
                        if(self.isHomeRadio)
                        {
                            for (NSString *key in data[@"radio"])
                            {
                                if ([key isEqualToString:@"radio_live_channel"])
                                {
                                    [self setupLiveChannelsController];
                                } else if ([key isEqualToString:@"latest_audios"])
                                {
                                    [self setupNowOnTVController];
                                }
                                if ([key isEqualToString:@"most_listened"])
                                {
                                    [self setupMostWatchedController];
                                }
                                if ([key isEqualToString:@"ramadan_programs"])
                                {
                                    [self setupRamadanController];
                                } else if ([key isEqualToString:@"radio_programs"])
                                {
                                    [self setupProgramsController];
                                } if ([key isEqualToString:@"latest_podcasts_of_ayn"])
                                {
                                    [self setupLatestPodcastsController];
                                } else if ([key isEqualToString:@"radio_show_recommendation"]) {
                                    if ([StaticData isUserLogins]) {
                                        [self setupOfRecomendationShowsVC];
                                    }
                                }
                            }
                        } else
                        {
                            for (NSString *key in data[@"tv"])
                            {
//                                if ([key isEqualToString:@"ramadan_programs"])
//                                {
//                                    [self setupRamadanController];
//                                } else if ([key isEqualToString:@"tv_programs"])
//                                {
//                                    [self setupProgramsController];
//                                } else if ([key isEqualToString:@"tv_live_channel"])
//                                {
//                                    [self setupLiveChannelsController];
//                                } else if ([key isEqualToString:@"latest_videos"])
//                                {
//                                    [self setupNowOnTVController];
//                                } else if ([key isEqualToString:@"most_watched"]) {
//                                    [self setupMostWatchedController];
//                                } else if ([key isEqualToString:@"custom_carousel"])
//                                {
//                                    [self setupCustomCarousel];
//                                    return;
//                                } else if ([key isEqualToString:@"tv_show_recommendation"]) {
//                                    if ([StaticData isUserLogins]) {
//                                        [self setupOfRecomendationShowsVC];
//                                    }
//                                }
                                
                                if ([key isEqualToString:@"custom_carousel"])
                                {
                                    [self setupCustomCarousel];
                                    return;
                                }
                            }
                        }
                    }
                }
                
                if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"resume_watching"])
                {
                    if(!self.isHomeRadio)
                    {
                        [self setupResumeWatchingCarousel];
                    }
                }
            }
        } else if ([callType isEqualToString:@"CustomCarousel"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                //[self setupOfCustomCarouselWithCarouselInfo:dicObj];
            }
            
            [self continueOrderingAfterCustomCarousel];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)continueOrderingAfterCustomCarousel
{
    BOOL isRadioTvTabFound = false;
    for (NSDictionary *value in self.homeConfig[@"data"][@"value"])
    {
        if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"radio_tv_tab"])
        {
            isRadioTvTabFound = true;
            for (NSDictionary *data in value[@"data"])
            {
                BOOL isFindCustomCarousel = false;
                for (NSString *key in data[@"tv"])
                {
                    if ([key isEqualToString:@"custom_carousel"])
                    {
                        isFindCustomCarousel = true;
                    }
                    
                    if (isFindCustomCarousel)
                    {
                        if ([key isEqualToString:@"ramadan_programs"])
                        {
                            [self setupRamadanController];
                        } else if ([key isEqualToString:@"tv_programs"])
                        {
                            [self setupProgramsController];
                        } else if ([key isEqualToString:@"tv_live_channel"])
                        {
                            //[self setupLiveChannelsController];
                        } else if ([key isEqualToString:@"latest_videos"])
                        {
                            [self setupNowOnTVController];
                        } else if ([key isEqualToString:@"most_watched"]) {
                            [self setupMostWatchedController];
                        } else if ([key isEqualToString:@"tv_show_recommendation"]) {
                            if ([StaticData isUserLogins]) {
                                [self setupOfRecomendationShowsVC];
                            }
                        }
                    }
                }
            }
        }
        
        if (isRadioTvTabFound)
        {
            if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"audiobooks"])
            {
                [self setupAudioBooksTabController];
                [self setupAudioBooksController];
            }
            
            if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"resume_watching"])
            {
                if(!self.isHomeRadio)
                {
                    [self setupResumeWatchingCarousel];
                }
            }
        }
    }
}

-(void)setupCustomCarousel
{
    SetupCustomCarousels *customCarousels = [SetupCustomCarousels new];
    [customCarousels loadCustomCarousels];

    customCarousels.CustomCarouselDataReceivedCallBack = ^(NSMutableArray  *data, NSString *title,NSDictionary *crouselData) {
        [self setupCustomCarouselController:data withTitle:title withCarouselData:crouselData];
    };

    customCarousels.LoadApiCustomCarouselCallBack = ^(NSDictionary * _Nonnull crouselData)
    {
        NSString *type = [StaticData checkStringNull:crouselData[@"type"]];
        if ([type isEqualToString:@"recommendation_module"])
        {
            if ([StaticData isUserLogins])
            {
                [self setupRecomendedCustomCarousel:crouselData];
            }
        } else
        {
            [self loadApiCustomCarouselController:crouselData];
        }
    };
    
    customCarousels.CustomCarouselFinishedCallBack = ^{
        
//        for (NSDictionary *value in self.homeConfig[@"data"][@"value"])
//        {
//            if ([[StaticData checkStringNull:value[@"section"]] isEqualToString:@"audiobooks"])
//            {
//                [self setupAudioBooksTabController];
//                [self setupAudioBooksController];
//            }
//        }
        
        [self setupAudioBooksTabController];
        [self setupAudioBooksController];
    };
}

-(void)setupCustomCarouselController:(NSMutableArray *)data withTitle:(NSString *)title withCarouselData:(NSDictionary *)crouselData
{
    if (data.count == 0) return;
    
    CustomCarouselsVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomCarouselsVC"];
    controller.carouselInfo = crouselData;
    [self addChildViewController:controller];
    [controller.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], self.scroll_view.frame.size.width, CarouselHeight)];
    [self.view_content addSubview:controller.view];
    [controller didMoveToParentViewController:self];
    
    NSString *type = [StaticData checkStringNull:crouselData[@"type"]];
    if ([type isEqualToString:@"original_content"])
    {
        controller.img_orignalLogo.image =  [UIImage imageNamed:@"adtv_original_ar"];
        controller.lbl_title.text = @"";
        controller.lbl_title2.text = @"";
    } else {
        controller.lbl_title.text = title;
    }
    
    controller.contentList = data;
    [StaticData reloadCollectionView:controller.cv_videos];

    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:CarouselHeight withTop:TopCarouselSpace completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        controller.heightConstraint = heightConstraint;
        controller.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];

    controller.UpdateScrollContentSizeCallBlock = ^{
        [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };
    
    controller.FocuseUpdateCallBack = ^(MediaInfo * _Nonnull info)
    {
        [weakSelf takeScrollViewAtTop];
        //[weakSelf getCustomCarouselFocus:controller withInfo:info];
    };
}

-(void)loadApiCustomCarouselController:(NSDictionary *)carouselData
{
    CustomCarouselsVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomCarouselsVC"];

    [self addChildViewController:controller];
    [controller.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], self.scroll_view.frame.size.width, CarouselHeight)];
    [self.view_content addSubview:controller.view];
    [controller didMoveToParentViewController:self];

    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:CarouselHeight withTop:TopCarouselSpace completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        controller.heightConstraint = heightConstraint;
        controller.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];

    controller.UpdateScrollContentSizeCallBlock = ^{
        [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };

    controller.FocuseUpdateCallBack = ^(MediaInfo * _Nonnull info){
        [weakSelf takeScrollViewAtTop];
        //[weakSelf getCustomCarouselFocus:controller withInfo:info];
    };
    [controller loadCustomCarouselsData:carouselData];
}

-(void)setupRecomendedCustomCarousel:(id)jsonResponse
{
    SetupCustomCarousels *customCarousels = [SetupCustomCarousels new];
    [customCarousels loadRecomendedCarouselsData:jsonResponse];

    CustomCarouselsVC *recomded1 = [self loadRecomendedCustomCarouselController];
    CustomCarouselsVC *recomded2 = [self loadRecomendedCustomCarouselController];
    CustomCarouselsVC *recomded3 = [self loadRecomendedCustomCarouselController];

    customCarousels.LoadRecomendedCustomCarouselCallBack = ^(NSMutableArray * _Nonnull data, NSString * _Nonnull title, NSDictionary * _Nonnull crouselData,NSString * _Nonnull carouselType)
    {
        if ([carouselType isEqualToString:@"recent_favorited"])
        {
            if (data.count > 0)
            {
                recomded1.view.hidden = false;
                recomded1.heightConstraint.constant = CarouselHeight;
                recomded1.lbl_title.text = title;
                recomded1.contentList = data;
                [recomded1 reloadCollectionData];
            } else
            {
                recomded1.heightConstraint.constant = 0.0;
            }

        } else if ([carouselType isEqualToString:@"recent_viewed"])
        {
            if (data.count > 0)
            {
                recomded2.view.hidden = false;
                recomded2.heightConstraint.constant = CarouselHeight;
                recomded2.lbl_title.text = title;
                recomded2.contentList = data;
                [recomded2 reloadCollectionData];
            } else
            {
                recomded1.heightConstraint.constant = 0.0;
            }
        } else if ([carouselType isEqualToString:@"most_viewed"])
        {
            if (data.count > 0)
            {
                recomded3.view.hidden = false;
                recomded3.heightConstraint.constant = CarouselHeight;
                recomded3.lbl_title.text = title;
                recomded3.contentList = data;
                [recomded3 reloadCollectionData];
            } else
            {
                recomded1.heightConstraint.constant = 0.0;
            }
        }
    };
}

-(CustomCarouselsVC *)loadRecomendedCustomCarouselController
{
    CustomCarouselsVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomCarouselsVC"];
    [self addChildViewController:controller];
    [controller.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], self.scroll_view.frame.size.width, CarouselHeight)];
    controller.view.hidden = true;
    [self.view_content addSubview:controller.view];
    [controller didMoveToParentViewController:self];

    //controller.lbl_title.text = title;
    //controller.contentList = data;
    [StaticData reloadCollectionView:controller.cv_videos];

    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:0.0 withTop:0 completed:^(NSLayoutConstraint *heightConstraint,NSLayoutConstraint *topConstraint) {
        controller.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];

    controller.UpdateScrollContentSizeCallBlock = ^{
        [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
        [weakSelf viewDidLayoutSubviews];
    };
    
    controller.FocuseUpdateCallBack = ^(MediaInfo * _Nonnull info) {
        [weakSelf takeScrollViewAtTop];
        //[weakSelf getCustomCarouselFocus:controller withInfo:info];
        
    };
    return controller;
}

//-(void)setupOfCustomCarouselWithCarouselInfo:(id)carouselInfo
//{
//    CustomCarouselVC *vc_customCarouselVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomCarouselVC"];
//    vc_customCarouselVC.carouselInfo = carouselInfo;
//    CGRect frame = self.vc_latestEpisodesVC.view.frame;
//    CGFloat height = 400;
//
//    [self addChildViewController:vc_customCarouselVC];
//
//    [vc_customCarouselVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
//
//    [self.view_content addSubview:vc_customCarouselVC.view];
//
//    [vc_customCarouselVC didMoveToParentViewController:self];
//
//    __weak typeof(self) weakSelf = self;
//    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:20.0  completed:^(NSLayoutConstraint *heightConstraint) {
//        vc_customCarouselVC.heightConstraint = heightConstraint;
//        [weakSelf viewDidLayoutSubviews];
//    }];
//
//    vc_customCarouselVC.UpdateContentSize = ^{
//       [weakSelf updateScrollViewContentSize];
//    };
//
//    [self.customCarouselListVC addObject:vc_customCarouselVC];
//}

- (UIViewController *)getParentController:(UIView *)sbView {
    UIResponder *responder = [sbView nextResponder];
    while (responder != nil) {
        if ([responder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)responder;
        }
        responder = [responder nextResponder];
    }
    return nil;
}
@end
