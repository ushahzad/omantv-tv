//
//  HomeLiveVC.h
//  OmanTV
//
//  Created by Curiologix on 29/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeLiveVC : UIViewController
<UICollectionViewDelegate,UICollectionViewDataSource>
@property void (^ChoosedChannelTypeCallBack)(BOOL isRadio,MediaInfo*info);
@property void(^UpdateContentSize)(void);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property(strong,nonatomic)IBOutlet UILabel *lbl_title;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) int isRadio;
-(void)loadDataFromServer;
@end

NS_ASSUME_NONNULL_END
