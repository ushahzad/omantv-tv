//
//  ChannelsTypeVC.h
//  OmanTV
//
//  Created by Ali Raza on 17/01/2022.
//  Copyright © 2022 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeChannelsTypeVC : UIViewController
@property void (^ChoosedChannelTypeCallBack)(BOOL isRadio,MediaInfo*info);
@property void(^FocuseUpdateCallBack)(void);
@property void(^UpdateContentSize)(void);
@property (nonatomic, retain) ChannelsTypeVC *vc_channelsTypeVC;
@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, assign) BOOL isRadio;
@end

NS_ASSUME_NONNULL_END
