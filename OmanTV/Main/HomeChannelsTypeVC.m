//
//  ChannelsTypeVC.m
//  OmanTV
//
//  Created by Ali Raza on 17/01/2022.
//  Copyright © 2022 MacUser. All rights reserved.
//

#import "HomeChannelsTypeVC.h"

@interface HomeChannelsTypeVC ()

@end

@implementation HomeChannelsTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupChannelsTypesController];
}

-(void)setupChannelsTypesController
{
    self.vc_channelsTypeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    self.vc_channelsTypeVC.isComeFromHomePage = true;
    CGRect frame = self.view.frame;
    CGFloat height = 100;
    
    [self addChildViewController:self.vc_channelsTypeVC];
    
    [self.vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_channelsTypes addSubview:self.vc_channelsTypeVC.view];
    
    [self.vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:self.vc_channelsTypeVC.view];
    
    self.vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info) {
        if (weakSelf.ChoosedChannelTypeCallBack)
        {
            weakSelf.ChoosedChannelTypeCallBack(isRadio, info);
        }
        
    };
    
    self.vc_channelsTypeVC.FocuseUpdateCallBack = ^{
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    };
    
    if (self.isRadio) {
        [self.vc_channelsTypeVC setRadioSelection];
    }
}

@end
