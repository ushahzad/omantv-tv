//
//  MostWatchedVC.h
//  OmanTV
//
//  Created by MacUser on 12/05/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MostWatchedVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^AudioClickCallBack)(void);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property(strong,nonatomic)IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
-(void)loadTVAndRadioMostWatched;

@end

NS_ASSUME_NONNULL_END
