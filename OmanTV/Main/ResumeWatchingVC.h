//
//  ResumeWatchingVC.h
//  OmanTV
//
//  Created by Curiologix on 11/01/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResumeWatchingVC : UIViewController
<UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateScrollContentSizeCallBlock)(void);
@property void(^UpdateContentSize)(void);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic, assign) BOOL isCallBackCalled;
-(void)reloadCollectionData;
-(void)getcontentList;
@end

NS_ASSUME_NONNULL_END
