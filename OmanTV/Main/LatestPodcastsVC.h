//
//  LatestPodcastsVC.h
//  OmanTV
//
//  Created by Curiologix on 19/04/2022.
//  Copyright © 2022 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LatestPodcastsVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^FocuseUpdateCallBack)(void);
@property void(^AudioClickCallBack)(void);

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UIImageView *img_exclusive;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *allShowsList;
@property (nonatomic, retain) NSMutableArray *channelsList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
-(void)loadDataFromServer;
-(void)loadTVAndRadioDataFromServer;

@end

NS_ASSUME_NONNULL_END
