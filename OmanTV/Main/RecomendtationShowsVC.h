//
//  RecomendtationShowsVC.h
//  OmanTV
//
//  Created by Curiologix on 18/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecomendtationShowsVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *allShowsList;
@property (nonatomic, retain) NSMutableArray *channelsList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
-(void)loadTVAndRadioDataFromServer;

@end

NS_ASSUME_NONNULL_END
