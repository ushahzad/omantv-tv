//
//  LatestEpisodesVC.h
//  OmanTV
//
//  Created by Curiologix on 25/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LatestEpisodesVC : UIViewController
<UICollectionViewDelegate, UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^HomeBannerSwipeUpCallBack)(void);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) UIImage *tempImage;
-(void)loadDataFromServer;
@end

NS_ASSUME_NONNULL_END
