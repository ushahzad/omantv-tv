//
//  HomeLiveVC.m
//  OmanTV
//
//  Created by Curiologix on 29/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "HomeLiveVC.h"
#import "LiveChannelPlayer.h"

@interface HomeLiveVC ()

@end

@implementation HomeLiveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentList = [NSMutableArray new];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadDataFromServer];
    
    self.lbl_title.text = @"البث المباشر";
}


-(void)loadDataFromServer
{
    [self resetChannels];
    
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/plus/live_channels?user_id=%@&key=%@&json=1&is_radio=%d&mixed=yes&info=1&mplayer=true&need_live=yes&need_next=yes&next_limit=2&need_playback=yes",BaseURL,BaseUID,BaseKEY,self.isRadio];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"LiveChannels"];
}

-(void)resetChannels
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"LiveChannels"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if (dicObj[@"catchup"] && [dicObj[@"catchup"] intValue] == 0)
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"id"]];
                
                info.thumbnail = [StaticData checkStringNull:dicObj[@"icon"]];
                if (info.thumbnail.length == 0)
                {
                    info.thumbnail = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                NSArray *live = [StaticData checkArrayNull:dicObj[@"live"]];
                for (NSDictionary *dic in live)
                {
                    info.thumbnailPath = [StaticData checkStringNull:dic[@"img"]];
                    info.title = [StaticData checkStringNull:dic[@"title"]];
                }
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"cover"]];
                }
                
                info.streamingPlaybackUrl = [StaticData checkStringNull:dicObj[@"playbackURL"]];
                
                info.isRadio = self.isRadio;
                info.title_ar = [StaticData checkStringNull:dicObj[@"title_ar"]];
                
                if ([StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData checkIsScueduleGeoBlockOnLiveChannel:dicObj])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData isLiveChannelDigitalRightsDisable:dicObj])
                {
                    info.isLiveShowDigitalRightDisable = true;
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(void)checkRecordFound
{
    if (self.contentList.count == 0) {
        self.heightConstraint.constant = 0.0;
    }
    
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 112)/4.5;
    CGFloat height = 300;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    
    _heightConstraint.constant = height+80;
    if (self.UpdateContentSize)
    {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
 
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];

    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    @try {
        NSString *encodedCover = [info.thumbnail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.img_highlight.contentMode = UIViewContentModeScaleAspectFill;
        [cell.img_highlight sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 /*if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }*/
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.img_highlight withUrl:imageURL completed:^(UIImage *image) {
                     /*if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }*/
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    //[StaticData blurEffectsOnView:cell.view_blur withEffectStyle:UIBlurEffectStyleLight];
    
    cell.view_blur.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    cell.img_highlight.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
    
    cell.view_blur.layer.cornerRadius = 12.0;
    cell.view_blur.clipsToBounds = true;
    
    cell.img_highlight.layer.cornerRadius = 12.0;
    cell.img_highlight.clipsToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    [self loadChannelScreenBySelectedChannel:info];
    
    LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
    vc.selectedChannel = info;
//    vc.isRadio = self.isRadio;
    [self.navigationController pushViewController:vc animated:true];
}

-(void)loadChannelScreenBySelectedChannel:(MediaInfo *)info
{
    UIViewController *parent = [self findParentViewController];
}

- (UIViewController*)findParentViewController
{
    for (UIView* next = [self.view superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController*)nextResponder;
        }
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [cell.lbl_titleMarquee pauseLabel];
            cell.view_info.hidden = true;
            cell.layer.borderColor = [UIColor clearColor].CGColor;
            cell.layer.borderWidth = 0.0;
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            [cell.lbl_titleMarquee unpauseLabel];
            cell.view_info.hidden = true;
            
            cell.clipsToBounds = true;
            cell.layer.borderColor = AppColor.CGColor;
            cell.layer.borderWidth = CellBorderWidth;
            cell.layer.cornerRadius = CellRounded;
            
            if(self.FocuseUpdateCallBack) {
                self.FocuseUpdateCallBack();
            }
        }
    }
    //[StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

@end

