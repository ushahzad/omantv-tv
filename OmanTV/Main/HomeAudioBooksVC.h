//
//  HomeAudioBooksVC.h
//  OmanTV
//
//  Created by Cenex Studio on 25/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeAudioBooksVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);

@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIImageView *img_exclusive;
@property (strong,nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (strong,nonatomic) IBOutlet UILabel *lbl_noResultFound;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *allShowsList;
@property (nonatomic, retain) NSMutableArray *channelsList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
-(void)loadDataFromServer:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END
