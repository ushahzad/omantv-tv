//
//  ResumeWatchingVC.m
//  OmanTV
//
//  Created by Curiologix on 11/01/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "ResumeWatchingVC.h"

@interface ResumeWatchingVC ()

@end

@implementation ResumeWatchingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contentList = [NSMutableArray new];
    
    [StaticData scaleToArabic:self.collectionView];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    if ([StaticData isLanguageEnglish])
    {
        self.lbl_title.text = @"RESUME WATCHING";
        self.lbl_title.textAlignment = NSTextAlignmentLeft;
    } else
    {
        self.lbl_title.textAlignment = NSTextAlignmentRight;
    }
}
	
-(void)viewWillAppear:(BOOL)animated
{
    [self.indicator startAnimating];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if (!self.isRadio) {
            [self getcontentList];
        }
    });
}

-(void)getcontentList
{
    [self.contentList removeAllObjects];
    [StaticData reloadCollectionView:self.collectionView];
    
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand/getResumeVideos?user_id=%@&key=%@&&channel_userid=%@&limit=20&p=1",BaseURL,BaseUID,BaseKEY,[StaticData findeLogedUserID]];
    [self executeApiCallToGetResponse:path withParameters:nil withCallType:@"ResumeWatching"];
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    NSLog(@"Resume Video = %@",path);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [self responseRecieved:responseObject withCallType:callType];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        [self.indicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        _heightConstraint.constant = 0;
        self.view.hidden = true;
        [self checkResultsFound];
    }];
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ResumeWatching"])
        {
            for (NSDictionary *dicObj in responseObjct)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                info.position = [StaticData checkStringNull:dicObj[@"position"]];
                info.duration = [StaticData checkStringNull:dicObj[@"duration"]];
                
                info.time = [self getTotalVideoTime:[info.duration intValue]];
                //if (![StaticData isVideoBlockInMyCountry:dicObj isLatestOrMost:false])
                //{
                    [self.contentList addObject:info];
                //}
            }
            
            [self checkResultsFound];
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"FeaturedShows has issue\n issue = %@",exception.description]];
        [self checkResultsFound];
    } @finally {
    }
}

-(void)checkResultsFound
{
    if (_contentList.count == 0) {
        _heightConstraint.constant = 0;
        self.view.hidden = true;
        if (self.UpdateScrollContentSizeCallBlock && !self.isCallBackCalled)
        {
            self.UpdateScrollContentSizeCallBlock();
            self.isCallBackCalled = true;
        }
    } else {
        _heightConstraint.constant = 150;
        self.view.hidden = false;
    }
    
    [StaticData reloadCollectionView:self.collectionView];
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    if (scrollView == self.collectionView) {
//        [self takeCellToCenter];
//    }
//}

-(void)takeCellToCenter
{
    CGPoint currentCellOffset = self.collectionView.contentOffset;
    currentCellOffset.x += self.collectionView.frame.size.width / 2;
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:currentCellOffset];
    [self.collectionView scrollToItemAtIndexPath:indexPath
                           atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                   animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 112)/4.5;
    CGFloat height = 300;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    
    _heightConstraint.constant = height+80;
    if (self.UpdateContentSize)
    {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.thumbnail.hidden = true;
        cell.lbl_title.text = @"";
        return cell;
    }
    
    cell.thumbnail.hidden = false;
    MediaInfo *info = self.contentList[indexPath.row];
    cell.lbl_title.text = info.title;
    cell.lbl_time.text = info.time;
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    CGFloat duration = [info.duration floatValue];
    CGFloat position = [info.position floatValue];
    
    cell.progressView.hidden = false;
    CGFloat ratio = position/duration;
    cell.progressView.progress = ratio;
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:info.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.indicator startAnimating];
        
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self.collectionView reloadData];
                     });
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self.collectionView reloadData];
                                                   });
                     }
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"FeaturedShows cell for item has issue\n issue = %@",exception.description]];
    } @finally {
    }

    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    
    cell.view_blur.layer.cornerRadius = CellRounded;
    cell.view_blur.layer.masksToBounds = true;
    
    cell.view_play.layer.cornerRadius = CellRounded;
    cell.view_play.layer.masksToBounds = true;
    
    [StaticData scaleToArabic:cell.thumbnail];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.contentList.count == 0) return;
    
    MediaInfo *info = self.contentList[indexPath.row];
    [StaticData openVideoPage:info withVC:self];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //[cell setTransform:CGAffineTransformMakeScale(0.8, 0.8)];
    });
    //NSLog(@"%d",indexPath.row);
}

-(void)reloadCollectionData
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

-(NSString *)getTotalVideoTime:(int)totalSeconds
{
    if (totalSeconds < 0) {
        return @"00:00";
    }
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%d:%@:%@",hours, minsString, secsString];
    }
    
    if (minutes > 0) {
        return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
    }
    
    return [NSString stringWithFormat:@"%@", secsString];
}

@end
