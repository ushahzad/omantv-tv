//
//  AudioPlayer.m
//  OmanTV
//
//  Created by Curiologix on 29/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "AudioPlayer.h"

@implementation AudioPlayer

static AudioPlayer *shared;

+(AudioPlayer *)shared
{
    if (!shared) {
        shared = [AudioPlayer new];
    } else {
        [shared.radioPlayer resetPlayer];
    }
    return shared;
}

-(void)playAudioWithInfo:(MediaInfo *)info withParentVC:(UIViewController *)parentVC
{
    self.parentVC = parentVC;
    [self.indicator startAnimating];
    
    _selectedShow = info;
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,_selectedShow.ID];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
}

#pragma mark - Load Categories Request
// make web service api call
-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"FavoriteShows"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.indicator stopAnimating];
        }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            if ([type isEqualToString:@"ShorterUrl"])
            {
                responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            }
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.indicator stopAnimating];
        }];
    }
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"RadioPlayBackUrl"])
        {
            _str_playbackURL = response[@"playback_url"];
            [self setupAuidoPlayer];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    self.radioPlayer = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    self.radioPlayer.playerInfo = self.selectedShow;
    [StaticData shared].radioPlayer = self.radioPlayer;
    
    [self.parentVC addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.parentVC.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.parentVC.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.parentVC.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = _selectedShow.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

-(void)Updaterequest
{
    @try {
        NSString *URLsettings = [NSString stringWithFormat:@"%@?/crosssitestats/UpdateOnline",BaseURL];
        
        NSData* userIdData = [BaseUID dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64EncodedUserID = [userIdData base64EncodedStringWithOptions:0];
        if (_selectedShow == nil) return;
        NSLog(@"Catchup ID = %@",_selectedShow.ID);
        
        NSDictionary *parametrs = @{@"userid":base64EncodedUserID,@"browserOS":@"appletv",@"videoid":_selectedShow.ID,@"channelid":@"",@"sessionid":self.sessionID,@"domain":@"",@"device":@"7"};
        
        [self executePostApiToGetResponse:URLsettings parameters:parametrs callType:@"UpDateOnline"];
    } @catch (NSException *exception) {
        NSLog(@"Update online Request = %@",exception.description);
    } @finally {
        
    }
}

@end
