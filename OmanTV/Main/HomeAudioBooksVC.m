//
//  HomeAudioBooksVC.m
//  OmanTV
//
//  Created by Cenex Studio on 25/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "HomeAudioBooksVC.h"

@interface HomeAudioBooksVC ()

@end

@implementation HomeAudioBooksVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.contentList = [NSMutableArray new];
    self.allShowsList = [NSMutableArray new];
    
    [self.lbl_noResultFound setHidden:true];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [StaticData scaleToArabic:self.collectionView];
}

-(void)loadDataFromServer:(NSString *)urlString
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];

    [self.indicator startAnimating];
//    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&t=1632487449&&channel_id=%@&cat_id=&genre_id=&p=1&ended=no&limit=5&is_radio=1&need_show_times=no&need_channel=&custom_order=yes&exclude_channel_id=&featured=&need_avg_rating=yes",BaseURL,BaseUID,BaseKEY,AudioBookChannelId];
    [self executeApiToGetApiResponse:urlString withParameters:nil withCallType:@"ContentList"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
//    [StaticData setSSLCertificate:manager];
    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            [self.allShowsList removeAllObjects];
            for (NSDictionary *dicObj in responseObject[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"category_channel_id"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                info.descriptions = [StaticData checkStringNull:dicObj[@"short_title_ar"]];
                if ([info.descriptions isEqualToString:@""]) {
                    info.descriptions = [StaticData checkStringNull:dicObj[@"description_ar"]];
                }
                
                info.rating = [[StaticData checkStringNull:dicObj[@"ratings_avg"]] floatValue];
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [self.contentList addObject:info];
                }
            }
            
            [self.collectionView reloadData];
            
            [self.lbl_noResultFound setHidden:true];
            if (self.contentList.count == 0) {
                [self.lbl_noResultFound setHidden:false];
            }
        }
    } @catch (NSException *exception) {
        [self.lbl_noResultFound setHidden:true];
        if (self.contentList.count == 0) {
            [self.lbl_noResultFound setHidden:false];
        }
//        [self checkRecordFound];
    } @finally {
    }
}

-(void)findChannelLogoPath:(MediaInfo *)info
{
    for (MediaInfo *channel in self.channelsList)
    {
        if ([channel.ID isEqualToString:info.channelID])
        {
            info.channelLogoPath = channel.channelLogoPath;
        }
    }
}

-(void)checkRecordFound
{
    self.view.hidden = false;
    self.heightConstraint.constant = 300.0;
    if (self.allShowsList.count == 0) {
        self.view.hidden = true;
        self.heightConstraint.constant = 0.0;
    } else {
        for (int i = 0; i < _allShowsList.count; i++)
        {
            int randomNumber = arc4random_uniform((int)_allShowsList.count);
            if (randomNumber <= self.allShowsList.count)
            {
                [self.contentList addObject:_allShowsList[randomNumber]];
            }
            if (self.contentList.count >= 10) {
                break;
            }
        }
    }
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width -10) / 2.5;
    CGFloat height = width;
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + 160;
        if (self.UpdateContentSize) {
            self.UpdateContentSize();
        }
    }
    
//    self.heightConstraint.constant = height + 30;
    self.heightConstraint.constant = 740;
    
    return CGSizeMake(300, 690);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_title.text = info.title;
    cell.lbl_date.text = info.descriptions;
    [StaticData addSpaceInLabelText:cell.lbl_date];
    cell.view_info.hidden = false;
    
    cell.btn_listenNow.layer.cornerRadius = cell.btn_listenNow.frame.size.height/2;
    cell.btn_listenNow.layer.borderColor = AppColor.CGColor;
    cell.btn_listenNow.layer.borderWidth = 2;
    cell.btn_listenNow.clipsToBounds = YES;
    
    cell.lbl_rating.text = [NSString stringWithFormat:@"%.01f/5",info.rating];
    cell.lbl_rating.adjustsFontSizeToFitWidth = true;
    
    cell.ratingView.value = info.rating;
    cell.ratingView.allowsHalfStars = YES;
    cell.ratingView.accurateHalfStars = YES;
    cell.ratingView.userInteractionEnabled = false;
    cell.ratingView.tintColor = AppColor;
    
//    cell.btn_listenNow.tag = indexPath.row;
//    [cell.btn_listenNow addTarget:self action:@selector(audioListenNow:) forControlEvents:UIControlEventTouchUpInside];
    
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    cell.thumbnail.image = nil;
    
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo_audio"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;

        cell.btn_listenNow.backgroundColor =  [UIColor clearColor];
        cell.btn_listenNow.layer.borderColor = AppColor.CGColor;
//        cell.btn_listenNow.layer.cornerRadius = cell.frame.size.height/2;
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        
        cell.btn_listenNow.backgroundColor =  AppColor;
    }
    
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = [MediaInfo new];
    info = [self.contentList objectAtIndex:indexPath.row];
    info.isRadio = true;
    [StaticData openShowDetailsPageWith:self withMediaInfo:info];
}

- (void)audioListenNow:(UIButton *)sender {
    MediaInfo *info = [self.contentList objectAtIndex:sender.tag];
    NSLog(@"Listen Audio:%@", info.title);
//    [[AudioPlayer sharedWithPlayTye:1] setupAudioDetails:info];
}

@end
