//
//  NowOnTVVC.m
//  OmanTV
//
//  Created by MacUser on 13/05/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "MostWatchedVC.h"

@interface MostWatchedVC ()

@end

@implementation MostWatchedVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.contentList = [NSMutableArray new];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }

    if (isEnglishLang) {
        self.lbl_title.text = @"MOST WATCHED";
        self.lbl_title.textAlignment = NSTextAlignmentLeft;
    }
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadTVAndRadioMostWatched];
}

-(void)loadTVAndRadioMostWatched
{
    [self.indicator startAnimating];
    
    if (self.isRadio)
    {
        self.lbl_title.text = @"الأكثر استماعاً";
    } else
    {
        self.lbl_title.text = @"الأكثر مشاهدة";
    }
    
    if (self.isRadio) {
        NSString *path = [NSString stringWithFormat:@"%@/nand?scope=audio&action=mostAudiosInDuration&user_id=%@&key=%@&page=1&limit=20&duration=30&need_category_info=yes&need_channel_info=yes&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,ExcludeChannelId];
        [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"RadiContentList"];
    } else {
        NSString *path = [NSString stringWithFormat:@"%@/nand/mostInPastDuration?user_id=%@&key=%@&page=1&limit=20&need_category_info=yes&need_channel_info=yes&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,ExcludeChannelId];
        [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"TVContentList"];
    }
    
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
         [self checkRecordFound];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"TVContentList"] || [callType isEqualToString:@"RadiContentList"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"ch_id"]];
                if (info.channelID.length == 0) {
                    info.channelID = [StaticData checkStringNull:dicObj[@"channel_id"]];
                }
                info.title = [StaticData title:dicObj];

                info.channelLogoPath = [StaticData checkStringNull:dicObj[@"channel_thumbnail"]];
                
                if (info.channelLogoPath.length == 0) {
                    info.channelLogoPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"cat_thumbnail"]];
                }
                info.duration = [StaticData checkStringNull:dicObj[@"duration"]];
                info.time = [StaticData getVideoDuration:[info.duration intValue]];
                
                int totalReviews = [dicObj[@"today_views"] intValue];
                info.reviews = [NSString stringWithFormat:@"%d ",totalReviews];
                
                info.date = [self getForamttedDate:[StaticData checkStringNull:dicObj[@"recorder_date"]]];
                
                if ([callType isEqualToString:@"RadiContentList"])
                {
                    info.isRadio = true;
                }
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(NSString *)getForamttedDate:(NSString *)time
{
    @try {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"YYYY-MM-dd"];
        NSDate *catchupDate = [df dateFromString:time];
        
        [df setDateFormat:@"dd-MM-YYYY"];
        
        NSString *time = [df stringFromDate:catchupDate];
        
        return time;
    } @catch (NSException *exception) {
        [StaticData log:exception.description];
    } @finally {
    }
}

-(void)checkRecordFound
{
    self.view.hidden = false;
    self.heightConstraint.constant = 300.0;
    if (self.contentList.count == 0) {
        self.heightConstraint.constant = 0.0;
        self.view.hidden = true;
    }
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + 50;
    }
    
    _heightConstraint.constant = height+80;
    
    if (self.UpdateContentSize)
    {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.channelLogo.image = nil;
       // cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    [self loadChannelLogo:cell withInfo:info];
    
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
//                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
//                     if (!self.tempImage && image) {
//                         self.tempImage = image;
//                         [self reloadCollectionView];
//                     }
//                     [cell.indicator stopAnimating];
//                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.channelLogo.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
    cell.channelLogo.layer.cornerRadius = CellRounded;
    cell.channelLogo.layer.masksToBounds = true;
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.contentList.count == 0) return;
    
    MediaInfo *info = self.contentList[indexPath.row];
    if (info.isRadio) {
        AudioPlayer *audioPlayer = [AudioPlayer shared];
        [audioPlayer playAudioWithInfo:info withParentVC:self.parentViewController];
        if (self.AudioClickCallBack) {
            self.AudioClickCallBack();
        }
    } else {
        [StaticData openVideoPage:info withVC:self];
    }
}

-(void)loadChannelLogo:(CollectionCell *)cell withInfo:(MediaInfo *)info
{
    @try {
        NSString * encodedCover = [info.channelLogoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.channelLogo.contentMode = UIViewContentModeScaleAspectFit;
        [cell.channelLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:info.isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

@end
