//
//  HomeAudioBooksTabVC.m
//  OmanTV
//
//  Created by Cenex Studio on 25/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "HomeAudioBooksTabVC.h"

@interface HomeAudioBooksTabVC ()

@end

@implementation HomeAudioBooksTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.audioTabsArray = [NSMutableArray new];
    
    MediaInfo *info = [MediaInfo new];
    info.title_ar = @"أحدث الكتب";
    info.title_en = @"Latest Program";
    info.ID = @"1";
    info.audioChannelPath = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&t=1632487449&channel_id=%@&cat_id=&genre_id=&p=1&ended=no&limit=5&is_radio=1&need_show_times=no&need_channel=&custom_order=yes&exclude_channel_id=&need_avg_rating=yes",BaseURL,BaseUID,BaseKEY,ExcludeChannelId];
    [self.audioTabsArray addObject:info];
    
    info = [MediaInfo new];
    info.title_ar = @"اختيارات المحرر";
    info.title_en = @"Editor's Picks";
    info.ID = @"2";
    info.audioChannelPath = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&t=1632487449&channel_id=%@&cat_id=&genre_id=&p=1&ended=no&limit=10&is_radio=1&need_show_times=no&need_channel=&custom_order=yes&exclude_channel_id=&featured=yes&need_avg_rating=yes",BaseURL,BaseUID,BaseKEY,ExcludeChannelId];
    [self.audioTabsArray addObject:info];
    
    info = [MediaInfo new];
    info.title_ar = @"الأكثر إستماعاً";
    info.title_en = @"Most Listened";
    info.ID = @"3";
    info.audioChannelPath = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&t=1632487449&&scope=audio&action=mostShowsInDuration&channel_id=%@&page=1&limit=10&duration=30&need_avg_rating=yes",BaseURL,BaseUID,BaseKEY,ExcludeChannelId];
    [self.audioTabsArray addObject:info];
    
    info = [MediaInfo new];
    info.title_ar = @"الكل";
    info.title_en = @"All";
    info.ID = @"4";
    [self.audioTabsArray addObject:info];
    
//    info = [MediaInfo new];
//    info.title_ar = @"+ المزيد";
//    info.title_en = @"More +";
//    info.ID = @"5";
//    info.thumbnail = @"btn_audio_book_more";
//    [self.audioTabsArray addObject:info];
    
    [StaticData scaleToArabic:self.collectionView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        MediaInfo *info = [MediaInfo new];
        info = [self.audioTabsArray objectAtIndex:0];

        if ([self ChooseAudioBookCallBack]) {
            self.ChooseAudioBookCallBack(info);
        }
    });
}

-(void)reloadCollectionView {
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = frame.size.width / 3.3;
    
    if (self.UpdateContentSize) {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.audioTabsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
 
    if (self.audioTabsArray.count == 0) {
        [cell.indicator startAnimating];
        return cell;
    }
        
    MediaInfo *info = self.audioTabsArray[indexPath.row];
    
    cell.lbl_title.text = info.title_ar;
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.lbl_title setTextColor:[UIColor whiteColor]];
    
    cell.layer.borderColor = AppColor.CGColor;
    cell.layer.borderWidth = 1.0;
    
    cell.layer.cornerRadius = cell.frame.size.height/2;
    cell.clipsToBounds = YES;
    
    if (indexPath.row == self.selectedIndex) {
        [cell.lbl_title setTextColor:[UIColor whiteColor]];
        [cell setBackgroundColor:AppColor];
        cell.layer.borderColor = AppColor.CGColor;
        cell.layer.borderWidth = 1.0;
    }
    else {
        [cell.lbl_title setTextColor:[UIColor whiteColor]];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.layer.borderColor = AppColor.CGColor;
        cell.layer.borderWidth = 1.0;
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MediaInfo *info = [MediaInfo new];
    info = [self.audioTabsArray objectAtIndex:indexPath.row];
    
    if ([info.ID isEqualToString:@"4"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AudioBooksVC"];
        [self.navigationController pushViewController:vc animated:true];
    }
    else {
        self.selectedIndex = (int)indexPath.row;
        
        if ([self ChooseAudioBookCallBack]) {
            self.ChooseAudioBookCallBack(info);
        }
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [self setupSelectedState:cell withIndexPath:context.previouslyFocusedIndexPath];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.view_info.hidden = true;
            cell.backgroundColor =  AppColor;
            cell.lbl_title.textColor = [UIColor whiteColor];
            
            if(self.FocuseUpdateCallBack) {
                self.FocuseUpdateCallBack();
            }
        } else
        {
            [StaticData reloadCollectionView:self.collectionView];
        }
    }
}
	
-(void)setupSelectedState:(CollectionCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.view_info.hidden = false;
    cell.lbl_title.textColor = [UIColor whiteColor];
    cell.backgroundColor =  [UIColor clearColor];
    
    MediaInfo *info = self.audioTabsArray[indexPath.row];

    if (info.isSelected) {
        cell.lbl_title.textColor = AppColor;
    }
}


@end
