//
//  AudioPlayer.h
//  OmanTV
//
//  Created by Curiologix on 29/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CatchupAudioPlayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface AudioPlayer : NSObject
@property (nonatomic, retain) UIViewController *parentVC;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) NSTimer *updateOlineTimer;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) NSMutableString *sessionID;
+(AudioPlayer *)shared;
-(void)playAudioWithInfo:(MediaInfo *)info withParentVC:(UIViewController *)parentVC;
@end

NS_ASSUME_NONNULL_END
