//
//  CustomCarouselsParentVC.h
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomCarouselsParentVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateScrollContentSizeCallBlock)(void);
@property void(^RecommendationModuleResultsDoundCallBlock)(id jsonResponse);
@property void (^FocuseUpdateCallBack)(MediaInfo *info);
@property void(^AudioClickCallBack)(void);
@property (nonatomic, weak) IBOutlet UICollectionView *cv_videos;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title2;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noResutlsFound;
@property (nonatomic, weak) IBOutlet UIButton *btn_more;
@property (nonatomic, weak) IBOutlet UIView *view_more;
@property (nonatomic, weak) IBOutlet UIImageView *img_netGoe;
@property (nonatomic, weak) IBOutlet UIImageView *img_orignalLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic, retain) NSDictionary *carouselInfo;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSString *imageAspectRatio;
@property (nonatomic, retain) UIImage *tempImage;
@property(nonatomic,retain) NSMutableArray *contentList;
@property (nonatomic, assign) BOOL isCallBackCalled;
@property (nonatomic, assign) BOOL isAudioPlay;
@property (nonatomic, retain) NSString *selectedGenreID;
-(void)loadCustomCarouselsData:(NSDictionary *)carousel;
-(void)reloadCollectionData;
@end

NS_ASSUME_NONNULL_END
