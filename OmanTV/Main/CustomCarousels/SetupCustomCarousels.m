//
//  CustomCarousels.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 19/10/2020.
//  Copyright © 2020 dotcom. All rights reserved.
//

#import "SetupCustomCarousels.h"

@implementation SetupCustomCarousels

-(void)loadCustomCarousels
{
    @try {
        NSString *path = [NSString stringWithFormat:@"%@/endpoint/blocks/list_blocks?key=%@&user_id=%@&p=1&limit=15&need_labels=yes&need_top_10=yes&need_season_count=yes&need_full_info=yes&need_genre=yes&need_category=yes&fav_type=1&X-API-KEY=%@",BaseURL, BaseKEY, BaseUID, [StaticData findLogedUserToken]];
        [self executeApiCallToGetResponse:path withParameters:nil withCallType:@"CustomContentList"];
    } @catch (NSException *exception) {
        [StaticData log:@"ContentList"];
    } @finally {
    }
}

-(void)loadRecomendedCarouselsData:(id)carousel
{
    @try {
        NSString *api = [StaticData checkStringNull:carousel[@"api"]];
        if (api.length > 0)
        {
            int limit = 15;
            NSString *type = [StaticData checkStringNull:carousel[@"type"]];
            if ([type isEqualToString:@"most_in_country"])
            {
                limit = 10;
            }
           NSString *path = [NSString stringWithFormat:@"%@/%@&p=1&limit=%d",BaseURL,api,limit];
            [self executeApiCallToGetResponse:path withParameters:nil withCallType:@"RecomededList"];
        }
    } @catch (NSException *exception) {
        [StaticData log:@"ContentList"];
    } @finally {
    }
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    //[manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self responseRecieved:responseObject withCallType:callType];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        @try {
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
            if (self.CustomCarouselFinishedCallBack) {
                self.CustomCarouselFinishedCallBack();
            }

            if (self.tryToLoadAgainContentCount < 3 && [callType isEqualToString:@"CustomContentList"])
            {
                [self loadCustomCarousels];
            }
            self.tryToLoadAgainContentCount ++;
        } @catch (NSException *exception) {
        } @finally {
        }
    }];
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"CustomContentList"])
        {
            for (NSDictionary *carousel in responseObjct[@"data"])
            {
                NSMutableArray *contentList = [NSMutableArray new];
                NSString *type = [StaticData checkStringNull:carousel[@"type"]];
                NSString *imageAspectRatio = [StaticData checkStringNull:carousel[@"image_aspect_ratio_tvapp"]];
                
                for (NSDictionary *subDic in carousel[@"items"])
                {
                    if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                    {
                        continue;
                    }
                    
                    MediaInfo *info = [MediaInfo new];
                    info.details = subDic;
                    info.ID = [StaticData checkStringNull:subDic[@"id"]];
                    //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                    info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                    info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                    
                    info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                    
                    if (info.thumbnailPath.length == 0)
                    {
                        if ([type isEqualToString:@"custom_videos"] || [type isEqualToString:@"latest_videos_in_show"])
                        {
                            info.isVideo = true;
                            info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                        } else if ([type isEqualToString:@"latest_audios_in_show"] || [type isEqualToString:@"custom_audios"])
                        {
                            info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
                        } else
                        {
                            info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                        }
                    }
                    
                    if ([type isEqualToString:@"custom_videos"] || [type isEqualToString:@"latest_videos_in_show"])
                    {
                        info.isVideo = true;
                    }
                    
                    if ([type isEqualToString:@"latest_audios_in_show"] || [type isEqualToString:@"custom_audios"] || [type isEqualToString:@"custom_carousel_radio"])
                    {
                        info.isRadio = false;
                        info.isVideo = false;
                        info.isRadioShow = true;
                    }
                    
                    info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
                    info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
                    if (info.atvImagPath.length == 0) {
                        info.atvImagPath = info.thumbnailPath;
                    }
                    
                    info.carouselType = [StaticData checkStringNull:carousel[@"type"]];
                    
                    info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
                    
                    NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                    if (labelsArray.count > 0) {
                        NSDictionary *labelDic = labelsArray[0];
                        info.signLogoPath = labelDic[@"image"];
                    }
                    
                    info.itemType = @"home";
                    [contentList addObject:info];
                }
    
                NSString *title = [StaticData checkStringNull:carousel[@"title_ar"]];
                if (carousel[@"items"])
                {
                    if (contentList.count > 0)
                    {
                        if (self.CustomCarouselDataReceivedCallBack) {
                            self.CustomCarouselDataReceivedCallBack(contentList, title,carousel);
                        }
                    }
                } else
                {
                    if (self.LoadApiCustomCarouselCallBack) {
                        self.LoadApiCustomCarouselCallBack(carousel);
                    }
                }
            }
            
            if (self.CustomCarouselFinishedCallBack) {
                self.CustomCarouselFinishedCallBack();
            }
        } else if ([callType isEqualToString:@"RecomededList"])
        {
            [self loadRecomendedContent:responseObjct withType:@"recent_favorited"];
            [self loadRecomendedContent:responseObjct withType:@"recent_viewed"];
            [self loadRecomendedContent:responseObjct withType:@"most_viewed"];
        }
    } @catch (NSException *exception) {
        if (self.CustomCarouselFinishedCallBack) {
            self.CustomCarouselFinishedCallBack();
        }
    } @finally {
    }
}

-(void)loadRecomendedContent:(id)responseObjct withType:(NSString *)type
{
    NSMutableArray *contentList = [NSMutableArray new];
    NSDictionary *carousel = [StaticData checkDictionaryNull:responseObjct[@"data"][type]];
    if (carousel)
    {
        NSString *imageAspectRatio = [StaticData checkStringNull:carousel[@"image_aspect_ratio_tvapp"]];
        for (NSDictionary *subDic in carousel[@"shows"])
        {
            if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
            {
                continue;
            }
            
            MediaInfo *info = [MediaInfo new];
            info.details = subDic;
            info.ID = [StaticData checkStringNull:subDic[@"id"]];
            //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
            info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
            info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
            info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
            
            if (info.thumbnailPath.length == 0)
            {
                info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
            }
            
            info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
            info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
            
            info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
            info.itemType = @"home";
            info.carouselType = type;
            
            NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
            if (labelsArray.count > 0) {
                NSDictionary *labelDic = labelsArray[0];
                info.signLogoPath = labelDic[@"image"];
            }

            [contentList addObject:info];
        }
        
        //NSDictionary *show = [StaticData checkDictionaryNull:carousel[@"show"]];
        NSString *title = [StaticData checkStringNull:carousel[@"section_title"]];
        if (self.LoadRecomendedCustomCarouselCallBack) {
            self.LoadRecomendedCustomCarouselCallBack(contentList, title,carousel,type);
        }
    }
}
@end
