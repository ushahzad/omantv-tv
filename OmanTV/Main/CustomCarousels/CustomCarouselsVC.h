//
//  CustomCarouselsVC.h
//  awaan-ios
//
//  Created by MacUser on 26/10/2020.
//  Copyright © 2020 Ali Raza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCarouselsParentVC.h"
#import "LMHCarouselDataVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface CustomCarouselsVC : CustomCarouselsParentVC<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

NS_ASSUME_NONNULL_END
