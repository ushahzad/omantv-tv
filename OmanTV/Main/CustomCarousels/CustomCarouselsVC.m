//
//  CustomCarouselsVC.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 19/10/2020.
//  Copyright © 2020 dotcom. All rights reserved.
//

#import "CustomCarouselsVC.h"
#import "LiveChannelPlayer.h"
#import "TVSubCategoriesVC.h"

@interface CustomCarouselsVC ()

@end

@implementation CustomCarouselsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentList = [NSMutableArray new];
    self.selectedGenreID = @"";
    
    if (@available(tvOS 11.0, *)) {
        self.cv_videos.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.cv_videos setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.lbl_noResutlsFound.hidden = true;
    self.lbl_noResutlsFound.text = NoRecordFound;
    
    self.cv_videos.remembersLastFocusedIndexPath = true;
    
    [StaticData scaleToArabic:self.cv_videos];
    
    if (![StaticData isLanguageEnglish])
    {
        self.lbl_title.textAlignment = NSTextAlignmentRight;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self reloadCollectionData];
    });
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *menuGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadMore)];
    [self.btn_more addGestureRecognizer:menuGestureRec];
    
    [self reloadCollectionData];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    /*if (self.contentList.count > 0)
    {
        MediaInfo *info = self.contentList[section];
        if ([info.carouselType isEqualToString:@"resume_watching"] || [info.carouselType isEqualToString:@"latest_episode"])
        {
            return 0.0;
        }
    }*/
    return 30.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (self.contentList.count > 0)
    {
        MediaInfo *info = self.contentList[section];
        if ([info.carouselType isEqualToString:@"resume_watching"] || [info.carouselType isEqualToString:@"latest_episode"])
        {
            return 0.0;
        }
    }
    return 30.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat extraSpace = 140;
    CGFloat width = (frame.size.width)/ 5.3;
    CGFloat height = 250.0;
    CGFloat titleHeight = 50.0;
    NSString *imageAspectRatio = [StaticData checkStringNull:self.carouselInfo[@"image_aspect_ratio_tvapp"]];
    
    if ([imageAspectRatio isEqualToString:@"v"])
    {
        width = 256;
        height = 359;
        
        height += titleHeight;
        
        self.heightConstraint.constant = height + extraSpace;
            
        if (self.UpdateScrollContentSizeCallBlock) {
            self.UpdateScrollContentSizeCallBlock();
        }
        return CGSizeMake(width, height);
    } else if ([imageAspectRatio isEqualToString:@"h"])
    {
        width = (frame.size.width)/ 5.3;
        height = 195.0;
//        width = 360;
//        height = 201;
        
        height += titleHeight;
        
        self.heightConstraint.constant = height + extraSpace;
            
        if (self.UpdateScrollContentSizeCallBlock) {
            self.UpdateScrollContentSizeCallBlock();
        }
        return CGSizeMake(width, height);
    }
    
    if (self.contentList.count > 0)
    {
        MediaInfo *info = [MediaInfo new];
        if (indexPath.row <= self.contentList.count) {
            info = self.contentList[indexPath.row];
        }
        
        if ([info.carouselType isEqualToString:@"live_channels"] || [info.carouselType isEqualToString:@"custom_live_channels"] || [info.carouselType isEqualToString:@"all_channels"])
        {
            CGFloat height = 211.0;
            CGFloat width = (frame.size.width)/ 3.3;
            
            if (self.tempImage) {
                CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
                height = (ratio * width);
            }
            
            if (self.btn_more.hidden) {
                self.heightConstraint.constant = height + extraSpace;
            } else {
                self.heightConstraint.constant = height + extraSpace;
            }
            
            if (self.UpdateScrollContentSizeCallBlock) {
                self.UpdateScrollContentSizeCallBlock();
            }
            
            return CGSizeMake(width, height);
        } else if ([info.carouselType isEqualToString:@"categories"] || [info.carouselType isEqualToString:@"custom_categories"])
        {
            CGFloat height = 140.0;
            CGFloat width = (frame.size.width)/ 5.3;
            
            height += titleHeight;
            
            self.heightConstraint.constant = height + extraSpace;
            
            if (self.UpdateScrollContentSizeCallBlock) {
                self.UpdateScrollContentSizeCallBlock();
            }
            
            return CGSizeMake(width, height );
        } else if ([info.carouselType isEqualToString:@"resume_watching"] || [info.carouselType isEqualToString:@"latest_episode"])
        {
            width = (frame.size.width - 160)/ 4.4;
        }
    }
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    
    height += titleHeight;
    
    self.heightConstraint.constant = height + extraSpace;
        
    if (self.UpdateScrollContentSizeCallBlock) {
        self.UpdateScrollContentSizeCallBlock();
    }
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (self.contentList.count == 0)
    {
        [cell.indicator startAnimating];
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
    
    if ([info.carouselType isEqualToString:@"live_channels"] || [info.carouselType isEqualToString:@"custom_live_channels"] || [info.carouselType isEqualToString:@"all_channels"]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell_live" forIndexPath:indexPath];
        
        @try {
            NSString *encodedCover = [info.thumbnail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
            [cell.indicator startAnimating];
            cell.img_highlight.contentMode = UIViewContentModeScaleAspectFill;
            [cell.img_highlight sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                     /*if (!self.tempImage) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }*/
                     [cell.indicator stopAnimating];
                 } else {
                     [StaticData downloadImageIfNotLoadinedBySdImage:cell.img_highlight withUrl:imageURL completed:^(UIImage *image) {
                         /*if (!self.tempImage && image) {
                             self.tempImage = image;
                             [self reloadCollectionView];
                         }*/
                         [cell.indicator stopAnimating];
                     }];
                 }
             }];
        } @catch (NSException *exception) {
        } @finally {
        }
        
        //[StaticData blurEffectsOnView:cell.view_blur withEffectStyle:UIBlurEffectStyleLight];
        
        cell.view_blur.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        cell.img_highlight.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
        
        cell.view_blur.layer.cornerRadius = 12.0;
        cell.view_blur.clipsToBounds = true;
        
        cell.img_highlight.layer.cornerRadius = 12.0;
        cell.img_highlight.clipsToBounds = true;
    } else if ([info.carouselType isEqualToString:@"resume_watching"]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        CGFloat duration = [info.duration floatValue];
        CGFloat position = [info.position floatValue];
        
        cell.progressView.hidden = false;
        CGFloat ratio = position/duration;
        cell.progressView.progress = ratio;
        [StaticData scaleToArabic:cell.progressView];
        
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    if ([info.carouselType isEqualToString:@"latest_audios_in_show"] || [info.carouselType isEqualToString:@"custom_audios"]) {
        info.isRadio = true;
    }
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:info.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [cell.indicator startAnimating];
        
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image)
            {
                if (!self.tempImage) {
                    self.tempImage = image;
                    [self reloadCollectionData];
                }
                [cell.indicator stopAnimating];
            } else {
                [cell.indicator stopAnimating];
            }
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    [cell.signLanguageLogo setHidden:true];
    @try {
        NSString *encodedCover = [info.signLogoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        cell.signLanguageLogo.contentMode = UIViewContentModeScaleAspectFit;
        [cell.signLanguageLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [cell.signLanguageLogo setHidden:false];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    cell.info = info;
    
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    cell.thumbnail.layer.cornerRadius = RoundeCorner;
    cell.view_info.layer.cornerRadius = RoundeCorner;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.contentList.count == 0) return;
        
        MediaInfo *info = self.contentList[indexPath.row];
        if ([info.carouselType isEqualToString:@"live_channels"] || [info.carouselType isEqualToString:@"custom_live_channels"] || [info.carouselType isEqualToString:@"all_channels"])
        {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            vc.selectedChannel = info;
        //    vc.isRadio = self.isRadio;
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.carouselType isEqualToString:@"radio_channels"] ) {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            vc.selectedChannel = info;
        //    vc.isRadio = self.isRadio;
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.carouselType isEqualToString:@"categories"] || [info.carouselType isEqualToString:@"custom_categories"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Categories" bundle:nil];
            TVSubCategoriesVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TVSubCategoriesVC"];
            vc.categoryList = self.contentList;
            vc.selectedCategory = info;
            [self.navigationController pushViewController:vc animated:true];
        } else if (info.isVideo)
        {
            [StaticData openVideoPage:info withVC:self];
        } else if (info.isRadio) {
            self.isAudioPlay = true;
            AudioPlayer *audioPlayer = [AudioPlayer shared];
            [audioPlayer playAudioWithInfo:info withParentVC:self.parentViewController];
            if (self.AudioClickCallBack) {
                self.AudioClickCallBack();
            }
        } else if (info.isRadioShow) {
            [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
//            MediaInfo *info = self.contentList[indexPath.row];
//            [StaticData openShowDetailsPageWith:self withMediaInfo:info];
        } else {
            MediaInfo *info = self.contentList[indexPath.row];
            [StaticData openShowDetailsPageWith:self withMediaInfo:info];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    
    if(self.FocuseUpdateCallBack) {
        self.FocuseUpdateCallBack(nil);
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.previouslyFocusedView == self.btn_more) {
        [UIView animateWithDuration:0.1 animations:^{
            self.view_more.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if (context.nextFocusedView == self.btn_more) {
        [UIView animateWithDuration:0.1 animations:^{
            self.view_more.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
    
    if(self.FocuseUpdateCallBack) {
        self.FocuseUpdateCallBack(nil);
    }
}

-(void)loadMore
{
    LMHCarouselDataVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LMHCarouselDataVC"];
    vc.carouselInfo = self.carouselInfo;
    [self.navigationController pushViewController:vc animated:true];
}

@end
