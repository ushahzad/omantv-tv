//
//  CustomCarouselsParentVC.m
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "CustomCarouselsParentVC.h"

@interface CustomCarouselsParentVC ()

@end

@implementation CustomCarouselsParentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view_more.hidden = true;
    self.btn_more.hidden = true;
    
//    self.view.backgroundColor = [UIColor redColor];
//    self.cv_videos.backgroundColor = [UIColor blueColor];
    
    NSString *type = [StaticData checkStringNull:_carouselInfo[@"type"]];
    
//    if ([type isEqualToString:@"live_channels"]) {
//        self.heightConstraint.constant = 0.0;
//        self.view.hidden = true;
//        return;
//    }
    
    self.img_orignalLogo.hidden = true;
    self.img_netGoe.hidden = true;
    
    if ([type isEqualToString:@"best_of_nat"]) {
        self.img_netGoe.hidden = false;
    } else if ([type isEqualToString:@"original_content"])
    {
        self.img_orignalLogo.hidden = false;
        self.img_orignalLogo.image =  [UIImage imageNamed:@"adtv_original_ar"];
        self.lbl_title.text = @"";
        self.lbl_title2.text = @"";
    }
    
    NSString *loadMore = [StaticData checkStringNull:_carouselInfo[@"load_more"]];
    if ([loadMore intValue] == 1) {
        self.btn_more.hidden = false;
        self.view_more.hidden = false;
    }
    
    if (self.btn_more.hidden) {
        self.bottomConstraint.constant = 0.0;
    } else {
        self.bottomConstraint.constant = 100.0;
    }
    
    self.view_more.layer.cornerRadius = 12.0;
    self.view_more.clipsToBounds = true;
    
    UITapGestureRecognizer *menuGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuTap)];
    menuGestureRec.allowedPressTypes = @[@(UIPressTypeMenu)];
    [self.view addGestureRecognizer:menuGestureRec];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (_carouselInfo)
    {
        NSString *type = [StaticData checkStringNull:_carouselInfo[@"type"]];
        if ([type isEqualToString:@"resume_watching"])
        {
            NSString *api = [StaticData checkStringNull:_carouselInfo[@"api"]];
            
            if (api.length > 0)
            {
                NSString *path = [NSString stringWithFormat:@"%@/%@&p=1&limit=15",BaseURL,api];
                [self executeApiCallToGetResponse:path withParameters:nil withCallType:type];
            }
        }
    }
}

-(void)handleMenuTap
{
    if(self.isAudioPlay){
        self.isAudioPlay = false;
        [[AudioPlayer shared].radioPlayer removeAudioPlayer];
    } else {
        exit(0);
    }
}

-(void)loadCustomCarouselsData:(NSDictionary *)carousel
{
    @try {
        _carouselInfo = carousel;
        self.contentList = [NSMutableArray new];
        NSString *title = [StaticData checkStringNull:carousel[@"title_ar"]];
        self.lbl_title.text = title;
        NSString *type = [StaticData checkStringNull:carousel[@"type"]];
        _imageAspectRatio = [StaticData checkStringNull:carousel[@"image_aspect_ratio_tvapp"]];
        
        NSString *api = [StaticData checkStringNull:carousel[@"api"]];
        
        int limit = 15;
        if ([type isEqualToString:@"most_in_country"])
        {
            limit = 10;
        }
        
        if (api.length > 0)
        {
            NSString *path = [NSString stringWithFormat:@"%@/%@&p=1&limit=%d",BaseURL,api,limit];
            [self executeApiCallToGetResponse:path withParameters:nil withCallType:type];
        } else
        {
            [self checkRecordFounds];
        }
        NSString *loadMore = [StaticData checkStringNull:carousel[@"load_more"]];
        if ([loadMore intValue] == 1) {
            self.btn_more.hidden = false;
            self.view_more.hidden = false;
        }
    } @catch (NSException *exception) {
        [self checkRecordFounds];
    } @finally {
    }
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    //[manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.contentList removeAllObjects];
        [self responseRecieved:responseObject withCallType:callType];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self checkRecordFounds];
        @try {
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        } @catch (NSException *exception) {
        } @finally {
        }
    }];
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"resume_watching"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.details = subDic;
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.isVideo = true;
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                info.position = [StaticData checkStringNull:subDic[@"position"]];
                info.duration = [StaticData checkStringNull:subDic[@"duration"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                info.carouselType = callType;
                info.itemType = @"home";
                [_contentList addObject:info];
            }
            
            [StaticData reloadCollectionView:self.cv_videos];
        } else if ([callType isEqualToString:@"latest_episode"])
        {
            for (NSDictionary *subDic in responseObjct)
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.details = subDic;
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                
                info.isVideo = true;
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                info.itemType = @"home";
                info.carouselType = callType;
                [_contentList addObject:info];
            }
            
            [StaticData reloadCollectionView:self.cv_videos];
        } else if ([callType isEqualToString:@"most_watched"])
        {
            for (NSDictionary *subDic in responseObjct)
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.details = subDic;
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.carouselType = callType;
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                }
                
                info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
                info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
                
                info.descriptions = [StaticData checkStringNull:subDic[@"cat_description_ar"]];
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                info.itemType = @"home";
                info.carouselType = callType;
                [_contentList addObject:info];
            }
            
            [StaticData reloadCollectionView:self.cv_videos];
        } else if ([callType isEqualToString:@"favorite_carousel"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.details = subDic;
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                }
                
                info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
                info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
                
                info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
                info.carouselType = callType;
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                [_contentList addObject:info];
            }
            
            [StaticData reloadCollectionView:self.cv_videos];
        } else if ([callType isEqualToString:@"most_in_country"])
        {
            for (NSDictionary *subDic in responseObjct)
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.details = subDic;
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                }
                
                info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
                info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
                
                info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                info.itemType = @"home";
                info.carouselType = callType;
                [_contentList addObject:info];
            }
            [StaticData reloadCollectionView:self.cv_videos];
        } else if ([callType isEqualToString:@"live_channels"] || [callType isEqualToString:@"all_channels"])
       {
           for (NSDictionary *chDic in responseObjct)
           {
               NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
               if ([enableLive isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
               {
                   continue;
               }
               
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:chDic[@"id"]];
               //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
               info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
               info.carouselType = callType;
               info.exclusive = [StaticData checkStringNull:chDic[@"exclusive"]];
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:chDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.thumbnail = [StaticData checkStringNull:chDic[@"icon"]];
               if (info.thumbnail.length == 0)
               {
                   info.thumbnail = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.itemType = @"home";
               info.carouselType = callType;
               
               [_contentList addObject:info];
           }
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"custom_live_channels"])
       {
           for (NSDictionary *chDic in responseObjct[@"data"])
           {
               NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
               if ([enableLive isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
               {
                   continue;
               }
               
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:chDic[@"id"]];
               //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
               info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
               info.carouselType = callType;
               info.exclusive = [StaticData checkStringNull:chDic[@"exclusive"]];
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:chDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.thumbnail = [StaticData checkStringNull:chDic[@"icon"]];
               if (info.thumbnail.length == 0)
               {
                   info.thumbnail = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.itemType = @"home";
               info.carouselType = callType;
               
               [_contentList addObject:info];
           }
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"resume_watching"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData title:subDic];
               info.isVideo = true;
               
               info.position = [StaticData checkStringNull:subDic[@"position"]];
               info.duration = [StaticData checkStringNull:subDic[@"duration"]];
       
               
               NSDictionary *show = [StaticData checkDictionaryNull:subDic[@"show"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:show];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:show[@"img"]];
               }
               
               info.season_title = [StaticData checkStringNull:show[@"title_ar"]];
               
               NSString *tags = [StaticData checkStringNull:subDic[@"tags"]];
               NSArray *tagList = [tags componentsSeparatedByString:@","];
               NSString *episodeTag = @"";
               if (tagList.count > 0) {
                   for (NSString *tag in tagList) {
                       if ([tag containsString:@"ep-"]) {
                           episodeTag = [tag stringByReplacingOccurrencesOfString:@"ep-" withString:@""];
                           info.title = [NSString stringWithFormat:@"الحلقة %@",episodeTag];
                       }
                   }
               }
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               info.carouselType = callType;
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"categories"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"][@"categories"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.details = subDic;
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
               info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
               info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
               
               info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"custom_categories"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"][@"custom_categories"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.details = subDic;
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
               info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
               info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
               
               info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self. cv_videos];
       } else if ([callType isEqualToString:@"shows_by_cast"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"][@"shows"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"resume_listening_audio"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"show_parent"][@"app_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"latest_audios"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"custom_carousel_radio"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadioShow = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"latest_audios_in_show"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"custom_audios"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"most_watched_radio"])
       {
           for (NSDictionary *subDic in responseObjct)
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.isRadioShow = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"radio_channels"])
       {
           for (NSDictionary *subDic in responseObjct)
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"shows_by_category"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"][@"shows"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:_imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [StaticData reloadCollectionView:self.cv_videos];
       } else if ([callType isEqualToString:@"recommendation_module"])
       {
           if (self.RecommendationModuleResultsDoundCallBlock) {
               self.RecommendationModuleResultsDoundCallBlock(responseObjct);
           }
       }
        
        [self checkRecordFounds];
    } @catch (NSException *exception) {
        [self checkRecordFounds];
    } @finally {
    }
}

-(void)checkRecordFounds
{
    if (self.contentList.count == 0) {
        self.heightConstraint.constant = 0.0;
        self.topConstraint.constant = 0.0;
        self.view.hidden = true;
    }
}

-(void)reloadCollectionData
{
    [StaticData reloadCollectionView:self.cv_videos];
}

@end
