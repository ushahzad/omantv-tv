//
//  LMHCarouselDataParentVC.m
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "LMHCarouselDataParentVC.h"

@interface LMHCarouselDataParentVC ()

@end

@implementation LMHCarouselDataParentVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@""];
    self.vc_tabbarVC.view.backgroundColor = [UIColor clearColor];
    
    _contentList = [NSMutableArray new];
    
    _pageNumber = 1;
    
    [self loadCustomCarouselsData];
}

-(void)loadCustomCarouselsData
{
    @try {
        self.lbl_title.text = [StaticData checkStringNull:self.carouselInfo[@"title_ar"]];
        
        NSString *type = [StaticData checkStringNull:_carouselInfo[@"type"]];
        
        NSString *api = [StaticData checkStringNull:self.carouselInfo[@"api"]];
        
        NSString *path = [NSString stringWithFormat:@"%@/%@&p=%d&limit=30",BaseURL,api,_pageNumber];
        
        [self executeApiCallToGetResponse:path withParameters:nil withCallType:type];
    } @catch (NSException *exception) {
        [StaticData log:@"ContentList"];
    } @finally {
    }
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    [self.indicator startAnimating];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.indicator stopAnimating];
        [self responseRecieved:responseObject withCallType:callType];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        @try {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        } @catch (NSException *exception) {
        } @finally {
        }
    }];
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        NSInteger total = _contentList.count;
        
        NSString *imageAspectRatio = [StaticData checkStringNull:self.carouselInfo[@"image_aspect_ratio_tvapp"]];
        
        if ([callType isEqualToString:@"shows_by_cast"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                
                info.title = [StaticData title:subDic];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                }
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                info.carouselType = callType;
                
                [_contentList addObject:info];
            }
            
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"resume_watching"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];;
                info.isVideo = true;
                
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                
                info.position = [StaticData checkStringNull:subDic[@"position"]];
                info.duration = [StaticData checkStringNull:subDic[@"duration"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                info.carouselType = callType;
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                [_contentList addObject:info];
            }
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"latest_episode"])
        {
            for (NSDictionary *subDic in responseObjct)
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.season_title = [StaticData checkStringNull:subDic[@"parent_title"]];
                info.isVideo = true;
                
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                
                info.carouselType = callType;
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                [_contentList addObject:info];
            }
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"custom_videos"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.season_title = [StaticData checkStringNull:subDic[@"parent_title"]];
                info.isVideo = true;
                
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                
                info.carouselType = callType;
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                [_contentList addObject:info];
            }
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"latest_videos_in_show"])
        {
            for (NSDictionary *subDic in responseObjct[@"videos"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:subDic[@"id"]];
                info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                info.season_title = [StaticData checkStringNull:subDic[@"parent_title"]];
                info.isVideo = true;
                
                info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                
                info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                
                if (info.thumbnailPath.length == 0)
                {
                    info.thumbnailPath = [StaticData checkStringNull:subDic[@"img"]];
                }
                
                info.carouselType = callType;
                
                NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                [_contentList addObject:info];
            }
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"custom_carousel"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"yes"])
                {
                    MediaInfo *info = [MediaInfo new];
                    info.ID = [StaticData checkStringNull:subDic[@"id"]];
                    //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                    info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                    
                    info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                    
                    info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                    
                    if (info.thumbnailPath.length == 0)
                    {
                        info.thumbnailPath = [StaticData checkStringNull:subDic[@"app_thumbnail"]];
                        if ([info.thumbnailPath length] == 0)
                        {
                            info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                        }
                    }
                    
                    NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                    if (labelsArray.count > 0) {
                        NSDictionary *labelDic = labelsArray[0];
                        info.signLogoPath = labelDic[@"image"];
                    }
                    
                    [_contentList addObject:info];
                }
            }
            
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"most_watched"])
        {
            for (NSDictionary *subDic in responseObjct)
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"yes"])
                {
                    MediaInfo *info = [MediaInfo new];
                    info.ID = [StaticData checkStringNull:subDic[@"id"]];
                    //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                    info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                    
                    info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                    
                    info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                    
                    if (info.thumbnailPath.length == 0)
                    {
                        info.thumbnailPath = [StaticData checkStringNull:subDic[@"app_thumbnail"]];
                        if ([info.thumbnailPath length] == 0)
                        {
                            info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                        }
                    }
                    
                    NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                    if (labelsArray.count > 0) {
                        NSDictionary *labelDic = labelsArray[0];
                        info.signLogoPath = labelDic[@"image"];
                    }
                    
                    [_contentList addObject:info];
                }
            }
            
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"favorite_carousel"])
        {
            for (NSDictionary *subDic in responseObjct[@"data"])
            {
                if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"yes"])
                {
                    MediaInfo *info = [MediaInfo new];
                    info.ID = [StaticData checkStringNull:subDic[@"id"]];
                    //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                    info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                    
                    info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                    
                    info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                    
                    if (info.thumbnailPath.length == 0)
                    {
                        info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                    }
                    
                    NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                    if (labelsArray.count > 0) {
                        NSDictionary *labelDic = labelsArray[0];
                        info.signLogoPath = labelDic[@"image"];
                    }
                    
                    [_contentList addObject:info];
                }
            }
            
            [self reloadCollectionData];
        } else if ([callType isEqualToString:@"live_channels"] || [callType isEqualToString:@"all_channels"])
       {
           for (NSDictionary *chDic in responseObjct)
           {
               NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
               if ([enableLive isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
               {
                   continue;
               }
               
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:chDic[@"id"]];
               //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
               info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
               info.carouselType = callType;
               
               info.exclusive = [StaticData checkStringNull:chDic[@"exclusive"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:chDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.thumbnail = [StaticData checkStringNull:chDic[@"icon"]];
               if (info.thumbnail.length == 0)
               {
                   info.thumbnail = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.itemType = @"home";
               info.carouselType = callType;
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"custom_live_channels"])
       {
           for (NSDictionary *chDic in responseObjct[@"data"])
           {
               NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
               if ([enableLive isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               
               if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
               {
                   continue;
               }
               
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:chDic[@"id"]];
               //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
               info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
               info.carouselType = callType;
               
               info.exclusive = [StaticData checkStringNull:chDic[@"exclusive"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:chDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.thumbnail = [StaticData checkStringNull:chDic[@"icon"]];
               if (info.thumbnail.length == 0)
               {
                   info.thumbnail = [StaticData checkStringNull:chDic[@"thumbnail"]];
               }
               
               info.itemType = @"home";
               info.carouselType = callType;
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"original_content"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"yes"])
               {
                   MediaInfo *info = [MediaInfo new];
                   info.ID = [StaticData checkStringNull:subDic[@"id"]];
                   //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                   info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
                   
                   info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
                   
                   info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
                   
                   if (info.thumbnailPath.length == 0)
                   {
                       info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
                   }
                   
                   NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
                   if (labelsArray.count > 0) {
                       NSDictionary *labelDic = labelsArray[0];
                       info.signLogoPath = labelDic[@"image"];
                   }

                   [_contentList addObject:info];
               }
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"categories"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"][@"categories"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.details = subDic;
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
               
               info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
               info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
               
               info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"custom_categories"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.details = subDic;
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
               
               info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               info.imgShowLogoPath = [StaticData checkStringNull:subDic[@"icon"]];
               info.atvImagPath = [StaticData checkStringNull:subDic[@"cover"]];
               
               info.descriptions = [StaticData checkStringNull:subDic[@"description_ar"]];
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"latest_audios"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"custom_carousel_radio"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadioShow = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"latest_audios_in_show"])
       {
           for (NSDictionary *subDic in responseObjct[@"audios"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               info.title = [StaticData checkStringNull:subDic[@"title_ar"]];
               
               info.exclusive = [StaticData checkStringNull:subDic[@"exclusive"]];
               
               NSDictionary *show = [StaticData checkDictionaryNull:subDic[@"show"]];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:show];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"custom_audios"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"parent_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"resume_listening_audio"])
       {
           for (NSDictionary *subDic in responseObjct[@"data"])
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"show_parent"][@"app_thumbnail"]];
               }
               
               info.isRadio = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"most_watched_radio"])
       {
           for (NSDictionary *subDic in responseObjct)
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.isRadioShow = true;
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"radio_channels"])
       {
           for (NSDictionary *subDic in responseObjct)
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       } else if ([callType isEqualToString:@"shows_by_category"])
       {
           for (NSDictionary *subDic in responseObjct)
           {
               if ([[StaticData checkStringNull:subDic[@"publish"]] isEqualToString:@"no"])
               {
                   continue;
               }
               MediaInfo *info = [MediaInfo new];
               info.ID = [StaticData checkStringNull:subDic[@"id"]];
               
               info.title = [StaticData title:subDic];
               
               info.thumbnailPath = [StaticData getBlockThumbnailWithAspectRatioType:imageAspectRatio WithCarousel:subDic];
               
               if (info.thumbnailPath.length == 0)
               {
                   info.thumbnailPath = [StaticData checkStringNull:subDic[@"thumbnail"]];
               }
               
               info.carouselType = callType;
               
               NSArray *labelsArray = [StaticData checkArrayNull:subDic[@"labels"]];
               if (labelsArray.count > 0) {
                   NSDictionary *labelDic = labelsArray[0];
                   info.signLogoPath = labelDic[@"image"];
               }
               
               [_contentList addObject:info];
           }
           
           [self reloadCollectionData];
       }
        
        self.isNeedToLoadMorePrograms = true;
        
        if (total == _contentList.count) {
            self.isNeedToLoadMorePrograms = false;
        }
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)reloadCollectionData
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(NSArray *)preferredFocusEnvironments
{
    return @[self.collectionView];
}

- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight+10)
        {
            // then we are at the end
            NSLog(@"reload");
            if (self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = false;
                _pageNumber += 1;
                
                //[self setupTableFooterView];
                
                [self loadCustomCarouselsData];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

@end
