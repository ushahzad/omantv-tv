//
//  LoadMoreHomeCarouselDataVC.h
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMHCarouselDataParentVC.h"
#import "LiveChannelPlayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMHCarouselDataVC : LMHCarouselDataParentVC
@end

NS_ASSUME_NONNULL_END
