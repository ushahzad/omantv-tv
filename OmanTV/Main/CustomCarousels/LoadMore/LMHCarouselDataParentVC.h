//
//  LMHCarouselDataParentVC.h
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LMHCarouselDataParentVC : UIViewController
@property void(^AudioClickCallBack)(void);
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *parentRightConstraint;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noResutlsFound;
@property (nonatomic, retain) NSDictionary *carouselInfo;
@property (nonatomic, retain) UIImage *tempImage;
@property(nonatomic,retain) NSMutableArray *contentList;
@property (nonatomic, weak) NSString *tabType;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) int totalVideos;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isMovie;
-(void)loadCustomCarouselsData:(NSDictionary *)carousel;
-(void)reloadCollectionData;
@end

NS_ASSUME_NONNULL_END
