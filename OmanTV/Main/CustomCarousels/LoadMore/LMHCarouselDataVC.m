//
//  LoadMoreHomeCarouselDataVC.m
//  ADtv
//
//  Created by MacUser on 23/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "LMHCarouselDataVC.h"
#import "TVSubCategoriesVC.h"

@interface LMHCarouselDataVC ()

@end

@implementation LMHCarouselDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentList = [NSMutableArray new];

    self.lbl_noResutlsFound.hidden = true;
    self.lbl_noResutlsFound.text = NoRecordFound;
    
    [StaticData scaleToArabic:self.collectionView];
    NSString *title = [StaticData checkStringNull:self.carouselInfo[@"title_ar"]];
    self.lbl_title.text = title;
    //NSString *type = [StaticData checkStringNull:self.carouselInfo[@"type"]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 450) / 4;
    CGFloat height = 300;
    CGFloat titleHeight = 50.0;
    
    NSString *imageAspectRatio = [StaticData checkStringNull:self.carouselInfo[@"image_aspect_ratio_tvapp"]];
    if ([imageAspectRatio isEqualToString:@"v"])
    {
        width = 256;
        height = 359;
        
        height += titleHeight;
        
        return CGSizeMake(width, height);
    } else if ([imageAspectRatio isEqualToString:@"h"])
    {
        width = 360;
        height = 201;
        
        height += titleHeight;
        
        return CGSizeMake(width, height);
    }
    
    if (self.isMovie)
    {
        //width = 170;
        height = 450;
    } else
    {
        if (self.tempImage) {
            CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
            height = ratio * width;
        }
    }
    
    height += titleHeight;
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (self.contentList.count == 0)
    {
        [cell.indicator startAnimating];
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    cell.info = info;
    cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
    
    if ([info.carouselType isEqualToString:@"live_channels"] || [info.carouselType isEqualToString:@"custom_live_channels"] || [info.carouselType isEqualToString:@"all_channels"]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell_live" forIndexPath:indexPath];
        @try {
            NSString *encodedCover = [info.thumbnail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
            [cell.indicator startAnimating];
            cell.img_highlight.contentMode = UIViewContentModeScaleAspectFill;
            [cell.img_highlight sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                     /*if (!self.tempImage) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }*/
                     [cell.indicator stopAnimating];
                 } else {
                     [StaticData downloadImageIfNotLoadinedBySdImage:cell.img_highlight withUrl:imageURL completed:^(UIImage *image) {
                         /*if (!self.tempImage && image) {
                             self.tempImage = image;
                             [self reloadCollectionView];
                         }*/
                         [cell.indicator stopAnimating];
                     }];
                 }
             }];
        } @catch (NSException *exception) {
        } @finally {
        }
        
        //[StaticData blurEffectsOnView:cell.view_blur withEffectStyle:UIBlurEffectStyleLight];
            
        cell.view_blur.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        cell.img_highlight.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
        
        cell.view_blur.layer.cornerRadius = 12.0;
        cell.view_blur.clipsToBounds = true;
        
        cell.img_highlight.layer.cornerRadius = 12.0;
        cell.img_highlight.clipsToBounds = true;
    }
    
    if ([info.carouselType isEqualToString:@"latest_audios_in_show"] || [info.carouselType isEqualToString:@"custom_audios"]) {
        info.isRadio = true;
    }
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:info.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [cell.indicator startAnimating];
        
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionData];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    [cell.signLanguageLogo setHidden:true];
    @try {
        NSString *encodedCover = [info.signLogoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        cell.signLanguageLogo.contentMode = UIViewContentModeScaleAspectFit;
        [cell.signLanguageLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [cell.signLanguageLogo setHidden:false];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    cell.thumbnail.layer.cornerRadius = RoundeCorner;
    cell.view_info.layer.cornerRadius = RoundeCorner;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.contentList.count == 0) return;
        
        MediaInfo *info = self.contentList[indexPath.row];
        if ([info.carouselType isEqualToString:@"live_channels"] || [info.carouselType isEqualToString:@"custom_live_channels"] || [info.carouselType isEqualToString:@"all_channels"])
        {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            vc.selectedChannel = info;
        //    vc.isRadio = self.isRadio;
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.carouselType isEqualToString:@"radio_channels"] ) {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            vc.selectedChannel = info;
        //    vc.isRadio = self.isRadio;
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.carouselType isEqualToString:@"categories"] || [info.carouselType isEqualToString:@"custom_categories"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Categories" bundle:nil];
            TVSubCategoriesVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TVSubCategoriesVC"];
            vc.categoryList = self.contentList;
            vc.selectedCategory = info;
            [self.navigationController pushViewController:vc animated:true];
        } else if (info.isVideo)
        {
            [StaticData openVideoPage:info withVC:self];
        } else if (info.isRadio) {
            AudioPlayer *audioPlayer = [AudioPlayer shared];
            [audioPlayer playAudioWithInfo:info withParentVC:self];
            if (self.AudioClickCallBack) {
                self.AudioClickCallBack();
            }
        } else if (info.isRadioShow) {
            [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
        } else {
            MediaInfo *info = self.contentList[indexPath.row];
            [StaticData openShowDetailsPageWith:self withMediaInfo:info];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)reloadCollectionData
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

@end
