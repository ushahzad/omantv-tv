//
//  CustomCarousels.h
//  awaan-ios
//
//  Created by MacUser on 26/10/2020.
//  Copyright © 2020 Ali Raza. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetupCustomCarousels : NSObject

@property void(^CustomCarouselDataReceivedCallBack)(NSMutableArray *data, NSString *title,NSDictionary *crouselData);

@property void(^CustomCarouselFinishedCallBack)(void);

@property void(^LoadApiCustomCarouselCallBack)(NSDictionary *crouselData);

@property void(^LoadRecomendedCustomCarouselCallBack)(NSMutableArray *data, NSString *title,NSDictionary *crouselData, NSString *carouselType);

@property (nonatomic, retain) NSString *selectedGenreID;
@property (nonatomic, assign) int tryToLoadAgainContentCount;

-(void)loadCustomCarousels;
-(void)loadRecomendedCarouselsData:(id)jsonResponse;
@end

NS_ASSUME_NONNULL_END
