//
//  NowOnTVVC.m
//  OmanTV
//
//  Created by MacUser on 13/05/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "NowOnTVVC.h"

@interface NowOnTVVC ()

@end

@implementation NowOnTVVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.contentList = [NSMutableArray new];

    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadDataFromServer];
}

-(void)loadDataFromServer
{
    [self resetData];
    
    if (self.isRadio) {
        self.lbl_title.text = @"الصوتيات";
    }  else {
//        self.lbl_title.text = @"شاشة المرئي";
        self.lbl_title.text = @"المرئيات";
    }
    
    if (self.isRadio) {
        [self loadRadioDataFromServer];
    } else {
        [self loadTVDataFromServer];
    }
}

-(void)loadTVDataFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand/latest?user_id=%@&key=%@&start=0&end=6&type=",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)loadRadioDataFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&t=1641388995&&scope=audio&action=latest_audio&channel_id=0&p=1&limit=20&exclude_channel_id=%@&need_channel_info=yes",BaseURL, BaseUID,BaseKEY,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)resetData
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
         [self checkRecordFound];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in self.isRadio ? responseObject[@"data"] : responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
            
                info.title = [StaticData title:dicObj];
                info.isRadio = self.isRadio;
                info.channelID = [StaticData checkStringNull:dicObj[@"channel_id"]];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                if (self.isRadio) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"parent_thumbnail"]];
                    if (info.thumbnailPath.length == 0 || [info.thumbnailPath isEqualToString:@"0"]) {
                        info.thumbnailPath = [StaticData checkStringNull:dicObj[@"channel_thumbnail"]];
                    }
                }
                
                info.date = [StaticData getForamttedDate:[StaticData checkStringNull:dicObj[@"create_time"]]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(void)checkRecordFound
{
    self.view.hidden = false;
    self.heightConstraint.constant = 300.0;
    if (self.contentList.count == 0) {
        self.heightConstraint.constant = 0.0;
        self.view.hidden = true;
    }
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
        
        _heightConstraint.constant = height+50+80;
        
        if (self.UpdateContentSize)
        {
            self.UpdateContentSize();
        }
    }
    
    return CGSizeMake(width, height + 50);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        cell.thumbnail.image = nil;
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:_isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.contentList.count == 0) return;
    
    MediaInfo *info = self.contentList[indexPath.row];
    if (self.isRadio)
    {
        AudioPlayer *audioPlayer = [AudioPlayer shared];
        [audioPlayer playAudioWithInfo:info withParentVC:self.parentViewController];
        if (self.AudioClickCallBack) {
            self.AudioClickCallBack();
        }
    } else {
        [StaticData openVideoPage:info withVC:self];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

@end
