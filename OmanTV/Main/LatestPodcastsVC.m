//
//  LatestPodcastsVC.m
//  OmanTV
//
//  Created by Curiologix on 19/04/2022.
//  Copyright © 2022 MacUser. All rights reserved.
//

#import "LatestPodcastsVC.h"

@interface LatestPodcastsVC ()

@end

@implementation LatestPodcastsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.contentList = [NSMutableArray new];
    self.allShowsList = [NSMutableArray new];
    self.channelsList = [NSMutableArray new];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    //[self.collectionView registerNib:[UINib nibWithNibName:isEnglishLang ? @"CollectionCellEN" : @"CollectionCellAR" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    if (!isEnglishLang) {
        self.lbl_title.text = @"البرنامج السمعي";
        self.lbl_title.textAlignment = NSTextAlignmentRight;
    }
    [StaticData scaleToArabic:self.collectionView];
    
    if (self.isRadio) {
//        self.lbl_title.text = isEnglishLang ? @"RADIO PROGRAMS" : @"برامج إذاعية";
        [self loadTVAndRadioDataFromServer];
    }
}

-(void)loadTVAndRadioDataFromServer
{
    [self.allShowsList removeAllObjects];
    [self.contentList removeAllObjects];
    [self reloadCollectionView];

    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&t=1650352077&&scope=audio&action=latest_audio&channel_id=247&p=1&limit=10&exclude_channel_id=%@&t=1650352079",BaseURL, BaseUID,BaseKEY,ExcludeChannelId];

    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)resetData
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
//    [StaticData setSSLCertificate:manager];
    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject[@"data"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"category_channel_id"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"cat_thumbnail"]];
                
                if ([dicObj[@"is_radio"] boolValue] == true) {
                    info.isRadio = true;
                }
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_allShowsList addObject:info];
                }
            }
            
            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        [self checkRecordFound];
    } @finally {
    }
}

-(void)findChannelLogoPath:(MediaInfo *)info
{
    for (MediaInfo *channel in self.channelsList)
    {
        if ([channel.ID isEqualToString:info.channelID])
        {
            info.channelLogoPath = channel.channelLogoPath;
        }
    }
}

-(void)checkRecordFound
{
    self.view.hidden = false;
    self.heightConstraint.constant = 300.0;
    if (self.allShowsList.count == 0) {
        self.view.hidden = true;
        self.heightConstraint.constant = 0.0;
    } else {
        for (int i = 0; i < _allShowsList.count; i++)
        {
//            int randomNumber = arc4random_uniform((int)_allShowsList.count);
//            if (randomNumber <= self.allShowsList.count)
//            {
//                [self.contentList addObject:_allShowsList[randomNumber]];
//            }
//            if (self.contentList.count >= 10) {
//                break;
//            }
            
            [self.contentList addObject:_allShowsList[i]];
        }
    }
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
        
        if (self.UpdateContentSize)
        {
            self.UpdateContentSize();
        }
    }
    
    _heightConstraint.constant = height + 50 + 150;
    
    return CGSizeMake(width, height + 50);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count == 0 ? 10 : self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        cell.thumbnail.image = nil;
        [cell.indicator startAnimating];
        cell.channelLogo.image = nil;
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    cell.lbl_date.text = info.date;
    cell.view_info.hidden = false;
    
    if (!isEnglishLang) {
        cell.lbl_title.textAlignment = NSTextAlignmentRight;
    }
    
    [self loadChannelLogo:cell withInfo:info];
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    //ssssss
//    if ([info.exclusive isEqualToString:@"yes"]) {
//        [cell.view_exclusive setHidden:false];
//        CAShapeLayer * maskLayer = [CAShapeLayer layer];
//        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: cell.view_exclusive.bounds byRoundingCorners: UIRectCornerTopLeft cornerRadii: (CGSize){12.0}].CGPath;
//
//        cell.view_exclusive.layer.mask = maskLayer;
//    }
//    else {
//        [cell.view_exclusive setHidden:true];
//    }
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.contentList.count == 0) return;
    
    MediaInfo *info = self.contentList[indexPath.row];
    AudioPlayer *audioPlayer = [AudioPlayer shared];
    [audioPlayer playAudioWithInfo:info withParentVC:self.parentViewController];
    if (self.AudioClickCallBack) {
        self.AudioClickCallBack();
    }
}

-(void)loadChannelLogo:(CollectionCell *)cell withInfo:(MediaInfo *)info
{
    @try {
        NSString * encodedCover = [info.channelLogoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.channelLogo.contentMode = UIViewContentModeScaleAspectFit;
        [cell.channelLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.channelLogo withUrl:imageURL completed:^(UIImage *image) {
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

@end
