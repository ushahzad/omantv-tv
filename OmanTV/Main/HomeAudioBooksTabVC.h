//
//  HomeAudioBooksTabVC.h
//  OmanTV
//
//  Created by Cenex Studio on 25/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeAudioBooksTabVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^ChooseAudioBookCallBack)(MediaInfo *info);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, strong) NSLayoutConstraint *heightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *topConstraint;

@property (nonatomic, strong) NSMutableArray *audioTabsArray;
@property (nonatomic) int selectedIndex;

@end

NS_ASSUME_NONNULL_END
