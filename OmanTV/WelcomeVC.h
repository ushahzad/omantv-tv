//
//  WelcomeVC.h
//  DubaiTV
//
//  Created by Curiologix on 01/02/2018.
//  Copyright © 2018 Ali Raza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UIViewController
@property void (^VideoPlayingFinishedCallBack)(void);
@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayerLayer *playerLayer;
@property (weak, nonatomic) IBOutlet UIView *playerView;

@property (retain, nonatomic) id appVersionInfo;
@property (assign, nonatomic) BOOL videoPlayingFinished;
@property (assign, nonatomic) BOOL checkingAppUpdateFinished;

@end
