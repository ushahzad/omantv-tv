//
//  FeaturedShowsVC.h
//  AbuDhabiAppleTV
//
//  Created by MacUser on 16/01/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeBanner : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
typedef void(^LoadFirstTimeImageCompletionBlock)(UIImage *image);
@property void(^UpdateContentSize)(void);
@property void(^HomeBannerSwipeUpCallBack)(void);
@property void(^LoadFirstItemCallBack)(MediaInfo *info);
@property void(^FocuseUpdateCallBack)(MediaInfo *info);
@property void(^FocuseOutUpdateCallBack)(void);
@property void(^FeaturedShowsNotFound)(void);

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIView *view_play;
@property (nonatomic, weak) IBOutlet UIView *view_right_arrow;
@property (nonatomic, weak) IBOutlet UIView *view_left_arrow;
@property (nonatomic, weak) IBOutlet UIView *view_info;
@property (nonatomic, weak) IBOutlet UIButton *btn_play;

@property (nonatomic, weak) IBOutlet UIButton *btn_right_arrow;
@property (nonatomic, weak) IBOutlet UIButton *btn_left_arrow;

@property (nonatomic, weak) IBOutlet UIImageView *img_right_arrow;
@property (nonatomic, weak) IBOutlet UIImageView *img_left_arrow;

@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_subTitle;

@property (nonatomic, retain) MediaInfo *selectedShow;

@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) UIImage *tempImage;
-(void)loadDataFromServer;
@end

NS_ASSUME_NONNULL_END
