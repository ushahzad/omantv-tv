//
//  ShowEpisodesVC.h
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShowEpisodesVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property void(^UpdateContentSize)(void);
@property void(^SeasonChoosedCallBack)(MediaInfo *info);
@property void(^NextFocuseCallBack)(void);

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_seasons;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noEpisodeFound;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;

@property (nonatomic, weak) IBOutlet UIView *view_dropdown;
@property (nonatomic, weak) IBOutlet UILabel *lbl_dropdown;
@property (nonatomic, weak) IBOutlet UIButton *btn_dropdown;
@property (nonatomic, weak) IBOutlet UIImageView *img_dropdown;

@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *seasonsList;
@property (nonatomic, retain) id showDetails;
@property (nonatomic, retain) MediaInfo *selectedSeason;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) MediaInfo *infoAudio;
@property (nonatomic, retain) NSString *selectedSeasonId;
@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) NSMutableString *sessionID;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) int resumeVideoPosition;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;

@property (nonatomic, strong) NSTimer *updateOlineTimer;
-(void)getPlayBackUrl:(MediaInfo *)info;
-(void)setupAllVideos;
@end

NS_ASSUME_NONNULL_END
