//
//  ShowDetailsVC.h
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "LoginVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsVC : UIViewController
@property void(^PlayerStartAndStopCallBack)(BOOL isStart);
@property void(^UpdateContentSize)(void);
@property void(^SeasonChoosedCallBack)(MediaInfo *info);
@property void(^PlayFirstShowVideoCallBack)(void);
@property void(^LoadShowImageCallBack)(NSString *showImagePath);
@property void(^SetupSeasonCallBack)(NSMutableArray *seasonList , MediaInfo *selectedSeason);
@property void(^NextFocuseCallBack)(void);

@property (nonatomic, weak) IBOutlet UIImageView *img_show;
@property (nonatomic, weak) IBOutlet UIImageView *img_showLogo;
@property (nonatomic, weak) IBOutlet UIImageView *img_exclusive;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *img_showLogoHeightConstraint;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_fav;
@property (nonatomic, weak) IBOutlet UILabel *lbl_watchLater;
@property (nonatomic, weak) IBOutlet UILabel *lbl_season;
@property (nonatomic, weak) IBOutlet UILabel *lbl_descriptions;
@property (nonatomic, weak) IBOutlet UILabel *lbl_actores;
@property (nonatomic, weak) IBOutlet UILabel *lbl_season_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_watchNow;

@property(strong,nonatomic)IBOutlet HCSStarRatingView *ratingView;
@property (nonatomic, weak) IBOutlet UILabel *lbl_rating;

@property (nonatomic, weak) IBOutlet UIButton *btn_add;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UIView *view_rating;
@property (nonatomic, weak) IBOutlet UIView *view_details;
@property (nonatomic, weak) IBOutlet UIView *view_openShow;
@property (nonatomic, weak) IBOutlet UIView *view_makeFavShow;
@property (nonatomic, weak) IBOutlet UIView *view_watchLaterShow;
@property (nonatomic, weak) IBOutlet UIView *view_interpretation;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *showImageWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (nonatomic, retain) MediaInfo *selectedSeason;
@property (nonatomic, retain) NSMutableArray *seasonsList;
@property (nonatomic, retain) id showDetails;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *castHeightConstraint;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) MediaInfo *infoAudio;
@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) NSMutableString *sessionID;

@property (nonatomic, strong) NSTimer *updateOlineTimer;

@property (nonatomic, weak) IBOutlet UIButton *btn_close;
@property (nonatomic, weak) IBOutlet UIButton *btn_volume;
@property (nonatomic, weak)IBOutlet UIButton *btn_play;
@property (nonatomic, weak)IBOutlet UIButton *btn_rating;
@property (nonatomic, weak) IBOutlet UIButton *btn_fav;
@property (nonatomic, weak) IBOutlet UIImageView *img_fav;

@property (nonatomic, weak) IBOutlet UIButton *btn_watchLater;
@property (nonatomic, weak) IBOutlet UIImageView *img_watchLater;


@property (nonatomic, retain) NSString *selectedShowID;
@property (nonatomic, retain) NSString *shorterURL;
@property (nonatomic, retain) NSString *showImagePath;
@property (nonatomic, retain) NSMutableArray *castList;

@property (nonatomic, assign) BOOL observingMediaPlayer;
@property (nonatomic, strong) id playbackObserver;
@property (nonatomic, assign) BOOL viewIsActive;
@property (nonatomic, assign) BOOL isTrailerLoaded;
@property (nonatomic, assign) BOOL isTrailerPlayed;
@property (nonatomic, assign) BOOL isFav;
@property (nonatomic, assign) BOOL isWatchLater;
@property (nonatomic, assign) BOOL isRadio;
-(void)setupShow;
@end

NS_ASSUME_NONNULL_END
