//
//  ShowEpisodesVC.m
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ShowEpisodesVC.h"

@interface ShowEpisodesVC ()

@end

@implementation ShowEpisodesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentList = [NSMutableArray new];

    _pageNumber = 1;
    
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.cv_seasons.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(iOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
        [self.cv_seasons setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.view_dropdown.layer.cornerRadius = 35.0;
    self.view_dropdown.clipsToBounds = true;
    
    self.lbl_dropdown.textColor = [UIColor whiteColor];
    self.img_dropdown.image = [StaticData changeImageColor:[UIImage imageNamed:@"dropdown"] withColor:[UIColor whiteColor]];
    self.view_dropdown.backgroundColor = [UIColor clearColor];
    self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
    self.view_dropdown.layer.borderWidth = 1.0;
    
    self.lbl_dropdown.text = self.selectedSeason.title;
    
    [StaticData scaleToArabic:self.collectionView];
    [StaticData scaleToArabic:self.cv_seasons];
    
    [self setupTriggerActionOnButtons];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
}

- (void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    @try {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight)
        {
            // then we are at the end
            if (!self.isNeedToLoadMorePrograms) {
                
                self.isNeedToLoadMorePrograms = true;
                _pageNumber += 1;
                
                //[self setupTableFooterView];
                
                [self executeApiCallForShows];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

-(void)executeApiCallForShows
{
    @try {
        NSString *path = [NSString stringWithFormat:@"%@/plus/show?user_id=%@&key=%@&show_id=%@&channel_userid=&limit_also=3&cast=yes&p=%d&limit=20&need_avg_rating=yes&need_channels=yes&all_fav_type_on_show_videos=yes",BaseURL,BaseUID,BaseKEY,self.selectedSeasonId, self.pageNumber];
        [self executePostApiToGetResponse1:path parameters:nil callType:@"ShowsInfo"];
        
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

#pragma mark - Load Categories Request
// make web service api call
-(void)executePostApiToGetResponse1:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"] || [type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"Response Received = %@",responseObject);
         if ([type isEqualToString:@"ShorterUrl"])
         {
             responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
         }
         [self responseReceived1:responseObject callType:type];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
        
         NSLog(@"Api Error: %@",error.description);
     }];
}

-(void)responseReceived1:(id) response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"ShowsInfo"])
        {
            self.showDetails = response;
            
            [self setupAllVideos];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_dropdown];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_dropdown)
        {
            [self openCategories];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)openCategories
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (MediaInfo *info in self.seasonsList)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:info.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.selectedSeason = info;
            if (self.SeasonChoosedCallBack) {
                self.SeasonChoosedCallBack(info);
            }
        }];
        [as addAction:action];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:Cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [as addAction:cancel];
    
    [self presentViewController:as animated:TRUE completion:nil];
}

-(void)setupAllVideos
{
    @try {
        NSDictionary *category = [StaticData checkDictionaryNull:_showDetails[@"cat"]];
        NSString *showTitle = [StaticData checkStringNull:category[@"title_ar"]];
        
        NSInteger totalResults = self.contentList.count;
        int lastIndex = -1;
        if (self.contentList.count > 0) {
            lastIndex = (int)self.contentList.count;
        }
        
        for (NSDictionary *dicObj in self.isRadio ? _showDetails[@"audio"] : _showDetails[@"videos"])
        {
            if ([dicObj[@"publish"] isEqualToString:@"yes"])
            {
                MediaInfo *info = [MediaInfo new];
                info.ID = dicObj[@"id"];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                info.isVideo = true;
                info.isRadio = self.isRadio;
                if ([showTitle isEqualToString:info.title]) {
                    info.title = @"";
                }
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                if (info.thumbnailPath.length == 0 && self.isRadio) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"cat_thumbnail"]];
                }
                info.date = [StaticData getForamttedDate:[StaticData checkStringNull:dicObj[@"recorder_date"]]];
                
                NSString *tags = [StaticData checkStringNull:dicObj[@"tags"]];
                NSArray *tagList = [tags componentsSeparatedByString:@","];
                NSString *episodeTag = @"";
                if (tagList.count > 0) {
                    for (NSString *tag in tagList) {
                        if ([tag containsString:@"ep-"]) {
                            episodeTag = [tag stringByReplacingOccurrencesOfString:@"ep-" withString:@""];
                            info.episodeCount = episodeTag;
                        }
                    }
                }
                
                if (![tags isEqualToString:@"trailer"]) {
                    [self.contentList addObject:info];
                }
                
                //if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                //{
//                    [self.contentList addObject:info];
                //}
            }
        }
        
        self.isNeedToLoadMorePrograms = true;
        if (totalResults != self.contentList.count)
        {
            self.isNeedToLoadMorePrograms = false;
        }
        
        if (lastIndex > 0)
        {
            NSMutableArray *indexPaths = [NSMutableArray new];
            for (int i = lastIndex; i < self.contentList.count; i++)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                [indexPaths addObject:indexPath];
            }
            [self.collectionView insertItemsAtIndexPaths:indexPaths];
        } else
        {
            [self checkEpisodesFound];
        }
        
//        [self checkEpisodesFound];
        
//        if (self.seasonsList.count > 0) {
//            [self setupSelectedTypeContent:self.seasonsList[0]];
//        }
//        [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)setupSelectedTypeContent:(MediaInfo *)contentType
{
    self.tempImage = nil;
    [self reloadCollectionView];
}

-(void)checkEpisodesFound
{
    if (self.contentList.count == 0) {
        self.lbl_title.hidden = false;
        self.heightConstraint.constant = 0.0 ;
        self.topConstraint.constant = 0.0;
    }
    
    [self reloadCollectionView];
    [self performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    if (collectionView == self.cv_seasons)
    {
        return CGSizeMake(75, 75);
    }
    
    CGFloat width = (frame.size.width - 60)/ 6;
    CGFloat height = width;
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + (info.isVideo ? 80 : 0);
    }
    
    CGFloat val = self.contentList.count / 4.0;
    val = ceil(val);
    CGFloat constant = (height  * val +  (self.contentList.count / 4.0) * 10);

//    if (constant < 700) {
//        constant = 1080;
//    }
//    self.heightConstraint.constant = constant + 70.0;
    
    if (self.UpdateContentSize) {
        self.UpdateContentSize();
    }
    return CGSizeMake(width, height);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.cv_seasons)
    {
        return self.seasonsList.count;
    }
    
    return _contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
 
    if (collectionView == self.cv_seasons)
    {
        CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        MediaInfo *info = self.seasonsList[indexPath.row];
        
        cell.info = info;
        cell.lbl_title.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
        
        if (info.isSelected) {
            cell.contentView.layer.borderColor = AppColor.CGColor;
        } else {
            cell.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        
        cell.contentView.layer.borderWidth = 1.0;
        cell.contentView.layer.cornerRadius = 8.0;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.info = info;
    
    if (info.isVideo) {
        if (info.title.length > 0)
        {
            cell.lbl_title.text = info.title;
            if (info.episodeCount.length > 0) {
                cell.lbl_title.text = [NSString stringWithFormat:@"%@  الحلقة %@",info.title,info.episodeCount];
            }
        } else
        {
            if (info.episodeCount.length > 0) {
                cell.lbl_title.text = [NSString stringWithFormat:@"الحلقة %@",info.episodeCount];
            }
        }
        
//        [StaticData addSpaceInLabelText:cell.lbl_title];
        
        cell.lbl_time.text = info.date;
    }
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:_isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 if (!self.tempImage) {
                     self.tempImage = [StaticData placeHolder];
                     [self reloadCollectionView];
                 }
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_seasons) {
        if (collectionView == self.cv_seasons)
        {
            MediaInfo *info = self.seasonsList[indexPath.row];
            self.selectedSeason = info;
            if (self.SeasonChoosedCallBack) {
                self.SeasonChoosedCallBack(info);
            }
        }
        return;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    if (self.isRadio)
    {
        [self getPlayBackUrl:info];
    } else
    {
        [StaticData openVideoPage:info withVC:self];
    }
    
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.cv_seasons];
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_seasons)
    {
        CollectionCell *cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.nextFocusedIndexPath];
        if (cell)
        {
            if (self.NextFocuseCallBack) {
                self.NextFocuseCallBack();
            }
            cell.contentView.layer.borderColor = AppColor.CGColor;
        }
        
        cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.previouslyFocusedIndexPath];
        if (cell)
        {
            cell.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
            MediaInfo *info = self.seasonsList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.contentView.layer.borderColor = AppColor.CGColor;
            }
        }
        
        cell.contentView.layer.borderWidth = 1.0;
        cell.contentView.layer.cornerRadius = 8.0;
    } else
    {
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == self.collectionView)
        {
            if (self.NextFocuseCallBack) {
                self.NextFocuseCallBack();
            }
        }
        
        [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    }
}


-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_dropdown)
        {
            self.view_dropdown.layer.borderColor = AppColor.CGColor;
            self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
         if (sender == self.btn_dropdown) {
             self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
             self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
}


-(void)getPlayBackUrl:(MediaInfo *)info
{
    [self.loading_indicator startAnimating];
    
    _selectedShow = info;
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=%@&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,info.ID,[StaticData findeLogedUserID]];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
}

-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"RadioPlayBackUrl"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            if ([type isEqualToString:@"ShorterUrl"])
            {
                responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            }
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    }
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        NSLog(@"%@",response);
        NSDictionary *resumeDic = [StaticData checkDictionaryNull:response[@"audio"]];
        NSDictionary *resume_position = [StaticData checkDictionaryNull:resumeDic[@"resume_position"]];
        
        if (resume_position && [resume_position count] > 0)
        {
            if (resume_position[@"position"])
            {
                self.resumeVideoPosition = [[StaticData checkStringNull:resume_position[@"position"]] intValue];
            }
        }
        _str_playbackURL = response[@"playback_url"];
        [self setupAuidoPlayer];
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    self.radioPlayer = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    
    [self.parentViewController addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.parentViewController.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.parentViewController.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.parentViewController.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    self.radioPlayer.resumeVideoPosition = self.resumeVideoPosition;
    self.radioPlayer.playerInfo = self.selectedShow;
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = _selectedShow.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    //self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

@end
