//
//  ShowDetailsVC.m
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ShowDetailsVC.h"
#import "QRLoginVC.h"

@interface ShowDetailsVC ()

@end

@implementation ShowDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.seasonsList = [NSMutableArray new];
    _castList = [NSMutableArray new];
    
    self.lbl_title.text = @"";
    self.lbl_season.text = @"";
    self.lbl_descriptions.text = @"";
    
    NSDictionary *category = [StaticData checkDictionaryNull:self.showDetails[@"cat"]];
    self.selectedShowID = [StaticData checkStringNull:category[@"id"]];
    
    self.viewIsActive = true;
    
    if ([StaticData isUserLogins]) {
        [self loadFavListFromServer];
        [self loadWatchLaterListFromServer];
    }
    
    self.ratingView.value = 0.0;
    self.ratingView.allowsHalfStars = YES;
    self.ratingView.accurateHalfStars = YES;
    self.ratingView.userInteractionEnabled = false;
    self.ratingView.tintColor = [StaticData colorFromHexString:@"#EFCE4A"];
    
    self.img_show.layer.cornerRadius = CellRounded;
    self.img_show.clipsToBounds = true;
    
    self.view_openShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    self.view_openShow.layer.cornerRadius = CellRounded;
    self.view_openShow.clipsToBounds = true;
    
    self.view_makeFavShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    self.view_makeFavShow.layer.cornerRadius = CellRounded;
    self.view_makeFavShow.clipsToBounds = true;
    
    self.view_watchLaterShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    self.view_watchLaterShow.layer.cornerRadius = CellRounded;
    self.view_watchLaterShow.clipsToBounds = true;
    
    self.view_interpretation.backgroundColor = [StaticData colorFromHexString:@"#EE1651"];
    self.view_interpretation.layer.cornerRadius = 6.0;
    
    self.view_rating.layer.cornerRadius = CellRounded;
    self.view_rating.clipsToBounds = true;
    
    [self setWatchLaterState:false];
    
    [StaticData scaleToArabic:self.collectionView];
    
    if (self.isRadio) {
        self.lbl_watchNow.text = @"استمع الآن";
        self.showImageWidthConstraint.constant = 450;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.viewIsActive = true;
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeShowFav)];
    [self.btn_fav addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeShowWathLater)];
    [self.btn_watchLater addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playFirstShowVideo)];
    [self.btn_play addGestureRecognizer:tab];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.viewIsActive = false;
}

-(void)setupShow
{
    [self setupShowInfo];
    [self setupSeassons];
    
    [self updateContentHeight];
}

-(void)updateContentHeight
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        CGRect frame = [self.view_details.subviews lastObject].frame;
        //self.heightConstraint.constant = frame.origin.y + frame.size.height + 30;
        
        if (self.UpdateContentSize) {
            self.UpdateContentSize();
        }
    });
}

-(void)setupShowInfo
{
    NSDictionary *category = [StaticData checkDictionaryNull:self.showDetails[@"cat"]];
    if (!category) return;
    
    [self setupRating:category isUserAddRating:false];
    
    self.lbl_title.text = [StaticData checkStringNull:category[@"title_ar"]];
    NSString *exclusive = [StaticData checkStringNull:category[@"exclusive"]];
    [StaticData isExclusive:exclusive exclusiveImg:_img_exclusive];
    NSString *categoryName = [StaticData checkStringNull:category[@"categories_names"]];
    NSString *seasonText = [StaticData checkStringNull:self.showDetails[@"season_count_text"]];
    
    NSString *signInterpetation = [StaticData checkStringNull:category[@"sign_language_interpretation"]];
    if ([signInterpetation isEqualToString:@"yes"]) {
        [self.view_interpretation setHidden:false];
    }
    
    self.lbl_season.text = seasonText;
    self.lbl_season_title.text = categoryName;
    
    if (self.selectedSeason) {
        self.lbl_descriptions.text = self.selectedSeason.descriptions;
    }
    
    if (self.lbl_descriptions.text.length == 0) {
        self.lbl_descriptions.text = [StaticData stringByStrippingHTML:[StaticData checkStringNull:category[@"description_ar"]]];
    }
    
    if (self.lbl_descriptions.text.length == 0) {
        self.lbl_descriptions.text = @"لا يوجد مواصفات";
    }
    self.lbl_descriptions.adjustsFontSizeToFitWidth = true;
    
    _castList = [self setupActoreProducerInfo:self.showDetails];
    
    if(_castList.count == 0)
    {
        self.castHeightConstraint.constant = 0.0;
    }
    
    [StaticData reloadCollectionView:self.collectionView];
}

-(NSMutableArray *)setupActoreProducerInfo:(NSDictionary *)info
{
    NSMutableArray *castList = [NSMutableArray new];
    @try {
        for (NSDictionary *castdic in info[@"cast"][@"actor"])
        {
            MediaInfo *info = [MediaInfo new];
            info.ID = [StaticData checkStringNull:castdic[@"id"]];
            info.title = [StaticData checkStringNull:castdic[@"fullname"]];
            [castList addObject:info];
        }
        
        for (NSDictionary *castdic in info[@"cast"][@"director"])
        {
            MediaInfo *info = [MediaInfo new];
            info.ID = [StaticData checkStringNull:castdic[@"id"]];
            info.title = [StaticData checkStringNull:castdic[@"fullname"]];
            [castList addObject:info];
        }
        
        for (NSDictionary *castdic in info[@"cast"][@"producer"])
        {
            MediaInfo *info = [MediaInfo new];
            info.ID = [StaticData checkStringNull:castdic[@"id"]];
            info.title = [StaticData checkStringNull:castdic[@"fullname"]];
            [castList addObject:info];
        }
        
        for (NSDictionary *castdic in info[@"cast"][@"autors"])
        {
            MediaInfo *info = [MediaInfo new];
            info.ID = [StaticData checkStringNull:castdic[@"id"]];
            info.title = [StaticData checkStringNull:castdic[@"fullname"]];
            [castList addObject:info];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return castList;
}

-(void)setupSeassons
{
    for (NSDictionary *season in self.showDetails[@"seasons"])
    {
        MediaInfo *info = [MediaInfo new];
        info.ID = [StaticData checkStringNull:season[@"id"]];
        info.title = [StaticData checkStringNull:season[@"title_ar"]];
        
        info.day = [StaticData checkStringNull:season[@"show_time_shortcut"]];
        
        info.descriptions = [StaticData checkStringNull:season[@"description_ar"]];
        info.thumbnailPath = [StaticData checkStringNull:season[@"app_cover"]];
        info.imgShowLogoPath = [StaticData checkStringNull:season[@"icon"]];
        [_seasonsList addObject:info];
    }

    NSString *defaultSeasonID = [StaticData checkStringNull:_showDetails[@"default_season"]];
    if (self.selectedSeason && self.selectedSeason.ID > 0) {
        defaultSeasonID = self.selectedSeason.ID;
    }
    if (defaultSeasonID.length > 0 && self.seasonsList.count > 0)
    {
        for (MediaInfo *season in self.seasonsList)
        {
            if ([season.ID isEqualToString:defaultSeasonID])
            {
                season.isSelected = true;
                self.selectedSeason = season;
            }
        }
    }
    
    NSLog(@"%@", self.selectedSeason.ID);
    
    if (self.selectedSeason)
    {
        _showImagePath = self.selectedSeason.thumbnailPath;
        if (self.selectedSeason.imgShowLogoPath.length > 0) {
            self.img_showLogoHeightConstraint.constant = self.img_show.frame.size.width;
            self.lbl_title.text = @"";
            self.img_showLogo.hidden = false;
            [self loadShowLogo:self.selectedSeason.imgShowLogoPath];
        } else {
            self.img_showLogoHeightConstraint.constant = 30.0;
        }
    }
    
    NSDictionary *category = [StaticData checkDictionaryNull:self.showDetails[@"cat"]];
    if (_showImagePath.length == 0) {
        _showImagePath = [self getShowImage:category];
    }
    
    [self loadShowImage];
    [self setupShowInfo];
    
    if (self.SetupSeasonCallBack) {
        self.SetupSeasonCallBack(self.seasonsList,self.selectedSeason);
    }
}

-(NSString *)getShowImage:(NSDictionary *)showInfo
{
    NSString *imagePath =  [StaticData checkStringNull:showInfo[@"app_cover"]];
    return imagePath;
}

-(void)setupRating:(NSDictionary *)ratingDic isUserAddRating:(BOOL)isNewRatingAdded
{
    if ([ratingDic[@"ratings_avg"] isKindOfClass:[NSNumber class]])
    {
        CGFloat rating = [ratingDic[@"ratings_avg"] floatValue];
        self.lbl_rating.text = [NSString stringWithFormat:@"%.01f/5",rating];
        
        self.ratingView.value = rating;
    } else if ([ratingDic[@"ratings_avg"] isKindOfClass:[NSString class]])
    {
        CGFloat rating = [[StaticData checkStringNull:ratingDic[@"ratings_avg"]] floatValue];
        self.lbl_rating.text = [NSString stringWithFormat:@"%.01f/5",rating];
        
        self.ratingView.value = rating;
    }else {
        if (!isNewRatingAdded) {
            self.lbl_rating.text = @"0.0/5";
            self.ratingView.value = 0.0;
        }
    }
}


-(void)loadShowImage
{
    @try {
        
        if (self.LoadShowImageCallBack) {
            self.LoadShowImageCallBack(_showImagePath);
        }

        [self.indicator startAnimating];
        NSString *imagePath = [BaseImageURL stringByAppendingString:_showImagePath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.img_show.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.img_show sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:_isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self updateShowImageSize:image];
                 [self.indicator stopAnimating];
             } else {
                 [self.indicator stopAnimating];
             }
         }];
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)loadShowLogo:(NSString *)path
{
    @try {
        
        [self.indicator startAnimating];
        NSString *imagePath = [BaseImageURL stringByAppendingString:path];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.img_showLogo.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.img_showLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:_isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:self.img_show withUrl:[NSURL URLWithString:imagePath] completed:^(UIImage *image) {
                     [self.indicator stopAnimating];
                 }];
             }
         }];
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)updateShowImageSize:(UIImage *)image
{
    CGRect frame = [UIScreen mainScreen].bounds;
    CGFloat ration = image.size.height / image.size.width;
    CGFloat height = ration * frame.size.width;
//    if (height < 375) {
//        height = 375.0;
//    }
    //_heightConstraint.constant = height;
    if (self.UpdateContentSize) {
        self.UpdateContentSize();
    }
}

-(IBAction)openSeasons:(id)sender
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (MediaInfo *info in self.seasonsList)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:info.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.selectedSeason = info;
            if (self.SeasonChoosedCallBack) {
                self.SeasonChoosedCallBack(info);
            }
        }];
        [as addAction:action];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [as addAction:cancel];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:as animated:TRUE completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:as];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

-(void)loadFavListFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&X-API-KEY=%@&fav_type=1&need_filter_by_type=yes&is_radio=%@",BaseURL,BaseUID,BaseKEY,[StaticData findLogedUserToken], self.isRadio ? @"1":  @"0"];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"FavShowList"];
}

-(void)loadWatchLaterListFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&X-API-KEY=%@&fav_type=0&need_filter_by_type=yes&is_radio=%@",BaseURL,BaseUID,BaseKEY,[StaticData findLogedUserToken], self.isRadio ? @"1":  @"0"];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"WatchLaterShowList"];
}

-(IBAction)makeShowFav
{
    if (![StaticData isUserLogins]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:PleaseLoginFirst preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"حسناً" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
        {
            QRLoginVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"QRLoginVC"];
            [self.navigationController pushViewController:vc animated:true];
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:true completion:nil];
        
        return;
    }
    
    [self.indicator startAnimating];
    NSDictionary *category = [StaticData checkDictionaryNull:self.showDetails[@"cat"]];
    
    NSString *showID = [StaticData checkStringNull:category[@"id"]];
    
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
    
    NSDictionary *parms = @{@"category_id":showID,@"fav_type":@"1"};
    if (self.isRadio) {
        path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
        parms = @{@"category_id":showID,@"fav_type":@"1"};
    }
    
    [self executeApiToGetApiResponse:path withParameters:parms withCallType:@"MakeShowFav"];
    
    if (self.isFav) {
        self.isFav = false;
        [self setFavState:false];
    } else {
        self.isFav = true;
        [self setFavState:true];
    }
}

-(IBAction)makeShowWathLater
{
    if (![StaticData isUserLogins]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:PleaseLoginFirst preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"حسناً" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
        {
            QRLoginVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"QRLoginVC"];
            [self.navigationController pushViewController:vc animated:true];
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:true completion:nil];
        
        return;
    }
    
    [self.indicator startAnimating];
    NSDictionary *category = [StaticData checkDictionaryNull:self.showDetails[@"cat"]];
    
    NSString *showID = [StaticData checkStringNull:category[@"id"]];
    
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
    
    NSDictionary *parms = @{@"category_id":showID,@"fav_type":@"0",@"X-API-KEY":[StaticData findLogedUserToken]};
    
    [self executeApiToGetApiResponse:path withParameters:parms withCallType:@"MakeShowWatchLater"];
    
    if (self.isFav) {
        self.isFav = false;
        [self setWatchLaterState:false];
    } else {
        self.isFav = true;
        [self setWatchLaterState:true];
    }
}

-(void)playFirstShowVideo
{
    if (self.PlayFirstShowVideoCallBack) {
        self.PlayFirstShowVideoCallBack();
    }
}

#pragma mark - Load Categories Request
// make web service api call
-(void)executeApiToGetApiResponse:(NSString *)url withParameters:(NSDictionary *)parms withCallType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [StaticData setSSLCertificate:manager];
    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"MakeShowFav"] || [type isEqualToString:@"ShorterUrl"]||[type isEqualToString:@"MakeShowWatchLater"])
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
        {
            [self.indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        }];
    } else
    {
        if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"] || [type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
        {
            [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        }
        
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             [self.indicator stopAnimating];
             NSLog(@"Response Received = %@",responseObject);
             [self responseReceived:responseObject callType:type];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
             NSLog(@"Api Error: %@",error.description);
             [self.indicator stopAnimating];
         }];
    }
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"FavShowList"])
        {
            for (NSDictionary *dicObj in response[@"data"])
            {
                NSString *showID = [StaticData checkStringNull:dicObj[@"id"]];
                if ([showID isEqualToString:self.selectedShowID])
                {
                    [self setFavState:true];
                    break;
                }
            }
        } else if ([type isEqualToString:@"WatchLaterShowList"])
        {
            for (NSDictionary *dicObj in response[@"data"])
            {
                NSString *showID = [StaticData checkStringNull:dicObj[@"id"]];
                if ([showID isEqualToString:self.selectedShowID])
                {
                    [self setWatchLaterState:true];
                    break;
                }
            }
        } else if ([type isEqualToString:@"ShorterUrl"])
        {
            BOOL isSuccess = [response[@"success"] boolValue];
            if (isSuccess) {
                self.shorterURL = [StaticData checkStringNull:response[@"data"][@"full_shorten_link"]];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setFavState:(BOOL)isFav
{
    if (isFav) {
        self.isFav = true;
        self.lbl_fav.text = @"إزالة من قائمتي";
    } else {
        self.isFav = false;
        self.lbl_fav.text = @"أضف إلى قائمتي";
    }
}

-(void)setWatchLaterState:(BOOL)isFav
{
    if (self.isRadio) {
        if (isFav) {
            self.isWatchLater = true;
            self.lbl_watchLater.text = @"احذف من الاستماع لاحقاً";
        } else {
            self.isWatchLater = false;
            self.lbl_watchLater.text = @"أضف إلى الاستماع لاحقاً";
        }
    }
    else {
        if (isFav) {
            self.isWatchLater = true;
            self.lbl_watchLater.text = @"احذف من المشاهدة لاحقاً";
        } else {
            self.isWatchLater = false;
            self.lbl_watchLater.text = @"أضف إلى المشاهدة لاحقاً";
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.castList[indexPath.row];
    CGSize size = [info.title sizeWithAttributes:NULL];
    size.width = size.width+80;
    size.height = collectionView.frame.size.height;
    return size;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _castList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.castList[indexPath.row];
    
    cell.info = info;
    
    cell.lbl_title.text = info.title;

    cell.contentView.layer.cornerRadius = CellRounded;
    
    for (id child in cell.contentView.subviews)
    {
        if ([child isKindOfClass:[UIVisualEffectView class]])
        {
            [child removeFromSuperview];
        }
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [StaticData blurEffectsOnView:cell.contentView withEffectStyle:UIBlurEffectStyleLight];
    });
    
    [StaticData scaleToArabic:cell.lbl_title];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.collectionView) {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == self.collectionView)
        {
            [UIView animateWithDuration:0.1 animations:^{
                context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == self.collectionView)
        {
            if (self.NextFocuseCallBack) {
                self.NextFocuseCallBack();
            }
            [UIView animateWithDuration:0.1 animations:^{
                context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }];
        }
    } else
    {
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == self.collectionView)
        {
            if (self.NextFocuseCallBack) {
                self.NextFocuseCallBack();
            }
        }
        
        [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.previouslyFocusedView == self.btn_play) {
        self.view_openShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    }
    
    if (context.nextFocusedView == self.btn_play) {
        if (self.NextFocuseCallBack) {
            self.NextFocuseCallBack();
        }
        self.view_openShow.backgroundColor = AppColor;
    }
    
    if (context.previouslyFocusedView == self.btn_fav) {
        self.view_makeFavShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    }
    
    if (context.nextFocusedView == self.btn_fav) {
        if (self.NextFocuseCallBack) {
            self.NextFocuseCallBack();
        }
        self.view_makeFavShow.backgroundColor = AppColor;
    }
    
    if (context.previouslyFocusedView == self.btn_watchLater) {
        self.view_watchLaterShow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    }
    
    if (context.nextFocusedView == self.btn_watchLater) {
        if (self.NextFocuseCallBack) {
            self.NextFocuseCallBack();
        }
        self.view_watchLaterShow.backgroundColor = AppColor;
    }
}

-(void)getPlayBackUrl:(MediaInfo *)info
{
    [self.loading_indicator startAnimating];
    
    _selectedShow = info;
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,info.ID];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
}

-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"RadioPlayBackUrl"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            if ([type isEqualToString:@"ShorterUrl"])
            {
                responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            }
            [self responseReceivedAudio:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    }
}

-(void)responseReceivedAudio:(id) response callType:(NSString *)type
{
    @try {
        _str_playbackURL = response[@"playback_url"];
        [self setupAuidoPlayer];
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    self.radioPlayer = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    self.radioPlayer.playerInfo = self.infoAudio;
    [StaticData shared].radioPlayer = self.radioPlayer;
    
    [self.parentViewController addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.parentViewController.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.parentViewController.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.parentViewController.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = _selectedShow.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    //self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

@end
