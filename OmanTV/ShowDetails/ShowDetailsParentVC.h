//
//  ShowDetailsParentVC.h
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowDetailsVC.h"
#import "ShowEpisodesVC.h"
#import "MenuVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsParentVC : UIViewController
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, weak) IBOutlet UIImageView *img_show;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *img_show_height_constraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewFixedHeightConstraint;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) MediaInfo *selectedSeason;

@property (nonatomic, retain) ShowDetailsVC *vc_showDetailsVC;
@property (nonatomic, retain) ShowEpisodesVC *vc_showEpisodesVC;

@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) int totalVideos;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isRadio;

@property (nonatomic, assign) BOOL isNeedToHideVideoDate;
@end

NS_ASSUME_NONNULL_END
