//
//  ShowDetailsParentVC.m
//  ADtv-TV
//
//  Created by Curiologix on 07/12/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ShowDetailsParentVC.h"

@interface ShowDetailsParentVC ()

@end

@implementation ShowDetailsParentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@""];
    self.vc_tabbarVC.view.backgroundColor = [UIColor clearColor];
    self.isRadio = self.selectedShow.isRadio;
    if (self.selectedShow.isRadioShow) {
        self.isRadio = true;
    }
    
    
    // for season push notifications
    if (self.selectedShow && self.selectedShow.seasonID.length > 0) {
        MediaInfo *info = [MediaInfo new];
        info.ID = self.selectedShow.seasonID;
        self.selectedSeason = info;
    }
    
    self.scroll_view.automaticallyAdjustsScrollIndicatorInsets = false;
    self.scroll_view.contentInset = UIEdgeInsetsZero;
    self.scroll_view.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    if (@available(tvOS 11.0, *)) {
        self.scroll_view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(tvOS 13.0, *)) {
        [self.scroll_view setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(haldePlayAndPause:)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];

}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view_content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self executeApiCallForShows];
}

#pragma mark Load Sub Categories
-(void)executeApiCallForShows
{
    @try {
        [self.indicator startAnimating];
        
        if (self.isRadio)
        {
            NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&&scope=audio&action=show&show_id=%@&season=%@&p=%d&limit=20&limit_also=3&channel_userid=%@&need_avg_rating=yes&need_ordered_audios=yes&all_fav_types_in_show_audios=yes&t=1589188186",BaseURL,BaseUID,BaseKEY,_selectedShow.ID,self.selectedSeason ? self.selectedSeason.ID:@"",self.pageNumber,[StaticData findeLogedUserID]];
            [self executePostApiToGetResponse:path parameters:nil callType:@"ShowsInfo"];
        } else
        {
//            NSString *path1 = [NSString stringWithFormat:@"%@/plus/show?user_id=%@&key=%@&app_id=%@&channel_userid=&show_id=%@&cast=yes&limit_also=3&season=%@&need_trailer=yes&need_channels=yes&all_fav_type_on_show_videos=yes&order_ended=yes&need_avg_rating=yes",BaseURL,BaseUID,BaseKEY,AppID,self.selectedShow.ID,self.selectedSeason ? self.selectedSeason.ID:@""];
            
            NSString *path = [NSString stringWithFormat:@"%@/plus/show?user_id=%@&key=%@&show_id=%@&channel_userid=&limit_also=3&cast=yes&p=1&limit=20&need_avg_rating=yes&need_channels=yes&all_fav_type_on_show_videos=yes",BaseURL,BaseUID,BaseKEY,self.selectedShow.ID];
            [self executePostApiToGetResponse:path parameters:nil callType:@"ShowsInfo"];
        }
        
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

#pragma mark - Load Categories Request
// make web service api call
-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"] || [type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"FavoriteShows"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            self.scroll_view.hidden = false;
             [self.indicator stopAnimating];
             NSLog(@"Response Received = %@",responseObject);
             [self responseReceived:responseObject callType:type];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
             NSLog(@"Api Error: %@",error.description);
             [self.indicator stopAnimating];
         }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             self.scroll_view.hidden = false;
             [self.indicator stopAnimating];
             NSLog(@"Response Received = %@",responseObject);
             if ([type isEqualToString:@"ShorterUrl"])
             {
                 responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
             }
             [self responseReceived:responseObject callType:type];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            
             NSLog(@"Api Error: %@",error.description);
             [self.indicator stopAnimating];
         }];
    }
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"ShowsInfo"])
        {
            [self.indicator stopAnimating];
        
            [self setupShowDetailsController:response];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupShowDetailsController:(id)showDetails
{
    _vc_showDetailsVC = [[StaticData getStoryboard:3] instantiateViewControllerWithIdentifier:@"ShowDetailsVC"];
    self.vc_showDetailsVC.isRadio = self.isRadio;
    self.vc_showDetailsVC.showDetails = showDetails;
    self.vc_showDetailsVC.selectedSeason = self.selectedSeason;
    CGRect frame = self.view.frame;
    CGFloat height = 750.0;
    [self addChildViewController:self.vc_showDetailsVC];
    
    [self.vc_showDetailsVC.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_showDetailsVC.view];
    
    [self.vc_showDetailsVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    self.view_content.tag = 1;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint, NSLayoutConstraint *topConstraint) {
        weakSelf.vc_showDetailsVC.heightConstraint = heightConstraint;
        weakSelf.vc_showDetailsVC.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_showDetailsVC.UpdateContentSize = ^{
        [weakSelf viewDidLayoutSubviews];
        [weakSelf.indicator stopAnimating];
    };
    
    self.vc_showDetailsVC.SeasonChoosedCallBack = ^(MediaInfo * _Nonnull info) {
        [weakSelf resetScrollContent];
        weakSelf.selectedSeason = info;
        [weakSelf executeApiCallForShows];
    };
    
    self.vc_showDetailsVC.PlayerStartAndStopCallBack = ^(BOOL isStart) {
        if (isStart) {
            weakSelf.img_show.hidden = true;
            //weakSelf.view_navBarParent.hidden = true;
        } else {
            weakSelf.img_show.hidden = false;
            //weakSelf.view_navBarParent.hidden = false;
        }
    };
    
    self.vc_showDetailsVC.LoadShowImageCallBack = ^(NSString * _Nonnull showImagePath) {
        [weakSelf loadShowImage:showImagePath];
    };
    
    self.vc_showDetailsVC.PlayFirstShowVideoCallBack = ^{
        if (weakSelf.vc_showEpisodesVC)
        {
            if (weakSelf.vc_showEpisodesVC.contentList.count > 0)
            {
                MediaInfo *info = weakSelf.vc_showEpisodesVC.contentList[0];
                if (info.isRadio) {
                    [weakSelf.vc_showEpisodesVC getPlayBackUrl:info];
                }
                else {
                    [StaticData openVideoPage:info withVC:weakSelf];
                }
                
            }
        }
    };
    
    self.vc_showDetailsVC.SetupSeasonCallBack = ^(NSMutableArray * _Nonnull seasonList, MediaInfo * _Nonnull selectedSeason) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf setupShowEpisodesController:showDetails withSeasonList:seasonList withSelectedSeason:selectedSeason];
        });
    };
    
    self.vc_showDetailsVC.NextFocuseCallBack = ^{
        weakSelf.scroll_view.scrollEnabled = true;
        weakSelf.vc_tabbarVC.view.backgroundColor = [UIColor clearColor];
        weakSelf.vc_tabbarVC.view.userInteractionEnabled = true;
        [weakSelf.scroll_view setContentOffset:CGPointZero animated:YES];
    };
    [self.vc_showDetailsVC setupShow];
}

-(void)loadShowImage:(NSString *)showImagePath
{
    @try {
        
        [self.indicator startAnimating];
        NSString *imagePath = [BaseImageURL stringByAppendingString:showImagePath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.img_show.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.img_show sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:_isRadio ? [StaticData radioPlaceHolder] : [StaticData placeHolder] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self updateShowImageSize:image];
                 [self.indicator stopAnimating];
             } else {
                 [self.indicator stopAnimating];
             }
         }];
        
        [StaticData blurEffectsOnView:self.img_show withEffectStyle:UIBlurEffectStyleDark];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)updateShowImageSize:(UIImage *)image
{
    CGRect frame = [UIScreen mainScreen].bounds;
    CGFloat ration = image.size.height / image.size.width;
    CGFloat height = ration * frame.size.width;
    self.img_show_height_constraint.constant = height;
}

-(void)resetScrollContent
{
    [self.view_content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self viewDidLayoutSubviews];
}

-(void)setupShowEpisodesController:(id)showDetails withSeasonList:(NSMutableArray *)seasonList withSelectedSeason:(MediaInfo *)selectedSeason
{
    self.vc_showEpisodesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowEpisodesVC"];
    self.vc_showEpisodesVC.seasonsList = seasonList;
    self.vc_showEpisodesVC.selectedSeason = selectedSeason;
    self.vc_showEpisodesVC.isRadio = self.isRadio;
    self.vc_showEpisodesVC.selectedSeasonId = self.selectedShow.ID;
    CGRect frame = self.view.frame;
    CGFloat height = 950.0;
    [self addChildViewController:self.vc_showEpisodesVC];
    
    [self.vc_showEpisodesVC.view setFrame:CGRectMake(0, [self yOfLastChildFromScrollView], frame.size.width, height)];
    
    [self.view_content addSubview:self.vc_showEpisodesVC.view];
    
    [self.vc_showDetailsVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint, NSLayoutConstraint *topConstraint) {
        weakSelf.vc_showEpisodesVC.heightConstraint = heightConstraint;
        weakSelf.vc_showEpisodesVC.topConstraint = topConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.vc_showEpisodesVC.UpdateContentSize = ^{
        [weakSelf viewDidLayoutSubviews];
        [weakSelf.indicator stopAnimating];
    };
    
    self.vc_showEpisodesVC.SeasonChoosedCallBack = ^(MediaInfo * _Nonnull info) {
        [weakSelf resetScrollContent];
        weakSelf.selectedSeason = info;
        [weakSelf executeApiCallForShows];
    };
    
    self.vc_showEpisodesVC.NextFocuseCallBack = ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            CGPoint bottomOffset = CGPointMake(0, weakSelf.scroll_view.contentSize.height - self.scroll_view.bounds.size.height + weakSelf.scroll_view.contentInset.bottom);
            [weakSelf.scroll_view setContentOffset:bottomOffset animated:YES];
            weakSelf.scroll_view.scrollEnabled = false;
            weakSelf.vc_tabbarVC.view.backgroundColor = [UIColor blackColor];
            weakSelf.vc_tabbarVC.view.userInteractionEnabled = false;
        });
    };
    
    self.vc_showEpisodesVC.showDetails = showDetails;
    [self.vc_showEpisodesVC setupAllVideos];
}

-(void)viewDidLayoutSubviews
{
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIView *view = [[[self.scroll_view.subviews objectAtIndex:0] subviews] lastObject];
            CGFloat contentSize = [view frame].origin.y + [view frame].size.height;
            [self.scroll_view setContentSize:CGSizeMake(self.scroll_view.frame.size.width, contentSize)];
           self.contentViewFixedHeightConstraint.constant = contentSize;
        });
    } @catch (NSException *exception) {
    } @finally {
    }
   
}

-(void)addConstraingOnScrollChilds:(UIView *)parent withFixedHeight:(CGFloat)fixedheight withTop:(CGFloat)topY completed:(HomeConstraintCompletionBlock)completedBlock
{
    NSArray *childs = [self.scroll_view.subviews[0] subviews];
    UIView *subView = [childs lastObject];
    UIView *secondLastSubView = nil;
    if (childs.count > 1) {
        secondLastSubView = childs[childs.count-2];
    }
    
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:subView
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:subView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:subView
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:parent
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:topY];
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:subView
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeHeight
                                  multiplier:0
                                  constant:fixedheight];
    
    //Add constraints to the Parent
    [parent addConstraint:trailing];
    if (secondLastSubView) {
        top = [NSLayoutConstraint
              constraintWithItem:subView
              attribute:NSLayoutAttributeTop
              relatedBy:NSLayoutRelationEqual
              toItem:secondLastSubView
              attribute:NSLayoutAttributeBottom
              multiplier:1
              constant:topY];
        [parent addConstraint:top];
    } else {
        [parent addConstraint:top];
    }
    [parent addConstraint:leading];
    
    //Add height constraint to the subview, as subview owns it.
    [subView addConstraint:height];
    
    completedBlock(height,top);
}
-(CGFloat)yOfLastChildFromScrollView
{
    UIView *subView = [[self.scroll_view.subviews[0] subviews] lastObject];
    
    CGFloat topY = 0;
    if (subView) {
        topY = subView.frame.origin.y + subView.frame.size.height;
    }
    return topY;
}

// somewhere in the class implementation
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}

-(void)haldePlayAndPause:(UITapGestureRecognizer *)gesture
{
    if (self.vc_showEpisodesVC.radioPlayer.btn_playRadio.tag == 0)
    {
        [self.vc_showEpisodesVC.radioPlayer playRadioNow];
    } else
    {
        [self.vc_showEpisodesVC.radioPlayer pauseRadioNow];
    }
}

@end
