//
//  SchedulesVC.h
//  OmanTV
//
//  Created by Curiologix on 27/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScheduleVideosVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface SchedulesVC : UIViewController
@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewChannels;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *parentViewBottomAnchor;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noRecordsFound;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_contentParent;
@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *channelList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) MediaInfo *selectedChannel;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) BOOL isRadio;
@end

NS_ASSUME_NONNULL_END
