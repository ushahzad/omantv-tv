//
//  ScheduleVideosVC.m
//  OmanTV
//
//  Created by Curiologix on 27/03/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "ScheduleVideosVC.h"

@interface ScheduleVideosVC ()

@end

@implementation ScheduleVideosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentList = [NSMutableArray new];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.view_dropdown.layer.cornerRadius = 35.0;
    self.view_dropdown.clipsToBounds = true;
    
    self.lbl_dropdown.textColor = [UIColor whiteColor];
    self.img_dropdown.image = [StaticData changeImageColor:[UIImage imageNamed:@"dropdown"] withColor:[UIColor whiteColor]];
    self.view_dropdown.backgroundColor = [UIColor clearColor];
    self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
    self.view_dropdown.layer.borderWidth = 1.0;
    
    self.btn_dropdown.hidden = true;
    self.view_dropdown.hidden = true;
    
    if (self.isNeedToOpenDropdown) {
        self.btn_dropdown.hidden = false;
        self.view_dropdown.hidden = false;
        
        if (self.channelList.count > 0)
        {
            [self updateSelectedChannelDropdownInfo];
        }
    }
    [StaticData scaleToArabic:self.collectionView];
    
    NSLog(@"%@",self.selectedCatchup.title);
    if ([self.selectedCatchup.title isEqualToString:@"اليوم"]) {
        self.lbl_title.text = self.selectedCatchup.title;
    }
    else {
        
        NSString *weekData = [self getWeekDayNameInArabic:self.selectedCatchup.title];
        if (weekData.length > 0)
        {
//            self.lbl_title.text = [NSString stringWithFormat:@"%@ - %@",self.selectedCatchup.title,weekData];
            
            self.lbl_title.text = weekData;
        }
    }
    
    self.contentList = self.selectedCatchup.items;
    [self setupTriggerActionOnButtons];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self reloadCollectionView];
}

-(NSString *)getScheduleDate:(NSString *)time
{
    @try {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"YYYY-MM-dd"];
        NSDate *catchupDate = [df dateFromString:time];
        
        [df setDateFormat:@"dd-MM-yyyy"];
        
        NSString *time = [df stringFromDate:catchupDate];
        
        return time;
    } @catch (NSException *exception) {
        [StaticData log:exception.description];
    } @finally {
    }
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_dropdown];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_dropdown)
        {
            [self openChannels];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)openChannels
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (MediaInfo *info in self.channelList)
    {
        UIAlertAction *action = [UIAlertAction actionWithTitle:info.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (self.ChannelClickCallBack)
            {
                self.ChannelClickCallBack(info);
            }
        }];
        [as addAction:action];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:Cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [as addAction:cancel];
    
    [self.parentVc presentViewController:as animated:TRUE completion:nil];
}

-(void)updateSelectedChannelDropdownInfo
{
    for (MediaInfo *info in self.channelList) {
        if (info.isSelected)
        {
            self.lbl_dropdown.text = info.title;
        }
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //CGRect frame = collectionView.frame;
    CGFloat width = (collectionView.frame.size.width - 40) / 3;
    CGFloat height = 170;
//    if (self.tempImage)
//    {
//        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
//        height = (ratio * width);
//    }
    
    _heightConstraint.constant = height+100;
    
    if (self.UpdateContentSize)
    {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    MediaInfo *info = self.contentList[indexPath.row];
    cell.info = info;
    
    cell.lbl_title.text = info.title;
//    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    cell.lbl_time.text = info.time;
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[StaticData placeHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.layer.cornerRadius = CellRounded;
    cell.clipsToBounds = true;
    
    [StaticData scaleToArabic:cell.thumbnail];
    [StaticData scaleToArabic:cell.lbl_title];
    [StaticData scaleToArabic:cell.lbl_time];
    [StaticData scaleToArabic:cell];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    if (info.ID.length > 0) {
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [cell.lbl_titleMarquee pauseLabel];
            cell.view_info.hidden = true;
            cell.layer.borderColor = [UIColor clearColor].CGColor;
            cell.layer.borderWidth = 0.0;
            
            [UIView animateWithDuration:0.1 animations:^{
                //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            [cell.lbl_titleMarquee unpauseLabel];
            cell.view_info.hidden = true;
            
            cell.clipsToBounds = true;
            cell.layer.borderColor = AppColor.CGColor;
            cell.layer.borderWidth = CellBorderWidth;
            cell.layer.cornerRadius = CellRounded;
            
            [UIView animateWithDuration:0.1 animations:^{
                //context.nextFocusedView.transform = CGAffineTransformMakeScale(1.12, 1.12);
            }];
            
            NSIndexPath *indexPath = context.nextFocusedIndexPath;
            if (indexPath)
            {
                //[self updateCollectionViewCells:indexPath withCollectionView:collectionView];
            }
        }
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_dropdown)
        {
            self.view_dropdown.layer.borderColor = AppColor.CGColor;
            self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
         if (sender == self.btn_dropdown) {
             self.view_dropdown.layer.borderColor = [StaticData colorFromHexString:@"#ffffff"].CGColor;
             self.view_dropdown.layer.borderWidth = 1.0;
        }
    }
}

-(NSString *)getWeekDayNameInArabic:(NSString *)dateStr
{
    @try {
        NSArray *weekDays = [StaticData readJsobFileWithName:@"WeekendDays"];
        
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"YYYY-MM-dd"];
        NSDate *dateDate = [df dateFromString:dateStr];
        NSString *date = [df stringFromDate:dateDate];
        
        [df setDateFormat:@"EEEE"];
        NSString *wDay = [df stringFromDate:dateDate];
        
        for (NSDictionary *weeks in weekDays)
        {
            if ([weeks[@"EName"] isEqualToString:wDay])
            {
                return isEnglishLang ? weeks[@"EName"] : weeks[@"AName"];
                break;
            }
        }
    } @catch (NSException *exception) {
    } @finally {

    }
    
    return @"";
}

@end
