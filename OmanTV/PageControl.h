//
//  DotcomPageControl.h
//  AbuDhabi-ios
//
//  Created by MacUser on 06/02/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PageControl : UIPageControl

-(void)updateBullets;

@end

NS_ASSUME_NONNULL_END
