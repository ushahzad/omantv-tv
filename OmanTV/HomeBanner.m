//
//  FeaturedShowsVC.m
//  AbuDhabiAppleTV
//
//  Created by MacUser on 16/01/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import "HomeBanner.h"
@interface HomeBanner ()

@end

@implementation HomeBanner

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentList = [NSMutableArray new];
    
    self.collectionView.remembersLastFocusedIndexPath = true;
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadDataFromServer];
    
    //self.lbl_title.text = info.title;
    //self.lbl_subTitle.text = info.subTitle;
    
    self.view_left_arrow.layer.cornerRadius = 35;
    self.view_left_arrow.clipsToBounds = true;
    
    self.view_right_arrow.layer.cornerRadius = 35;
    self.view_right_arrow.clipsToBounds = true;
    
    self.view_play.layer.cornerRadius = 12.0;
    self.view_play.clipsToBounds = true;
    [self.view_play setBackgroundColor:[StaticData colorFromHexString:@"#808080"]];
    
    [self isRightArrowFoucuse:false];
    [self isLeftArrowFoucuse:false];
    
    self.view_info.hidden = true;
    /*UISwipeGestureRecognizer *rightRecongnizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler)];
    rightRecongnizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:rightRecongnizer];
    
    UISwipeGestureRecognizer *leftRecongniser = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler)];
    leftRecongniser.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftRecongniser];*/
    
   // s//elf.pageController.currentPageIndicatorTintColor = AppColor;
    
    //[self performSelector:@selector(loadDataFromServer) withObject:nil afterDelay:20];
    
    //[self setupTriggerActionOnButtons];
    [self.indicator startAnimating];
}

-(void)viewDidAppear:(BOOL)animated
{
    UISwipeGestureRecognizer *upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(takeSwipeGestureToTabbar:)];
    upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:upSwipe];
    
    UITapGestureRecognizer *btn_prev = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openNextShow)];
    [self.btn_right_arrow addGestureRecognizer:btn_prev];
    
    UITapGestureRecognizer *btn_next = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPreviousShow)];
    [self.btn_left_arrow addGestureRecognizer:btn_next];
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
}

-(void)takeSwipeGestureToTabbar:(UISwipeGestureRecognizer *)swipe
{
    if (self.HomeBannerSwipeUpCallBack) {
        self.HomeBannerSwipeUpCallBack();
    }
}


-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_play,self.btn_left_arrow,self.btn_right_arrow];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_play)
        {
            if (self.selectedShow) {
                [StaticData openShowDetailsPageWith:self withMediaInfo:self.selectedShow];
            }
        } else if (sender == self.btn_left_arrow)
        {
        } else if (sender == self.btn_right_arrow)
        {
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)openPreviousShow
{
    @try {
        NSLog(@"left swipe");
        
        BOOL isFindeSelectedCategory = false;
        NSNumber *scrollToIndex;
        MediaInfo *nextChannel;
        
        for (int i=0; i<self.contentList.count; i++)
        {
            MediaInfo *info = self.contentList[i];
            if (info.isSelected)
            {
                isFindeSelectedCategory = true;
            } else
            {
                if (isFindeSelectedCategory) {
                    scrollToIndex = [NSNumber numberWithInt:i];
                    nextChannel = info;
                    break;
                }
            }
        }
        
        if (!nextChannel) return;
        
        for (MediaInfo *info in self.contentList) {
            info.isSelected = false;
        }
        
        nextChannel.isSelected = true;
        [self updateShowInfo:[scrollToIndex integerValue]];

        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:[scrollToIndex intValue] inSection:0];
        //CollectionCell *nextCell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:nextIndexPath];
        
        [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
 
    } @catch (NSException *exception) {
        NSLog(@"Left Swipe Have exception = %@",exception.description);
    } @finally {
    }
}

-(void)openNextShow
{
    @try {
        NSLog(@"right swipe");
        
        NSNumber *scrollToIndex;
        MediaInfo *nextChannel;
        
        for (int i=0; i<self.contentList.count; i++)
        {
            MediaInfo *info = self.contentList[i];
            if (info.isSelected) {
                break;
            } else {
                scrollToIndex = [NSNumber numberWithInt:i];
                nextChannel = info;
            }
        }
        
        if (!nextChannel) return;
        
        if (nextChannel)
        {
            for (MediaInfo *info in self.contentList)
            {
                info.isSelected = false;
            }
            nextChannel.isSelected = true;

            [self updateShowInfo:[scrollToIndex integerValue]];

            NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:[scrollToIndex intValue] inSection:0];
            //CollectionCell *nextCell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:nextIndexPath];
            
            [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
        }
        
    } @catch (NSException *exception) {
        NSLog(@"Right Swipe have Exception = %@",exception.description);
    } @finally {
    }
}

-(void)loadDataFromServer
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/dynamic_content/home_banner?user_id=%@&key=%@&&type=home_banner_tv&p=1&limit=5&need_trailer=yes&need_category=yes&need_playback=yes&need_channel=yes",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"BannerVideos"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"BannerVideos"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject[@"home_banners"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"category_id"]];
                info.title = [StaticData checkStringNull:dicObj[@"cat_title_ar"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"cat_exclusive"]];
                info.subTitle = [StaticData checkStringNull:dicObj[@"cat_description_ar"]];
                info.pageType = 1;
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            if (self.contentList.count > 0) {
                self.view_info.hidden = false;
             
                MediaInfo *info = self.contentList[0];
                if(self.FocuseUpdateCallBack) {
                    self.FocuseUpdateCallBack(info);
                }
            }
            [self reloadCollectionView];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(void)checkRecordFound
{
    if (self.contentList.count == 0) {
        self.heightConstraint.constant = 0.0;
    }
    
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
        
        _heightConstraint.constant = height;
        
        if (self.UpdateContentSize)
        {
            self.UpdateContentSize();
        }
    }
    
    return CGSizeMake(width, height);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_title.text = info.title;
    cell.lbl_subTitle.text = info.subTitle;
    
    cell.view_play.layer.cornerRadius = 12.0;
    cell.view_play.clipsToBounds = true;
    [cell.view_play setBackgroundColor:[StaticData colorFromHexString:@"#808080"]];
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     if (image)
                     {
                         //[StaticData imageLoaded:image withCell:cell];
                     }
                     
                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    MediaInfo *info = self.contentList[indexPath.row];
//    [StaticData openShowDetailsPageWith:self withMediaInfo:info];
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.collectionView)
    {
        for (MediaInfo *info in self.contentList) {
            info.isSelected = false;
        }
        
        NSInteger currentPage = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
        [self updateShowInfo:currentPage];
    }
}

-(void)updateShowInfo:(NSInteger)currentPage
{
    @try {
        MediaInfo *info = self.contentList[currentPage];
        info.isSelected = true;
        self.selectedShow = info;
        
        self.lbl_title.text = info.title;
        self.lbl_subTitle.text = info.subTitle;
        
    } @catch (NSException *exception) {
        NSLog(@"exception found here %@",exception.description);
    } @finally {
    }
    
}

-(IBAction)playVideo:(id)sender
{
}

- (void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    @try {
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]])
        {
            NSIndexPath *indexPath = context.nextFocusedIndexPath;
            if (indexPath)
            {
                if (self.contentList.count > 0)
                {
                    MediaInfo *info = self.contentList[indexPath.row];
                    if(self.FocuseUpdateCallBack) {
                        self.FocuseUpdateCallBack(info);
                    }
                }
            }
        }
        
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
        {
            if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == self.collectionView)
            {
                
                if (self.FocuseOutUpdateCallBack) {
                    self.FocuseOutUpdateCallBack();
                }
            }
        }
       
        [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_play)
    {
        self.view_play.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    }
    
    if (context.nextFocusedView == self.btn_play)
    {
        self.view_play.backgroundColor = AppColor;
    }
    
    if (context.previouslyFocusedView == self.btn_right_arrow)
    {
        [self isRightArrowFoucuse:false];
    }
    
    if (context.nextFocusedView == self.btn_right_arrow)
    {
        [self isLeftArrowFoucuse:false];
        [self isRightArrowFoucuse:true];
    }
    
    if (context.previouslyFocusedView == self.btn_left_arrow)
    {
        [self isLeftArrowFoucuse:false];
    }
    
    if (context.nextFocusedView == self.btn_left_arrow)
    {
        [self isRightArrowFoucuse:false];
        [self isLeftArrowFoucuse:true];
    }
    
}

-(void)isRightArrowFoucuse:(BOOL)isTrue
{
    
    if (isTrue)
    {
        self.view_right_arrow.backgroundColor = AppColor;
        self.img_right_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"right_arrow"] withColor:[UIColor whiteColor]];
    } else
    {
        self.view_right_arrow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
        self.img_right_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"right_arrow"] withColor:[UIColor whiteColor]];
    }
}

-(void)isLeftArrowFoucuse:(BOOL)isTrue
{
    
    if (isTrue)
    {
        self.view_left_arrow.backgroundColor = AppColor;
        self.img_left_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"left_arrow"] withColor:[UIColor whiteColor]];
    } else
    {
        self.view_left_arrow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
        self.img_left_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"left_arrow"] withColor:[UIColor whiteColor]];
    }
}

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.contentList.count > 0) {
            for (int i = 0; i < self.contentList.count; i++)
            {
                MediaInfo *channel = self.contentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

@end
