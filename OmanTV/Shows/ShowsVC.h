//
//  ShowsVC.h
//  OmanTV
//
//  Created by Curiologix on 26/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChannelsTypeVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShowsVC : UIViewController
<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_categries;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, strong) ChannelsTypeVC *vc_channelsTypeVC;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) NSMutableArray *categoriesList;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) MediaInfo *selectedCategory;
@property (nonatomic, retain) UIImage *tempImage;

@property (nonatomic, weak) IBOutlet UILabel *lbl_noRecordsFound;

@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, retain) NSTimer *nextFocuseUpdateTimer;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic, assign) int pageNumber;

-(void)loadDataFromServer;
@end

NS_ASSUME_NONNULL_END
