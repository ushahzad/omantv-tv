//
//  ChannelsTypeVC.m
//  OmanTV
//
//  Created by MacUser on 16/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ChannelsTypeVC.h"

@interface ChannelsTypeVC ()

@end

@implementation ChannelsTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *tvTitle = self.liveTvTitle.length > 0 ? self.liveTvTitle : @"مرئي";
    NSString *radioTitle = self.liveRadioTitle.length > 0 ? self.liveRadioTitle : @"سمعي";
    
    self.collectionView.remembersLastFocusedIndexPath = true;
    
    self.contentList = [NSMutableArray new];
    MediaInfo *info = [MediaInfo new];
    info.title = isEnglishLang ? @"LIVE TV" : tvTitle;
    info.itemType = @"TV";
    info.isSelected = true;
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = isEnglishLang ? @"LIVE RADIO" : radioTitle;
    info.itemType = @"RADIO";
    [self.contentList addObject:info];
    
    [StaticData scaleToArabic:self.collectionView];
}

-(void)setRadioSelection
{
    for (MediaInfo *info in self.contentList) {
        info.isSelected = false;
        if ([info.itemType isEqualToString:@"RADIO"])
        {
            info.isSelected = true;
        }
    }
    [StaticData reloadCollectionView:self.collectionView];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat totalCellWidth = 400.0;
    CGFloat totalSpacingWidth = 0;
    CGFloat leftInset = (collectionView.bounds.size.width - (totalCellWidth + totalSpacingWidth)) / 2;
    CGFloat rightInset = leftInset;
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(0, leftInset, 0, rightInset);
    return sectionInset;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    return CGSizeMake(200, frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.lbl_title.text = info.title;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.view_info.hidden = false;
    cell.alpha = 1.0;
    
    if (info.isSelected) {
        [self setupSelectedState:cell withIndexPath:indexPath];
        cell.backgroundColor = LightWidthColor;
        cell.view_info.hidden = true;
    } else {
        cell.lbl_title.textColor = [UIColor whiteColor];
    }
    
    if (self.isComeFromHomePage)
    {
        cell.view_info.hidden = true;
        cell.lbl_title.textColor = AppColor;
    } else
    {
        cell.view_info.backgroundColor = [StaticData colorFromHexString:@"#262626"];
        cell.view_info.layer.cornerRadius = 12.0;
        cell.view_info.layer.masksToBounds = true;
    }
    
    cell.layer.cornerRadius = 12.0;
    cell.layer.masksToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (MediaInfo *info in self.contentList) {
        info.isSelected = false;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    info.isSelected = true;
    [StaticData reloadCollectionView:self.collectionView];
    
    if (self.ChoosedChannelTypeCallBack)
    {
        if ([info.itemType isEqualToString:@"TV"]) {
            self.ChoosedChannelTypeCallBack(false,info);
        } else {
            self.ChoosedChannelTypeCallBack(true,info);
        }
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [self setupSelectedState:cell withIndexPath:context.previouslyFocusedIndexPath];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.view_info.hidden = true;
            cell.backgroundColor =  [UIColor whiteColor];
            cell.lbl_title.textColor = AppColor;
            
            if(self.FocuseUpdateCallBack) {
                self.FocuseUpdateCallBack();
            }
        } else
        {
            [StaticData reloadCollectionView:self.collectionView];
        }
    }
    
}

-(void)setupSelectedState:(CollectionCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    if (self.isComeFromHomePage) {
        cell.view_info.hidden = true;
        cell.lbl_title.textColor = AppColor;
    } else {
        cell.view_info.hidden = false;
        cell.lbl_title.textColor = [UIColor whiteColor];
    }
    cell.backgroundColor =  [UIColor clearColor];
    
    MediaInfo *info = self.contentList[indexPath.row];

    if (info.isSelected) {
        cell.lbl_title.textColor = AppColor;
    }
}

@end
