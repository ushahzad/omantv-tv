//
//  ChannelsTypeVC.h
//  OmanTV
//
//  Created by MacUser on 16/08/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChannelsTypeVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property void (^ChoosedChannelTypeCallBack)(BOOL isRadio,MediaInfo*info);
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) NSMutableArray *contentList;

@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic,retain) NSString *liveTvTitle;
@property (nonatomic,retain) NSString *liveRadioTitle;
@property (nonatomic, assign) BOOL isComeFromHomePage;
-(void)setRadioSelection;
@end

NS_ASSUME_NONNULL_END
