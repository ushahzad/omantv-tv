//
//  ShowsVC.m
//  OmanTV
//
//  Created by Curiologix on 26/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ShowsVC.h"

@interface ShowsVC ()

@end

@implementation ShowsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"programs"];
    self.vc_tabbarVC.ChooseChannelTypeCallBack = ^(BOOL isRadio, MediaInfo * _Nonnull info) {
        weakSelf.isRadio = isRadio;
        
        [weakSelf resetControllerData];
        [weakSelf loadProgramsByChannel];
    };
    
    self.categoriesList = [NSMutableArray new];
    self.contentList = [NSMutableArray new];

//    if (@available(tvOS 11.0, *)) {
//        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        // Fallback on earlier versions
//    }
//
//    if (@available(tvOS 13.0, *)) {
//        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
//    } else {
//        // Fallback on earlier versions
//    }
    
    [StaticData scaleToArabic:self.cv_categries];
    [StaticData scaleToArabic:self.collectionView];
    
    //[self setupChannelsTypesController];
    
    [self loadProgramsByChannel];
    
    [self.indicator startAnimating];
}

-(void)viewDidAppear:(BOOL)animated
{
    
//    if (@available(tvOS 11.0, *)) {
//        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        // Fallback on earlier versions
//    }
//
//    if (@available(tvOS 13.0, *)) {
//        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
//    } else {
//        // Fallback on earlier versions
//    }
}

-(void)setupChannelsTypesController
{
    ChannelsTypeVC *vc_channelsTypeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    
    CGRect frame = self.view.frame;
    CGFloat height = 70;
    
    [self addChildViewController:vc_channelsTypeVC];
    
    [vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_channelsTypes addSubview:vc_channelsTypeVC.view];
    
    [vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:vc_channelsTypeVC.view];
    
    vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info) {
        weakSelf.isRadio = isRadio;
        
        [weakSelf resetControllerData];
        [weakSelf loadProgramsByChannel];
    };
}

-(void)loadProgramsByChannel
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&channel_id=&cat_id=&genre_id=&p=1&ended=no&limit=1000&is_radio=%d&need_show_times=no&need_channel=yes&custom_order=yes&exclude_channel_id=%@&need_labels=yes",BaseURL,BaseUID,BaseKEY,self.isRadio,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)resetControllerData
{
    [self.contentList removeAllObjects];
    self.lbl_noRecordsFound.hidden = true;
    [self reloadCollectionView];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"Categoreis"])
        {
            for (NSDictionary *dicObj in responseObject)
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_categoriesList addObject:info];
                }
            }
            
            if (self.categoriesList.count > 0) {
                [self loadSelectedCategory:self.categoriesList[0]];
            }
            [StaticData reloadCollectionView:self.cv_categries];
        } else if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.isRadio = self.isRadio;
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"app_thumbnail"]];
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                NSArray *labelsArray = [StaticData checkArrayNull:dicObj[@"labels"]];
                if (labelsArray.count > 0) {
                    NSDictionary *labelDic = labelsArray[0];
                    info.signLogoPath = labelDic[@"image"];
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            NSMutableArray *indexPaths = [NSMutableArray new];
            for (int i = 0; i < _contentList.count; i++)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                [indexPaths addObject:indexPath];
            }
            [self.collectionView insertItemsAtIndexPaths:indexPaths];

//            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(void)checkRecordFound
{
    if (self.contentList.count == 0) {
        self.lbl_noRecordsFound.hidden = false;
    } else {
        self.lbl_noRecordsFound.hidden = true;
    }
    
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_categries) {
        CGRect frame = collectionView.frame;
        CGFloat width = frame.size.width;
        return CGSizeMake(width, 100);
    }
    
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 314) / 5;
    CGFloat height = width;
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    return CGSizeMake(width, height+ 50);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.cv_categries) {
        return self.categoriesList.count;
    }
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView ==self.cv_categries) {
        CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        MediaInfo *info = self.categoriesList[indexPath.row];
        
        cell.lbl_title.text = info.title;
        if (info.isSelected) {
            cell.lbl_title.textColor = AppColor;
        } else {
            cell.lbl_title.textColor = [UIColor whiteColor];
        }
        return cell;
    }
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    [cell awakeFromNib];

    cell.lbl_titleMarquee.text = info.title;

    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];

    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:cell.thumbnail withUrl:imageURL completed:^(UIImage *image) {
                     if (!self.tempImage && image) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     if (image)
                     {
                         //[StaticData imageLoaded:image withCell:cell];
                     }

                     [cell.indicator stopAnimating];
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    [cell.signLanguageLogo setHidden:true];
    @try {
        NSString *encodedCover = [info.signLogoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        cell.signLanguageLogo.contentMode = UIViewContentModeScaleAspectFit;
        [cell.signLanguageLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [cell.signLanguageLogo setHidden:false];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }

    cell.layer.cornerRadius = 12.0;
    cell.clipsToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_categries) {
        for (MediaInfo *info in self.categoriesList) {
            info.isSelected = false;
        }
        MediaInfo *info = self.categoriesList[indexPath.row];
        [self loadSelectedCategory:info];
        [self.cv_categries reloadData];
    }else{
        MediaInfo *info = self.contentList[indexPath.row];
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    }
}

-(void)loadSelectedCategory:(MediaInfo *)info
{
    self.selectedCategory = info;
    self.selectedCategory.isSelected = true;
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"Current Page = %ld",(long)indexPath.row);
}

-(void)startSliderTimer
{
//    if ([self.sliderTimer isValid]) {
//        [self.sliderTimer invalidate];
//    }
//
//    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollToNextSliderItem) userInfo:nil repeats:true];
}


-(IBAction)playVideo:(id)sender
{
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_categries) {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            //cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
            cell.backgroundColor = [UIColor clearColor];
            cell.lbl_title.textColor = [UIColor whiteColor];
            
            MediaInfo *info = self.categoriesList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.lbl_title.textColor = AppColor;
            }
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            //cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
            cell.backgroundColor = AppColor;
            cell.lbl_title.textColor = [UIColor whiteColor];
            cell.layer.cornerRadius = 12.0;
            cell.clipsToBounds = true;
        }
        return;
    }
    
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

/*+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}*/

/*-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }
            
        }];
    }
    
    if (context.nextFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }
        }];
    }
}*/

-(CollectionCell *)findSelectedCell
{
//    @try {
//        if (self.contentList.count > 0) {
//            for (int i = 0; i < self.contentList.count; i++)
//            {
//                MediaInfo *channel = self.contentList[i];
//                if (channel.isSelected)
//                {
//                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
//                    if (indexPath) {
//                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
//
//                        if (cell) {
//                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
//                            return cell;
//                        }
//                    } else {
//                        return nil;
//                    }
//                }
//            }
//        }
//        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
//        if (indexPath) {
//            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
//            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
//            return cell;
//        }
//        return nil;
//    } @catch (NSException *exception) {
//        return nil;
//    } @finally {
//    }
    return nil;
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}

@end
