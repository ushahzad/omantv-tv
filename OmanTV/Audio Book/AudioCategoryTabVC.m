//
//  AudioCategoryTabVC.m
//  OmanTV
//
//  Created by Cenex Studio on 04/10/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "AudioCategoryTabVC.h"

@interface AudioCategoryTabVC ()

@end

@implementation AudioCategoryTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.contentList = [NSMutableArray new];
    
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadDataFromServer];
}

-(void)loadDataFromServer
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];

    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/filled_categories_by_genre?user_id=%@&key=%@&t=1632488243&audio_book_channel_id=%@&is_radio=1",BaseURL, BaseUID, BaseKEY, AudioBookChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    

    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            
            MediaInfo *mInfo = [MediaInfo new];
            mInfo.ID = @"-1";
            mInfo.title = @"الكل";
            [self.contentList addObject:mInfo];
            
            for (NSDictionary *dicObj in responseObject[@"data"][@"categories"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"category_channel_id"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                info.descriptions = [StaticData checkStringNull:dicObj[@"description_ar"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [self.contentList addObject:info];
                }
            }
            
//            [self checkRecordFound];
            [self reloadCollectionView];
            
            if (self.UpdateContentSize) {
                self.UpdateContentSize();
            }
        }
    } @catch (NSException *exception) {
//        [self checkRecordFound];
    } @finally {
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = frame.size.width / 3.3;
    
    if (self.UpdateContentSize) {
        self.UpdateContentSize();
    }
    
    return CGSizeMake(width, frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_title.text = info.title;
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.lbl_title setTextColor:[UIColor whiteColor]];
//    [cell.thumbnail setImage:[UIImage imageNamed:info.thumbnail]];
    
    if (indexPath.row == self.selectedIndex) {
        [cell setBackgroundColor:AppColor];
        cell.layer.borderColor = AppColor.CGColor;
        cell.layer.borderWidth = 1.0;
    }
    else {
//        [cell.lbl_title setTextColor:[UIColor blackColor]];
//        [cell setBackgroundColor:[UIColor whiteColor]];
        cell.layer.borderColor = [UIColor grayColor].CGColor;
        cell.layer.borderWidth = 1.0;
    }
    
    cell.layer.cornerRadius = cell.frame.size.height/2;
    cell.clipsToBounds = YES;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = (int)indexPath.row;
    [self reloadCollectionView];
    
    MediaInfo *info = [self.contentList objectAtIndex:indexPath.row];
    self.ChooseAudioBookTabCallBack(info);
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [self setupSelectedState:cell withIndexPath:context.previouslyFocusedIndexPath];
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.view_info.hidden = true;
            cell.backgroundColor =  AppColor;
            cell.lbl_title.textColor = [UIColor whiteColor];
            
            if(self.FocuseUpdateCallBack) {
                self.FocuseUpdateCallBack();
            }
        } else
        {
            [StaticData reloadCollectionView:self.collectionView];
        }
    }
}

-(void)setupSelectedState:(CollectionCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.view_info.hidden = false;
    cell.lbl_title.textColor = [UIColor whiteColor];
    cell.backgroundColor =  [UIColor clearColor];
    
    MediaInfo *info = self.contentList[indexPath.row];

    if (info.isSelected) {
        cell.lbl_title.textColor = AppColor;
    }
}


@end
