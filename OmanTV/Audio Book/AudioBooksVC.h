//
//  AudioBooksVC.h
//  OmanTV
//
//  Created by Cenex Studio on 24/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioCategoryTabVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface AudioBooksVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);

@property (nonatomic, retain) NavigationBar *navBar;
@property (nonatomic, weak) IBOutlet UIView *view_navBarParent;
@property (nonatomic, weak) IBOutlet UIView *view_audioCategory;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *tabCollectionView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIImageView *img_exclusive;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noResultFound;
//@property (nonatomic, weak) AudioCategoryTabVC *vc_audioCategoryTab;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *tabContentList;
@property (nonatomic, retain) NSMutableArray *allShowsList;
@property (nonatomic, retain) NSMutableArray *channelsList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isRadio;
@property (nonatomic) int selectedIndex;
-(void)loadDataFromServer:(NSString *)categoryId;

@end

NS_ASSUME_NONNULL_END
