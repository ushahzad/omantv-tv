//
//  AudioBooksVC.m
//  OmanTV
//
//  Created by Cenex Studio on 24/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "AudioBooksVC.h"

@interface AudioBooksVC ()

@end

@implementation AudioBooksVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.tabContentList = [NSMutableArray new];
    self.contentList = [NSMutableArray new];
    self.allShowsList = [NSMutableArray new];
    
    [StaticData scaleToArabic:self.collectionView];
    [StaticData scaleToArabic:self.tabCollectionView];
    
//    [self performSelector:@selector(setupAudioCategoryTab) withObject:self afterDelay:0];
    [self loadTabDataFromServer];
    
    if (@available(tvOS 11.0, *)) {
        self.tabCollectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.tabCollectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    [self.lbl_noResultFound setHidden:true];
}

- (void)orientationChanged:(NSNotification *)note
{
    [self reloadCollectionView];
}

//-(void)setupAudioCategoryTab
//{
//    self.vc_audioCategoryTab = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioCategoryTabVC"];
//
//    CGRect frame = self.view.frame;
//    CGFloat height = 100;
//
//    [self addChildViewController:self.vc_audioCategoryTab];
//
//    [self.vc_audioCategoryTab.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
//    [self.view_audioCategory addSubview:self.vc_audioCategoryTab.view];
//    [self.vc_audioCategoryTab didMoveToParentViewController:self];
//
//    __weak typeof(self) weakSelf = self;
//
//    self.vc_audioCategoryTab.UpdateContentSize = ^{
//        [weakSelf performSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
//        [weakSelf viewDidLayoutSubviews];
//    };
//
//    self.vc_audioCategoryTab.ChooseAudioBookTabCallBack = ^(MediaInfo *info){
//        NSLog(@"%@", info.ID);
//        if ([info.ID isEqualToString:@"-1"]) {
//            [weakSelf loadDataFromServer:@""];
//        }
//        else {
//            [weakSelf loadDataFromServer:info.ID];
//        }
//    };
//}


-(void)loadTabDataFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/filled_categories_by_genre?user_id=%@&key=%@&t=1632488243&channel_id=%@&is_radio=1",BaseURL, BaseUID, BaseKEY, ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"TabContentList"];
}

-(void)loadDataFromServer:(NSString *)categoryId
{
    [self.contentList removeAllObjects];
    [self reloadCollectionView];

    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/genres/shows_by_genre?user_id=%@&key=%@&t=1632488243&&channel_id=233&cat_id=%@&genre_id=&p=1&ended=no&limit=20&is_radio=1&need_show_times=no&need_channel=&custom_order=yes&need_avg_rating=yes",BaseURL, BaseUID, BaseKEY, categoryId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         
        [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ContentList"])
        {
            [self.contentList removeAllObjects];
            
            for (NSDictionary *dicObj in responseObject[@"data"][@"shows"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"category_channel_id"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                info.descriptions = [StaticData checkStringNull:dicObj[@"short_title_ar"]];
                if ([info.descriptions isEqualToString:@""]) {
                    info.descriptions = [StaticData checkStringNull:dicObj[@"description_ar"]];
                }
                
                info.channelLogoPath = [StaticData checkStringNull:dicObj[@"channel_thumbnail"]];
                info.rating = [[StaticData checkStringNull:dicObj[@"ratings_avg"]] floatValue];
                
                if ([dicObj[@"is_radio"] boolValue] == true) {
                    info.isRadio = true;
                }
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [self.contentList addObject:info];
                }
            }
            
            [self.lbl_noResultFound setHidden:true];
            if (self.contentList.count == 0) {
                [self.lbl_noResultFound setHidden:false];
            }
            [self reloadCollectionView];
        }
        else if ([callType isEqualToString:@"TabContentList"])
        {
            [self.tabContentList removeAllObjects];
            
            MediaInfo *mInfo = [MediaInfo new];
            mInfo.ID = @"";
            mInfo.title = @"الكل";
            [self.tabContentList addObject:mInfo];
            
            for (NSDictionary *dicObj in responseObject[@"data"][@"categories"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"category_channel_id"]];
                info.title = [StaticData title:dicObj];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                info.descriptions = [StaticData checkStringNull:dicObj[@"description_ar"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [self.tabContentList addObject:info];
                }
            }
            
//            [self checkRecordFound];
            [StaticData reloadCollectionView:self.tabCollectionView];
            [self performSelector:@selector(preferredFocusEnvironments) withObject:self afterDelay:1];
            
            if (self.tabContentList.count > 0) {
                MediaInfo *info = [MediaInfo new];
                info = [self.tabContentList objectAtIndex:0];
                [self loadDataFromServer:info.ID];
            }
        }
    } @catch (NSException *exception) {
//        [self checkRecordFound];
    } @finally {
    }
}

-(void)findChannelLogoPath:(MediaInfo *)info
{
    for (MediaInfo *channel in self.channelsList)
    {
        if ([channel.ID isEqualToString:info.channelID])
        {
            info.channelLogoPath = channel.channelLogoPath;
        }
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.tabCollectionView) {
        CGRect frame = collectionView.frame;
        CGFloat width = frame.size.width / 3.3;
        
        return CGSizeMake(width, frame.size.height);
    }
    return CGSizeMake(400, 780);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.tabCollectionView) {
        return self.tabContentList.count;
    }
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.tabCollectionView) {
        CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"tabCell" forIndexPath:indexPath];
        
        MediaInfo *info = self.tabContentList[indexPath.row];
        
        cell.lbl_title.text = info.title;
        
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.lbl_title setTextColor:[UIColor whiteColor]];
        
        cell.layer.borderColor = AppColor.CGColor;
        cell.layer.borderWidth = 1.0;
        
        cell.thumbnail.layer.cornerRadius = cell.frame.size.height/2;
        cell.thumbnail.clipsToBounds = YES;
        cell.layer.cornerRadius = cell.frame.size.height/2;
        cell.clipsToBounds = YES;
        
        if (indexPath.row == self.selectedIndex) {
            [cell.lbl_title setTextColor:[UIColor whiteColor]];
            [cell setBackgroundColor:AppColor];
            cell.layer.borderColor = AppColor.CGColor;
            cell.layer.borderWidth = 1.0;
        }
        else {
            [cell.lbl_title setTextColor:[UIColor whiteColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
            cell.layer.borderColor = AppColor.CGColor;
            cell.layer.borderWidth = 1.0;
        }
        
        return cell;
    }
    else {
        CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        MediaInfo *info = self.contentList[indexPath.row];
        
        cell.lbl_title.text = info.title;
        cell.lbl_date.text = info.descriptions;
        [StaticData addSpaceInLabelText:cell.lbl_date];
        cell.view_info.hidden = false;
        
        cell.btn_listenNow.layer.cornerRadius = cell.btn_listenNow.frame.size.height/2;
        cell.btn_listenNow.layer.borderColor = AppColor.CGColor;
        cell.btn_listenNow.layer.borderWidth = 2;
        cell.btn_listenNow.clipsToBounds = YES;
        
        cell.lbl_rating.text = [NSString stringWithFormat:@"%.01f/5",info.rating];
        cell.lbl_rating.adjustsFontSizeToFitWidth = true;
        
        cell.ratingView.value = info.rating;
        cell.ratingView.allowsHalfStars = YES;
        cell.ratingView.accurateHalfStars = YES;
        cell.ratingView.userInteractionEnabled = false;
        cell.ratingView.tintColor = AppColor;
        
        cell.btn_listenNow.tag = indexPath.row;
        [cell.btn_listenNow addTarget:self action:@selector(audioListenNow:) forControlEvents:UIControlEventTouchUpInside];
        
        [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
        
        @try {
            NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
            [cell.indicator startAnimating];
            cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                     if (!self.tempImage) {
                         self.tempImage = image;
                         [self reloadCollectionView];
                     }
                     [cell.indicator stopAnimating];
                 } else {
                     [cell.indicator stopAnimating];
                 }
             }];
        } @catch (NSException *exception) {
        } @finally {
        }
        
        cell.thumbnail.layer.cornerRadius = CellRounded;
        cell.thumbnail.layer.masksToBounds = true;
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.tabCollectionView) {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
        {
            if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
            {
                CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
                
                if (context.previouslyFocusedIndexPath.row == self.selectedIndex) {
                    [cell.lbl_title setTextColor:[UIColor whiteColor]];
                    [cell setBackgroundColor:AppColor];
                    cell.layer.borderColor = AppColor.CGColor;
                    cell.layer.borderWidth = 1.0;
                }
                else {
                    cell.view_info.hidden = true;
                    cell.backgroundColor =  [UIColor clearColor];
                    cell.layer.borderColor = AppColor.CGColor;
                    cell.layer.borderWidth = 1.0;
                }
                
                [UIView animateWithDuration:0.1 animations:^{
                    //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                }];
            }
            
            if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
            {
                CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
                cell.view_info.hidden = true;
                
                if (context.nextFocusedIndexPath.row == self.selectedIndex) {
                    [cell.lbl_title setTextColor:[UIColor whiteColor]];
                    [cell setBackgroundColor:AppColor];
                    cell.layer.borderColor = AppColor.CGColor;
                    cell.layer.borderWidth = 1.0;
                }
                else {
                    cell.clipsToBounds = true;
                    cell.backgroundColor =  AppColor;
                    cell.layer.borderColor = AppColor.CGColor;
                    cell.layer.borderWidth = CellBorderWidth;
                    cell.layer.cornerRadius = cell.frame.size.height/2;
                }
            }
        }
    }
    else {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;

            cell.btn_listenNow.backgroundColor =  [UIColor clearColor];
            cell.btn_listenNow.layer.borderColor = AppColor.CGColor;
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            
            cell.btn_listenNow.backgroundColor =  AppColor;
        }
        
        [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = [MediaInfo new];
    if (collectionView == self.tabCollectionView) {
        info = [self.tabContentList objectAtIndex:indexPath.row];
        [self loadDataFromServer:info.ID];
        
        self.selectedIndex = (int)indexPath.row;
        
        for (int i = 0; i < [self.tabContentList count]; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            CollectionCell *cell = (CollectionCell *)[self.tabCollectionView cellForItemAtIndexPath:indexPath];
            
            if (i == self.selectedIndex) {
                [cell.lbl_title setTextColor:[UIColor whiteColor]];
                [cell setBackgroundColor:AppColor];
                cell.layer.borderColor = AppColor.CGColor;
                cell.layer.borderWidth = 1.0;
            }
            else {
                [cell.lbl_title setTextColor:[UIColor whiteColor]];
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.layer.borderColor = AppColor.CGColor;
                cell.layer.borderWidth = 1.0;
            }
        }
        
        
    }
    else {
        info = [self.contentList objectAtIndex:indexPath.row];
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    }
//    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioBookDetailVC"];
//    [self.navigationController pushViewController:vc animated:true];
}

- (void)audioListenNow:(UIButton *)sender {
    MediaInfo *info = [MediaInfo new];
    info = [self.contentList objectAtIndex:sender.tag];
    
//    AudioBookDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioBookDetailVC"];
//    vc.selectedCategoryInfo = info;
//    [self.navigationController pushViewController:vc animated:true];
}

-(NSArray *)preferredFocusEnvironments
{
    CollectionCell *cell = [self findSelectedCell];
    if (cell) {
        return @[cell];
    }
    
    return nil;
}

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.tabContentList.count > 0) {
            for (int i = 0; i < self.tabContentList.count; i++)
            {
                MediaInfo *channel = self.tabContentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    CollectionCell *cell = (CollectionCell *)[self.tabCollectionView cellForItemAtIndexPath:indexPath];
                    if (cell){
                        [self.tabCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
                        return cell;
                    }
                    return nil;
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        CollectionCell *cell = (CollectionCell *)[self.tabCollectionView cellForItemAtIndexPath:indexPath];
        if (cell) {
            [self.tabCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return nil;
}

@end
