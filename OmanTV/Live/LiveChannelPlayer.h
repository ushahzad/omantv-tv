//
//  LiveChannelPlayer.h
//  AbuDhabi-ios

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "CustomAssetLoaderDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveChannelPlayer : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property void (^ResponseReceiveCallBack)(id json);
@property void (^ReloadCatchupCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *page_indicator;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_channels;

@property (nonatomic, retain) NSMutableArray *channelsList;
@property (nonatomic, retain) MediaInfo *selectedChannel;

@property (nonatomic, retain) AVPlayer *videoPlayer;
@property (nonatomic, retain) AVPlayerItem *playerItem;
@property (nonatomic, retain) AVPlayerLayer *videoPlayerLayer;
@property (nonatomic ,retain) CustomAssetLoaderDelegate *loaderDelegate;
@property (nonatomic ,retain) AVURLAsset *asset;
@property (nonatomic, retain) NSDictionary *playbackInfo;

@property (nonatomic, retain) NSLayoutConstraint *videoParentFixedHeight;

@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UIView *view_videoParent;
@property (nonatomic, weak) IBOutlet UIView *view_video;
@property (nonatomic, weak) IBOutlet UIView *view_videoControls;
@property (nonatomic, weak) IBOutlet UIView *view_airplay;
@property (nonatomic, weak) IBOutlet UIView *view_touchPad;
@property (nonatomic, retain) UITapGestureRecognizer *touchGesture;

@property (nonatomic, weak) IBOutlet UIImageView *img_channelCover;
@property (nonatomic, weak) IBOutlet UIImageView *img_defaultLogoAudio;
@property (nonatomic, weak) IBOutlet UIImageView *img_nav_logo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *img_side_menuRightConstraint;
@property (nonatomic, weak) IBOutlet UIImageView *img_side_menu;

@property (nonatomic, weak) IBOutlet UIView *view_openChannels;
@property (nonatomic, weak) IBOutlet UIImageView *img_openChannelDropdown;
@property (nonatomic, weak) IBOutlet UIImageView *img_selectedChannelLogo;
@property (nonatomic, weak) IBOutlet UIImageView *img_selectedChannelArrow;
@property (nonatomic, weak) IBOutlet UIButton *btn_openChannels;

@property (nonatomic, weak) IBOutlet UIButton *btn_play;
@property (nonatomic, weak) IBOutlet UIButton *btn_fullScreen;
@property (nonatomic, assign) BOOL viewIsActive;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingAdsVideoIndicator;
@property (nonatomic, weak) IBOutlet UIView *view_chromCast;

@property (nonatomic, strong) id playbackObserver;
@property (nonatomic, assign) BOOL observingMediaPlayer;
@property (nonatomic, assign) BOOL observingAdsMediaPlayer;

@property (nonatomic, strong) NSTimer *hideVideoControlsTimer;
@property (nonatomic, strong) NSTimer *updateOlineTimer;
@property (nonatomic,retain) NSMutableString *sessionID;
@property (nonatomic, retain) NSString *shorterURL;

@property (nonatomic, retain) NSMutableArray *preAdList;
@property (nonatomic, retain) NSMutableArray *midAdList;
@property (nonatomic, retain) NSMutableArray *postAdList;

@property (nonatomic, assign) NSString *playbackUrl;
@property (nonatomic, assign) BOOL isFindLocalVideoPath;

@property(nonatomic, strong) AVPlayer *adPlayer;
@property(nonatomic, strong) AVPlayerLayer *adPlayerLayer;

@property(nonatomic, weak) IBOutlet UIView *adsVideoView;
@property(nonatomic, weak) IBOutlet UIView *adsControlsView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_adsCount;
@property (weak, nonatomic) IBOutlet UIButton *btn_skip;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

@property (nonatomic, retain) NSString *channelName;
@property (nonatomic, retain) NSString *channelThumbnail;

@property (nonatomic, retain) UIImage *tempImage;

@property (nonatomic, assign) BOOL isLiveShowBlockedInMyCountry;
@property (nonatomic, assign) BOOL isLiveShowDigitalRightDisable;
@property (nonatomic, assign) BOOL isNormalVideoPlaying;
@property (nonatomic, assign) BOOL playVideoNormalyNow;
@property (nonatomic, assign) int currentPlayingVideoTime;
@property (nonatomic, assign) BOOL isReadyToPlay;
@property (nonatomic, assign) int isRadio;
-(void)removePlayerObserve;

@end

NS_ASSUME_NONNULL_END
