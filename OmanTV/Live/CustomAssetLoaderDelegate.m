//
// 0 0Custom0AssetLoaderDelegate.m
//  DRMPla0yer
//
//  Created by MacUser on 26/090/2020.
//  Copyright0 © 2020 MacUser. All rights reserved.
//

#import "CustomAssetLoaderDelegate.h"

//#define KEY_SERVER_URL @"https://fps.ezdrm.com/api/licenses/e884521e-f98f-4029-a378-a6a5798b2505?ClientIP=39.49.20.168"

@implementation CustomAssetLoaderDelegate

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)setupServerInfoWithLicenses:(NSString *)licenses withCertificate:(NSString *)certificate
{
    self.keyServerUrl = licenses;
    self.certificateUrl = certificate;
}

-(void)removeKey:(NSFileManager *)fileManager
{
    
}

/*------------------------------------------
**
** getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest
**
** takes the bundled SPC and sends it the the
** key server defined at KEY_SERVER_URL in the View Controller
** it returns a CKC which then is returned.
** ---------------------------------------*/
-(NSData *)getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest:(NSData *)requestBytes assetId:(NSString *)assetId customParams:(NSString *)customParams errorOut:(NSError *)errorOut
{
    NSData *decodedData = nil;
    NSURLResponse *response = nil;
    
    NSURL *ksmURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",self.keyServerUrl,assetId,customParams]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:ksmURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-type"];
    request.HTTPBody = requestBytes;
    
    @try {
        NSError *error;
        NSData *dataVal = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        decodedData = dataVal;
    } @catch (NSException *exception) {
    } @finally {
    }
    
    
    return decodedData;
}


/*------------------------------------------
**
** getAppCertificate
**
** returns the apps certificate for authenticating against your server
** the example here uses a local certificate
** but you may need to edit this function to point to your certificate
** ---------------------------------------*/

-(NSData *)getAppCertificate:(NSString *)assetId
{
    NSData *certificate = nil;
    NSURL *cert = [NSURL URLWithString:self.certificateUrl];
    certificate = [NSData dataWithContentsOfURL:cert];
    if (!certificate) {
        return nil;
    }
    
    return certificate;
}

-(BOOL)resourceLoader:(AVAssetResourceLoader *)resourceLoader shouldWaitForLoadingOfRequestedResource:(nonnull AVAssetResourceLoadingRequest *)loadingRequest
{
    NSLog(@"%@",loadingRequest.request);
    
    NSURL *assetURI = loadingRequest.request.URL;
    NSString *assetID = @"e884521e-f98f-4029-a378-a6a5798b2505";
    NSString *scheme = assetURI.scheme;
    
    NSData *requestBytes;
    NSData *certificate;
    
    //skd is the scheme that the key requests use,makse sure that we are only doing key requests
    if (!([scheme isEqualToString:@"skd"])){
        return false;
    }
    
    @try {
        certificate = [self getAppCertificate:assetID];
    } @catch (NSException *exception) {
        [loadingRequest finishLoadingWithError:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorClientCertificateRejected userInfo:nil]];
    } @finally {
    }
    
    NSError *error;
    @try {
        requestBytes = [loadingRequest streamingContentKeyRequestDataForApp:certificate contentIdentifier:[assetID dataUsingEncoding:NSUTF8StringEncoding] options:nil error:&error];
    } @catch (NSException *exception) {
        [loadingRequest finishLoadingWithError:nil];
    } @finally {
    }
    
    NSString *passthruParams = @"?customdata=MTp3c2lsdmFAc2Nob29sb2ZuZXQuY29tOjc3Mjc6Y291cnNl";
    NSData *responseData;
    
    responseData = [self getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest:requestBytes assetId:assetID customParams:passthruParams errorOut:error];
    
    if (responseData != nil) {
        AVAssetResourceLoadingDataRequest *dataRequest = [loadingRequest dataRequest];
        [dataRequest respondWithData:responseData];
        [loadingRequest finishLoading];
    } else {
        [loadingRequest finishLoadingWithError:error];
    }
    
    return true;
}

-(BOOL)resourceLoader:(AVAssetResourceLoader *)resourceLoader shouldWaitForRenewalOfRequestedResource:(nonnull AVAssetResourceRenewalRequest *)renewalRequest
{
    return [self resourceLoader:resourceLoader shouldWaitForLoadingOfRequestedResource:renewalRequest];
}
@end
