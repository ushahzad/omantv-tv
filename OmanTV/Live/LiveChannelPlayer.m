//
//  LiveChannelPlayer.m
//  AbuDhabi-ios
#import "LiveChannelPlayer.h"

@interface LiveChannelPlayer ()
@end

@implementation LiveChannelPlayer

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.channelsList = [NSMutableArray new];
    
    if (@available(iOS 11.0, *)) {
        self.cv_channels.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }

    if (@available(iOS 13.0, *)) {
        [self.cv_channels setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    _viewIsActive = true;
    
    UISwipeGestureRecognizer *upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showHideLiveChanelsList:)];
    upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:upSwipe];
    
    UISwipeGestureRecognizer *downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showHideLiveChanelsList:)];
    downSwipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:downSwipe];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playPausePlayer)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuButtonPressed:)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeMenu], nil];
    [self.view addGestureRecognizer:gesture];

    [StaticData scaleToArabic:self.cv_channels];
    [Notify addObserver:self selector:@selector(pauseChannel) name:LiveChannelNOTI object:nil];
    [Notify addObserver:self selector:@selector(removePlayerObserve) name:RemovePlayerObserveNOTI object:nil];	
    
    self.view_openChannels.layer.cornerRadius = 25.0;
    self.view_openChannels.clipsToBounds = true;
    self.view_openChannels.backgroundColor = [StaticData colorFromHexString:@"#6B6B6B"];
    
    self.channelName = @"";
    self.channelThumbnail = @"";
    
//    self.lbl_title.text = self.selectedChannel.title;
    
//    if (self.isRadio) {
//        [self loadRadioChannelsFromServer];
//    }
//    else {
//        [self loadTVChannelsFromServer];
//    }
    
    [self.img_defaultLogoAudio setHidden:true];
    [self loadTVChannelsFromServer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setupAnalytics];
    
    [self.btn_openChannels addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.viewIsActive = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationDidBecomeActiveNotification object:nil];
}


-(void)appWillResignActive:(NSNotification*)note
{
    if (!self.videoPlayer.externalPlaybackActive) {
        [self pauseChannel];
    }
}

-(void)appWillEnterForeground:(NSNotification*)note
{
    _viewIsActive = true;
}

-(void)appWillTerminate:(NSNotification*)note
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (self.updateOlineTimer.isValid) {
        [self.updateOlineTimer invalidate];
    }
    
    [self removePlayerObserve];
}

-(void)menuButtonPressed:(UITapGestureRecognizer *)gesture
{
    if (!self.view_videoControls.hidden) {
        [self isNeedToHideVideoControls:true];
    } else {
        //[self varifyBeforeExitApp];
        [self pauseChannel];
        [self.navigationController popViewControllerAnimated:true];
        //exit(0);
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_openChannels)
        {
            if (self.cv_channels.hidden)
            {
                self.cv_channels.hidden = false;
            } else
            {
                //self.img_selectedChannelArrow.image = [UIImage imageNamed:@"channel_dopdown_arrow"];
                self.cv_channels.hidden = true;
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)playPausePlayer
{
    if (self.videoPlayer.rate == 1.0)
    {
        [self isNeedToHideVideoControls:false];
        [self pauseChannel];
    } else {
        [self playChannel];
    }
}

-(void)pauseChannel
{
    _viewIsActive = false;
    [_videoPlayer pause];
    _view_videoControls.hidden = false;
    
    [self.hideVideoControlsTimer invalidate];
    self.hideVideoControlsTimer = nil;
}

-(void)playChannel
{
    _viewIsActive = true;
    [_videoPlayer play];
    [self hideVideoControls];
}

-(void)openChannels
{
    if (self.cv_channels.hidden) {
        [self isNeedToHideVideoControls:false];
    } else {
        [self isNeedToHideVideoControls:true];
    }
}

-(void)showHideLiveChanelsList:(UISwipeGestureRecognizer *)swipe
{
    if (swipe.direction == UISwipeGestureRecognizerDirectionUp)
    {
        [self hideVideoControls];
        [self isNeedToHideVideoControls:false];
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionDown)
    {
        [self isNeedToHideVideoControls:true];
    }
}

-(void)hideVideoControls
{
    [self.hideVideoControlsTimer invalidate];
    self.hideVideoControlsTimer = nil;
    
    self.hideVideoControlsTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hideVControls) userInfo:nil repeats:NO];
}

-(void)hideVControls
{
    if (self.videoPlayer.rate == 1.0) {
        [self isNeedToHideVideoControls:true];
    }
}

-(void)isNeedToHideVideoControls:(BOOL)isHide
{
    self.cv_channels.hidden = isHide;
    self.view_videoControls.hidden = isHide;
}

- (void)orientationChanged:(NSNotification *)note
{
//    CGRect bound = [UIScreen mainScreen].bounds;
//
//    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) || bound.size.width > bound.size.height)
//    {
//        [self setupFullScreen];
//    } else
//    {
//        [self setupSmallScreen];
//    }
//    self.extendedLayoutIncludesOpaqueBars = true;
}

-(void)setupPlayer:(MediaInfo *)channelInfo
{
    [self resetPlayer];
    
    self.selectedChannel = channelInfo;
}

-(void)loadTVChannelsFromServer
{
    //[self resetControllerData];
    
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/plus/live_channels?user_id=%@&key=%@&&json=1&is_radio=0&mixed=yes&info=1&mplayer=true&need_live=yes&need_next=yes&next_limit=2&need_playback=yes",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"TVChannelList"];
}

-(void)loadRadioChannelsFromServer
{
    //[self resetControllerData];
    
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/plus/live_channels?user_id=%@&key=%@&&json=1&is_radio=1&mixed=yes&info=1&mplayer=true&need_live=yes&need_next=yes&next_limit=2&need_playback=yes",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"RadioChannelList"];
}

-(void)loadLiveChannelDetails
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/plus/getchanneldetails?key=%@&user_id=%@&channel_id=%@&app_id=&need_playback=yes",BaseURL,BaseKEY,BaseUID,self.selectedChannel.ID];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[NSString stringWithFormat:@"ChannelDetails/%@",self.selectedChannel.ID]];
}

-(void)loadNowNextChannelInfo:(NSString *)channelID
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/plus/live?user_id=%@&key=%@&channel_id=%@&previous_limit=0&next_limit=2&app_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedChannel.ID,AppID];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"NowNext"];
}

#pragma mark setup Ads
-(void)checkAdsForLiveChannel
{
    [self executeApiToGetApiResponse:[NSString stringWithFormat:@"%@/api/getLiveAds/%@/%@/%@",BaseURL,self.selectedChannel.ID,BaseUID,BaseKEY] withParameters:nil withCallType:@"LiveDFPAds"];
}

-(void)getLiveStreamingURL
{
    [self.indicator startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/plus/getStreamURL?user_id=%@&key=%@&channel_id=%@&app_id=%@",BaseURL,BaseUID,BaseKEY,self.selectedChannel.ID,AppID];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"LiveStreaming"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    if ([callType isEqualToString:@"ShorterUrl"])
    {
        //manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        if ([callType isEqualToString:@"ShorterUrl"])
        {
            responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        }
        
         [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
         [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"TVChannelList"])
        {
            [self loadRadioChannelsFromServer];
            
            for (NSDictionary *chDic in responseObject)
            {
                NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
                if ([enableLive isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
                {
                    continue;
                }
                
                if (chDic[@"catchup"] && [chDic[@"catchup"] intValue] == 0)
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:chDic[@"id"]];
                info.playbackUrl = [StaticData checkStringNull:chDic[@"playbackURL"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
                info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
                
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:chDic[@"icon"]];
                }
                info.thumbnail = [StaticData checkStringNull:chDic[@"cover"]];
                
                NSArray *live = [StaticData checkArrayNull:chDic[@"live"]];
                for (NSDictionary *dic in live)
                {
                    info.title = [StaticData checkStringNull:dic[@"title"]];
                }
                NSLog(@"%@",info.title);
                
                if ([StaticData isVideoBlockInMyCountry:chDic isShowGeoStatus:false])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData checkIsScueduleGeoBlockOnLiveChannel:chDic])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData isLiveChannelDigitalRightsDisable:chDic])
                {
                    info.isLiveShowDigitalRightDisable = true;
                }
                
                [_channelsList addObject:info];
                
            } 
            
            if (self.channelsList.count > 0)
            {
                MediaInfo *info = self.channelsList[0];
                if (self.selectedChannel) {
                    for (MediaInfo *infoInfo in _channelsList)
                    {
                        if ([self.selectedChannel.ID isEqualToString:infoInfo.ID])
                        {
                            info = infoInfo;
                            
                            if (info.isLiveShowDigitalRightDisable)
                            {
                                [self channelDigitalRightIsDiable];
                            } else if (info.isLiveShowBlockedInMyCountry)
                            {
                                [self channelIsBlockInYourCountry];
                            } else
                            {
                                [self playSelectedChannel:info];
                            }
                        }
                    }
                    
                }
                else {
                    if (info.isLiveShowDigitalRightDisable)
                    {
                        [self channelDigitalRightIsDiable];
                    } else if (info.isLiveShowBlockedInMyCountry)
                    {
                        [self channelIsBlockInYourCountry];
                    } else
                    {
                        [self playSelectedChannel:info];
                    }
                }
                
            }
            
            self.cv_channels.hidden = true;
            [StaticData reloadCollectionView:self.cv_channels];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.cv_channels.hidden = false;
                [self updateFocusOnShowHideButton];
            });
            
        } else if ([callType isEqualToString:@"RadioChannelList"])
        {
            for (NSDictionary *chDic in responseObject)
            {
                NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
                if ([enableLive isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([[StaticData checkStringNull:chDic[@"activated"]] isEqualToString:@"0"])
                {
                    continue;
                }
                
                if (chDic[@"catchup"] && [chDic[@"catchup"] intValue] == 0)
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.isRadioChannel= true;
                info.ID = [StaticData checkStringNull:chDic[@"id"]];
                info.playbackUrl = [StaticData checkStringNull:chDic[@"playbackURL"]];
                //info.categoryID = [StaticData checkStringNull:subDic[@"cat_id"]];
                info.title = [StaticData checkStringNull:chDic[@"title_ar"]];
                info.thumbnailPath = [StaticData checkStringNull:chDic[@"thumbnail"]];
                
                if (info.thumbnailPath.length == 0) {
                    info.thumbnailPath = [StaticData checkStringNull:chDic[@"icon"]];
                }
                
                info.thumbnail = [StaticData checkStringNull:chDic[@"cover"]];
                
                if ([StaticData isVideoBlockInMyCountry:chDic isShowGeoStatus:false])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData checkIsScueduleGeoBlockOnLiveChannel:chDic])
                {
                    info.isLiveShowBlockedInMyCountry = true;
                }
                
                if ([StaticData isLiveChannelDigitalRightsDisable:chDic])
                {
                    info.isLiveShowDigitalRightDisable = true;
                }
                
                [_channelsList addObject:info];
            }
            
            if (self.channelsList.count > 0)
            {
                MediaInfo *info = self.channelsList[0];
                if (self.selectedChannel) {
                    for (MediaInfo *infoInfo in _channelsList)
                    {
                        if ([self.selectedChannel.ID isEqualToString:infoInfo.ID])
                        {
                            info = infoInfo;
                        }
                    }
                    
                    if (info.isLiveShowDigitalRightDisable)
                    {
                        [self channelDigitalRightIsDiable];
                    } else if (info.isLiveShowBlockedInMyCountry)
                    {
                        [self channelIsBlockInYourCountry];
                    } else
                    {
                        [self playSelectedChannel:info];
                    }
                    
                    if (info.isRadioChannel) {
                        [self.img_defaultLogoAudio setHidden:false];
                    }
                }
                else {
                    if (info.isLiveShowDigitalRightDisable)
                    {
                        [self channelDigitalRightIsDiable];
                    } else if (info.isLiveShowBlockedInMyCountry)
                    {
                        [self channelIsBlockInYourCountry];
                    } else
                    {
                        [self playSelectedChannel:info];
                    }
                }
            }
            
            self.cv_channels.hidden = true;
            [StaticData reloadCollectionView:self.cv_channels];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.cv_channels.hidden = false;
                [self updateFocusOnShowHideButton];
            });
        } else if ([callType containsString:@"ChannelDetails"])
        {
            NSArray *types = [callType componentsSeparatedByString:@"/"];
            if (types.count > 1) {
                NSString *activeChannelID = types[1];
                if (![_selectedChannel.ID isEqualToString:activeChannelID])
                {
                    return;
                }
            }
            NSDictionary *chDic = responseObject;
            NSString *enableLive = [StaticData checkStringNull:chDic[@"enable_live"]];
            if ([enableLive isEqualToString:@"no"])
            {
                self.isLiveShowBlockedInMyCountry = true;
            }
            
            self.channelName = [StaticData checkStringNull:chDic[@"AppName"]];
            self.channelThumbnail = [StaticData checkStringNull:chDic[@"secondary_thumbnail"]];
            
            if ([StaticData isVideoBlockInMyCountry:chDic isShowGeoStatus:false])
            {
                self.isLiveShowBlockedInMyCountry = true;
            }
            
            if ([StaticData checkIsScueduleGeoBlockOnLiveChannel:chDic])
            {
                self.isLiveShowBlockedInMyCountry = true;
            }
            
            if ([StaticData isLiveChannelDigitalRightsDisable:chDic])
            {
                self.isLiveShowDigitalRightDisable = true;
            }
            
            if (self.isLiveShowDigitalRightDisable)
            {
                [self channelDigitalRightIsDiable];
            } else if (self.isLiveShowBlockedInMyCountry)
            {
                [self channelIsBlockInYourCountry];
            } else
            {
                [self initOnlineURLAsset:chDic];
            }
            
        } else if ([callType isEqualToString:@"LiveStreaming"])
        {
            _playbackUrl = [StaticData checkStringNull:responseObject[@"URL"]];
            
        } else if ([callType isEqualToString:@"NowNext"])
        {
            if (self.ResponseReceiveCallBack) {
                self.ResponseReceiveCallBack(responseObject);
            }
        } else if ([callType isEqualToString:@"LiveDFPAds"])
        {
            [self checkAdsFindForSelectedChannel:responseObject];
        } else if ([callType isEqualToString:@"ShorterUrl"])
        {
            BOOL isSuccess = [responseObject[@"success"] boolValue];
            if (isSuccess) {
                self.shorterURL = [StaticData checkStringNull:responseObject[@"data"][@"full_shorten_link"]];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)loadChannelCurrentShowImage:(NSString *)showThumbnailPath
{
    @try {
        self.img_channelCover.hidden = false;
        NSString * encodedCover = [showThumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        self.img_channelCover.contentMode = UIViewContentModeScaleAspectFill;
        [self.img_channelCover sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:self.img_channelCover withUrl:imageURL completed:^(UIImage *image) {
                 }];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

/* -----------------------------------------
** initURLAsset
**
** creates the AVURLAseet to use for playback if one has not been already created
** since we only have one stream we can presume the asset will always be the same
** so by checking we can save memory and prevent state issues due to configuration
** on the asset changing
** ----------------------------------------*/
-(void)initOnlineURLAsset:(id)chDic
{
    if (chDic && [chDic[@"playbackURL"] isKindOfClass:[NSDictionary class]])
    {
        _playbackInfo = [StaticData checkDictionaryNull:chDic[@"playbackURL"]];
        _playbackUrl = [StaticData checkStringNull:_playbackInfo[@"m3u8_url"]];
        
        BOOL drm = [_playbackInfo[@"drm"] boolValue];
        [self initOnlineURLAsset:chDic];
        if (drm) {
            NSURL *url = [NSURL URLWithString:_playbackUrl];
            //set AssetLoaderDelegate to version that dosn't set keys
            self.loaderDelegate = [CustomAssetLoaderDelegate new];
            
            NSString *licenses = [StaticData checkStringNull:_playbackInfo[@"fairplay_license_url"]];
            NSString *certificate = [StaticData checkStringNull:_playbackInfo[@"fairplay_certificate_uri"]];
            
            [self.loaderDelegate setupServerInfoWithLicenses:licenses withCertificate:certificate];
            self.asset = [AVURLAsset assetWithURL:url];
            [self.asset.resourceLoader setDelegate:self.loaderDelegate queue:[self globalNotificationQueue]];
            
            [self readyMediaStream];
        }
    } else
    {
        _playbackUrl = [StaticData checkStringNull:chDic[@"playbackURL"]];
        NSURL *url = [NSURL URLWithString:_playbackUrl];
        self.asset = [AVURLAsset assetWithURL:url];
        
        [self initPlay:self.asset];
    }
     
}

/* ----------------------------
 ** globalNotificationQueue
 
 ** Returns a Dispatch Que for the AVAssetLoader Delegate
 
---------------------------- */
-(dispatch_queue_t)globalNotificationQueue
{
    dispatch_queue_attr_t qos = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, -1);
    dispatch_queue_t recordingQueue = dispatch_queue_create("Stream Queue", qos);
    return recordingQueue;
}

-(void)readyMediaStream
{
    NSArray *requestedKeys = @[@"playable"];
    if (self.asset)
    {
        [self.asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:^{
            NSError *error;
            AVKeyValueStatus status = [self.asset statusOfValueForKey:@"playable" error:&error];
            switch (status) {
                case AVKeyValueStatusLoaded:
                    if([self.asset isPlayable]){
                        [self initPlay:self.asset];
                    }
                    else {
                        NSLog(@"Not Playable");
                    }
                    break;
                
                case AVKeyValueStatusFailed:
                    NSLog(@"No Playable Status");
                    break;
                case AVKeyValueStatusCancelled:
                    NSLog(@"Loading Cancelled");
                    break;
                    
                default:
                    NSLog(@"loading error Unknown");
                    break;
            }
        }];
    }
}

//makes the asset preperation calls Async so other actions can occur at the same time
-(void)initPlay:(AVURLAsset *)asset
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self prepareToPlayAsset:asset];
        
        [self checkAdsForLiveChannel];
    });
}

/* --------------------------------------------------------------
**
**  prepareToPlayAsset:withKeys
**
**  Invoked at the completion of the loading of the values for all
**  keys on the asset that we require. Checks whether loading was
**  successfull and whether the asset is playable. If so, sets up
**  an AVPlayerItem and an AVPlayer to play the asset.
**
** ----------------------------------------------------------- */
#pragma mark PlayVideoNow
-(void)prepareToPlayAsset:(AVURLAsset *)asset
{
    @try {
        if (_playbackUrl.length == 0) {
            [self getLiveStreamingURL];
            return;
        }
        
        [self resetPlayer];
        
        // if we are already have an AVPlayer Item stop listening to it
        if(self.playerItem != nil){
            [self.playerItem removeObserver:self forKeyPath:@"status"];
            [self removePlayerObserve];
        }
        
        if (self.selectedChannel.isRadioChannel) {
            [self loadChannelCurrentShowImage:self.selectedChannel.thumbnail];
        } else {
            self.img_channelCover.hidden = true;
        }
        
        CGRect frame = CGRectMake(0, 0, self.view_video.frame.size.width, self.view_video.frame.size.height);
        
        // Create a new AVPlayerItem with the given AVURLAsset
        self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
        
        self.videoPlayer = [AVPlayer playerWithPlayerItem:self.playerItem];
        self.videoPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.videoPlayer];
        [self.videoPlayerLayer setFrame:frame];
        [_view_video.layer addSublayer:self.videoPlayerLayer];
        self.videoPlayer.usesExternalPlaybackWhileExternalScreenIsActive = true;
        
        //if (self.playVideoNormalyNow) {
            [_videoPlayer play];
            [self.btn_play setImage:[UIImage imageNamed:@"paus"] forState:UIControlStateNormal];
//        } else{
//            [self.videoPlayer pause];
//            [self.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
//        }
        

        [self.videoPlayer setAllowsExternalPlayback:YES];
        self.videoPlayer.usesExternalPlaybackWhileExternalScreenIsActive = YES;
        
        [StaticData setupExternalPlayer:self.videoPlayer];
        [Notify postNotificationName:HomeLiveChannelNOTI object:nil];
        [Notify postNotificationName:VideoPlayerNOTI object:nil];
        
//        MPVolumeView *airplayButton = [[MPVolumeView alloc] init];
//        frame = self.view_airplay.frame;
//        frame.origin.x = 0;
//        frame.origin.y = 0;
//        airplayButton.frame = frame;
//        [airplayButton setShowsVolumeSlider:NO];
//        [self.view_airplay addSubview:airplayButton];
        
        CMTime interval = CMTimeMake(33, 1000);
        __weak __typeof(self) weakself = self;
        self.playbackObserver = [self.videoPlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
            
            if (weakself.viewIsActive)
            {
                if (!weakself.viewIsActive)
                {
                    [weakself.videoPlayer pause];
                    [weakself.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                }
            } else {
                [weakself.videoPlayer pause];
                [weakself.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
            }
        }];
        
        NSError *setCategoryError = nil;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error: &setCategoryError];
        
        // Add an Observer to the new AVPlayerItem to listen to the Status Key for when it is ready to play
        [self.playerItem addObserver:self forKeyPath:@"status" options: NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew  context:nil];
        
        [self.playerItem addObserver:self
                                   forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew
                                      context:nil];
        [self.playerItem addObserver:self
                                   forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew
                                      context:nil];
        [self.playerItem addObserver:self
                                   forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionNew
                                      context:nil];
        
        [self setupAnalytics];
        
        _observingMediaPlayer = true;
        
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    NSLog(@"observeValueForKeyPath %@", keyPath);
    if (!_videoPlayer.currentItem || (object != _videoPlayer.currentItem)) {
        return;
    }
    
    if ([keyPath isEqualToString:@"status"]) {
        if (_videoPlayer.status == AVPlayerStatusReadyToPlay) {
            self.isReadyToPlay = true;
            [self.indicator stopAnimating];
            if (!_viewIsActive)
            {
                [_videoPlayer pause];
            }
        }
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        // Show loader
        [self.indicator startAnimating];
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        // Hide loader
        [self.indicator stopAnimating];
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        // Hide loader
        [self.indicator stopAnimating];
    }
}

-(void)removePlayerObserve
{
    @try {
        if (self.updateOlineTimer.isValid) {
            [self.updateOlineTimer invalidate];
        }
        [self.playerItem removeObserver:self forKeyPath:@"status"];
        [self.playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [self.playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        [self.playerItem removeObserver:self forKeyPath:@"playbackBufferFull"];
    } @catch (NSException *exception) {
    } @finally {
    }
}
    
-(void)playListStreamingNow
{
    if (self.playbackUrl.length == 0) {
        [self getLiveStreamingURL];
    } else {
        
    }
}

-(NSString *)myCountryCode
{
    @try {
        NSString *countryCode = [StaticData shared].myCountryCode;
        if (countryCode.length == 0) {
            countryCode = [StaticData getMyCountryCode];
        }
        
        // in case if country code not able to find from server then we use thiss
        if (countryCode.length == 0) {
            NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
            countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        }
        
        return countryCode;
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

-(void)channelIsBlockInYourCountry
{
    [self.indicator stopAnimating];
    [self.videoPlayer pause];
    [self.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [StaticData showAlertView:ShowNotAvailableInYourCountry withvc:self];
}

-(void)channelDigitalRightIsDiable
{
    [self.indicator stopAnimating];
    [self.videoPlayer pause];
    [self.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [StaticData showAlertView:DigitalRightIsDiabel withvc:self];
}

-(NSString*)getStringFromCMTime:(CMTime)time
{
    Float64 currentSeconds = CMTimeGetSeconds(time);
    int mins = currentSeconds/60.0;
    int secs = fmodf(currentSeconds, 60.0);
    NSString *minsString = mins < 10 ? [NSString stringWithFormat:@"0%d", mins] : [NSString stringWithFormat:@"%d", mins];
    NSString *secsString = secs < 10 ? [NSString stringWithFormat:@"0%d", secs] : [NSString stringWithFormat:@"%d", secs];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}

-(IBAction)playVideo:(id)sender
{
    if (self.isLiveShowDigitalRightDisable) {
        [self channelDigitalRightIsDiable];
        return;
    } else if (self.isLiveShowBlockedInMyCountry) {
        [self channelIsBlockInYourCountry];
        return;
    }
    
    if (!self.videoPlayer.externalPlaybackActive && [StaticData shared].externalVideoPlayer.isExternalPlaybackActive)
    {
        [StaticData setupExternalPlayer:self.videoPlayer];
    }
    
    [Notify postNotificationName:HomeLiveChannelNOTI object:nil];
    [Notify postNotificationName:VideoPlayerNOTI object:nil];
    
    if ([self.btn_play.currentImage isEqual:[UIImage imageNamed:@"play"]])
    {
        [_videoPlayer play];
        [self.btn_play setImage:[UIImage imageNamed:@"paus"] forState:UIControlStateNormal];
        [self hideVideoControls];
    } else
    {
        [_videoPlayer pause];
        [self.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    }
}

-(void)setupAnalytics
{
    @try {
        if (self.sessionID.length == 0) {
            NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
            self.sessionID = [NSMutableString stringWithCapacity:32];
            for (NSUInteger i = 0U; i < 32; i++) {
                u_int32_t r = arc4random() % [alphabet length];
                unichar c = [alphabet characterAtIndex:r];
                [self.sessionID appendFormat:@"%C", c];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.updateOlineTimer.isValid) {
                [self.updateOlineTimer invalidate];
            }
            self.updateOlineTimer = nil;
            self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(UpdateAnalyticsRequest) userInfo:nil repeats:YES];
        });
    } @catch (NSException *exception) {
    } @finally {
    }
}


-(void)UpdateAnalyticsRequest
{
    @try {
        if (!self.selectedChannel) return;
        
        NSString *channelID = self.selectedChannel.ID;
        [MangoMoloAnalytics updateOnlineWithChannelID:channelID withSessionID:self.sessionID withVideoID:@""];
    } @catch (NSException *exception) {
        NSLog(@"Update online Request = %@",exception.description);
    } @finally {
        
    }
}

-(void)resetPlayer
{
    [_videoPlayer pause];
    _videoPlayer = nil;
    [_videoPlayerLayer removeFromSuperlayer];
    [self.btn_play setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
}

- (NSString *)dateFormatted:(MediaInfo *)object
{
    NSString *time = object.startTime;
    NSDateFormatter *df = [NSDateFormatter new];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [df setTimeZone:gmt];
    [df setDateFormat:@"HH:mm"];
    NSDate *videoDate = [df dateFromString:time];
    [df setDateFormat:@"hh:mm"];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"MeccaTime"])
    {
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        // add 3 hours + with current time (this is for mecca time)
        dayComponent.hour = -4;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        videoDate = [theCalendar dateByAddingComponents:dayComponent toDate:videoDate options:0];
    }
    
    NSString *newTime = [df stringFromDate:videoDate];
    
    return newTime;
}

#pragma mark Check Ads Find
-(void)checkAdsFindForSelectedChannel:(id)response
{
    @try {
        self.preAdList = [NSMutableArray new];
        self.midAdList = [NSMutableArray new];
        self.postAdList = [NSMutableArray new];
        
//        NSString *const kTestAppAdTagUrl =
//        @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&"
//        @"iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&"
//        @"gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&"
//        @"cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";
//
//        [self.preAdList addObject:@{@"ad_type":@"double_click",@"google_doubleclick":kTestAppAdTagUrl}];
        
        if ([response valueForKey:@"preroll"])
        {
            for (NSDictionary *adDetails in [response valueForKey:@"preroll"])
            {
                [self.preAdList addObject:adDetails];
            }
        }
        
        if ([response valueForKey:@"midroll"])
        {
            for (NSDictionary *adDetails in [response valueForKey:@"midroll"])
            {
                [self.midAdList addObject:adDetails];
            }
        }
        
        if ([response valueForKey:@"postroll"])
        {
            for (NSDictionary *adDetails in [response valueForKey:@"postroll"])
            {
                [self.postAdList addObject:adDetails];
            }
        }
        
        if (self.preAdList.count > 0)
        {
            //[self setupPlayingAds:self.preAdList];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(BOOL)checkVideoDuration:(NSMutableArray *)adsList
{
    for (NSDictionary *dic in adsList)
    {
        if (self.currentPlayingVideoTime >= [[dic valueForKey:@"duration"] intValue])
        {
            return true;
        }
    }
    
    return false;
}

-(int)checkAddType:(NSMutableArray *)adData
{
    for (NSDictionary *dic in adData)
    {
        if ([dic valueForKey:@"ad_type"])
        {
            if ([[dic valueForKey:@"ad_type"] isEqualToString:@"local_server"])
            {
                return 1;
            } else if ([[dic valueForKey:@"ad_type"] isEqualToString:@"double_click"])
            {
                return 2;
            }
        }
    }
    
    return 0;
}

- (void)requestAdsPre:(NSString *)url {
    [self.videoPlayer pause];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/5.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width);
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.channelsList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (self.channelsList.count == 0)
    {
        [cell.indicator startAnimating];
        return cell;
    }
    
    MediaInfo *info = self.channelsList[indexPath.row];
    
    cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
    cell.layer.cornerRadius = CellRounded;
    cell.layer.masksToBounds = true;
    
    cell.backgroundColor = [[StaticData colorFromHexString:@"#C2C2C2"] colorWithAlphaComponent:1.0];

    cell.lbl_title.text = info.title_ar;
    
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:info.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [cell.indicator startAnimating];
        
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [StaticData reloadCollectionView:self.cv_channels];
                 }
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }

    cell.layer.cornerRadius= 16.0;
    cell.clipsToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.channelsList.count == 0) return;

        for (MediaInfo *info in self.channelsList)
        {
            info.isSelected = false;
        }
        
        self.playVideoNormalyNow = true;
        
        [self isNeedToHideVideoControls:true];
        
        MediaInfo *info = self.channelsList[indexPath.row];
        if (info.isRadioChannel) {
            [self.img_defaultLogoAudio setHidden:false];
        }
        else {
            [self.img_defaultLogoAudio setHidden:true];
        }
        
        [self playSelectedChannel:info];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)playSelectedChannel:(MediaInfo *)info
{
    if (info.isLiveShowDigitalRightDisable)
    {
        [self channelDigitalRightIsDiable];
    } else if (info.isLiveShowBlockedInMyCountry)
    {
        [self channelIsBlockInYourCountry];
    } else
    {
        self.isLiveShowDigitalRightDisable = false;
        self.isLiveShowBlockedInMyCountry = false;
        
        info.isSelected = true;
        self.selectedChannel = info;
        [self loadCoverImage:info];
        
        self.lbl_title.text = self.selectedChannel.title;
        [self resetPlayer];
//        [StaticData reloadCollectionView:self.cv_channels];
        
        [self loadLiveChannelDetails];
        
        [self loadSelectedChannelLogo];
    }
}

-(void)loadSelectedChannelLogo
{
    @try {
        NSString *imagePath = [BaseImageURL stringByAppendingString:self.selectedChannel.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [self.img_selectedChannelLogo sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
             } else {
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        cell.view_info.hidden = true;
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        cell.view_info.hidden = false;
        cell.backgroundColor = [UIColor whiteColor];
        
        if (!self.isReadyToPlay) {
            return;
        }
        if (self.channelsList.count == 0) return;

        for (MediaInfo *info in self.channelsList)
        {
            info.isSelected = false;
        }
        
        self.playVideoNormalyNow = true;
        
        //[self isNeedToHideVideoControls:true];
        
        NSIndexPath *indexPath = context.nextFocusedIndexPath;
        
        MediaInfo *info = self.channelsList[indexPath.row];
        if (info.isRadioChannel) {
            [self loadCoverImage:info];
            
            [self.img_defaultLogoAudio setHidden:false];
        }
        else {
            [self.img_defaultLogoAudio setHidden:true];
        }
        
        [self playSelectedChannel:info];
    }
    
    [self hideVideoControls];
    
    
    
    
    /*if ([context.previouslyFocusedItem isKindOfClass:[CollectionCell class]])
    {
        MediaInfo *info = self.channelsList[context.previouslyFocusedIndexPath.row];
        if (info.isSelected)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedItem;
            if (cell) {
                cell.layer.borderColor = [StaticData colorFromHexString:@"#E4013B"].CGColor;
                cell.layer.borderWidth = CellBorderWidth;
            }
        }
    }*/
}

-(void)loadCoverImage:(MediaInfo *)info
{
    NSString *imagePath = [BaseImageURL stringByAppendingString:info.thumbnail];
    imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    if ([imagePath containsString:@".svg"]) {
        imagePath = [BaseImageURL stringByAppendingString:info.thumbnailPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    [self.img_defaultLogoAudio sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {}];
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.nextFocusedView == self.btn_openChannels) {
        self.view_openChannels.backgroundColor = [UIColor whiteColor];
        UIImage *image = [StaticData changeImageColor:[UIImage imageNamed:@"channel_dopdown_arrow_selected"] withColor:AppColor];
        self.img_selectedChannelArrow.image = image;
    }
    
    if (context.previouslyFocusedView == self.btn_openChannels) {
        self.view_openChannels.backgroundColor = [StaticData colorFromHexString:@"#6B6B6B"];
        
        UIImage *image = [StaticData changeImageColor:[UIImage imageNamed:@"channel_dopdown_arrow_selected"] withColor:[UIColor whiteColor]];
        self.img_selectedChannelArrow.image = image;
    }
}

-(void)updateFocusOnShowHideButton
{
    [self setNeedsFocusUpdate];
    [self updateFocusIfNeeded];
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        CollectionCell *cell = [self findSelectedCell];
        if (cell) {
            return @[cell];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return @[];
}

-(CollectionCell *)findSelectedCell
{
    @try {
        for (MediaInfo *info in self.channelsList) {
            if (info.isSelected)
            {
                NSInteger index = [self.channelsList indexOfObject:info];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
                CollectionCell *cell = (CollectionCell *)[self.cv_channels cellForItemAtIndexPath:indexPath];
                return cell;
            }
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return nil;
}

@end

