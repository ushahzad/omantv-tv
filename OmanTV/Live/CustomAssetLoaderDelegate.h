//
//  CustomAssetLoaderDelegate.h
//  DRMPlayer
//
//  Created by MacUser on 26/09/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVKit/AVKit.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface CustomAssetLoaderDelegate : NSObject <AVAssetResourceLoaderDelegate>
@property (nonatomic, retain) NSString *keyServerUrl;
@property (nonatomic, retain) NSString *certificateUrl;

-(void)removeKey:(NSFileManager *)fileManager;
-(void)setupServerInfoWithLicenses:(NSString *)licenses withCertificate:(NSString *)certificate;
@end

NS_ASSUME_NONNULL_END
