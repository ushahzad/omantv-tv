//
//  FeaturedShowsHomeInfo.h
//  AwaanAppleTV-New
//
//  Created by Curiologix on 13/09/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewAligned.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeaturedShowsHomeInfo : UIViewController
@property void(^PlayerPlayCallBlock)(void);
@property void(^PlayerStartPlayingCallBlock)(void);
@property void(^HomeBannerSwipeUpCallBack)(void);

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, retain) IBOutlet UIView *view_liveChannelShowDetails;
@property (nonatomic, retain) IBOutlet UIImageView *img_channelShowGradient;
@property (nonatomic, weak) IBOutlet UIImageView *img_channelPreview;

@property (nonatomic, retain) IBOutlet UIView *view_liveChannelPlayer;

@property (nonatomic, weak) IBOutlet UIView *view_play;
@property (nonatomic, weak) IBOutlet UIView *view_right_arrow;
@property (nonatomic, weak) IBOutlet UIView *view_left_arrow;
@property (nonatomic, weak) IBOutlet UIView *view_info;
@property (nonatomic, weak) IBOutlet UIButton *btn_play;

@property (nonatomic, weak) IBOutlet UIButton *btn_right_arrow;
@property (nonatomic, weak) IBOutlet UIButton *btn_left_arrow;

@property (nonatomic, weak) IBOutlet UIImageView *img_right_arrow;
@property (nonatomic, weak) IBOutlet UIImageView *img_left_arrow;

@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UILabel *lbl_subTitle;
@property (nonatomic, retain) AVPlayer *channelPlayer;
@property (nonatomic, retain) AVPlayerLayer *channelPlayerLayer;

@property (nonatomic, retain) NSMutableString *sessionID;

@property (nonatomic, retain) NSTimer *updateOlineTimer;
@property (nonatomic, retain) NSTimer *checkPlayerVisibility;
@property (nonatomic, retain) NSTimer *loadPlayerTimer;

@property (nonatomic, assign) BOOL isCurrentPlayerIsRemoved;
@property (nonatomic, assign) BOOL isViewActive;
@property (nonatomic, assign) BOOL isPlayerPause;

@property (nonatomic, retain) MediaInfo *selectedShow;

@property (nonatomic, retain) NSString *activeImagePath;

-(void)setupShowDetails:(MediaInfo *)show;
@end

NS_ASSUME_NONNULL_END
