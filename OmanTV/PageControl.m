//
//  DotcomPageControl.m
//  AbuDhabi-ios
//
//  Created by MacUser on 06/02/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "PageControl.h"

@implementation PageControl

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.transform = CGAffineTransformMakeScale(2, 2);
}

-(void)updateBullets
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView * dot = [self imageViewForSubview:  [self.subviews objectAtIndex: i]];
        if (i == self.currentPage)
        {
            dot.image = [UIImage imageNamed:@"selected_pgController"];
            
        } else {
            dot.image = [UIImage imageNamed:@"unselected_pgController"];
            
        }
    }
    self.transform = CGAffineTransformMakeScale(2, 2);
}

- (UIImageView *) imageViewForSubview: (UIView *) view
{
    UIImageView * dot = nil;
    if ([view isKindOfClass: [UIView class]])
    {
        for (UIView* subview in view.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                dot = (UIImageView *)subview;
                break;
                
            }
            
        }
        if (dot == nil)
        {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, view.frame.size.width, view.frame.size.height)];
            //[view addSubview:dot];
            
        }
    } else
    {
        dot = (UIImageView *) view;
        
    }
    return dot;
    
}
@end
