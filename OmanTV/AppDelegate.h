//
//  AppDelegate.h
//  OmanTV
//
//  Created by MacUser on 21/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

#define appDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)
@property (strong, nonatomic) UIWindow *window;


@end

