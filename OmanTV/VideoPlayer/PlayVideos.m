//
//  PlayVideos.m
//  TvDcnDigital
//
//00  Created by Ali on 3/13/16.
//  Copyright © 2016 Ali. All rights reserved.0
//

#import "PlayVideos.h"
#import "MangoMoloAnalytics.h"
#import "SRTParser.h"
#import "LocalAdPlayer.h"
#import "IMAAdPlayer.h"
@import GoogleInteractiveMediaAds;

// The content URL to play.
NSString *const kTestAppContentUrl_MP4 =
@"https://storage.googleapis.com/gvabox/media/samples/stock.mp4";

// Ad tag
//NSString *const kTestAppAdTagUrl = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&"
//@"iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&"
//@"output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&"
//@"correlator=";

// Ad tag
NSString *const kTestAppAdTagUrl = @"https://pubads.g.doubleclick.net/gampad/ads?sz=640x360&iu=/7229/Awaan&ciu_szs=300x250,300x600,320x50,320x100,728x90,970x90,970x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=https://www.awaan.ae&ad_rule=1&correlator=1567656365&cmsid=2470034&vid=26859004&cust_params=inApp%3Dyes";

@interface PlayVideos () <IMAAdsLoaderDelegate, IMAAdsManagerDelegate>
{
    NSDictionary *bgjson;
    // NSDictionary *viewsjson;
    NSString *video_url;
    NSTimer *timer;
    int videoPlayingTag;
    
}
// SDK
/// Entry point for the SDK. Used to make ad requests.
@property(nonatomic, strong) IMAAdsLoader *adsLoader;

/// Playhead used by the SDK to track content video progress and insert mid-rolls.
@property(nonatomic, strong) IMAAVPlayerContentPlayhead *contentPlayhead;

/// Main point of interaction with the SDK. Created by the SDK as the result of an ad request.
@property(nonatomic, strong) IMAAdsManager *adsManager;

@end

@implementation PlayVideos
@synthesize videoDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.videoThumbnails = [NSMutableArray new];
    
    self.view_progressThumbnail.hidden = true;
    view_videoControls.hidden = true;
    
    [StaticData scaleToArabic:cv_relatedVideos];
    
    self.btn_plus.hidden = true;
    self.img_plus.hidden = true;
    self.view_nextView.hidden = true;
    
    UITapGestureRecognizer *menuGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuTap)];
    menuGestureRec.allowedPressTypes = @[@(UIPressTypeMenu)];
    [self.view addGestureRecognizer:menuGestureRec];
    
    self.progressThumb.image = [self.progressThumb.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.progressThumb setTintColor:AppColor];
    
    [self isNeedToShowVideoThumbnails:false];
    
    [self loadVideoDetails];
    
    self.btn_introSkip.hidden = true;
    self.btn_introSkip.layer.cornerRadius = 14;
    self.btn_introSkip.layer.borderColor = [StaticData colorFromHexString:[StaticData appColorCode]].CGColor;
    self.btn_introSkip.layer.borderWidth = 1.0;
    self.btn_introSkip.clipsToBounds = YES;
    
    self.btn_subtitle.hidden = true;
    self.btn_subtitle.layer.cornerRadius = 14;
    self.btn_subtitle.clipsToBounds = YES;
}

-(void)loadVideoDetails
{
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    self.view_nextOverLay.layer.borderWidth = 10;
    self.view_nextOverLay.layer.borderColor = [UIColor whiteColor].CGColor;
    self.view_nextOverLay.hidden = true;
    
    [self setupVideoApiRequest];
}

-(void)handleMenuTap
{
    if(view_videoControls.hidden == false){
        view_videoControls.hidden = true;
    } else {
        [self removePlayer];
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

-(void)setupVideoApiRequest
{
    self.viewIsActive = true;
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    thirtyTwoChar = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [thirtyTwoChar appendFormat:@"%C", c];
    }
    
    relatedVideos = [NSMutableArray new];
    self.preAdList = [NSMutableArray new];
    self.midAdList = [NSMutableArray new];
    self.postAdList = [NSMutableArray new];
    
    self.resumeVideoPosition = 0;
    
    self.img_ads.hidden = true;
    
    [self.indicator startAnimating];
    
    NSString *fullVideoPath = @"";
    if ([StaticData shared].userInfo) {
        fullVideoPath = [NSString stringWithFormat:@"%@/nand/fullVideo?key=%@&user_id=%@&id=%@&channel_userid=%@&app_id=%@",BaseURL,BaseKEY,BaseUID,self.show_id,[StaticData shared].userInfo.ID,AppID];
    } else {
        fullVideoPath = [NSString stringWithFormat:@"%@/nand/fullVideo?key=%@&user_id=%@&id=%@&app_id=%@&device_id=%@",BaseURL,BaseKEY,BaseUID,self.show_id,AppID,[StaticData shared].deviceID];
    }
    
    [self executePostApiToGetResponse:fullVideoPath parameters:nil callType:@"Video"];
}

#pragma mark - Api Call Request
// make web service api call
-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if ([type isEqualToString:@"setVideoComplitionRate"])
    {
        [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    } else if ([type isEqualToString:@"ShorterUrl"])
    {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    } else {
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        if ([type isEqualToString:@"ShorterUrl"] || [type isEqualToString:@"setVideoComplitionRate"])
        {
            responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        }
        NSLog(@"Response Received = %@",responseObject);
        [self responseReceived:responseObject callType:type];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
        NSLog(@"Api Error: %@",error.description);
    }];
}

#pragma mark response received
-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        if ([type isEqualToString:@"Video"])
        {
            self.btn_plus.hidden = false;
            self.img_plus.hidden = false;
            
            NSDictionary *dicObj = [StaticData checkDictionaryNull:response[@"parent"]];
            
            bgjson = response;
            self.videoDetails = response;
            
            [self setupSubtitles];
            
            [self setupVideoOnWaterMark];
            
            self.img_wide = [StaticData checkStringNull:response[@"img_wide"]];
            // here we check length > 1 because some time img_wide have 0
            if (self.img_wide.length > 0 && self.img_wide.length > 1) {
                [self loadVVTLargeImage:self.img_wide];
            }
            
            // find related videos of selected video
            [relatedVideos removeAllObjects];
            for (NSDictionary *dic in response[@"related"])
            {
                if ([[StaticData checkStringNull:dic[@"smartTV"]] intValue] == 1)
                {
                    if ([[StaticData checkStringNull:dic[@"publish"]] isEqualToString:@"yes"])
                    {
                        MediaInfo *info = [MediaInfo new];
                        info.ID = [StaticData checkStringNull:dic[@"id"]];
                        info.title_ar = [self removeShowNameWithTitle:[StaticData checkStringNull:dic[@"title_ar"]]];
                        info.title_en = [self removeShowNameWithTitle:[StaticData checkStringNull:dic[@"title_en"]]];
                        info.imagePath = [StaticData checkStringNull:dic[@"img"]];
                        if ([[StaticData checkStringNull:dic[@"id"]] isEqualToString:self.show_id]) {
                            info.isSelected = true;
                        }
                        
                        if (![StaticData isVideoBlockInMyCountry:dic isShowGeoStatus:false])
                        {
                            //[relatedVideos addObject:info];
                        }
                    }
                }
            }
            
            if (relatedVideos.count == 0) {
                self.cv_heightAnchor.constant = 0.0;
            } else {
                self.cv_heightAnchor.constant = 0.0;//200.0;
            }
            
            [self reloadCollectionView];
            
            [self performSelector:@selector(openPlayingVideo) withObject:nil afterDelay:1];
            
            // find next episode of selected video show
            for (NSDictionary *nextEP in response[@"next"])
            {
                self.nextEpisode = nextEP;
                [self setupNextEpisodeOfSelectedVideoShow];
            }
            
            [self setupVideoResumePosition:response];
            
            // here we are getting show title like showname:mm/dd/yyyy so we will remove the title plus : and keep the date if show title dont have : then we will take the full title
            NSString *showTitle = [StaticData checkStringNull:response[@"title_ar"]];
            NSArray *showsTiles = [showTitle componentsSeparatedByString:@":"];
            if (showsTiles.count > 1)
            {
                if (self.isComeFromResumeWatching)
                {
                    _VideoTitle.text = [NSString stringWithFormat:@"%@ - %@",[StaticData checkStringNull:response[@"cat_ar"]],showsTiles[1]];
                } else {
                    _VideoTitle.text = showsTiles[1];
                }
            } else {
                if (self.isComeFromResumeWatching)
                {
                    _VideoTitle.text = [NSString stringWithFormat:@"%@ - %@",[StaticData checkStringNull:response[@"cat_ar"]],showTitle];
                } else {
                    _VideoTitle.text = showTitle;
                }
            }
            
            _lbl_time.text = @"";
            /*NSString *time =  response[@"create_time"];
             if (![time isKindOfClass:[NSNull class]] && time.length > 0)
             {
             _lbl_time.text = [self getTimeFromString:time];
             }*/
            
            // check here video is allowed in your country or not based on your geo statu
            BOOL videoIsAllowedInMyCountry = true;
            
            if ([StaticData isVideoBlockInMyCountry:response isShowGeoStatus:false])
            {
                videoIsAllowedInMyCountry = false;
                [self videoIsNotAllowedToPlay];
            }
            
            video_url = [StaticData checkStringNull:response[@"playbackURL"]];
            
            NSDictionary *adsJson = response[@"ads"];
            
            NSArray *adsArray = [StaticData checkArrayNull:adsJson[@"overlay"]];
            for (NSDictionary *ad in adsArray) {
                
                NSString *countries = ad[@"countries"];
                if ([countries containsString:[StaticData shared].myCountryCode]) {
                    self.overlayDic = ad;
                    [self overlayAd];
                    break;
                }
            }
            
            for (NSDictionary *ad in adsJson[@"preroll"]) {
                [self.preAdList addObject:ad];
            }
            
            for (NSDictionary *ad in adsJson[@"midroll"]) {
                [self.midAdList addObject:ad];
            }
            
            for (NSDictionary *ad in adsJson[@"postroll"]) {
                [self.postAdList addObject:ad];
            }
            
            // Play video
            if (videoIsAllowedInMyCountry)
            {
                //if (self.preAdList.count > 0)
                //{
                //    videoPlayingTag = 1;
                //    [self setupPlayingAds:self.preAdList];
                //} else {
                videoPlayingTag = 2;
                [self setUpContentPlayer];
                [self setupPlayingAds:self.preAdList];
                //}
                
                [self.updatePlayBackTimer invalidate];
                self.updatePlayBackTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updatePlaybackProgressFromTimer:) userInfo:nil repeats:YES];
            }
        } else if ([type isEqualToString:@"UpDateOnline"])
        {
            
        } else if ([type isEqualToString:@"setResume"])
        {
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"PlayVideos has issue\n issue = %@",exception.description]];
        
        // if video details api have some problem then we will force to play video
        if ([type isEqualToString:@"Video"] && response)
        {
            video_url = [StaticData checkStringNull:response[@"playbackURL"]];
            if (video_url.length > 0)
            {
                [self setUpContentPlayer];
            }
        }
    } @finally {
    }
}

-(void)overlayAd
{
    NSString *mediaType = [StaticData checkStringNull:self.overlayDic[@"media_type"]];
    if ([mediaType isEqualToString:@"image"])
    {
        self.img_ads.hidden = true;
        NSString *imagePath = [NSString stringWithFormat:@"https://%@",[StaticData checkStringNull:self.overlayDic[@"m3u8"]]];
        
        [self.img_ads sd_setImageWithURL:[NSURL URLWithString:imagePath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
}

-(void)setupVideoOnWaterMark
{
    @try {
        NSDictionary *playerInfo = [StaticData checkDictionaryNull:videoDetails[@"player_info"]];
        if (playerInfo)
        {
            self.img_player_watermark.hidden = false;
            NSString * encodedCover = [[StaticData checkStringNull:playerInfo[@"logo"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSString *coverPath = [BaseImageURL stringByAppendingString:encodedCover];
            
//            self.lbl_nextvideoTitle.text = [StaticData checkStringNull:self.nextEpisode.title];

            self.img_player_watermark.contentMode = UIViewContentModeScaleAspectFit;
            [self.img_player_watermark sd_setImageWithURL:[NSURL URLWithString:coverPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                NSLog(@"Success");
            }];
            
            [self updateWaterMarkPosition];
        } else
        {
            self.img_player_watermark.hidden = true;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)updateWaterMarkPosition
{
    CGFloat paddingTop = 50.0;
    CGFloat sidePadding = 50.0;
    CGRect frame = self.img_player_watermark.frame;
    frame.size.width = 180.0;
    frame.size.height = 95.0;
    
//    if ([StaticData isViewLandscapeMode]) {
//        frame.size.width = 50.0;
//        frame.size.height = 30.0;
//    }
    
    NSDictionary *playerInfo = [StaticData checkDictionaryNull:videoDetails[@"player_info"]];
    NSString *positionType = @"top-right";
    if ([[StaticData checkStringNull:playerInfo[@"position"]] isEqualToString:positionType])
    {
        frame.origin.x = self.videoView.frame.size.width - frame.size.width - sidePadding;
        frame.origin.y = paddingTop;
        self.img_player_watermark.frame = frame;
    } else if ([[StaticData checkStringNull:playerInfo[@"position"]] isEqualToString:positionType])
    {
        frame.origin.x = sidePadding;
        frame.origin.y = paddingTop;
        self.img_player_watermark.frame = frame;
    } else if ([[StaticData checkStringNull:playerInfo[@"position"]] isEqualToString:positionType])
    {
        frame.origin.x = sidePadding;
        frame.origin.y = self.videoView.frame.size.height - frame.size.height - paddingTop;
        self.img_player_watermark.frame = frame;
    } else if ([[StaticData checkStringNull:playerInfo[@"position"]] isEqualToString:positionType])
    {
        frame.origin.x = self.videoView.frame.size.width - frame.size.width - sidePadding;
        frame.origin.y = self.videoView.frame.size.height - frame.size.height - paddingTop;
        self.img_player_watermark.frame = frame;
    } else if ([positionType isEqualToString:positionType])
    {
        frame.origin.x = (self.videoView.frame.size.width / 2) - sidePadding;
        frame.origin.y = (self.videoView.frame.size.height / 2) - paddingTop;
        self.img_player_watermark.frame = frame;
    }
}


-(void)setupVideoResumePosition:(id)response
{
    @try {
        //find resume position of selected video
        if ([response[@"resume_position"] isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *posotion = response[@"resume_position"];
            if (posotion[@"position"])
            {
                int duration = [[StaticData checkStringNull:response[@"duration"]] intValue];
                int position = [[StaticData checkStringNull:posotion[@"position"]] intValue];
                
                if (position+20 >= duration) {
                    self.resumeVideoPosition = position - 20;
                } else if (position < duration){
                    self.resumeVideoPosition = position;
                }
            }
        }
        
        if (self.resumeVideoPosition == 0)
        {
            if ([response[@"skip_duration"] isKindOfClass:[NSNumber class]])
            {
                _skipDurationPosition = [response[@"skip_duration"] intValue];
                self.btn_introSkip.hidden = false;
            } else if ([response[@"skip_duration"] isKindOfClass:[NSString class]])
            {
                _skipDurationPosition = [[StaticData checkStringNull:response[@"skip_duration"]] intValue];
                self.btn_introSkip.hidden = false;
            }
        }
        
        if (_skipDurationPosition == 0 || _skipDurationPosition < 0) {
            if ([response[@"cat_skip_duration"] isKindOfClass:[NSNumber class]])
            {
                _skipDurationPosition = [response[@"cat_skip_duration"] intValue];
                self.btn_introSkip.hidden = false;
            } else if ([response[@"cat_skip_duration"] isKindOfClass:[NSString class]])
            {
                _skipDurationPosition = [[StaticData checkStringNull:response[@"cat_skip_duration"]] intValue];
                self.btn_introSkip.hidden = false;
            }
        }
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)skipIntro
{
    self.btn_introSkip.hidden = true;
    
    self.resumeVideoPosition = self.skipDurationPosition;
    if (self.resumeVideoPosition > 0 && self.videoPlayer) {
        CMTime resumeTimer = CMTimeMake(self.resumeVideoPosition, 1);
        [self.videoPlayer seekToTime:resumeTimer toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        
        self.resumeVideoPosition = 0;
        self.skipDurationPosition = 0;
    }
}

// here we are getting show title like showname:mm/dd/yyyy so we will remove the title plus : and keep the date if show title dont have : then we will take the full title
-(NSString *)removeShowNameWithTitle:(NSString *)videoTitle
{
    @try {
        NSArray *showsTiles = [videoTitle componentsSeparatedByString:@":"];
        if (showsTiles.count >= 1)
        {
            return showsTiles[1];
        } else {
            return videoTitle;
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    return videoTitle;
}

-(void)loadVVTLargeImage:(NSString *)widePath
{
    if (widePath.length > 0)
    {
        NSString *path = [WideImageUrl stringByAppendingString:widePath];
        _videoThumbnails = [[SRTParser shared] loadVideoThumbnailsWithUrl:path];
        [StaticData reloadCollectionView:self.cv_thumbnails];
        
        if (_videoThumbnails.count > 0) {
            [self loadLargeVideoThumbnail:_videoThumbnails[0]];
        }
    }
}

-(void)openPlayingVideo
{
    @try {
        for (MediaInfo *info in relatedVideos) {
            if (info.isSelected)
            {
                NSInteger index = [relatedVideos indexOfObject:info];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
                [cv_relatedVideos scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
                break;
            }
        }
    } @catch (NSException *exception) {
        [StaticData log:[NSString stringWithFormat:@"openPlayingVideo has issue\n issue = %@",exception.description]];
    } @finally {
    }
}

-(NSString *)getTimeFromString:(NSString *)time
{
    @try {
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormater dateFromString:time];
        
        NSDateFormatter *dfTime = [[NSDateFormatter alloc] init];
        [dfTime setDateFormat:@"dd/MM/yyyy"];
        NSString *current_time = [dfTime stringFromDate:date];
        return current_time;
    } @catch (NSException *exception) {
    } @finally {
    }
    return @"";
}

#pragma mark Setup Next Video
-(void)setupNextEpisodeOfSelectedVideoShow
{
    @try {
        if (self.nextEpisode) {
            NSString * encodedCover = [self.nextEpisode[@"img"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSString *coverPath = [BaseImageURL stringByAppendingString:encodedCover];
            
            self.lbl_nextShowTitle.text = [self removeShowNameWithTitle:[StaticData checkStringNull:self.nextEpisode[@"title_ar"]]];
            [StaticData findeDigitFromString:self.lbl_nextShowTitle];
            
            [self.nextShowIndicator startAnimating];
            self.img_nextShow.contentMode = UIViewContentModeScaleAspectFit;
            [self.img_nextShow sd_setImageWithURL:[NSURL URLWithString:coverPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                self.img_nextShow.image = image;
                [self.nextShowIndicator stopAnimating];
                
                if (!image) {
                    [StaticData downloadImageIfNotLoadinedBySdImage:self.img_nextShow withUrl:imageURL completed:^(UIImage *image) {
                    }];
                }
            }];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)videoIsNotAllowedToPlay
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:@"هذا المحتوى غير مسموح في بلدك" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.viewIsActive = true;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(haldePlayAndPause:)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [btn_playPause addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(replayVideoAction)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [btn_replay addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNextEpisodeNow)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [self.btn_nextVideo addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSeasonsAndEpisodes)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [self.btn_plus addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(seekVideoBasedOnProgressThumbnail)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [btn_progressFocuse addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipIntro)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [self.btn_introSkip addGestureRecognizer:gesture];
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadSubtitle)];
    gesture.allowedPressTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:UIPressTypeSelect], nil];
    [self.btn_subtitle addGestureRecognizer:gesture];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(haldePlayAndPause:)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownToShowRelativeVideo)];
    recognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:recognizer];
    
    /*UISwipeGestureRecognizer *rightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forwardVideo:)];
     rightGesture.direction = UISwipeGestureRecognizerDirectionRight;
     [btn_progressFocuse addGestureRecognizer:rightGesture];
     
     UISwipeGestureRecognizer *leftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forwardBackVideo:)];
     leftGesture.direction = UISwipeGesture00RecognizerDirectionLeft;
     [btn_progressFocuse addGestureRecognizer:leftGesture];*/
    
    // add pan recognizer to the view when initialized
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
    [panRecognizer setDelegate:self];
    [btn_progressFocuse addGestureRecognizer:panRecognizer];
}

-(void)panRecognized:(UIPanGestureRecognizer *)sender
{
    @try {
        if (!isProgressActive){
            return;
        }
        //[btn_progressFocuse setBackgroundColor:[UIColor greenColor]];
        
        UIPanGestureRecognizer *panRecog = (UIPanGestureRecognizer *)sender;
        CGPoint vel = [panRecog velocityInView:self.view];
        NSLog(@"vel: %@", NSStringFromCGPoint(vel));
        
        NSLog(@"state: %ld", (long)panRecog.state);
        
        UIView *recogView = [panRecog view];
        
        CGPoint translation = [panRecog translationInView:recogView];
        //CGFloat curY = recogView.frame.origin.x;
        
        CGRect frame = self.progressThumb.frame;
        

        if (!self.currentProgressStateChanged) {
            self.currentPlayerSeconds = CMTimeGetSeconds(self.videoPlayer.currentTime);
        }
        
        self.currentProgressStateChanged = true;
        double duration = CMTimeGetSeconds(self.videoPlayer.currentItem.asset.duration);
        CGFloat xPosition = ((self.progressBar.frame.size.width) / (duration)) * 5;
        [self.videoPlayer pause];
        CGFloat xValue = 0;
        if (translation.x > 0) {
            
            xValue = frame.origin.x + xPosition;
            if (frame.origin.x > self.view.frame.size.width - 50)
            {
                xValue = self.view.frame.size.width - 50;
            }
            
            if (xValue <= duration) {
                frame.origin.x = xValue;
                self.progressThumb.frame = frame;
            }
            
        } else {
            xValue = frame.origin.x -= xPosition;
            if (xValue < 50)
            {
                xValue = 50;
            }
            frame.origin.x = xValue;
            self.progressThumb.frame = frame;
        }
        
        
        if (translation.x > 0)
        {
            self.currentPlayerSeconds += 5;
            if (self.currentPlayerSeconds >= duration)
            {
                self.currentPlayerSeconds = duration;
            }
        } else {
            _currentPlayerSeconds -= 5;
            if (self.currentPlayerSeconds < 0)
            {
                self.currentPlayerSeconds = 0;
            }
        }
        
        
        if (frame.origin.x > self.view.frame.size.width - 100 - 50)
        {
        } else if (frame.origin.x < 50)
        {
            xValue = 50;
            self.currentPlayerSeconds = 0.0;
        }
        

        if (self.img_wide.length > 0 && self.img_wide.length > 1) {
            self.view_progressThumbnail.hidden = false;
            MediaInfo *info = [self updateVideoThumbnailPosition:self.currentPlayerSeconds];
            if (info) {
                self.selectedPorgressThumbnailInfo = info;
                //self.currentPlayerSeconds = info.thumbStartTime;
                NSLog(@"Thumbanil found");
            } else {
                NSLog(@"no Thumbanil found");
            }
        }
        
        CMTime seekTime = CMTimeMakeWithSeconds(self.currentPlayerSeconds, self.videoPlayer.currentTime.timescale);
        //CMTime seekTime = CMTimeMakeWithSeconds(self.progressBar.progress * (double)self.videoPlayer.currentItem.asset.duration.value/(double)self.videoPlayer.currentItem.asset.duration.timescale, self.videoPlayer.currentTime.timescale);
        //[self.videoPlayer seekToTime:seekTime];
        
        NSString *currentTime = [self getStringFromCMTime:seekTime];
        self.lbl_currentTime.text = currentTime;
        self.lbl_progressTime.text = currentTime;
        
        [self startTimerToHideVideoControls];
        
        //[self updateProgressThumbnailValue]//;
        
        
        //        double duration = CMTimeGetSeconds(self.videoPlayer.currentItem.asset.duration);
        //        double normalizedTime = (double) self.currentPlayerSeconds /  duration;
        //        self.progressBar.progress = normalizedTime;
        
        [self updateProgressThumbnailPosition];
        
        //[self updateProgressThumbnailValue];
        
        //    } else if (panRecog.state == UIGestureRecognizerStateEnded)
        //    {
        //
        //    }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    //if (panRecog.state == UIGestureRecognizerStateChanged)
    //{
     //   NSLog(@"state changed");
        // drag view vertially
        
        
        
        //           CGRect frame = recogView.frame;
        //           frame.origin.x= curY + translation.x;
        //           if (frame.origin.x > self.view.frame.size.width - 100 - 50)
        //           {
        //               frame.origin.x = self.view.frame.size.width - 100;
        //           } else if (frame.origin.x < 50)
        //           {
        //               frame.origin.x = 50;
        //           }
        //           recogView.frame = frame;
        //[panRecog setTranslation:CGPointMake(0.0f, 0.0f) inView:recogView];
        
    //} else {
    //    NSLog(@"state not changed");
    //}
    
    /*else if (panRecog.state == UIGestureRecognizerStateEnded) {
     
     // touch up, animate to top of self.view w/ spring
     NSLog(@"final vel: %@", NSStringFromCGPoint(vel));
     
     CGFloat finalX = recogView.frame.origin.x;
     CGFloat finalY = 50.0f;
     CGFloat curY = recogView.frame.origin.y;
     
     CGFloat distance = finalX - finalY;
     
     CGFloat animationDuration = 2.0f;
     
     // normalize velocity as per docs
     // multiply by -1 in this case b/c final desitination y is less
     // than current y and recog's y velocity is negative when draggin up
     // (therefore also works when released when dragging down)
     CGFloat springVelocity = -1.0f * vel.x / distance;
     
     CGFloat springDampening = 0.5f;
     
     NSLog(@"dist: %f - spring vel: %f", distance, springVelocity);
     
     // for clean velocity transfer, use UIViewAnimationOptionCurveLinear
     [UIView animateWithDuration:animationDuration delay:0.0 usingSpringWithDamping:springDampening initialSpringVelocity:springVelocity options:UIViewAnimationOptionCurveLinear animations:^{
     
     //               CGRect frame = recogView.frame;
     //               frame.origin.x = finalX;
     //               if (frame.origin.x < 50)
     //               {
     //                   frame.origin.x = 50;
     //               }
     //               //frame.origin.y = finalY;
     //               recogView.frame = frame;
     
     } completion:^(BOOL finished) {
     NSLog(@"done animating");
     }];
     
     
     }*/
    
    //    CGPoint distance = [sender translationInView:self.view];
    //
    //    //NSLog(@"distance.x - %f", distance.x);
    //    //NSLog(@"distance.y - %f", distance.y);
    //
    //    if (sender.state == UIGestureRecognizerStateChanged) {
    //        [sender cancelsTouchesInView];
    //
    //
    //        if (distance.x > 70 && distance.y > -50 && distance.y < 50) { // right
    //
    //            NSLog(@"user swiped right");
    //            NSLog(@"distance.x - %f", distance.x);
    //
    //            CGRect frame = self.progressThumb.frame;
    //
    //            CGFloat xValue = frame.origin.x + distance.x;
    //
    //            if (xValue <= self.view.frame.size.width) {
    //                frame.origin.x = xValue;
    //                self.progressThumb.frame = frame;
    //                btn_progressFocuse.frame = frame;
    //            } else {
    //                xValue = self.view.frame.size.width - 200;
    //                frame.origin.x = xValue;
    //                self.progressThumb.frame = frame;
    //                btn_progressFocuse.frame = frame;
    //            }
    //
    //        } else if (distance.x < -70 && distance.y > -50 && distance.y < 50) { //left
    //
    //            NSLog(@"user swiped left");
    //            NSLog(@"distance.x - %f", distance.x);
    //
    //            CGRect frame = self.progressThumb.frame;
    //
    //            NSInteger xdistance = fabs(distance.x);
    //            CGFloat xValue = frame.origin.x - xdistance;
    //
    //            if (xValue > 0) {
    //                frame.origin.x = xValue;
    //                self.progressThumb.frame = frame;
    //                btn_progressFocuse.frame = frame;
    //            } else {
    //                frame.origin.x = 0;
    //                self.progressThumb.frame = frame;
    //                btn_progressFocuse.frame = frame;
    //            }
    //        }
    //
    ////        if (distance.y > 0) { // down
    ////            NSLog(@"user swiped down");
    ////            NSLog(@"distance.y - %f", distance.y);
    ////        } else if (distance.y < 0) { //up
    ////            NSLog(@"user swiped up");
    ////            NSLog(@"distance.y - %f", distance.y);
    ////        }
    //    }
}

-(void)hideProgressThumbnails
{
    self.view_progressThumbnail.hidden = true;
}

-(void)viewDidDisappear:(BOOL)animated
{
    if (self.isMovingFromParentViewController)
    {
        [self removePlayer];
    }
}

-(void)removePlayer
{
    [self setResumePositionOfVideo];
    [self setVideoComplitionRate];
    [timer invalidate];
    timer = nil;
    [timerToUpdateOnlineApi invalidate];
    timerToUpdateOnlineApi = nil;
    [self.updatePlayBackTimer invalidate];
    self.updatePlayBackTimer = nil;
    [self removeEvertyThingFormView];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (isFindLocalVideoPath) return;
    
    CGPoint point = [(UITouch*)[touches anyObject] locationInView:self.view];
    if ([self.videoView pointInside:point withEvent:event]) {
        if (videoControls.hidden == false) {
            videoControls.hidden = YES;
        } else {
            videoControls.hidden = NO;
        }
    }
}

-(void)proressBarChangeEnded:(UIProgressView*)sender
{
    [self.videoPlayer play];
}

-(IBAction)playVideo:(UIButton *)button
{
    if (self.videoPlayer.rate == 1.0) {
        [self.videoPlayer pause];
        [button setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    } else {
        [self.videoPlayer play];
        [button setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    }
}
-(NSString*)getStringFromCMTime:(CMTime)time
{
    Float64 currentSeconds = CMTimeGetSeconds(time);
    
    //int mins = currentSeconds/60.0;
    //int secs = fmodf(currentSeconds, 60.0);
    int totalSeconds = (int)currentSeconds;
    
    if (totalSeconds < 0) {
        return @"00:00";
    }
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        NSString *hoursString = hours < 10 ? [NSString stringWithFormat:@"0%d", hours] : [NSString stringWithFormat:@"%d", hours];
        NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
        NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
        return [NSString stringWithFormat:@"%@:%@:%@",hoursString,minsString, secsString];
    }
    
    NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}

-(void)Updaterequest{
    @try {
        
        NSString *channelID = [StaticData checkStringNull:self.videoDetails[@"channel_id"]];
        [MangoMoloAnalytics updateOnlineWithChannelID:channelID withSessionID:thirtyTwoChar withVideoID:self.show_id];
    } @catch (NSException *exception) {
        NSLog(@"Error find = %@",exception.description);
    } @finally {
    }
}

//https://partv.cdn.mgmlcdn.com/vod/_definst_/smil:2021-05-20/vidiPD7uCGosL.smil/playlist.m3u8


#pragma mark Content Player Setup
- (void)setUpContentPlayer
{
    if (!self.viewIsActive) return;
    
    CGRect frame = CGRectMake(0, 0, self.videoView.frame.size.width, self.videoView.frame.size.height);
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[[NSURL alloc] initWithString:video_url]];
    
    self.videoPlayer = [AVPlayer playerWithPlayerItem:playerItem];
    self.playerLayer  = [AVPlayerLayer playerLayerWithPlayer:self.videoPlayer];
    [self.playerLayer setFrame:frame];
    //[self.videoPlayer seekToTime:CMTimeMakeWithSeconds(100, 1000)];
    [self.videoView.layer addSublayer:self.playerLayer];
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    [self.videoPlayer play];
    [self.videoView bringSubviewToFront:videoControls];
    [self.videoView bringSubviewToFront:_img_player_watermark];
    [self.videoView bringSubviewToFront:self.subtitling.containerView];
    [btn_playPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    self.progressBar.userInteractionEnabled = YES;
    self.progressBar.alpha = 1.0;
    videoControls.hidden = NO;
    isFindLocalVideoPath = NO;
    
    if (self.subtitleTrackList.count > 0)
    {
//        self.btn_subtitleWidthAnchor.constant = 42.0;
        self.btn_subtitle.hidden = false;
        for (MediaInfo *subtitle in self.subtitleTrackList)
        {
            if (subtitle.isSelected)
            {
                [self loadSubtitleSource:subtitle];
            }
        }
        self.btn_subtitle.hidden = false;
    } else
    {
//        self.btn_subtitleWidthAnchor.constant = 0.0;
    }
    
    if (self.resumeVideoPosition > 0) {
        CMTime seconds = CMTimeMake(self.resumeVideoPosition, 1);
        
        [self.videoPlayer seekToTime:seconds toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }
    
    CMTime interval = CMTimeMake(33, 1000);
    __weak __typeof(self) weakself = self;
    playbackObserver = [self.videoPlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        CMTime endTime = CMTimeConvertScale (weakself.videoPlayer.currentItem.asset.duration, weakself.videoPlayer.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
            double normalizedTime = (double) weakself.videoPlayer.currentTime.value / (double) endTime.value;
            weakself.progressBar.progress = normalizedTime;
            
            [weakself updateProgressThumbnailValue];
        }
        
        CMTime subtract = CMTimeSubtract(weakself.videoPlayer.currentItem.asset.duration,weakself.videoPlayer.currentTime);
        Float64 currentSeconds = CMTimeGetSeconds(weakself.videoPlayer.currentTime);
        Float64 remaingSeconds = CMTimeGetSeconds(subtract);
        Float64 duration = CMTimeGetSeconds(weakself.videoPlayer.currentItem.asset.duration);
        int timeLeftForNextEpisode = (duration / 100) * 10;
        if (currentSeconds > 0 && remaingSeconds < timeLeftForNextEpisode) {
            if (weakself.nextEpisode) {
                weakself.view_nextView.hidden = NO;
            } else {
      
                weakself.view_nextView.hidden = true;
            }
        } else {
            weakself.view_nextView.hidden = true;
        }
        
        if (weakself.overlayDic)
        {
            int overlayDuration =  [[StaticData checkStringNull:weakself.overlayDic[@"duration"]] intValue];
            if (currentSeconds >= overlayDuration)
            {
                weakself.img_ads.hidden = false;
            }
        }
        
        if (currentSeconds >= weakself.skipDurationPosition) {
            weakself.btn_introSkip.hidden = true;
        }
        
        //CMTime subtract = CMTimeSubtract(weakself.videoPlayer.currentItem.asset.duration,weakself.videoPlayer.currentTime);
        
        NSString *currentTime = [weakself getStringFromCMTime:weakself.videoPlayer.currentTime];
        //NSString *remaingTime = [weakself getStringFromCMTime:subtract];
        
        weakself.lbl_currentTime.text = currentTime;
        weakself.lbl_remainTime.text = [weakself getTotalVideoTime];
        
        if (!weakself.viewIsActive)
        {
            [weakself.adPlayer pause];
            [weakself.videoPlayer pause];
            [btn_playPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        }
    }];
    
    if (_observingMediaPlayer) {
        /*[_videoPlayer.currentItem removeObserver:self forKeyPath:@"status"];
         [_videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
         [_videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
         [_videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferFull"];*/
    }
    
    _observingMediaPlayer = true;
    
    [_videoPlayer.currentItem addObserver:self
                               forKeyPath:@"status" options:NSKeyValueObservingOptionNew
                                  context:nil];
    
    [_videoPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew
                                  context:nil];
    [_videoPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew
                                  context:nil];
    [_videoPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionNew
                                  context:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.videoPlayer currentItem]];
    
    view_videoControls.hidden = false;
    [self setupAdsLoader];
}

-(NSString *)getTotalVideoTime
{
    int totalSeconds = (int) CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
    
    if (totalSeconds < 0) {
        return @"00:00";
    }
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    NSString *minsString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%d:%@:%@",hours, minsString, secsString];
    }
    
    if (minutes > 0) {
        return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
    }
    
    return [NSString stringWithFormat:@"%@", secsString];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    NSLog(@"observeValueForKeyPath %@", keyPath);
    if ([keyPath isEqualToString:@"status"]) {
        if (_videoPlayer.status == AVPlayerStatusReadyToPlay) {
            [self.indicator stopAnimating];
            [self startTimerToHideVideoControls];
        }
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        // Show loader
        [self.indicator startAnimating];
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        // Hide loader
        [self.indicator stopAnimating];
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        // Hide loader
        [self.indicator stopAnimating];
    }
}

-(void)setupNextVideo
{
    [self.adPlayer pause];
    self.adPlayer = nil;
    [self.adVideoView removeFromSuperview];
    [bannar removeFromSuperview];
    isAdsPlaying = false;
    
    if (self.preAdList.count > 0) {
        [self setupPlayingAds:self.preAdList];
    } else  if (self.midAdList.count > 0) {
        [self setupPlayingAds:self.midAdList];
    } else if (self.postAdList.count > 0) {
        [self setupPlayingAds:self.postAdList];
    } else {
        if (currentPlayAbleVideoTime > 0) {
            [self hideVideoControls];
            videoControls.hidden = NO;
            isFindLocalVideoPath = false;
            [self.videoPlayer play];
            videoControls.hidden = NO;
        } else {
            [self setUpContentPlayer];
        }
    }
}

-(void)setupPlayingAds:(NSMutableArray *)adsList
{
    if (self.isAdPlaying) return;
    
    isInitContnentPlayer = true;
    if (adsList.count > 0)
    {
        int tag = [self checkAddType:adsList];
        
        if (![self checkVideoDuration:adsList])
        {
            if (currentPlayingVideoTime == 0)
            {
                [self setUpContentPlayer];
            } else{
                isInitContnentPlayer = true;
                [self.adPlayer pause];
                [self.adVideoView removeFromSuperview];
                [bannar removeFromSuperview];
                isAdsPlaying = false;
                [self.videoPlayer play];
                videoControls.hidden = NO;
                [self hideVideoControls];
            }
            return;
        }
        
        if (tag == 1)
        {
            isFindLocalVideoPath = true;
            localMovieAddPath = [NSString stringWithFormat:@"http://%@",[adsList[0] valueForKey:@"m3u8"]];
            [self playLocalAdWithPath:localMovieAddPath];
            [adsList removeObjectAtIndex:0];
        } else if (tag == 2)
        {
            isFindLocalVideoPath = true;
            if (self.videoPlayer) {
                [self.videoPlayer play];
            } else {
                [self setUpContentPlayer];
            }
            [self playIMAAdWithPath:[adsList[0] valueForKey:@"google_doubleclick"]];
            [adsList removeObjectAtIndex:0];
        }
    }
}

-(void)playLocalAdWithPath:(NSString *)path
{
    if (!self.viewIsActive && !self.isAdPlaying) return;
    
    if (self.isAdPlaying) return;
    
    self.isAdPlaying = true;
    self.viewIsActive = false;
    [self pauseVideoWithHideVideoControls:false];
    LocalAdPlayer *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalAdPlayer"];
    vc.adPath = path;
    [self presentViewController:vc animated:false completion:nil];
    
    vc.AdPlayingFinished = ^{
        self.isAdPlaying = false;
        self.viewIsActive = true;
        [self hideVideoControls];
        [self playVideo];
    };
}

-(void)playIMAAdWithPath:(NSString *)path
{
    if (!self.viewIsActive && !self.isAdPlaying) return;
    
    if (self.isAdPlaying) return;
    
    self.isAdPlaying = true;
    self.viewIsActive = false;
    [self pauseVideoWithHideVideoControls:true];
    IMAAdPlayer *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"IMAAdPlayer"];
    vc.adPath = path;
    [self presentViewController:vc animated:false completion:nil];
    
    vc.AdPlayingFinished = ^{
        self.isAdPlaying = false;
        self.viewIsActive = true;
        
        [self hideVideoControls];
        [self playVideo];
    };
}

-(BOOL)checkVideoDuration:(NSMutableArray *)adsList
{
    for (NSDictionary *dic in adsList)
    {
        if (currentPlayingVideoTime >= [[dic valueForKey:@"duration"] intValue])
        {
            return true;
        }
    }
    
    return false;
}

-(int)checkAddType:(NSMutableArray *)adData
{
    for (NSDictionary *dic in adData)
    {
        if ([dic valueForKey:@"ad_type"])
        {
            if ([[dic valueForKey:@"ad_type"] isEqualToString:@"local_server"])
            {
                return 1;
            } else if ([[dic valueForKey:@"ad_type"] isEqualToString:@"double_click"])
            {
                return 2;
            }
        }
    }
    
    return 0;
}

-(NSString *)findNextVideDuration:(NSMutableArray *)adsList
{
    for (NSDictionary *dic in self.preAdList)
    {
        return [dic valueForKey:@"duration"];
    }
    
    return @"";
}
- (void)updatePlaybackProgressFromTimer:(NSTimer *)timer
{
    //[self.video setScalingMode:MPMovieScalingModeFill];
    Float64 currentSeconds = CMTimeGetSeconds(self.videoPlayer.currentItem.currentTime);
    Float64 adSeconds = CMTimeGetSeconds(self.adPlayer.currentItem.currentTime);
    Float64 duration = CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
    int videotime = currentSeconds;
    int adtime = adSeconds;
    if (videotime > 0) {
        currentPlayAbleVideoTime = videotime;
    }
    
    if (videotime > 0 || adtime > 0) {
        [self willChangeValueForKey:@"currentTime"];
        
        _currentTime = videotime;
        isInitContnentPlayer = false;
        
        if (isFindLocalVideoPath) {
            if (adtime <= 5) {
                int adTime = 5 - adtime;
                bannar.lbl_title.text = [NSString stringWithFormat:@"You can skip this ad in (%d seconds)",adTime];
            }
            
            if (adtime >= 5) {
                bannar.lbl_title.hidden = YES;
                bannar.btn_skip.hidden = NO;
            }
        }
        
        if (self.preAdList.count > 0)
        {
            Float64 duration = CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
            if (videotime > 0 && duration > 0)
            {
                currentPlayingVideoTime = videotime;
                if(videotime > [[self.preAdList[0] valueForKey:@"duration"] intValue] && !isAdsPlaying){
                    [self.videoPlayer pause];
                    [self setupPlayingAds:self.preAdList];
                }
            }
        }
        
        if (self.midAdList.count > 0 && self.preAdList.count == 0)
        {
            Float64 duration = CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
            if (videotime > 0 && duration > 0)
            {
                currentPlayingVideoTime = videotime;
                if(videotime > [[self.midAdList[0] valueForKey:@"duration"] intValue] && !isAdsPlaying){
                    [self.videoPlayer pause];
                    [self setupPlayingAds:self.midAdList];
                }
            }
        }
        
        if (self.postAdList.count > 0 && self.preAdList.count == 0 && self.midAdList.count == 0)
        {
            Float64 duration = CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
            if (videotime > 0 && duration > 0)
            {
                currentPlayingVideoTime = videotime;
                if(videotime > [[self.postAdList[0] valueForKey:@"duration"] intValue] && !isAdsPlaying){
                    [self.videoPlayer pause];
                    [self setupPlayingAds:self.postAdList];
                }
            }
        }
        [self didChangeValueForKey:@"currentTime"];
    }
}

-(void)setResumePositionOfVideo
{
    self.viewIsActive = false;
    
    if (bgjson != nil) {
        
        Float64 currentSeconds = CMTimeGetSeconds(self.videoPlayer.currentItem.currentTime);
        int videotime = currentSeconds;
        if (videotime > 0)
        {
            [self executePostApiToGetResponse:[NSString stringWithFormat:@"%@/nand/setResumePosition?user_id=%@&key=%@&app_id=%@",BaseURL,BaseUID,BaseKEY,AppID] parameters:[self setResumePostParms] callType:@"setResume"];
        }
        
    }
}

-(NSDictionary *)setResumePostParms
{
    @try {
        
        //here we will update the episode video position in episodes array that we take from previous screen
        
        Float64 currentSeconds = CMTimeGetSeconds(self.videoPlayer.currentItem.currentTime);
        int videotime = currentSeconds;
        
        for (MediaInfo *info in self.episodes) {
            if ([info.ID isEqualToString:self.show_id])
            {
                info.position = [@(videotime) stringValue];
            }
        }
        
        
        if ([StaticData shared].userInfo) {
            return @{@"video_id":self.show_id,@"position":[@(videotime) stringValue],@"channel_userid":[StaticData shared].userInfo.ID,@"device_id":@""};
        }
        return @{@"video_id":self.show_id,@"position":[@(videotime) stringValue],@"channel_userid":@"",@"device_id":[StaticData shared].deviceID};
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return @{@"":@""};
}

-(void)setVideoComplitionRate
{
    @try {
        int currentSeconds = (int)CMTimeGetSeconds(self.videoPlayer.currentItem.currentTime);
        int duration = (int)CMTimeGetSeconds(self.videoPlayer.currentItem.asset.duration);
        
        [MangoMoloAnalytics setVideoComplitionRateWithVideoDuration:duration withVideoPlayedTime:currentSeconds withVideoDetails:self.videoDetails];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)executeApiToPostVideoResume:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
        NSLog(@"Error: %@", error.description);
    }];
}

-(void)removeEvertyThingFormView
{
    self.viewIsActive = false;
    [self.videoPlayer pause];
    self.videoPlayer = nil;
    [self.videoView removeFromSuperview];
    [self.adPlayer pause];
    self.adPlayer = nil;
    self.adPlayerLayer = nil;
    [self.adsManager pause];
    self.adsManager = nil;
    self.adsLoader = nil;
    self.contentPlayhead = nil;
    [bannar removeFromSuperview];
}

-(void)itemDidFinishPlaying:(NSNotification*)notification
{
    /*if (isFindLocalVideoPath) {
     [self setupNextVideo];
     }*/
    
    // player finished playing video so now show video details
    [btn_playPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    view_videoControls.hidden = false;
    
    [self videoFinishedAndLetsUpdateOnServer];
    
    [self setupNextVideoTimerAfterCurrentVideoEnd];
}

-(void)setupNextVideoTimerAfterCurrentVideoEnd
{
    _nextVideoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimeCount) userInfo:nil repeats:YES];
    
    _recordingProgress = [[Progress alloc] initWithFrame:_view_progress.bounds];
    _recordingProgress.percent = 1;
    [_view_progress addSubview:_recordingProgress];
    
    [self showReplayButton:true];
}

-(void)startTimeCount
{
    // If we can decrement our percentage, do so, and redraw the view
    if (_recordingProgress.percent > 0) {
        _recordingProgress.percent = _recordingProgress.percent + 1;
        [_recordingProgress setNeedsDisplay];
        
        if (_recordingProgress.percent >= 15) {
            [self itemFinishedPlayingVideo];
        }
    }
}

-(void)itemFinishedPlayingVideo
{
    @try {
        [self showReplayButton:false];
        
        [self hideVideoControls];
        
        if (self.nextEpisode) {
            
            self.show_id = self.nextEpisode[@"id"];
            
            self.nextEpisode = nil;
            self.view_nextView.hidden = true;
            
            [self hideVideoControls];
            
            [relatedVideos removeAllObjects];
            
            [self loadVideoDetails];
        }
        
        CMTime restartPlayer = CMTimeMake(0, 1);
        btn_playPause.tag = 1;
        [self.videoPlayer seekToTime:restartPlayer];
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

-(void)removeReplayVideoTimer
{
    [_nextVideoTimer invalidate];
    [_recordingProgress removeFromSuperview];
}

-(void)showReplayButton:(BOOL)isTrue
{
    if (isTrue) {
        btn_replay.hidden = false;
        btn_playPause.hidden = true;
    } else {
        [self removeReplayVideoTimer];
        btn_replay.hidden = true;
        btn_playPause.hidden = false;
        [_nextVideoTimer invalidate];
    }
}

-(void)replayVideoAction
{
    CMTime restartPlayer = CMTimeMake(0, 1);
    btn_playPause.tag = 1;
    [self.videoPlayer seekToTime:restartPlayer];
    
    [self showReplayButton:false];
    
    btn_playPause.tag = 1;
    
    [self haldePlayAndPause:nil];
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey {
    BOOL automatic = NO;
    if ([theKey isEqualToString:@"currentTime"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

#pragma mark SDK Setup

- (void)requestAdsPre:(NSString *)url {
    
    [bannar removeFromSuperview];
    
    [self.videoPlayer pause];
    [self.adPlayer pause];
    self.adPlayerLayer = nil;
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    //    if (!self.isStopFocuFromStart) {
    //        self.isStopFocuFromStart = true;
    //        return;
    //    }
    float nextFocuse = 1.1;
    float preFocuse = 1.0;
    
    NSLog(@"Next View: %@",context.nextFocusedView);
    NSLog(@"Previous View %@",context.previouslyFocusedView);
    
    [self startTimerToHideVideoControls];
    
    if (context.nextFocusedView == _btn_subtitle)
    {
        UIImage *img = [UIImage imageNamed:@"choose_language_selected"];//[StaticData changeImageColor:[UIImage imageNamed:@"skip_into_selected"] withColor:[StaticData colorFromHexString:@"#01D19D"]];
        [_btn_subtitle setImage:img forState:UIControlStateNormal];
    }
    
    if (context.previouslyFocusedView == _btn_subtitle)
    {
        UIImage *img = [UIImage imageNamed:@"choose_language"];
        [_btn_subtitle setImage:img forState:UIControlStateNormal];
    }
    

    if (context.nextFocusedView == _btn_plus)
    {
        _img_plus.image = [StaticData changeImageColor:[UIImage imageNamed:@"plus"] withColor:[StaticData colorFromHexString:@"#01D19D"]];
    }
    
    if (context.nextFocusedView == _btn_introSkip)
    {
        UIImage *img = [UIImage imageNamed:@"skip_into_selected"];//[StaticData changeImageColor:[UIImage imageNamed:@"skip_into_selected"] withColor:[StaticData colorFromHexString:@"#01D19D"]];
        [_btn_introSkip setBackgroundImage:img forState:UIControlStateNormal];
    }
    
    if (context.previouslyFocusedView == _btn_introSkip)
    {
        UIImage *img = [UIImage imageNamed:@"skip_into"];
        [_btn_introSkip setBackgroundImage:img forState:UIControlStateNormal];
    }
    
    if (context.previouslyFocusedView == _btn_plus)
    {
        _img_plus.image = [UIImage imageNamed:@"plus"];
    }
    
    if (context.nextFocusedView == btn_playPause)
    {
        [self isNeedToShowVideoThumbnails:false];
        
        [self updatePlayPauseButtonStateOnFocuse:true];
    }
    
    if (context.previouslyFocusedView == btn_playPause)
    {
        [self updatePlayPauseButtonStateOnFocuse:false];
    }
    
    if (context.nextFocusedView == self.btn_nextVideo)
    {
        [self isNeedToShowVideoThumbnails:false];
        
        self.view_nextOverLay.hidden = false;
    }
    
    if (context.previouslyFocusedView == self.btn_nextVideo)
    {
        self.view_nextOverLay.hidden = true;
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]]) {
        
        [coordinator addCoordinatedAnimations:^{
            
            context.previouslyFocusedView.transform = CGAffineTransformMakeScale(preFocuse, preFocuse);
            if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]])
            {
                //CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
                //if (resSetImage) {
                //cell.thumbnail.image = resSetImage;
                //}
                [context.previouslyFocusedView setBackgroundColor:[UIColor clearColor]];
            }
            
        } completion:nil];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == cv_relatedVideos) {
        
        [self isNeedToShowVideoThumbnails:false];
        
        [coordinator addCoordinatedAnimations:^
         {
            context.nextFocusedView.transform = CGAffineTransformMakeScale(nextFocuse, nextFocuse);
            
            if ([context.nextFocusedView isKindOfClass:[CollectionCell class]])
            {
                //CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
                //resSetImage = cell.thumbnail.image;
            }
        } completion:nil];
    }
    
    if ([context.nextFocusedView isKindOfClass:[btn_replay class]]) {
        
        [self isNeedToShowVideoThumbnails:false];
        
        [coordinator addCoordinatedAnimations:^{
            context.nextFocusedView.transform = CGAffineTransformMakeScale(nextFocuse, nextFocuse);
        } completion:nil];
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[btn_replay class]]) {
        [coordinator addCoordinatedAnimations:^{
            context.previouslyFocusedView.transform = CGAffineTransformMakeScale(preFocuse, preFocuse);
        } completion:nil];
    }
    
    if (context.nextFocusedView == btn_progressFocuse) {
        self.currentPlayerSeconds = CMTimeGetSeconds(self.videoPlayer.currentTime);
        isProgressActive = true;
        // here we check length > 1 because some time img_wide have 0
        if (self.img_wide.length > 0 && self.img_wide.length > 1) {
            [self isNeedToShowVideoThumbnails:true];
            cv_relatedVideos.hidden = true;
        }
        
        [coordinator addCoordinatedAnimations:^{
            //self.progressBar.transform = CGAffineTransformMakeScale(nextFocuse, nextFocuse);
            [self.progressBar setTintColor:[UIColor whiteColor]];
            self.progressBar.progressTintColor = AppColor;
            
            self.progressThumb.image = [self.progressThumb.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.progressThumb setTintColor:AppColor];
            
            Float64 currentSeconds = CMTimeGetSeconds(self.videoPlayer.currentTime);
            [self updateVideoThumbnailPosition:currentSeconds];
        } completion:nil];
    } else if (context.previouslyFocusedView == btn_progressFocuse)
    {
        self.view_progressThumbnail.hidden = true;
        [self updateProgressThumbnailValue];
        isProgressActive =  false;
        [coordinator addCoordinatedAnimations:^{
            //self.progressBar.transform = CGAffineTransformMakeScale(preFocuse, nextFocuse);
            [self.progressBar setTintColor:[StaticData colorFromHexString:@"#01D19D"]];
            self.progressBar.progressTintColor = AppColor;
            
            self.progressThumb.image = [self.progressThumb.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.progressThumb setTintColor:AppColor];
            
        } completion:nil];
    }
}

-(void)isNeedToShowVideoThumbnails:(BOOL)isShow
{
    if (isShow) {
        self.cv_thumbnails.hidden = false;
        cv_relatedVideos.hidden = true;
        if (relatedVideos.count > 0) {
            self.cv_heightAnchor.constant = 0;
        }
    } else {
        
        self.cv_thumbnails.hidden = true;
        cv_relatedVideos.hidden = false;
        
        if (relatedVideos.count == 0) {
            self.cv_heightAnchor.constant = 0;
            return;
        }
        
        self.cv_heightAnchor.constant = 0.0;//200;
        
        CGRect frame = cv_relatedVideos.frame;
        CGFloat width = (frame.size.width)/7;
        CGFloat height = width;
        if (_tempImage)
        {
            CGFloat ratio = _tempImage.size.height / _tempImage.size.width;
            height = (ratio * width);
            //self.cv_heightAnchor.constant = height + 80;
        }
    }
}

-(void)updatePlayPauseButtonStateOnFocuse:(BOOL)isFocusedFinished
{
    UIImage *currentImg = btn_playPause.currentImage;
    if ([currentImg isEqual:[UIImage imageNamed:@"play"]] || [currentImg isEqual:[UIImage imageNamed:@"play_highlight"]])
    {
        if (isFocusedFinished) {
            [btn_playPause setImage:[UIImage imageNamed:@"play_highlight"] forState:UIControlStateNormal];
        } else {
            [btn_playPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        }
    } else
    {
        if (isFocusedFinished) {
            [btn_playPause setImage:[UIImage imageNamed:@"pause_heighlight"] forState:UIControlStateNormal];
        } else {
            [btn_playPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        }
    }
}

-(void)startTimerToHideVideoControls
{
    [vidContTimer invalidate];
    vidContTimer = nil;
    
    vidContTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(hideVideoControls) userInfo:nil repeats:NO];
}

-(void)swipeDownToShowRelativeVideo
{
    if (self.adsManager.adPlaybackInfo.isPlaying) {
        return;
    }
    
    
    
    view_videoControls.hidden = false;
}

- (void)pressesEnded:(NSSet<UIPress *> *)presses withEvent:(UIEvent *)event;
{
    NSArray *object = [presses allObjects];
    for (id obj in object) {
        if ([obj isKindOfClass:[UIPress class]])
        {
            UIPress *pressType = (UIPress *)obj;
            if (pressType.type == 3)
            {
                if (self.progressBar.tintColor == [UIColor whiteColor]) {
                    NSLog(@"slider value: %f",self.progressBar.progress);
                    //[self progressBarChanged:0.008000];
                }
            } else if (pressType.type == 2)
            {
                if (self.progressBar.tintColor == [UIColor whiteColor]) {
                    NSLog(@"slider value: %f",self.progressBar.progress);
                    //[self progressBarChanged:-0.008000];
                }
            } else if (pressType.type == 1)
            {
            }
            NSLog(@"%ld",(long)pressType.type);
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
}

-(void)forwardVideo:(UITapGestureRecognizer *)gesture
{
    NSLog(@"forward video");
    
    NSLog(@"slider value: %f",self.progressBar.progress);
    [self progressBarChanged:0.002000];
    
    if (self.img_wide.length > 0 && self.img_wide.length > 1) {
        self.view_progressThumbnail.hidden = false;
        [self.progressBarThumbnailTimer invalidate];
        self.progressBarThumbnailTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideProgressThumbnails) userInfo:nil repeats:NO];
    }
}

-(void)forwardBackVideo:(UITapGestureRecognizer *)gesture
{
    NSLog(@"back video");
    NSLog(@"slider value: %f",self.progressBar.progress);
    [self progressBarChanged:-0.002000];
    
    if (self.img_wide.length > 0 && self.img_wide.length > 1) {
        self.view_progressThumbnail.hidden = false;
        [self.progressBarThumbnailTimer invalidate];
        self.progressBarThumbnailTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideProgressThumbnails) userInfo:nil repeats:NO];
    }
}

-(void)progressBarChanged:(float)movement
{
    //[self.videoPlayer pause];
    
    NSLog(@"slider value: %f",self.progressBar.progress);
    
    self.progressBar.progress = self.progressBar.progress + movement;
    
    Float64 currentSeconds = CMTimeGetSeconds(self.videoPlayer.currentTime);
    currentSeconds += 5;
    MediaInfo *info = [self updateVideoThumbnailPosition:currentSeconds];
    if (info) {
        currentSeconds = info.thumbStartTime;
    }
    CMTime seekTime = CMTimeMakeWithSeconds(currentSeconds, self.videoPlayer.currentTime.timescale);
    //CMTime seekTime = CMTimeMakeWithSeconds(self.progressBar.progress * (double)self.videoPlayer.currentItem.asset.duration.value/(double)self.videoPlayer.currentItem.asset.duration.timescale, self.videoPlayer.currentTime.timescale);
    [self.videoPlayer seekToTime:seekTime];
    
    NSString *currentTime = [self getStringFromCMTime:seekTime];
    self.lbl_currentTime.text = currentTime;
    
    [self startTimerToHideVideoControls];
    
    [self updateProgressThumbnailValue];
}

-(MediaInfo *)updateVideoThumbnailPosition:(Float64)currentSeconds
{
    @try {
        for (int i = 0; i < _videoThumbnails.count; i++)
        {
            MediaInfo *info = self.videoThumbnails[i];
            NSInteger startTime = info.thumbStartTime;
            NSInteger stopTime = info.thumbStopTime;
            if (currentSeconds <= startTime && currentSeconds <= stopTime)
            {
                self.lbl_progressTime.text = info.startTime;
                [self.indicator_progressThumbnail startAnimating];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // WARNING: is the cell still using the same data by this point??
                    UIImage *image = self.largeVideoThumbnail;
                    image = [self imageByCroppingImage:image withImageInfo:info];
                    self.img_progressThumbnail.image = image;
                    [self.indicator_progressThumbnail stopAnimating];
                });
                
                //                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                //                //CollectionCell *cell = (CollectionCell *)[self.cv_thumbnails cellForItemAtIndexPath:indexPath];
                //                [self.cv_thumbnails scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:false];
                //
                //                for (MediaInfo *info in self.videoThumbnails) {
                //                    info.isSelected = false;
                //                }
                //                info.isSelected = true;
                //                [StaticData reloadCollectionView:self.cv_thumbnails];
                
                return info;
            }
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    return nil;
}

-(void)updateProgressThumbnailValue
{
    CGRect frame = self.progressThumb.frame;
    
    CGRect progressFrame = self.progressBar.frame;
    CGFloat xValue = progressFrame.origin.x + (self.progressBar.frame.size.width * self.progressBar.progress);
    
    frame.origin.x = xValue;
    
    self.progressThumb.frame = frame;
    
    [self updateProgressThumbnailPosition];
}

-(void)updateProgressThumbnailPosition
{
    CGRect progressFrame = self.progressThumb.frame;
    
    CGRect progressThumbnailFrame = self.view_progressThumbnail.frame;
    
    progressThumbnailFrame.origin.y = ((self.progressThumb.superview.frame.origin.y  + self.progressThumb.superview.frame.size.height) - progressThumbnailFrame.size.height) - 50;
    
    CGFloat xPosition = progressFrame.origin.x - (progressThumbnailFrame.size.width / 2);
    if (xPosition > 0)
    {
        if (xPosition < (self.progressBar.frame.size.width - 200) - progressThumbnailFrame.size.width / 2)
        {
            progressThumbnailFrame.origin.x = xPosition;
            self.view_progressThumbnail.frame = progressThumbnailFrame;
        } else {
            progressThumbnailFrame.origin.x = ((self.progressBar.frame.size.width - progressThumbnailFrame.size.width / 2) - 150);
            self.view_progressThumbnail.frame = progressThumbnailFrame;
        }
    } else {
        progressThumbnailFrame.origin.x = 50;
        self.view_progressThumbnail.frame = progressThumbnailFrame;
    }
    
}
-(void)hideVideoControls
{
    view_videoControls.hidden = YES;
}

-(void)seekVideoBasedOnProgressThumbnail
{
    if (self.currentProgressStateChanged)
    {
        self.isVideoPaused = false;
        [self hideProgressThumbnails];
        [self.videoPlayer play];
        self.currentProgressStateChanged = false;
        
        if (self.selectedPorgressThumbnailInfo)
        {
            CMTime seekTime = CMTimeMakeWithSeconds(_currentPlayerSeconds, NSEC_PER_SEC);
            //CMTime seekTime = CMTimeMakeWithSeconds(self.progressBar.progress * (double)self.videoPlayer.currentItem.asset.duration.value/(double)self.videoPlayer.currentItem.asset.duration.timescale, self.videoPlayer.currentTime.timescale);
            [self.videoPlayer seekToTime:seekTime
            toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            //[self.videoPlayer seekToTime:seekTime];
        }
    }
}

-(void)haldePlayAndPause:(UITapGestureRecognizer *)gesture
{
    if (self.adsManager.adPlaybackInfo.isPlaying) {
        return;
    }
    
    if (self.currentProgressStateChanged)
    {
        [self seekVideoBasedOnProgressThumbnail];
    } else
    {
        if (self.isVideoPaused)
        {
            self.isVideoPaused = false;
            [self playVideo];
        } else
        {
            if (self.img_wide.length > 0 && self.img_wide.length > 1) {
                self.view_progressThumbnail.hidden = false;
//                self.currentPlayerSeconds = CMTimeGetSeconds(self.videoPlayer.currentTime);
//                MediaInfo *info = [self updateVideoThumbnailPosition:self.currentPlayerSeconds];
//                if (info) {
//                    self.currentPlayerSeconds = info.thumbStartTime;
//                }
                
                [self updateFocuseOnVideoThumbnails];
            }
            
            [self pauseVideoWithHideVideoControls:false];
        }
    }
}

-(void)playVideo
{
    UIImage *currentImg = btn_playPause.currentImage;
    btn_playPause.tag = 0;
    if ([currentImg isEqual:[UIImage imageNamed:@"pause_heighlight"]])
    {
        [btn_playPause setImage:[UIImage imageNamed:@"pause_heighlight"] forState:UIControlStateNormal];
    } else {
        [btn_playPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    }
    focuseUpdate = false;
    [self.videoPlayer play];
    [self hideVideoControls];
}

-(void)pauseVideoWithHideVideoControls:(BOOL)isHide
{
    self.isVideoPaused = true;
    UIImage *currentImg = btn_playPause.currentImage;
    btn_playPause.tag = 1;
    if ([currentImg isEqual:[UIImage imageNamed:@"play_highlight"]])
    {
        [btn_playPause setImage:[UIImage imageNamed:@"play_highlight"] forState:UIControlStateNormal];
    } else {
        [btn_playPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    }
    focuseUpdate = true;
    [self.videoPlayer pause];
    
    view_videoControls.hidden = isHide;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_thumbnails)
    {
        CGRect frame = self.cv_thumbnails.frame;
        CGFloat width = (frame.size.width)/7;
        
        CGFloat height = 130;
        if (_tempVideoThumbnail)
        {
            CGFloat ratio = _tempVideoThumbnail.size.height / _tempVideoThumbnail.size.width;
            height = (ratio * width);
            self.cv_thumbHeightAnchor.constant = height+67;
        }
        return CGSizeMake(width, height + 67);
    }
    
    CGRect frame = cv_relatedVideos.frame;
    CGFloat width = (frame.size.width)/7;
    CGFloat height = width;
    if (_tempImage)
    {
        CGFloat ratio = _tempImage.size.height / _tempImage.size.width;
        height = (ratio * width);
        //self.cv_heightAnchor.constant = height + 80;
    }
    return CGSizeMake(width, height+80);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.cv_thumbnails) {
        return self.videoThumbnails.count;
    } else if (collectionView == cv_relatedVideos) {
        return relatedVideos.count;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (collectionView == self.cv_thumbnails) {
        cell = [self loadThumbnail:collectionView cellForItemAtIndexPath:indexPath];
        return cell;
    }
    
    if (relatedVideos.count == 0)
    {
        [cell.indicator startAnimating];
    } else
    {
        MediaInfo *info = relatedVideos[indexPath.row];
        
        @try {
            cell.lbl_title.text = info.title;
            [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
            cell.thumbnail.image = nil;
            
            NSString *imgPath = [BaseImageURL stringByAppendingString:info.imagePath];
            [cell.indicator startAnimating];
            
            cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imgPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image != nil)
                {
                    if (!self.tempImage) {
                        self.tempImage = image;
                        [self reloadCollectionView];
                    }
                    [cell.indicator stopAnimating];
                }
            }];
            
            cell.tag = indexPath.row;
            
            [StaticData scaleToArabic:cell.lbl_title];
            [StaticData scaleToArabic:cell.thumbnail];
            
        } @catch (NSException *exception) {
            [StaticData log:[NSString stringWithFormat:@"PlayVideos cell for item has issue\n issue = %@",exception.description]];
        } @finally {
        }
    }
    
    return cell;
}

-(CollectionCell *)loadThumbnail:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.videoThumbnails[indexPath.row];
    
    cell.lbl_title.text = info.startTime;
    cell.thumbnail.image = nil;
    
    cell.thumbnail.contentMode = UIViewContentModeScaleAspectFit;
    
    [cell.indicator startAnimating];
    
    if (info.isSelected) {
        cell.thumbnail.layer.borderColor = SelectedColor.CGColor;
        cell.thumbnail.layer.borderWidth = 5.0;
        cell.thumbnail.clipsToBounds = true;
    } else {
        cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
        cell.thumbnail.layer.borderWidth = 0.0;
        cell.thumbnail.clipsToBounds = true;
    }
    
    if (self.largeVideoThumbnail)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // WARNING: is the cell still using the same data by this point??
            UIImage *image = self.largeVideoThumbnail;
            image = [self imageByCroppingImage:image withImageInfo:info];
            cell.thumbnail.image = image;
            [cell.indicator stopAnimating];
            
            if (!self.tempVideoThumbnail) {
                self.tempVideoThumbnail = image;
                [StaticData reloadCollectionView:self.cv_thumbnails];
            }
        });
    } else {
        [self loadLargeVideoThumbnail:info];
    }
    
    return cell;
}

-(void)loadLargeVideoThumbnail:(MediaInfo *)info
{
    @try {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: info.thumbnail]];
            if ( data == nil ){
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                self.largeVideoThumbnail = [UIImage imageWithData: data];
                if (self.largeVideoThumbnail){
                    [StaticData reloadCollectionView:self.cv_thumbnails];
                }
            });
        });
    } @catch (NSException *exception) {
    } @finally {
    }
}

- (UIImage *)imageByCroppingImage:(UIImage *)image withImageInfo:(MediaInfo *)info
{
    @try {
        // not equivalent to image.size (which depends on the imageOrientation)!
        //double refWidth = CGImageGetWidth(image.CGImage);
        //double refHeight = CGImageGetHeight(image.CGImage);
        
        double x = info.x;
        double y = info.y;
        double width = info.w;
        double height = info.h;
        
        CGRect cropRect = CGRectMake(x, y, width,height);
        CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
        
        UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:image.imageOrientation];
        CGImageRelease(imageRef);
        
        return cropped;
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_thumbnails)
    {
        [self resumeVideoBySelectedVideoThumbnail:self.videoThumbnails[indexPath.row]];
    } else
    {
        MediaInfo *info = relatedVideos[indexPath.row];
        [self loadSelectedVideo:info];
    }
}

-(void)resumeVideoBySelectedVideoThumbnail:(MediaInfo *)info
{
    CMTime seconds = CMTimeMake(info.thumbStartTime, 1);
    [self.videoPlayer seekToTime:seconds toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
    [self updateProgressThumbnailValue];
    
    [self startTimerToHideVideoControls];
}

-(void)loadSelectedVideo:(MediaInfo *)info
{
    self.nextEpisode = nil;
    self.view_nextView.hidden = true;
    
    [self showReplayButton:false];
    
    [self videoFinishedAndLetsUpdateOnServer];
    
    self.show_id = info.ID;
    
    [relatedVideos removeAllObjects];
    
    [self hideVideoControls];
    
    [self loadVideoDetails];
}

-(void)playNextEpisodeNow
{
    @try {
        [self showReplayButton:false];
        
        [self hideVideoControls];
        
        [self videoFinishedAndLetsUpdateOnServer];
        
        if (self.nextEpisode) {
            
            self.show_id = self.nextEpisode[@"id"];
            
            self.nextEpisode = nil;
            self.view_nextView.hidden = true;
            
            [self hideVideoControls];
            
            [relatedVideos removeAllObjects];
            
            [self loadVideoDetails];
        }
        
        CMTime restartPlayer = CMTimeMake(0, 1);
        btn_playPause.tag = 1;
        [self.videoPlayer seekToTime:restartPlayer];
    } @catch (NSException *exception) {
        NSLog(@"Error-----find = %@",exception.description);
    } @finally {
        //
    }
}

-(void)videoFinishedAndLetsUpdateOnServer
{
    [self setVideoComplitionRate];
    [self setResumePositionOfVideo];
    
    [self.videoPlayer pause];
    [self.playerLayer removeFromSuperlayer];
    self.videoPlayer = nil;
    
    [self.adPlayer pause];
    self.adPlayer = nil;
    self.adPlayerLayer = nil;
    
    [self reloadCollectionView];
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:cv_relatedVideos];
}

- (void)videoOnError:(NSString*)error
{
    NSLog(@"%@",error.description);
}

-(CollectionCell *)findSelectedCell
{
    for (MediaInfo *info in relatedVideos) {
        if (info.isSelected)
        {
            NSInteger index = [relatedVideos indexOfObject:info];
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            CollectionCell *cell = (CollectionCell *)[cv_relatedVideos cellForItemAtIndexPath:indexPath];
            return cell;
        }
    }
    return nil;
}

-(void)updateFocuseOnVideoThumbnails
{
    [self setNeedsFocusUpdate];
    [self updateFocusIfNeeded];
}

-(UIView *)preferredFocusedView
{
    //    CollectionCell *cell = [self findSelectedCell];
    //    if (cell) {
    //        return self.progressThumb;
    //    }
    return btn_progressFocuse;
}

- (void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (!self.isStopFocuFromStartOfCollection) {
        self.isStopFocuFromStartOfCollection = true;
        return;
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]])
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            [cell.lbl_titleMarquee pauseLabel];
            
            if (collectionView == self.cv_thumbnails) {
                cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
                cell.thumbnail.layer.borderWidth = 0.0;
                cell.thumbnail.clipsToBounds = true;
            } else {
                [UIView animateWithDuration:0.1 animations:^{
                    context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                }];
            }
            [self startTimerToHideVideoControls];
        }
        
        if (([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == cv_relatedVideos) || context.nextFocusedView.superview == self.cv_thumbnails)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            [cell.lbl_titleMarquee unpauseLabel];
            
            if (collectionView == self.cv_thumbnails) {
                cell.thumbnail.layer.borderColor = SelectedColor.CGColor;
                cell.thumbnail.layer.borderWidth = 5.0;
                cell.thumbnail.clipsToBounds = true;
            } else {
                [UIView animateWithDuration:0.1 animations:^{
                    context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                }];
            }
            
            
            [self startTimerToHideVideoControls];
        }
    }
}


#pragma mark SDK Setup

- (void)setupAdsLoader {
    self.adsLoader = [[IMAAdsLoader alloc] initWithSettings:nil];
    self.adsLoader.delegate = self;
}

- (void)requestAdsWithAdTag:(NSString *)adTagUrl {
    
    // Create an ad display container for ad rendering.
    IMAAdDisplayContainer *adDisplayContainer =
    [[IMAAdDisplayContainer alloc] initWithAdContainer:self.videoView viewController:self companionSlots:nil];
    // Create an ad request with our ad tag, display container, and optional user context.
    IMAAdsRequest *request = [[IMAAdsRequest alloc] initWithAdTagUrl:adTagUrl adDisplayContainer:adDisplayContainer contentPlayhead:self.contentPlayhead userContext:nil];
    [self.adsLoader requestAdsWithRequest:request];
}

- (void)contentDidFinishPlaying:(NSNotification *)notification {
    // Make sure we don't call contentComplete as a result of an ad completing.
    if (notification.object == self.videoPlayer.currentItem) {
        [self.adsLoader contentComplete];
    }
}

#pragma mark AdsLoader Delegates

- (void)adsLoader:(IMAAdsLoader *)loader adsLoadedWithData:(IMAAdsLoadedData *)adsLoadedData {
    // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
    self.adsManager = adsLoadedData.adsManager;
    self.adsManager.delegate = self;
    // Create ads rendering settings to tell the SDK to use the in-app browser.
    IMAAdsRenderingSettings *adsRenderingSettings = [[IMAAdsRenderingSettings alloc] init];
    adsRenderingSettings.webOpenerPresentingController = self;
    // Initialize the ads manager.
    [self.adsManager initializeWithAdsRenderingSettings:adsRenderingSettings];
}

- (void)adsLoader:(IMAAdsLoader *)loader failedWithErrorData:(IMAAdLoadingErrorData *)adErrorData {
    // Something went wrong loading ads. Log the error and play the content.
    NSLog(@"Error loading ads: %@", adErrorData.adError.message);
    [self.videoPlayer play];
}

#pragma mark AdsManager Delegates

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdEvent:(IMAAdEvent *)event {
    
    
    switch (event.type) {
        case kIMAAdEvent_LOADED:
            [adsManager start];
            [self pauseVideoWithHideVideoControls:true];
            break;
        case kIMAAdEvent_PAUSE:
            [self playVideo];
            break;
        case kIMAAdEvent_RESUME:
            [self.adsManager resume];
            [self pauseVideoWithHideVideoControls:true];
            break;
        case kIMAAdEvent_TAPPED:
            //[self showFullscreenControls:nil];
            break;
        case kIMAAdEvent_CLICKED:
            [self.adsManager pause];
            break;
        default:
            break;
    }
}

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdError:(IMAAdError *)error {
    // Something went wrong with the ads manager after ads were loaded. Log the error and play the
    // content.
    NSLog(@"AdsManager error: %@", error.message);
    [self playVideo];
}

- (void)adsManagerDidRequestContentPause:(IMAAdsManager *)adsManager {
    // The SDK is going to play ads, so pause the content.
    [self pauseVideoWithHideVideoControls:false];
}

- (void)adsManagerDidRequestContentResume:(IMAAdsManager *)adsManager {
    // The SDK is done playing ads (at least for now), so resume the content.
    [self playVideo];
}

-(void)loadSubtitle
{
    @try {
        NSString *subtitleType = @"English";
        NSArray *subtitleTrackList =  [StaticData checkArrayNull:self.videoDetails[@"subtitle_tracks"]];
        if (subtitleTrackList.count > 0)
        {
            NSDictionary *subtitleTrack = [StaticData checkDictionaryNull:subtitleTrackList[0]];
            subtitleType = [StaticData checkStringNull:subtitleTrack[@"label"]];
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Subtitles" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Disable" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
        {
            [self disableSubtitle];
        }];
        [alert addAction:action];
        
        for (MediaInfo *subtitle in self.subtitleTrackList)
        {
            UIAlertAction *english = [UIAlertAction actionWithTitle:subtitle.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
            {
                self.subtitling.containerView.hidden = false;
                self.subtitling.label.hidden = false;
                self.subtitling.visible = true;
                self.subtitling.containerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
                self.subtitling.label.textColor = [UIColor whiteColor];
                [self.subtitling computeStyle];
                self.btn_subtitle.tag = 1;
                [self.btn_subtitle setSelected:true];
                
                for (MediaInfo *info in self.subtitleTrackList) {
                    info.isSelected = false;
                }
                subtitle.isSelected = true;
                
                [self loadSubtitleSource:subtitle];
            }];
            [alert addAction:english];
        }
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:cancel];
        
//        UIPopoverPresentationController *popPresenter = [alert
//                                                      popoverPresentationController];
//
//        popPresenter.sourceView = sender;
//        popPresenter.sourceRect = [sender bounds];
        
        [self presentViewController:alert animated:true completion:nil];
        
    } @catch (NSException *exception) {
    } @finally {
    }
    
}

-(void)setupSubtitles
{
    @try {
        self.subtitleTrackList = [NSMutableArray new];
        
        for (NSDictionary *subtitle in [StaticData checkArrayNull:self.videoDetails[@"subtitle_tracks"]])
        {
            MediaInfo *info = [MediaInfo new];
            info.title = [StaticData checkStringNull:subtitle[@"label"]];
            info.src = [StaticData checkStringNull:subtitle[@"src"]];
            info.srclang = [StaticData checkStringNull:subtitle[@"srclang"]];
            info.isSelected = [subtitle[@"default"] boolValue];
            [self.subtitleTrackList addObject:info];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)loadSubtitleSource:(MediaInfo *)subtitle
{
    @try {
//        [self.loadingIndicator startAnimating];
//        [self pauseVideoWithPlayerPlayingType:PauseButton];
//        [self.videoPlayer pause];
        self.subtitling.player = self.videoPlayer;
        self.subtitling.visible = false;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSError *error;
            NSURL *subtitlesURL = [NSURL URLWithString:subtitle.src];
            [self.subtitling loadSubtitlesAtURL:subtitlesURL error:&error];
            dispatch_sync(dispatch_get_main_queue(), ^{
                // This will be called on the main thread, so that
                // you can update the UI, for example.
                self.subtitling.visible = true;
                self.subtitling.containerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
                self.subtitling.containerView.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.0].CGColor;
                self.subtitling.containerView.hidden = true;
                
                [self.videoPlayer play];
//                [self.loadingIndicator stopAnimating];
//                [self playVideoWithPlayerPlayingType:PlayButton];
            });
        });
        
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)disableSubtitle
{
    self.subtitling.containerView.hidden = true;
    self.subtitling.label.hidden = true;
    self.subtitling.visible = false;
    self.subtitling.containerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.subtitling.label.textColor = [UIColor clearColor];
    [self.subtitling computeStyle];
    [self.btn_subtitle setSelected:false];
    self.btn_subtitle.tag = 0;
}

@end
