//
//  Progress.h
//  ZDT-DownloadVideoObjC
//
//  Created by Curiologix on 09/11/2016.
//  Copyright © 2016 Szabolcs Sztanyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Progress : UIView
// the layer that shows the actual progress
@property (nonatomic, assign) int percent;
@end
