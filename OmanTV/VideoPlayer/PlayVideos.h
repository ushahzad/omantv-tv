//
//  PlayVideos.h
//  TvDcnDigital
//
//  Created by Ali on 3/13/16.
//  Copyright © 2016 Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ASBPlayerSubtitling.h"
#import "Progress.h"
#import "VideoBanner.h"
#import "MediaInfo.h"

@interface PlayVideos : UIViewController <UIGestureRecognizerDelegate>
{
    IBOutlet UIView *view_relatedVideos;
    IBOutlet UICollectionView *cv_relatedVideos;
    
    IBOutlet NSLayoutConstraint *relatedVideosHeight;
    IBOutlet UIView *videoControls;
    
    IBOutlet UIView *view_videoControls;
    IBOutlet UIView *view_parentPlayer;
    IBOutlet UIView *overlay;
    
    IBOutlet UIButton *btn_playPause;
    IBOutlet UIButton *btn_replay;
    
    IBOutlet UIButton *fullScreen;
    IBOutlet UIButton *btn_progressFocuse;
    
    NSMutableArray *relatedVideos;
    id playbackObserver;
    BOOL _observingMediaPlayer;
    
    NSString *localMovieAddPath;
    BOOL isFindLocalVideoPath;
    BOOL isNormalVideoPlaying;
    BOOL isInitContnentPlayer;
    BOOL isMovieEnterInFullScreenMode;
    BOOL isAdsPlaying;
    VideoBanner *bannar;
    
    NSTimer *timerToUpdateOnlineApi;
    NSTimer *vidContTimer;
    
    CGRect activeFrame;
    CGRect currentVideoFrame;
    
    BOOL viewRotateComplete;
    BOOL videoIsInLandscapeModeAndFullScree;
    BOOL focuseUpdate;
    
    UIImage *resSetImage;
    
    NSString *parentID;
    int currentPlayingVideoTime;
    int currentPlayAbleVideoTime;
    BOOL playbackDurationSet;
    bool isProgressActive;
    
    NSMutableString *thirtyTwoChar;
    
    CGRect videoParent;
}
@property (nonatomic, assign) double lastChange;
@property (nonatomic, assign) CGPoint finalSpeed;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL viewIsActive;
@property (nonatomic, assign) BOOL isStopFocuFromStart;
@property (nonatomic, assign) BOOL isStopFocuFromStartOfCollection;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *collectionHeight;
@property (retain, nonatomic) IBOutlet UIButton *btn_play;
@property (retain, nonatomic) IBOutlet UIProgressView *progressBar;
@property (retain, nonatomic) IBOutlet UILabel *lbl_currentTime;
@property (retain, nonatomic) IBOutlet UILabel *lbl_remainTime;
@property (strong, nonatomic) IBOutlet ASBPlayerSubtitling *subtitling;
// Content Player
@property(nonatomic, weak) IBOutlet UIView *videoView;
@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic, weak) IBOutlet UIImageView *img_player_watermark;
@property (nonatomic, strong) NSTimer *updatePlayBackTimer;
// SDK

@property(nonatomic, readonly) NSTimeInterval currentTime;
@property(nonatomic, readonly) NSTimeInterval ad_timing;

@property(weak, nonatomic) NSString *show_id;
@property(weak, nonatomic) NSString *url;

@property (weak, nonatomic) IBOutlet UILabel *VideoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;
@property (weak, nonatomic) IBOutlet UITextView *VideoDescription;
@property (weak, nonatomic) IBOutlet UILabel *VideoRating;
//@property (weak, nonatomic) IBOutlet UILabel *VideoViews;
@property (weak, nonatomic) IBOutlet UILabel *VideoCat;
@property (weak, nonatomic) IBOutlet UIButton *MoreVideos;
- (IBAction)More:(id)sender;
- (IBAction)Share:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Fav;
- (IBAction)Favorite:(id)sender;
- (IBAction)RemoveFav:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *Faved;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator_progressThumbnail;
@property (weak, nonatomic) IBOutlet UIView *view_progressThumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *img_progressThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_progressTime;

@property (retain, nonatomic) AVPlayer *videoPlayer;
@property (retain, nonatomic) AVPlayerLayer *playerLayer;
@property(nonatomic, strong) AVPlayer *adPlayer;
@property(nonatomic, strong) AVPlayerLayer *adPlayerLayer;
@property(nonatomic, strong) UIView *adVideoView;
@property (nonatomic,retain) NSMutableDictionary *videoDetails;
@property (nonatomic, retain) NSMutableArray *episodes;
@property (nonatomic, retain) NSMutableArray *videoThumbnails;
@property (nonatomic, retain) NSMutableArray *subtitleTrackList;
@property(nonatomic, assign) BOOL isAdPlaying;
#pragma mark NextVideoProperties
@property (weak, nonatomic) IBOutlet UIImageView *progressThumb;
@property (weak, nonatomic) IBOutlet UIView *view_nextView;
@property (weak, nonatomic) IBOutlet UIView *view_nextOverLay;
@property (weak, nonatomic) IBOutlet UIButton *btn_nextVideo;
@property (weak, nonatomic) IBOutlet UIImageView *img_nextShow;
@property (weak, nonatomic) IBOutlet UIImageView *img_ads;
@property (nonatomic, retain) IBOutlet UIImageView *img_bbcPlayer;
@property (weak, nonatomic) IBOutlet UIButton *btn_plus;
@property (weak, nonatomic) IBOutlet UIImageView *img_plus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nextShowTitle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *nextShowIndicator;

@property (weak, nonatomic) IBOutlet UICollectionView *cv_thumbnails;
@property (weak, nonatomic) IBOutlet UIView *view_progress;
@property (nonatomic, retain) Progress *recordingProgress;
@property (nonatomic, retain) NSTimer *nextVideoTimer;
@property (nonatomic, retain) NSTimer *progressBarThumbnailTimer;

@property (nonatomic, retain) NSDictionary *nextEpisode;
@property (nonatomic, retain) NSDictionary *overlayDic;

@property(nonatomic, weak) IBOutlet NSLayoutConstraint *cv_heightAnchor;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint *cv_thumbHeightAnchor;

@property(nonatomic, retain) id adsInfo;

@property(nonatomic, retain) MediaInfo *selectedPorgressThumbnailInfo;

@property(nonatomic, retain) NSMutableArray *preAdList;
@property(nonatomic, retain) NSMutableArray *midAdList;
@property(nonatomic, retain) NSMutableArray *postAdList;
@property(nonatomic, retain) UIImage *largeVideoThumbnail;
@property(nonatomic, retain) UIImage *tempVideoThumbnail;
@property(nonatomic, retain) NSString *img_wide;
@property(nonatomic, assign) int resumeVideoPosition;
@property (weak, nonatomic) IBOutlet UIButton *btn_subtitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_introSkip;
@property (nonatomic, assign) int skipDurationPosition;

@property(nonatomic, assign) int currentPlayerSeconds;
@property (nonatomic, assign) BOOL currentProgressStateChanged;
@property (nonatomic, assign) BOOL isMovie;
@property (nonatomic, assign) BOOL isVideoPaused;
@property (nonatomic, assign) BOOL isComeFromResumeWatching;

@end
