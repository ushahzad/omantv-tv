//
//  LocalAdPlayer.m
//  AbuDhabiAppleTV
//
//  Created by MacUser on 14/02/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import "LocalAdPlayer.h"

@interface LocalAdPlayer ()

@end

@implementation LocalAdPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lbl_skipCounting.hidden = true;
    self.lbl_skipCounting.text = @"You can skip this ad in (5 seconds)";
    self.btn_skip.hidden = true;
    self.viewIsActive = true;
    [self playLocalAdNow];
    
    UITapGestureRecognizer *menuGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuTap)];
    menuGestureRec.allowedPressTypes = @[@(UIPressTypeMenu)];
    [self.view addGestureRecognizer:menuGestureRec];
    
}

-(void)handleMenuTap
{
    [self.avPlayer pause];
    [self dismissViewControllerAnimated:false completion:^{
        if (self.AdPlayingFinished) {
            self.AdPlayingFinished();
        }
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNormalVideoNow)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeSelect]];
    [self.btn_skip addGestureRecognizer:tap];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.viewIsActive = false;
}

- (void)playLocalAdNow{
    
    [self.indicator startAnimating];
    
    NSURL *url = [[NSURL alloc] initWithString:self.adPath];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:url];
    
    self.avPlayer = [AVPlayer playerWithPlayerItem:playerItem];
    self.avPlayerLayer  = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    [self.avPlayerLayer setFrame:self.view.frame];
    [self.view_player.layer addSublayer:self.avPlayerLayer];
    [self.avPlayer play];
    
    CMTime interval = CMTimeMake(33, 1000);
    __weak __typeof(self) weakself = self;
    self.playbackObserver = [self.avPlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        
        [weakself checkPlayBackTime];
        
        if (weakself.viewIsActive)
        {
            
        } else
        {
            [weakself.avPlayer pause];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackStateChanged:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.avPlayer currentItem]];
    
    [self.avPlayer.currentItem addObserver:self
                               forKeyPath:@"status"
                                  options:NSKeyValueObservingOptionNew
                                  context:nil];
    [self.avPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew
                                  context:nil];
    [self.avPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew
                                  context:nil];
    [self.avPlayer.currentItem addObserver:self
                               forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionNew
                                  context:nil];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    NSLog(@"observeValueForKeyPath %@", keyPath);
    
    if (self.avPlayer.currentItem == object)
    {
        if ([keyPath isEqualToString:@"status"]) {
            if (self.avPlayer.status == AVPlayerStatusReadyToPlay) {
                [self.indicator stopAnimating];
            }
        }
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        // Show loader
        [self.indicator startAnimating];
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        // Hide loader
        [self.indicator stopAnimating];
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        // Hide loader
        [self.indicator stopAnimating];
    } else if (self.avPlayer.currentItem == object)
    {
        if ([keyPath isEqualToString:@"status"]) {
            if (self.avPlayer.status == AVPlayerStatusReadyToPlay)
            {
                [self.indicator stopAnimating];
            } else if (self.avPlayer.status == AVPlayerStatusFailed)
            {
                [self playNormalVideoNow];
            }
        }
    }
}

- (void)checkPlayBackTime {
    
    self.lbl_skipCounting.hidden = false;
    
    int videoCurrentTime = (int)CMTimeGetSeconds(self.avPlayer.currentItem.currentTime);
    if (videoCurrentTime > 0) {

        if (videoCurrentTime > 0) {
            if (videoCurrentTime <= 5) {
                self.lbl_skipCounting.text = [NSString stringWithFormat:@"You can skip this ad in (%d seconds)",5-videoCurrentTime];
            }
            
            if (videoCurrentTime >= 5) {
                self.lbl_skipCounting.hidden = YES;
                self.btn_skip.hidden = NO;
                [self setNeedsFocusUpdate];
            }
        }
    }
}

#pragma mark Local Ads Playing Finished
-(void) moviePlayerPlaybackStateChanged:(NSNotification*)notification
{
    [self playNormalVideoNow];
}

-(NSArray *)preferredFocusEnvironments
{
    return @[self.btn_skip];
}

-(void)playNormalVideoNow
{
    [self dismissViewControllerAnimated:false completion:^{
        if (self.AdPlayingFinished) {
            self.AdPlayingFinished();
        }
    }];
}

@end
