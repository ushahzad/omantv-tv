//
//  IMAPlayer.h
//  AbuDhabiAppleTV
//
//  Created by MacUser on 15/02/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import <UIKit/UIKit.h>

@import GoogleInteractiveMediaAds;

NS_ASSUME_NONNULL_BEGIN

@interface IMAAdPlayer : UIViewController <IMAAdsLoaderDelegate, IMAAdsManagerDelegate>

@property  void (^AdPlayingFinished)(void);
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

// SDK
/// Entry point for the SDK. Used to make ad requests.
@property(nonatomic, strong) IMAAdsLoader *adsLoader;

/// Playhead used by the SDK to track content video progress and insert mid-rolls.
@property(nonatomic, strong) IMAAVPlayerContentPlayhead *contentPlayhead;

/// Main point of interaction with the SDK. Created by the SDK as the result of an ad request.
@property(nonatomic, strong) IMAAdsManager *adsManager;
@property (nonatomic, assign) NSString *adPath;
@end

NS_ASSUME_NONNULL_END
