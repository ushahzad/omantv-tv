//
//  VideoBanner.h
//  Awan
//
//  Created by Ali on 5/3/16.
//  Copyright © 2016 Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoBanner : UIView
@property (nonatomic, retain) IBOutlet UILabel *lbl_title;
@property (nonatomic, retain) IBOutlet UIButton *btn_skip;
@end
