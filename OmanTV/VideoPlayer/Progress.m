//
//  Progress.m
//  ZDT-DownloadVideoObjC
//
//  Created by Curiologix on 09/11/2016.
//  Copyright © 2016 Szabolcs Sztanyi. All rights reserved.
//

#import "Progress.h"

@interface Progress () {
    CGFloat startAngle;
    CGFloat endAngle;
}

@end

@implementation Progress

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 1.5;
        endAngle = startAngle + (M_PI * 2);
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Display our percentage as a string
    //NSString* textContent = [NSString stringWithFormat:@"%d", self.percent];
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.height/2)
                          radius:(rect.size.width/2)-10
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (_percent / 15.0) + startAngle
                       clockwise:YES];
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 10;
    [[StaticData colorFromHexString:@"#ffffff"] setStroke];
    [bezierPath stroke];
    
    // Text Drawing
    /*CGRect textRect = CGRectMake((rect.size.width / 2.0) - 71/2.0, (rect.size.height / 2.0) - 45/2.0, 71, 45);
    [[UIColor blackColor] setFill];
    [textContent drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 42.5] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];*/
}

@end
