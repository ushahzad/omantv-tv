//
//  IMAPlayer.m
//  AbuDhabiAppleTV
//
//  Created by MacUser on 15/02/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import "IMAAdPlayer.h"

@interface IMAAdPlayer ()

@end

@implementation IMAAdPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.indicator startAnimating];
    UITapGestureRecognizer *menuGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuTap)];
    menuGestureRec.allowedPressTypes = @[@(UIPressTypeMenu)];
    [self.view addGestureRecognizer:menuGestureRec];
//
//            NSString *const kTestAppAdTagUrl =
//            @"https://pubads.g.doubleclick.net/gampad/ads?slotname=/124319096/external/ad_rule_samples&sz=640x480&ciu_szs=300x250&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&url=https://developers.google.com/&unviewed_position_start=1&output=xml_vast3&impl=s&env=vp&gdfp_req=1&ad_rule=0&vad_type=linear&vpos=preroll&pod=1&ppos=1&lip=true&min_ad_duration=0&max_ad_duration=30000&vrid=5776&video_doc_id=short_onecue&cmsid=496&kfa=0&tfcd=0";
////
//    self.adPath = kTestAppAdTagUrl;
    [self requestAdNow];
}

-(void)handleMenuTap
{
    [self.adsManager pause];
    self.adsManager = nil;
    
    if (self.AdPlayingFinished) {
        self.AdPlayingFinished();
    }
}

#pragma mark SDK Setup

- (void)setupAdsLoader {
    self.adsLoader = [[IMAAdsLoader alloc] initWithSettings:nil];
    self.adsLoader.delegate = self;
}

- (void)requestAdNow {
    [self setupAdsLoader];
    // Create an ad display container for ad rendering.
    IMAAdDisplayContainer *adDisplayContainer =
    [[IMAAdDisplayContainer alloc] initWithAdContainer:self.view viewController:self];
    // Create an ad request with our ad tag, display container, and optional user context.
    IMAAdsRequest *request = [[IMAAdsRequest alloc] initWithAdTagUrl:self.adPath adDisplayContainer:adDisplayContainer contentPlayhead:self.contentPlayhead userContext:nil];
    [self.adsLoader requestAdsWithRequest:request];
}

- (void)contentDidFinishPlaying:(NSNotification *)notification {
    // Make sure we don't call contentComplete as a result of an ad completing.
    [self.adsLoader contentComplete];
}

#pragma mark AdsLoader Delegates

- (void)adsLoader:(IMAAdsLoader *)loader adsLoadedWithData:(IMAAdsLoadedData *)adsLoadedData {
    // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
    self.adsManager = adsLoadedData.adsManager;
    self.adsManager.delegate = self;
    // Create ads rendering settings to tell the SDK to use the in-app browser.
    IMAAdsRenderingSettings *adsRenderingSettings = [[IMAAdsRenderingSettings alloc] init];
    adsRenderingSettings.webOpenerPresentingController = self;
    // Initialize the ads manager.
    [self.adsManager initializeWithAdsRenderingSettings:adsRenderingSettings];
}

- (void)adsLoader:(IMAAdsLoader *)loader failedWithErrorData:(IMAAdLoadingErrorData *)adErrorData {
    // Something went wrong loading ads. Log the error and play the content.
    NSLog(@"Error loading ads: %@", adErrorData.adError.message);
    [self playNormalVideoNow];
}

#pragma mark AdsManager Delegates

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdEvent:(IMAAdEvent *)event {
    
    
    switch (event.type) {
        case kIMAAdEvent_LOADED:
            [self.indicator stopAnimating];
            [adsManager start];
            break;
        case kIMAAdEvent_PAUSE:
            [self playNormalVideoNow];
            break;
        case kIMAAdEvent_RESUME:
            [self.adsManager resume];
            break;
        case kIMAAdEvent_TAPPED:
            //[self showFullscreenControls:nil];
            break;
        case kIMAAdEvent_CLICKED:
            [self.adsManager pause];
            break;
        default:
            break;
    }
}

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdError:(IMAAdError *)error {
    // Something went wrong with the ads manager after ads were loaded. Log the error and play the
    // content.
    NSLog(@"AdsManager error: %@", error.message);
    [self playNormalVideoNow];
}

- (void)adsManagerDidRequestContentPause:(IMAAdsManager *)adsManager {
    // The SDK is going to play ads, so pause the content.
    [self.indicator stopAnimating];
}

- (void)adsManagerDidRequestContentResume:(IMAAdsManager *)adsManager {
    // The SDK is done playing ads (at least for now), so resume the content.
    [self playNormalVideoNow];
}

-(void)playNormalVideoNow
{
    [self.adsManager pause];
    self.adsManager = nil;
    
    [self dismissViewControllerAnimated:false completion:^{
        if (self.AdPlayingFinished) {
            self.AdPlayingFinished();
        }
    }];
}

@end
