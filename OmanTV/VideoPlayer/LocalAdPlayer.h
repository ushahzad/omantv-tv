//
//  LocalAdPlayer.h
//  AbuDhabiAppleTV
//
//  Created by MacUser on 14/02/2020.
//  Copyright © 2020 DotCome. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocalAdPlayer : UIViewController
@property  void (^AdPlayingFinished)(void);
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *view_player;
@property (weak, nonatomic) IBOutlet UIView *view_details;
@property (weak, nonatomic) IBOutlet UILabel *lbl_skipCounting;
@property (weak, nonatomic) IBOutlet UIButton *btn_skip;
@property (retain, nonatomic) AVPlayer *avPlayer;
@property (retain, nonatomic) AVPlayerLayer *avPlayerLayer;
@property (nonatomic, strong) id playbackObserver;
@property (nonatomic, assign) BOOL viewIsActive;
@property (nonatomic, assign) NSString *adPath;
@end

NS_ASSUME_NONNULL_END
