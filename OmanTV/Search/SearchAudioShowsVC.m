//
//  SearchAudioShowsVC.m
//  OmanTV
//
//  Created by Ali Raza on 13/04/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "SearchAudioShowsVC.h"

@interface SearchAudioShowsVC ()

@end

@implementation SearchAudioShowsVC

- (void)viewDidLoad {
    [super viewDidLoad];

    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    if (self.carouselInfo) {
        self.lbl_title.text = [StaticData checkStringNull:self.carouselInfo[@"title_ar"]];
    }
    [StaticData scaleToArabic:self.collectionView];
}

-(void)viewDidAppear:(BOOL)animated
{
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + 50;
        
        _heightConstraint.constant = height+80;
        
        if (self.UpdateContentSize)
        {
            self.UpdateContentSize();
        }
    }
    
    return CGSizeMake(width, height);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[StaticData radioPlaceHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    info.isRadio = true;
//    [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}

-(void)startSliderTimer
{
//    if ([self.sliderTimer isValid]) {
//        [self.sliderTimer invalidate];
//    }
//
//    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollToNextSliderItem) userInfo:nil repeats:true];
}


-(IBAction)playVideo:(id)sender
{
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

/*+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}*/

/*-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }
            
        }];
    }
    
    if (context.nextFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }
        }];
    }
}*/

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.contentList.count > 0) {
            for (int i = 0; i < self.contentList.count; i++)
            {
                MediaInfo *channel = self.contentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

@end
