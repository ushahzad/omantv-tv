//
//  SearchAudioVC.h
//  OmanTV
//
//  Created by Ali Raza on 12/04/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchAudioVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property void(^UpdateContentSize)(void);
@property void(^HomeBannerSwipeUpCallBack)(void);
@property (nonatomic, weak) IBOutlet UILabel *lbl_title;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSLayoutConstraint *heightConstraint;
@property (nonatomic, retain) NSLayoutConstraint *topConstraint;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) id carouselInfo;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (nonatomic, retain) CatchupAudioPlayer *radioPlayer;
@property (nonatomic, retain) MediaInfo *selectedShow;
@property (nonatomic, retain) MediaInfo *infoAudio;
@property (nonatomic, retain) NSString *str_playbackURL;
@property (nonatomic, retain) NSMutableString *sessionID;

@property (nonatomic, strong) NSTimer *updateOlineTimer;

-(void)loadDataFromServer;

@end

NS_ASSUME_NONNULL_END
