//
//  SearchAudioVC.m
//  OmanTV
//
//  Created by Ali Raza on 12/04/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "SearchAudioVC.h"

@interface SearchAudioVC ()

@end

@implementation SearchAudioVC

- (void)viewDidLoad {
    [super viewDidLoad];

    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playPausePlayer)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:tap];
    
    if (self.carouselInfo) {
        self.lbl_title.text = [StaticData checkStringNull:self.carouselInfo[@"title_ar"]];
    }
    [StaticData scaleToArabic:self.collectionView];
}

-(void)viewDidAppear:(BOOL)animated
{
}

-(void)viewWillDisappear:(BOOL)animated {
    if (![StaticData shared].isSearching) {
        [self.radioPlayer resetPlayer];
        [self.radioPlayer.view removeFromSuperview];
    }
}

-(void)playPausePlayer
{
    if (self.radioPlayer.player.rate == 1.0)
    {
        [self.radioPlayer.player pause];
    } else {
        [self.radioPlayer.player play];
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width - 225)/4.5;
    CGFloat height = width;
    if (self.tempImage)
    {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + 50;
        
        _heightConstraint.constant = height+80;
        
        if (self.UpdateContentSize)
        {
            self.UpdateContentSize();
        }
    }
    
    
    return CGSizeMake(width, height);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        NSLog(@"%@",imagePath);
        [cell.indicator startAnimating];
        cell.thumbnail.image = nil;
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[StaticData radioPlaceHolder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 if (!self.tempImage) {
                     self.tempImage = image;
                     [self reloadCollectionView];
                 }
                 //[StaticData imageLoaded:image withCell:cell];
                 [cell.indicator stopAnimating];
             } else {
                 if (!self.tempImage) {
                     self.tempImage = [StaticData radioPlaceHolder];
                     [self reloadCollectionView];
                 }
                 
                 [cell.indicator stopAnimating];
             }
         }];
    } @catch (NSException *exception) {
    } @finally {
    }

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    self.infoAudio = info;
    [self getPlayBackUrl:info];
}

-(void)getPlayBackUrl:(MediaInfo *)info
{
    [self.loading_indicator startAnimating];
    
    _selectedShow = info;
    
    [self.radioPlayer resetPlayer];
    [self.radioPlayer.view removeFromSuperview];
    
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=audio_details&id=%@&full=1&channel_userid=&need_next_audios=yes&need_next_audios_limit=8&need_all_fav_types=yes&need_avg_rating=yes&need_all_fav_types_in_next_audios=yes",BaseURL,BaseUID,BaseKEY,info.ID];
    [self executePostApiToGetResponse:path parameters:nil callType:@"RadioPlayBackUrl"];
}

-(void)executePostApiToGetResponse:(NSString *)url parameters:(NSDictionary *)parms callType:(NSString *)type
{
    NSLog(@"Calling Api...... = %@",url);
    NSLog(@"Calling Post Parameters...... = %@",parms);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    if ([type isEqualToString:@"AddToFavoriteShow"] || [type isEqualToString:@"FavoriteShows"] || [type isEqualToString:@"RemoveToFavoriteShow"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    [manager.requestSerializer setTimeoutInterval:60.0];
    
    if ([type isEqualToString:@"Like"] || [type isEqualToString:@"Dislike"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
    }
    
    if ([type isEqualToString:@"RadioPlayBackUrl"])
    {
        [manager.requestSerializer setValue:[StaticData findLogedUserToken] forHTTPHeaderField:@"X-API-KEY"];
        
        [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    } else
    {
        [manager POST:url parameters:parms progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.loading_indicator stopAnimating];
            NSLog(@"Response Received = %@",responseObject);
            if ([type isEqualToString:@"ShorterUrl"])
            {
                responseObject =[NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            }
            [self responseReceived:responseObject callType:type];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
            NSLog(@"Api Error: %@",error.description);
            [self.loading_indicator stopAnimating];
        }];
    }
}

-(void)responseReceived:(id) response callType:(NSString *)type
{
    @try {
        _str_playbackURL = response[@"playback_url"];
        [self setupAuidoPlayer];
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
        
    }
}

-(void)setupAuidoPlayer
{
    if (self.radioPlayer) {
        [self.radioPlayer setResumePositionOfVideo];
    }
    self.radioPlayer = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CatchupAudioPlayer"];
    self.radioPlayer.playerInfo = self.infoAudio;
    [StaticData shared].radioPlayer = self.radioPlayer;
    
    [self.parentViewController addChildViewController:self.radioPlayer];
    CGRect frame = self.radioPlayer.view.frame;
    frame.size.height = 130;
    frame.origin.y = self.parentViewController.view.frame.size.height;
    self.radioPlayer.view.frame = frame;
    [self.parentViewController.view addSubview:self.radioPlayer.view];
    [self.radioPlayer didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.radioPlayer.view.frame;
        frame.origin.y = self.parentViewController.view.frame.size.height - frame.size.height;
        self.radioPlayer.view.frame = frame;
    }];
    
    
    self.radioPlayer.str_playbackURL = _str_playbackURL;
    self.radioPlayer.lbl_title.text = _selectedShow.title;
    [self.radioPlayer setupGestureActionsOnPlayer];
    [self.radioPlayer playRadio];
    
    
    if (self.updateOlineTimer) {
        [self.updateOlineTimer invalidate];
    }
    //self.updateOlineTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(Updaterequest) userInfo:nil repeats:YES];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    self.sessionID = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [self.sessionID appendFormat:@"%C", c];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Current Page = %ld",(long)indexPath.row);
}

-(void)startSliderTimer
{
//    if ([self.sliderTimer isValid]) {
//        [self.sliderTimer invalidate];
//    }
//
//    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollToNextSliderItem) userInfo:nil repeats:true];
}


-(IBAction)playVideo:(id)sender
{
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}

/*+(void)updateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator withCollectionView:(UICollectionView *)collectionView
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
    }
}*/

/*-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }
            
        }];
    }
    
    if (context.nextFocusedView == self.btn_openShow) {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = [self findSelectedCell];
            if (cell)
            {
                cell.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }
        }];
    }
}*/

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.contentList.count > 0) {
            for (int i = 0; i < self.contentList.count; i++)
            {
                MediaInfo *channel = self.contentList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

@end
