//
//  SearchResultsVC.h
//  Sports
//
//  Created by MacUser on 02/01/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchAudioVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface SearchResultsVC : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property void(^FocuseUpdateCallBack)(void);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;

@property (nonatomic, weak) IBOutlet UIView *view_tabbar;

@property (nonatomic, weak) IBOutlet UIView *view_mostSearch;
//@property (nonatomic, weak) IBOutlet UICollectionView *mostSearchCollectionView;
@property (nonatomic, assign) BOOL isNeedToLoadMorePrograms;
@property (nonatomic, assign) int mostSearchPageNumber;

@property (nonatomic, weak) IBOutlet UILabel *lbl_searchType;
@property (nonatomic, weak) IBOutlet UILabel *lbl_searchText;
@property (nonatomic, weak) IBOutlet UILabel *lbl_totalResults;
@property (nonatomic, weak) IBOutlet UILabel *lbl_search;
@property (nonatomic, weak) IBOutlet UIButton *btn_ar;
@property (nonatomic, weak) IBOutlet UIView *view_ar;

@property (nonatomic, retain) SearchAudioVC *searchAudios;

@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_content;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_types;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *parentViewBottomAnchor;

@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UILabel *lbl_category;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noResultsFound;

@property (nonatomic, weak) IBOutlet UITextField *txt_search;
@property (nonatomic, weak) IBOutlet UIView *inputAccessoryView;

@property (nonatomic, strong) NSTimer *searchTypeTimer;

@property (nonatomic, retain) MediaInfo *selectedSearchType;
@property (nonatomic, retain) NSMutableArray *programsList;
@property (nonatomic, retain) NSMutableArray *seriesList;
@property (nonatomic, retain) NSMutableArray *moviesList;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *mostSearchList;
@property (nonatomic, retain) NSMutableArray *typesList;

@property (nonatomic, retain) MediaInfo *selectedCategory;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, retain) NSString *searchText;

@property (nonatomic, retain) UIFocusGuide *focusGuide;
@property (nonatomic, retain) NSString *searchType;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic, assign) int limit;

@end

NS_ASSUME_NONNULL_END
