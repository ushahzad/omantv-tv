//
//  ViewController.m
//  ArabicKeyboard
//
//  Created by MacUser on 29/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import "ArabicSearch.h"
#import "CollectionCell.h"
#import "KeyboardCharacters.h"
#import "KeyboardShiftCharacters.h"

#define CellCollor [self colorFromHexString:@"#A4A4A4"];
#define SelectedCellCollor [UIColor whiteColor];

@interface ArabicSearch ()

@end

@implementation ArabicSearch

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.characterAddedLocations = [NSMutableArray new];
    
    [self loadKeyboardData];
    
    self.btn_searchNow.layer.cornerRadius = 8.0;
    self.btn_searchNow.clipsToBounds = true;
    
    self.btn_space.layer.cornerRadius = 8.0;
    self.btn_space.clipsToBounds = true;
    
    self.btn_english.layer.cornerRadius = 8.0;
    self.btn_english.clipsToBounds = true;
    
    [StaticData scaleToArabic:self.cv_keyboardRow1];
    [StaticData scaleToArabic:self.cv_keyboardRow2];
    [StaticData scaleToArabic:self.cv_keyboardRow3];
    [StaticData scaleToArabic:self.cv_keyboardRow4];
    
    [self setupTriggerActionOnButtons];
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_space,self.btn_searchNow,self.btn_english];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    if (sender == self.btn_space)
    {
        [self appendNewTextWithSearchText:@" "];
    } else if (sender == self.btn_searchNow)
    {
        if (self.searchField.text.length > 0)
        {
            
        }
        
        if (self.SearchNowCallBack) {
            self.SearchNowCallBack(self.searchField.text);
        }
        [self dismissViewControllerAnimated:true completion:nil];
    } else if (sender == self.btn_english)
    {
        if (self.EnglishSearchCallBack) {
            self.EnglishSearchCallBack();
        }
        [self dismissViewControllerAnimated:true completion:nil];
    }
}


-(void)loadKeyboardData
{
    if (self.isShiftPress) {
        self.keyboardRow1 = [[KeyboardShiftCharacters shared] keyboardRow1Data];
        self.keyboardRow2 = [[KeyboardShiftCharacters shared] keyboardRow2Data];
        self.keyboardRow3 = [[KeyboardShiftCharacters shared] keyboardRow3Data];
        self.keyboardRow4 = [[KeyboardShiftCharacters shared] keyboardRow4Data];
    } else {
        self.keyboardRow1 = [[KeyboardCharacters shared] keyboardRow1Data];
        self.keyboardRow2 = [[KeyboardCharacters shared] keyboardRow2Data];
        self.keyboardRow3 = [[KeyboardCharacters shared] keyboardRow3Data];
        self.keyboardRow4 = [[KeyboardCharacters shared] keyboardRow4Data];
    }
    
    [self reloadCollectionView];
}

-(void)reloadCollectionView;
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.cv_keyboardRow1 reloadData];
        [self.cv_keyboardRow2 reloadData];
        [self.cv_keyboardRow3 reloadData];
        [self.cv_keyboardRow4 reloadData];
    });
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = 0.0;
    
    if (collectionView == self.cv_keyboardRow1) {
        width = (frame.size.width - (20 * self.keyboardRow1.count)) / self.keyboardRow1.count;
    } else if (collectionView == self.cv_keyboardRow2) {
        width = (frame.size.width - (20 * self.keyboardRow2.count)) / self.keyboardRow2.count;
    } else if (collectionView == self.cv_keyboardRow3) {
        width = (frame.size.width - (20 * self.keyboardRow3.count)) / self.keyboardRow3.count;
    } else if (collectionView == self.cv_keyboardRow4) {
        width = (frame.size.width - (20 * self.keyboardRow4.count)) / self.keyboardRow4.count;
    }
    
    return CGSizeMake(width, frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.cv_keyboardRow1) {
        return self.keyboardRow1.count;
    } else if (collectionView == self.cv_keyboardRow2) {
        return self.keyboardRow2.count;
    } else if (collectionView == self.cv_keyboardRow3) {
        return self.keyboardRow3.count;
    } else if (collectionView == self.cv_keyboardRow4) {
        return self.keyboardRow4.count;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    KeyboradInfo *info = nil;
    if (collectionView == self.cv_keyboardRow1) {
        info = self.keyboardRow1[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow2) {
        info = self.keyboardRow2[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow3) {
        info = self.keyboardRow3[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow4) {
        info = self.keyboardRow4[indexPath.row];
    }
    
    cell.lbl_title.text = info.title;
    
    cell.layer.cornerRadius = 8.0;
    cell.clipsToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    KeyboradInfo *info = nil;
    if (collectionView == self.cv_keyboardRow1) {
        info = self.keyboardRow1[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow2) {
        info = self.keyboardRow2[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow3) {
        info = self.keyboardRow3[indexPath.row];
    } else if (collectionView == self.cv_keyboardRow4) {
        info = self.keyboardRow4[indexPath.row];
    }
    
     self.searchField.inputDelegate = self;
    
    // 1 is cross
    if (info.type == 1) {
        if (self.searchField.text.length > 0)
        {
            self.searchField.text = [self.searchField.text substringToIndex:[self.searchField.text length] - 1];
            
//            NSString *characters = [self.characterAddedLocations lastObject];
//            NSString *newString = self.searchField.text;
//            newString = [self.searchField.text substringFromIndex:characters.length];
//            self.searchField.text = newString;
//            [self.characterAddedLocations removeLastObject];
        }
    } else {
        if (info.type == 2)
        {
            if (self.isShiftPress) {
                self.isShiftPress = false;
            } else {
                self.isShiftPress = true;
            }
            [self loadKeyboardData];
            return;
        }
        
        [self appendNewTextWithSearchText:info.title];
    }
}

-(void)appendNewTextWithSearchText:(NSString *)text
{
    //[_characterAddedLocations addObject:text];
    NSString *search = _searchField.text;
    search = [_searchField.text stringByAppendingString:text];
    self.searchField.text = search;
}

- (void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    @try {
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.previouslyFocusedIndexPath];
            cell.lbl_title.textColor = [UIColor whiteColor];
            cell.backgroundColor = CellCollor;
        }];
        
        [UIView animateWithDuration:0.1 animations:^{
            CollectionCell *cell = (CollectionCell *)[collectionView cellForItemAtIndexPath:context.nextFocusedIndexPath];
            cell.lbl_title.textColor = [UIColor blackColor];
            cell.backgroundColor = SelectedCellCollor;
            //context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.nextFocusedView == self.btn_space)
    {
        self.btn_space.backgroundColor = SelectedCellCollor;
        [self.btn_space setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } else if (context.nextFocusedView == self.btn_searchNow)
    {
        self.btn_searchNow.backgroundColor = SelectedCellCollor;
        [self.btn_searchNow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } else if (context.nextFocusedView == self.btn_english)
    {
        self.btn_english.backgroundColor = SelectedCellCollor;
        [self.btn_english setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    if (context.previouslyFocusedView == self.btn_space)
    {
        self.btn_space.backgroundColor = CellCollor;
        [self.btn_space setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (context.previouslyFocusedView == self.btn_searchNow)
    {
        self.btn_searchNow.backgroundColor = CellCollor;
        [self.btn_searchNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (context.previouslyFocusedView == self.btn_english)
    {
        self.btn_english.backgroundColor = CellCollor;
        [self.btn_english setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

-(void)keyboardRow1Data
{
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"ذ";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"١";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٢";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٣";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٤";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٥";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٦";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٧";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٨";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٩";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٠";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"-";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"=";
    [self.keyboardRow1 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"X";
    info.type = 1;
    [self.keyboardRow1 addObject:info];
}

-(void)keyboardRow2Data
{
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"ض";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ص";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ث";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ق";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ف";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"غ";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ع";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ه";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"خ";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ح";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ج";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"د";
    [self.keyboardRow2 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"\\";
    [self.keyboardRow2 addObject:info];
}

-(void)keyboardRow3Data
{
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"SF";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ش";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"س";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ي";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ب";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ل";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ا";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ت";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ن";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"م";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ك";
    [self.keyboardRow3 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ط";
    [self.keyboardRow3 addObject:info];
}

-(void)keyboardRow4Data
{
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"SF";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ئ";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ء";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ؤ";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ر";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"لا";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ى";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ة";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"و";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ز";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ظ";
    [self.keyboardRow4 addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"SF";
    [self.keyboardRow4 addObject:info];
}

// Assumes input like "#00FF00" (#RRGGBB).
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
