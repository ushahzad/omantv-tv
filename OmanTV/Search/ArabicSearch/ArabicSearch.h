//
//  ViewController.h
//  ArabicKeyboard
//
//  Created by MacUser on 29/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboradInfo.h"
#import "MyTextField.h"

@interface ArabicSearch : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,UITextFieldDelegate,UITextInputDelegate>

@property void(^SearchNowCallBack)(NSString *searchText);
@property void(^EnglishSearchCallBack)(void);
@property (nonatomic, weak) IBOutlet MyTextField *searchField;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_keyboardRow1;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_keyboardRow2;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_keyboardRow3;
@property (nonatomic, weak) IBOutlet UICollectionView *cv_keyboardRow4;

@property (nonatomic, weak) IBOutlet UIButton *btn_space;
@property (nonatomic, weak) IBOutlet UIButton *btn_searchNow;
@property (nonatomic, weak) IBOutlet UIButton *btn_english;

@property (nonatomic, retain) NSMutableArray *keyboardRow1;
@property (nonatomic, retain) NSMutableArray *keyboardRow2;
@property (nonatomic, retain) NSMutableArray *keyboardRow3;
@property (nonatomic, retain) NSMutableArray *keyboardRow4;

@property (nonatomic, retain) NSMutableArray *characterAddedLocations;

@property (nonatomic, assign) BOOL isShiftPress;
@end

