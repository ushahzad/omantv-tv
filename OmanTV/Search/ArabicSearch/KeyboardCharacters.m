//
//  KeyboardCharacters.m
//  ArabicKeyboard
//
//  Created by MacUser on 30/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import "KeyboardCharacters.h"
#import "KeyboradInfo.h"

@implementation KeyboardCharacters

static KeyboardCharacters *sharedInstance;

+(KeyboardCharacters *)shared
{
    if (!sharedInstance)
    {
        sharedInstance = [KeyboardCharacters new];
    }
    return sharedInstance;
}

-(NSMutableArray *)keyboardRow1Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"ذ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"١";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٢";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٣";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٤";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٥";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٦";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٧";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٨";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٩";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"٠";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"-";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"=";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"X";
    info.type = 1;
    [keyboarChar addObject:info];
    
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow2Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"ض";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ص";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ث";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ق";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ف";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"غ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ع";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ه";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"خ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ح";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ج";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"د";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"\\";
    [keyboarChar addObject:info];
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow3Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"SF";
    info.type = 2;
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ش";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"س";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ي";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ب";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ل";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ا";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ت";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ن";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"م";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ك";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ط";
    [keyboarChar addObject:info];
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow4Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];

    info.title = @"ئ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ء";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ؤ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ر";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"لا";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ى";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ة";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"و";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ز";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ظ";
    [keyboarChar addObject:info];
    return keyboarChar;
}


@end
