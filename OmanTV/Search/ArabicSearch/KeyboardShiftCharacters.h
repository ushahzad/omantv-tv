//
//  KeyboardShiftCharacters.h
//  ArabicKeyboard
//
//  Created by MacUser on 30/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KeyboardShiftCharacters : NSObject
+(KeyboardShiftCharacters *)shared;
-(NSMutableArray *)keyboardRow1Data;
-(NSMutableArray *)keyboardRow2Data;
-(NSMutableArray *)keyboardRow3Data;
-(NSMutableArray *)keyboardRow4Data;
@end

NS_ASSUME_NONNULL_END
