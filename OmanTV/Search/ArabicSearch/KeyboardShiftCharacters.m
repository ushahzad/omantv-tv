//
//  KeyboardShiftCharacters.m
//  ArabicKeyboard
//
//  Created by MacUser on 30/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import "KeyboardShiftCharacters.h"
#import "KeyboradInfo.h"

@implementation KeyboardShiftCharacters

static KeyboardShiftCharacters *sharedInstance;

+(KeyboardShiftCharacters *)shared
{
    if (!sharedInstance)
    {
        sharedInstance = [KeyboardShiftCharacters new];
    }
    return sharedInstance;
}

-(NSMutableArray *)keyboardRow1Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"ّ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"!";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"@";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"#";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"$";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"%";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"^";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"&";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"*";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @")";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"(";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"_";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"+";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"X";
    info.type = 1;
    [keyboarChar addObject:info];
    
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow2Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"َ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ً";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ُ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ٌ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"لإ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"إ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"‘";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"÷";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"×";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"؛";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"<";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @">";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"|";
    [keyboarChar addObject:info];
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow3Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info.title = @"SF";
    info.type = 2;
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ِ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ِ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ى";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ة";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"لأ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"أ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ـ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"،";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"//";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @":";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"\"";
    [keyboarChar addObject:info];
    return keyboarChar;
}

-(NSMutableArray *)keyboardRow4Data
{
    NSMutableArray *keyboarChar = [NSMutableArray new];
    KeyboradInfo *info = [KeyboradInfo new];
    info = [KeyboradInfo new];
    info.title = @"~";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ء";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ء";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"ئ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"لآ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"آ";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"’";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @",";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @".";
    [keyboarChar addObject:info];
    
    info = [KeyboradInfo new];
    info.title = @"؟";
    [keyboarChar addObject:info];
    return keyboarChar;
}


@end
