//
//  KeyboradInfo.h
//  ArabicKeyboard
//
//  Created by MacUser on 30/10/2019.
//  Copyright © 2019 AliRaza. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KeyboradInfo : NSObject
@property (nonatomic, retain) NSString *title;
@property (nonatomic, assign) int type;
@end

NS_ASSUME_NONNULL_END
