//
//  SearchResultsVC.m
//  Sports
//
//  Created by MacUser on 02/01/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "SearchResultsVC.h"
#import "ArabicSearch.h"
#import "SearchProgramsVC.h"
#import "SearchVideosVC.h"
#import "SearchAudioVC.h"
#import "SearchAudioShowsVC.h"

#define CarousHeight 600.0

@interface SearchResultsVC ()

@end

@implementation SearchResultsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"search"];
    
//    self.focusGuide = [UIFocusGuide new];
//    [self.view addLayoutGuide:self.focusGuide];
//    self.focusGuide.preferredFocusEnvironments = @[self.txt_search];
//
//    // define constraints
//    [self.focusGuide.leftAnchor constraintEqualToAnchor:self.scroll_view.leftAnchor].active = YES;
//    [self.focusGuide.rightAnchor constraintEqualToAnchor:self.scroll_view.rightAnchor].active = YES;
//    [self.focusGuide.topAnchor constraintEqualToAnchor:self.txt_search.topAnchor].active = YES;
//    [self.focusGuide.bottomAnchor constraintEqualToAnchor:self.txt_search.bottomAnchor].active = YES;
    
    if (@available(tvOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(tvOS 13.0, *)) {
        [self.collectionView setAutomaticallyAdjustsScrollIndicatorInsets:false];
    } else {
        // Fallback on earlier versions
    }
    
    self.typesList = [NSMutableArray new];
    self.programsList = [NSMutableArray new];
    self.seriesList = [NSMutableArray new];
    self.contentList = [NSMutableArray new];
    self.mostSearchList = [NSMutableArray new];
    
    MediaInfo *info = [MediaInfo new];
    info.title = @"البرامج";
    info.isSelected = true;
    info.type = 1;
    [self.typesList addObject:info];
    self.selectedSearchType = info;
    
    info = [MediaInfo new];
    info.title = @"المسلسلات";
    info.type = 2;
    [self.typesList addObject:info];
    
//    info = [MediaInfo new];
//    info.title = @"أفلام";
//    info.type = 3;
//    [self.typesList addObject:info];
    
    self.cv_types.layer.cornerRadius = 35.0;
    self.cv_types.clipsToBounds = true;
    self.cv_types.backgroundColor = [StaticData colorFromHexString:@"#353535"];
    //self.txt_search.text = @"هذا";
    
    // @"الخافي";//@"غمز البارود ";  //@"انا";//self.searchText;
    
    if (_searchText.length > 0)
    {
        self.txt_search.text = _searchText;
        if (self.txt_search.text.length >= 3) {
            //[self searchFromServer];
        }
    }
    
    self.view_ar.backgroundColor = AppColor;
    self.view_ar.layer.cornerRadius = 8.0;
    
    self.searchType = @"all";
    self.limit = 6;
    
    //self.view_parent.hidden = true;
    
    [StaticData textFieldPadding:self.txt_search];
    [StaticData textFieldPlaceHolderColor:self.txt_search];
    
    [StaticData scaleToArabic:self.cv_types];
    [StaticData scaleToArabic:self.collectionView];
    
    [self loadProgramFromServer];
    [self openArabicSearchPage];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self.txt_search becomeFirstResponder];
    
    //[self updateFocus];
    
    [self addGesture];
}

-(void)addGesture
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openArabicSearchPage)];
    tap.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeSelect]];
    [self.btn_ar addGestureRecognizer:tap];
}

-(void)openArabicSearchPage
{
    [StaticData shared].isSearching = true;
    ArabicSearch *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ArabicSearch"];
    vc.SearchNowCallBack = ^(NSString *searchText) {
        self.tabBarController.tabBar.userInteractionEnabled = false;
        //[self updateFocus];
        
        self.searchText = searchText;
       self.txt_search.text = searchText;
       if (self.txt_search.text.length > 0) {
           [self resetSearchingContent];
           [self searchContentFromServer:self.txt_search.text];
//           [self searchContentFromServer:@"جلالة السلطان"];
       }
        
        if (searchText.length == 0 && self.mostSearchList.count > 0) {
            self.view_mostSearch.hidden = false;
        }
        else {
            self.view_mostSearch.hidden = true;
        }
    };
    
    vc.EnglishSearchCallBack = ^{
        [self performSelector:@selector(openKeyboard) withObject:nil afterDelay:1];
    };
    [self presentViewController:vc animated:true completion:nil];
}

-(void)openKeyboard
{
    [self.txt_search becomeFirstResponder];
}

-(void)searchContentFromServer:(NSString *)searchAbleText
{
    self.lbl_noResultsFound.hidden = true;
    [self.indicator startAnimating];
    NSString *text = [searchAbleText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *path = [NSString stringWithFormat:@"%@/plus/search_result?user_id=%@&key=%@&ch_id=&query=%@&p=1&limit=50&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,text,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"Search"];
}

-(void)searchAudioContentFromServer:(NSString *)searchAbleText
{
    self.lbl_noResultsFound.hidden = true;
    [self.indicator startAnimating];
    NSString *text = [searchAbleText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *path = [NSString stringWithFormat:@"%@/nand?user_id=%@&key=%@&scope=audio&action=search_result&query=%@&p=%d&limit=6&channel_id=&cat_id&need_channel=yes&exclude_channel_id=%@",BaseURL,BaseUID,BaseKEY,text,self.pageNumber,ExcludeChannelId];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"SearchAudio"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [self.indicator startAnimating];
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        self.view_parent.hidden = false;
        [StaticData log:[NSString stringWithFormat:@"Call Type %@ = %@",callType,responseObject]];
         
         [self responseReceived:responseObject withCallType:callType];
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.indicator stopAnimating];
         self.view_parent.hidden = false;
         [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
     }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"Search"])
        {
            [self searchAudioContentFromServer:self.txt_search.text];
            if (self.txt_search.text.length == 0){
                [self resetSearchingContent];
                return;
            }
            
            int subTotal = 0;
            // first we will try to fetch series
            NSMutableArray *contentList = [NSMutableArray new];
            for (NSDictionary *dicObj in responseObject[@"results"][@"categories"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([dicObj[@"is_radio"] intValue] == 1) {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = dicObj[@"id"];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                if (info.title.length == 0) {
                    info.title = [StaticData checkStringNull:dicObj[@"title_en"]];
                }
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [contentList addObject:info];
                }
            }
            
            subTotal += contentList.count;
            [self setupProgramsVCWithContentList:contentList];
            
            // second we will try to fetch programs
            contentList = [NSMutableArray new];
            for (NSDictionary *dicObj in responseObject[@"results"][@"videos"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                if ([dicObj[@"is_radio"] intValue] == 1) {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = dicObj[@"id"];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                if (info.title.length == 0) {
                    info.title = [StaticData checkStringNull:dicObj[@"title_en"]];
                }
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [contentList addObject:info];
                }
            }
            
            subTotal += contentList.count;
            [self setupVideosVCWithContentList:contentList];
            
            if (subTotal == 0) {
                self.lbl_noResultsFound.hidden = false;
                [self resetSearchingContent];
            } else
            {
                self.lbl_noResultsFound.hidden = true;
            }
            
            [self loadContentBySearchType];
        }else if ([callType isEqualToString:@"SearchAudio"])
        {
            if (self.txt_search.text.length == 0){
                [self resetSearchingContent];
                return;
            }
            
            int subTotal = 0;
            NSMutableArray *contentList = [NSMutableArray new];
            for (NSDictionary *dicObj in responseObject[@"categories"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
//                if ([dicObj[@"is_radio"] intValue] == 1) {
//                    continue;
//                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = dicObj[@"id"];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.channelID = [StaticData checkStringNull:dicObj[@"channel_id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                if (info.title.length == 0) {
                    info.title = [StaticData checkStringNull:dicObj[@"title_en"]];
                }
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [contentList addObject:info];
                }
            }
            
            subTotal += contentList.count;
            [self setupAudiosShowsVCWithContentList:contentList];
            
            subTotal = 0;
            int total = 0;
            // New add movies we will try to fetch Movies
            contentList = [NSMutableArray new];
            for (NSDictionary *dicObj in responseObject[@"audios"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = dicObj[@"id"];
                info.channelID = [StaticData checkStringNull:dicObj[@"channel_id"]];
                info.title = [StaticData title:dicObj];
                info.duration = [StaticData checkStringNull:dicObj[@"duration"]];
                info.time = [StaticData getVideoDuration:[info.duration intValue]];
                
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [contentList addObject:info];
                }
            }
            
            total = [responseObject[@"total_audios"] intValue];
            subTotal += total;
            
            if (contentList.count > 0 || total > 0) {
                [self setupAudiosVCWithContentList:contentList];
            }
            
            if (contentList.count != 0) {
                self.lbl_noResultsFound.hidden = true;
            }
            
//            [self findTotalSearchResults];
            
            //[self scrollToSpecificItems];
        } else if ([callType isEqualToString:@"ContentList"])
        {
            NSInteger count = self.mostSearchList.count;
            for (NSDictionary *dicObj in responseObject[@"data"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                
                info.title = [StaticData title:dicObj];
                info.exclusive = [StaticData checkStringNull:dicObj[@"exclusive"]];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                if (info.thumbnailPath.length == 0 || [info.thumbnailPath isEqualToString:@"0"]) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"channel_cover"]];
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_mostSearchList addObject:info];
                }
            }
            
            self.isNeedToLoadMorePrograms = false;
            if (count != self.mostSearchList.count) {
                self.isNeedToLoadMorePrograms = true;
            }
            
            if (self.mostSearchList.count == 0) {
                self.view_mostSearch.hidden = true;
            }
            
            [self.collectionView reloadData];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)setupProgramsVCWithContentList:(NSMutableArray *)contentList
{
    SearchProgramsVC *SearchProgramsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchProgramsVC"];
    SearchProgramsVC.contentList = [contentList mutableCopy];
    CGRect frame = SearchProgramsVC.view.frame;
    CGFloat height = 680;
    if (contentList.count == 0) {
        height = 0;
        return;
    }
    
    [self addChildViewController:SearchProgramsVC];
    
    [SearchProgramsVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:SearchProgramsVC.view];
    
    [SearchProgramsVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
        SearchProgramsVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    SearchProgramsVC.HomeBannerSwipeUpCallBack = ^{
    };
    
    SearchProgramsVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
}

-(void)setupVideosVCWithContentList:(NSMutableArray *)contentList
{
    SearchVideosVC *SearchProgramsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchVideosVC"];
    SearchProgramsVC.contentList = [contentList mutableCopy];
    CGRect frame = SearchProgramsVC.view.frame;
    CGFloat height = 680;
    if (contentList.count == 0) {
        height = 0;
        return;
    }
    
    [self addChildViewController:SearchProgramsVC];
    
    [SearchProgramsVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:SearchProgramsVC.view];
    
    [SearchProgramsVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
        SearchProgramsVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    SearchProgramsVC.HomeBannerSwipeUpCallBack = ^{
    };
    
    SearchProgramsVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
}

-(void)setupAudiosShowsVCWithContentList:(NSMutableArray *)contentList
{
    SearchAudioShowsVC *SearchAudioShowVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchAudioShowsVC"];
    SearchAudioShowVC.contentList = [contentList mutableCopy];
    CGRect frame = SearchAudioShowVC.view.frame;
    CGFloat height = 400;
    if (contentList.count == 0) {
        height = 0;
        return;
    }
    
    [self addChildViewController:SearchAudioShowVC];
    
    [SearchAudioShowVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:SearchAudioShowVC.view];
    
    [SearchAudioShowVC didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
        SearchAudioShowVC.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    SearchAudioShowVC.HomeBannerSwipeUpCallBack = ^{
    };
    
    SearchAudioShowVC.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
}

-(void)setupAudiosVCWithContentList:(NSMutableArray *)contentList
{
     self.searchAudios = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchAudioVC"];
    self.searchAudios.contentList = [contentList mutableCopy];
    CGRect frame = self.searchAudios.view.frame;
    CGFloat height = 400;
    if (contentList.count == 0) {
        height = 0;
        return;
    }
    
    [self addChildViewController:self.searchAudios];
    
    [self.searchAudios.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_content addSubview:self.searchAudios.view];
    
    [self.searchAudios didMoveToParentViewController:self];
    
    __weak typeof(self) weakSelf = self;
    [self addConstraingOnScrollChilds:self.view_content withFixedHeight:height withTop:0.0  completed:^(NSLayoutConstraint *heightConstraint) {
        self.searchAudios.heightConstraint = heightConstraint;
        [weakSelf viewDidLayoutSubviews];
    }];
    
    self.searchAudios.HomeBannerSwipeUpCallBack = ^{
    };
    
    self.searchAudios.UpdateContentSize = ^{
       [weakSelf updateScrollViewContentSize];
    };
}

-(void)loadContentBySearchType
{
    if (self.selectedSearchType.type == 1)
    {
        self.contentList = [_programsList mutableCopy];
    } else if (self.selectedSearchType.type == 2)
    {
        self.contentList = [_seriesList mutableCopy];
    } else if (self.selectedSearchType.type == 3)
    {
        self.contentList = [_moviesList mutableCopy];
    }
    
    self.lbl_totalResults.text = [NSString stringWithFormat:@"%lu نتائج %@",(unsigned long)self.contentList.count,self.txt_search.text];
    self.lbl_searchType.text = self.selectedSearchType.title;
    
    [self.collectionView setContentOffset:CGPointZero animated:NO];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self.searchTypeTimer invalidate];
    [self resetSearchingContent];
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.searchTypeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(searchingTextWithDelay:) userInfo:searchStr repeats:NO];
    
    [self.indicator stopAnimating];
    
    if (searchStr.length > 0)
    {
        [self.indicator startAnimating];
        self.lbl_noResultsFound.hidden = true;
    }
    return true;
}

-(void)searchingTextWithDelay:(NSTimer *)timer
{
    @try {
        NSString *searchText = timer.userInfo;
        [self resetSearchingContent];
        if (searchText.length > 0) {
            [self searchContentFromServer:searchText];
//            [self searchAudioContentFromServer:searchText];
        }
        
        if (searchText.length == 0 && self.mostSearchList.count > 0) {
            self.view_mostSearch.hidden = false;
        }
        else {
            self.view_mostSearch.hidden = true;
        }
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)searchTextNow:(NSString *)searchAbleText
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    if (_txt_search.text.length > 0) {
//        self.txt_search.text = textField.text;
//        [self resetSearchingContent];
//        [self searchContentFromServer:self.txt_search.text];
//        [self.txt_search resignFirstResponder];
//    } else {
//        [StaticData showAlertView:@"الحد الأدنى للبحث كلمة من ثلاثة أحرف" withvc:self];
//    }
    [self.txt_search resignFirstResponder];
    return true;
}

-(void)resetSearchingContent
{
    @try {
        self.lbl_searchType.text = @"";
        self.searchType = @"all";
        self.limit = 50;
        [self.searchAudios.radioPlayer removeAudioPlayer];
        [self.view_content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(IBAction)hideKeyboard:(id)sender
{
    [self.txt_search resignFirstResponder];
}

-(void)updateScrollViewContentSize
{
    [self viewDidLayoutSubviews];
    //[self perfor  mSelector:@selector(viewDidLayoutSubviews) withObject:nil afterDelay:0.5];
}

-(void)viewDidLayoutSubviews
{
    @try {
        UIView *view = [[[self.scroll_view.subviews objectAtIndex:0] subviews] lastObject];
        CGFloat contentSize = [view frame].origin.y + [view frame].size.height + 50;
        [self.scroll_view setContentSize:CGSizeMake(self.scroll_view.frame.size.width, contentSize + 50)];
        //self.contentViewBottomConstraint.constant = contentSize;
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    } @finally {
        
    }
}

-(CGFloat)yOfLastChildFromScrollView
{
    @try {
        UIView *subView = [[self.scroll_view.subviews[0] subviews] lastObject];
        
        CGFloat yOfLastObject = 0;
        
        if (subView) {
            //yOfLastObject = subView.frame.size.width + subView.frame.origin.y;
        }
        return yOfLastObject;
    } @catch (NSException *exception) {
    } @finally {
    }
    return 0.0;
}

-(void)addConstraingOnScrollChilds:(UIView *)parent withFixedHeight:(CGFloat)fixedheight withTop:(CGFloat)topY completed:(ConstraintCompletionBlock)completedBlock
{
    @try {
        NSArray *childs = [self.scroll_view.subviews[0] subviews];
        UIView *subView = [childs lastObject];
        UIView *secondLastSubView = nil;
        if (childs.count > 1) {
            secondLastSubView = childs[childs.count-2];
        }
        
        
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Trailing
        NSLayoutConstraint *trailing =[NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Leading
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Bottom
        NSLayoutConstraint *top =[NSLayoutConstraint
                                  constraintWithItem:subView
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:parent
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:topY];
        //Height to be fixed for SubView same as AdHeight
        NSLayoutConstraint *height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeHeight
                                      multiplier:0
                                      constant:fixedheight];
        
        //Add constraints to the Parent
        [parent addConstraint:trailing];
        if (secondLastSubView) {
            top =[NSLayoutConstraint
                  constraintWithItem:subView
                  attribute:NSLayoutAttributeTop
                  relatedBy:NSLayoutRelationEqual
                  toItem:secondLastSubView
                  attribute:NSLayoutAttributeBottom
                  multiplier:1
                  constant:topY];
            [parent addConstraint:top];
        } else {
            [parent addConstraint:top];
        }
        [parent addConstraint:leading];
        
        //Add height constraint to the subview, as subview owns it.
        [subView addConstraint:height];
        
        completedBlock(height);
    } @catch (NSException *exception) {
    } @finally {
    }
    
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.previouslyFocusedView == self.btn_ar) {
        self.view_ar.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    
    if (context.nextFocusedView == self.btn_ar) {
        self.view_ar.transform = CGAffineTransformMakeScale(1.1, 1.1);
    }
}

-(NSArray *)preferredFocusEnvironments
{
    return [super preferredFocusEnvironments];
}

-(void)loadProgramFromServer
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/top_search_module/most_searched_all_shows?user_id=%@&key=%@&page=%d&limit=18&country_code=%@&is_all_countries=yes",BaseURL,BaseUID,BaseKEY,self.mostSearchPageNumber,[StaticData shared].myCountryCode];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"ContentList"];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGFloat devideCount = 4.8;
    CGRect frame = collectionView.frame;
    CGFloat width = (frame.size.width / devideCount) - 5;
    
    CGFloat height = width;
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = (ratio * width) + 40;
    }
    
    return CGSizeMake(width, height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.mostSearchList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    MediaInfo *info = self.mostSearchList[indexPath.row];
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:info.isRadio ? @"cell_radio" : @"cell_radio" forIndexPath:indexPath];
    
    cell.lbl_titleMarquee.text = info.title;
    [StaticData isExclusive:info.exclusive exclusiveImg:cell.img_exclusive];
    
    @try {
        NSString *encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        
        if ([imagePath containsString:@".jpeg"]) {
            cell.thumbnail.image = [UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"];
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imagePath]];
                if ( data == nil ) {
                    [cell.indicator stopAnimating];
                    return;
                }
                    
                dispatch_async(dispatch_get_main_queue(), ^{
                    // WARNING: is the cell still using the same data by this point??
                    [cell.indicator stopAnimating];
                    cell.thumbnail.image = [UIImage imageWithData: data];
                });
            });
        }
        else {
            [cell.indicator startAnimating];
            cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:info.isRadio ? @"default_logo_audio" : @"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                     if (!self.tempImage) {
                         self.tempImage = image;
                     }
                     [cell.indicator stopAnimating];
                 } else {
                     [cell.indicator stopAnimating];
                 }
             }];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.mostSearchList.count == 0) return;
    
    MediaInfo *info = self.mostSearchList[indexPath.row];
    if (info.isRadio) {
        [StaticData openRadioShowDetailsPageWith:self withMediaInfo:info];
    } else {
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        if(self.FocuseUpdateCallBack) {
            self.FocuseUpdateCallBack();
        }
    }
    [StaticData UpdateHorizontalCarouselFocusInContext:context withCollectionView:collectionView];
}


@end
