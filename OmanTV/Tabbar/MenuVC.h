//
//  MenuVC.h
//  OmanTV
//
//  Created by Curiologix on 25/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MenuVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) UIViewController *parentVC;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic ,weak) IBOutlet UIImageView *img_bg;
@property (nonatomic ,weak) IBOutlet UIImageView *img_dropDownBG;
@property (nonatomic ,weak) IBOutlet UIImageView *img_dropDownarrow;
@property (nonatomic ,weak) IBOutlet UIImageView *img_searchBG;
@property (nonatomic ,weak) IBOutlet UIButton *btn_opeDropDown;
@property (nonatomic ,weak) IBOutlet UIButton *btn_opeSearch;
@end

NS_ASSUME_NONNULL_END
