//
//  MenuVC.m
//  OmanTV
//
//  Created by Curiologix on 25/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "MenuVC.h"
#import "ShowsVC.h"
#import "CatchupVC.h"
#import "LiveChannelPlayer.h"
#import "CategoriesVC.h"
#import "LiveChannelsVC.h"
#import "SearchResultsVC.h"
@interface MenuVC ()

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentList = [NSMutableArray new];
    
    self.img_bg.hidden = true;
    self.view.backgroundColor = [UIColor clearColor];
    [StaticData scaleToArabic:self.collectionView];
    
    MediaInfo *info = [MediaInfo new];
    info.title = @"الرئيسية";
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البرامج";
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البث المباشر";
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"لا يفوتك";
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"الفئات";
    [self.contentList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"القنوات";
    [self.contentList addObject:info];
    
    [self setupTriggerActionOnButtons];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupParentChildsInteraction:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self hideMenu];
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_opeDropDown,self.btn_opeSearch];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_opeDropDown)
        {
            if (self.btn_opeDropDown.tag == 0)
            {
                self.img_bg.hidden = false;
                self.img_dropDownarrow.image = [UIImage imageNamed:@"up_arrow"];
                self.btn_opeDropDown.tag = 1;
                [UIView animateWithDuration:0.5 animations:^{
                    CGRect frame = self.view.frame;
                    frame.origin.y = 0;
                    self.view.frame = frame;
                }completion:^(BOOL finished) {
                    self.view.backgroundColor = [UIColor clearColor];
                    [self setupParentChildsInteraction:false];
                }];
            } else
            {
                [self hideMenu];
            }
        } else if (sender == self.btn_opeSearch)
        {
            SearchResultsVC *vc = [[StaticData getStoryboard:7] instantiateViewControllerWithIdentifier:@"SearchResultsVC"];
            [self.navigationController pushViewController:vc animated:true];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)hideMenu
{
    self.img_dropDownarrow.image = [UIImage imageNamed:@"down_arrow"];
    self.btn_opeDropDown.tag = 0;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -(frame.size.height - 150);
        self.view.frame = frame;
    }completion:^(BOOL finished) {
        self.img_bg.hidden = true;
        self.view.backgroundColor = [UIColor clearColor];
        [self setupParentChildsInteraction:true];
    }];
}

-(void)setupParentChildsInteraction:(BOOL)isEnabel
{
    NSArray *subViews = self.parentVC.view.subviews;
    for (id child in subViews)
    {
        if (child != self.view)
        {
            [child setUserInteractionEnabled:isEnabel];
        }
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    CGFloat width = frame.size.width / self.contentList.count;
    CGFloat height = frame.size.height;
    return CGSizeMake(width, height);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (self.contentList.count == 0) {
        [cell.indicator startAnimating];
        cell.view_info.hidden = true;
        return cell;
    }
    
    MediaInfo *info = self.contentList[indexPath.row];
    
    cell.lbl_title.text = info.title;
    
    cell.layer.cornerRadius = 12.0;
    cell.clipsToBounds = true;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MediaInfo *info = self.contentList[indexPath.row];
    if ([info.title isEqualToString:@"الرئيسية"])
    {
        [StaticData setRootViewController];
    } else if ([info.title isEqualToString:@"برامج"])
    {
        ShowsVC *vc = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ShowsVC"];
        [self.navigationController pushViewController:vc animated:true];
    } else if ([info.title isEqualToString:@"لا يفوتك"])
    {
        CatchupVC *vc = [[StaticData getStoryboard:2] instantiateViewControllerWithIdentifier:@"CatchupVC"];
        [self.navigationController pushViewController:vc animated:true];
    } else if ([info.title isEqualToString:@"البث المباشر"])
    {
        LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
        [self.navigationController pushViewController:vc animated:true];
    } else if ([info.title isEqualToString:@"الفئات"])
    {
        CategoriesVC *vc = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CategoriesVC"];
        [self.navigationController pushViewController:vc animated:true];
    } else if ([info.title isEqualToString:@"القنوات"])
    {
        LiveChannelsVC *vc = [[StaticData getStoryboard:6] instantiateViewControllerWithIdentifier:@"LiveChannelsVC"];
        [self.navigationController pushViewController:vc animated:true];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            //cell.thumbnail.transform = CGAffineTransformMakeScale(1.0, 1.0);
            cell.backgroundColor = [UIColor clearColor];
            cell.lbl_title.textColor = [UIColor whiteColor];
        }];
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [UIView animateWithDuration:0.1 animations:^{
            //cell.thumbnail.transform = CGAffineTransformMakeScale(1.1, 1.1);
            cell.backgroundColor = [UIColor whiteColor];
            cell.lbl_title.textColor = AppColor;
            cell.layer.cornerRadius = 12.0;
            cell.clipsToBounds = true;
        }];
    }
    //[StaticData updateFocusInContext:context withAnimationCoordinator:coordinator withCollectionView:self.collectionView];
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_opeDropDown)
        {
            self.img_dropDownBG.image = [UIImage imageNamed:@"menu_option_bg_selected"];
        } else if (sender == self.btn_opeSearch) {
            self.img_searchBG.image = [UIImage imageNamed:@"search_field_bg_selected"];
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
        if (sender == self.btn_opeDropDown)
        {
            self.img_dropDownBG.image = [UIImage imageNamed:@"menu_option_bg"];
        } else if (sender == self.btn_opeSearch) {
            self.img_searchBG.image = [UIImage imageNamed:@"search_field_bg"];
        }
    }
}


@end
