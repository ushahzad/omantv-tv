//
//  RightSideMenuVC.h
//  OmanTV
//
//  Created by Curiologix on 10/04/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RightSideMenuVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property void (^ChooseChannelTypeCallBack)(BOOL isRadio,MediaInfo *info);
@property (nonatomic, retain) UIViewController *parentVC;
@property (nonatomic,weak) IBOutlet UICollectionView *cv_tabbar;
@property (nonatomic,weak) IBOutlet UICollectionView *cv_menu;
@property (nonatomic ,weak) IBOutlet UIImageView *img_searchBG;
@property (nonatomic ,weak) IBOutlet UIButton *btn_opeDropDown;
@property (nonatomic ,weak) IBOutlet UIButton *btn_opeSearch;
@property (nonatomic ,weak) IBOutlet UIButton *btn_cross;
@property (nonatomic ,weak) IBOutlet UIView *view_sideMenu;
@property (nonatomic ,weak) IBOutlet UIView *view_cross;
@property (nonatomic ,weak) IBOutlet UILabel *lbl_cross;
@property (nonatomic ,weak) UIViewController *vc_prev;

@property (nonatomic, weak) IBOutlet UIView *view_channelsTypes;
@property (nonatomic ,retain) IBOutlet NSLayoutConstraint *collectionWidthConstraing;
@property (nonatomic ,retain) NSLayoutConstraint *parentHeightConstraint;

@property (nonatomic,retain) NSMutableArray *tabsList;
@property (nonatomic,retain) NSMutableArray *menuList;

@property (nonatomic,retain) NSString *pageType;

@property (nonatomic,assign) BOOL isMenuOpen;
-(CollectionCell *)findSelectedCell;

@end

NS_ASSUME_NONNULL_END
