//
//  TabbarController.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 06/11/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "RightSideMenuVC.h"
#import "SearchResultsVC.h"
#import "ShowsVC.h"
#import "CatchupVC.h"
#import "LiveChannelPlayer.h"
#import "CategoriesVC.h"
#import "LiveChannelsVC.h"
#import "SearchResultsVC.h"
#import "SchedulesVC.h"
#import "LoginVC.h"
#import "MyListVC.h"
#import "RightSideMenuVC.h"
#import "QRLoginVC.h"


@interface RightSideMenuVC ()

@end

@implementation RightSideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabsList = [NSMutableArray new];
    self.menuList = [NSMutableArray new];
    
    [self isCrossFocused:false];
    self.view_cross.layer.cornerRadius = 30.0;
    self.view_cross.clipsToBounds = true;
    
    self.view_sideMenu.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
    
    [self setupTabbarItems];
    [self setupMenuItems];
    
    self.cv_tabbar.tag = 101;
    self.cv_menu.tag = 101;
     
    [StaticData scaleToArabic:self.cv_tabbar];
    [StaticData scaleToArabic:self.cv_menu];
    
    self.cv_tabbar.remembersLastFocusedIndexPath = true;
    
    [self setupChannelTypeController];
    
    [self setupTriggerActionOnButtons];
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_cross];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_cross)
        {
            [self dismissViewControllerAnimated:false completion:nil];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, 80);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.menuList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.menuList[indexPath.row];
    cell.lbl_title.text = info.title;
    cell.lbl_subTitle.text = info.title;
    
    cell.view_info.backgroundColor = [UIColor whiteColor];
    
    if ([info.itemType isEqualToString:self.pageType]) {
        cell.view_info.hidden = false;
        cell.view_info.backgroundColor = LightWidthColor
    } else {
        cell.view_info.hidden = true;
    }
    
    cell.thumbnail.image = [StaticData changeImageColor:[UIImage imageNamed:info.thumbnail] withColor:[UIColor whiteColor]];
    
    cell.view_info.layer.cornerRadius = 12.0;
    cell.view_info.clipsToBounds = true;

    cell.view_border.hidden = true;
    if ([info.itemType isEqualToString:@"live"])
    {
        cell.view_border.hidden = false;
    }
    
    [StaticData scaleToArabic:cell.contentView];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:false completion:^{
        MediaInfo *info = self.menuList[indexPath.row];
        if ([info.itemType isEqualToString:@"home"])
        {
            [StaticData setRootViewController];
        } else if ([info.itemType isEqualToString:@"programs"])
        {
            ShowsVC *vc = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ShowsVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"catchup"])
        {
            CatchupVC *vc = [[StaticData getStoryboard:2] instantiateViewControllerWithIdentifier:@"CatchupVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"categories"])
        {
            CategoriesVC *vc = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CategoriesVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"channels"])
        {
            LiveChannelsVC *vc = [[StaticData getStoryboard:6] instantiateViewControllerWithIdentifier:@"LiveChannelsVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"live"])
        {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"login"])
        {
            if ([StaticData isUserLogins])
            {
                MyListVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"MyListVC"];
                [self.parentVC.navigationController pushViewController:vc animated:true];
            } else
            {
                QRLoginVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"QRLoginVC"];
                [self.parentVC.navigationController pushViewController:vc animated:true];
            }
        } else if ([info.itemType isEqualToString:@"search"])
        {
            [[StaticData shared].radioPlayer.player pause];
            SearchResultsVC *vc = [[StaticData getStoryboard:7] instantiateViewControllerWithIdentifier:@"SearchResultsVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"schedule"])
        {
            SchedulesVC *vc = [[StaticData getStoryboard:8] instantiateViewControllerWithIdentifier:@"SchedulesVC"];
            [self.parentVC.navigationController pushViewController:vc animated:true];
        }
    }];
}

-(void)shpwHideMenu
{
    self.parentHeightConstraint.constant = self.isMenuOpen ? 150.0 : 265.0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.parentViewController.view layoutIfNeeded];
    }];
    self.isMenuOpen = !self.isMenuOpen;
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_tabbar)
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:[UIColor whiteColor]];
            
            MediaInfo *info = self.tabsList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:AppColor];
            }
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:AppColor];
        }
    } else if (collectionView == self.cv_menu)
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            cell.lbl_title.textColor = [UIColor whiteColor];
            
            MediaInfo *info = self.menuList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.view_info.backgroundColor = LightWidthColor;
            }
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.lbl_title.textColor = AppColor;
            
            cell.view_info.backgroundColor = [UIColor whiteColor];
        }
    }
    
    if (context.nextFocusedView.superview.tag != 101)
    {
        if (self.isMenuOpen)
        {
            [self shpwHideMenu];
        }
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_cross)
        {
            [self isCrossFocused:true];
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
         if (sender == self.btn_cross) {
             [self isCrossFocused:false];
        }
    }
}

-(void)setupTabbarItems
{
    MediaInfo *info = [MediaInfo new];
    info.itemType = @"menu";
    info.thumbnail = @"tabbar_icon_menu";
    [self.tabsList addObject:info];
    
//    info = [MediaInfo new];
//    info.itemType = @"home";
//    info.thumbnail = @"tabbar_icon_home";
//    [self.tabsList addObject:info];
//
//    info = [MediaInfo new];
//    info.itemType = @"schedule";
//    info.thumbnail = @"tabbar_icon_schedule";
//    [self.tabsList addObject:info];
//
//    info = [MediaInfo new];
//    info.itemType = @"profile";
//    info.thumbnail = @"tabbar_icon_profile";
//    [self.tabsList addObject:info];
    
    BOOL isSelectedFound = false;
    for (MediaInfo *info in self.tabsList)
    {
        if ([info.itemType isEqualToString:self.pageType])
        {
            isSelectedFound = true;
            info.isSelected = true;
        }
    }
    
    if (!isSelectedFound) {
        MediaInfo *info = self.tabsList[0];
        info.isSelected = true;
    }
}

-(void)setupMenuItems
{
    MediaInfo *info = [MediaInfo new];
    info.title = @"الرئيسية";
    info.itemType = @"home";
    info.thumbnail = @"menu_home";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البرامج";
    info.itemType = @"programs";
    info.thumbnail = @"menu_program";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"القنوات";
    info.itemType = @"channels";
    info.thumbnail = @"menu_channel";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"الفئات";
    info.itemType = @"categories";
    info.thumbnail = @"menu_categories";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"لا يفوتك";
    info.itemType = @"catchup";
    info.thumbnail = @"menu_schedule";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"جدول البرامج";
    info.itemType = @"schedule";
    info.thumbnail = @"menu_schedule";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البث المباشر";
    info.itemType = @"live";
    info.thumbnail = @"menu_live_broadcast";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"login";
    info.title = @"حسابي";
    info.thumbnail = @"menu_user_login";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.itemType = @"search";
    info.title = @"بحث";
    info.thumbnail = @"menu_search";
    [self.menuList addObject:info];
    
//    info = [MediaInfo new];
//    info.title = @"اللغات";
//    info.itemType = @"langauge";
//    info.thumbnail = @"tabbar_icon_menu";
//    [self.menuList addObject:info];
//
//    info = [MediaInfo new];
//    info.title = @"إعدادات";
//    info.itemType = @"settings";
//    info.thumbnail = @"tabbar_icon_menu";
//    [self.menuList addObject:info];
    
    for (MediaInfo *info in self.menuList)
    {
        if ([info.itemType isEqualToString:self.pageType])
        {
            info.isSelected = true;
        }
    }
}

-(void)setupChannelTypeController
{
    /*if ([self.pageType isEqualToString:@"schedule"])
    {
        [self setupChannelsTypesController:@"مرئي مباشر" withRadioLiveTitle:@"سمعي مباشر"];
    } else*/
    if ([self.pageType isEqualToString:@"catchup"])
    {
        [self setupChannelsTypesController:@"" withRadioLiveTitle:@""];
    } else
    {
        self.collectionWidthConstraing.constant = 1100.0;
    }
}

-(void)setupChannelsTypesController:(NSString *)liveTvTitle withRadioLiveTitle:(NSString *)liveRadioTitle
{
    ChannelsTypeVC *vc_channelsTypeVC = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    vc_channelsTypeVC.liveTvTitle = liveTvTitle;
    vc_channelsTypeVC.liveRadioTitle = liveRadioTitle;
    CGRect frame = self.view.frame;
    CGFloat height = 70;
    
    [self addChildViewController:vc_channelsTypeVC];
    
    [vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];
    
    [self.view_channelsTypes addSubview:vc_channelsTypeVC.view];
    
    [vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:vc_channelsTypeVC.view];
    
    vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info)
    {
        if (weakSelf.ChooseChannelTypeCallBack)
        {
            weakSelf.ChooseChannelTypeCallBack(isRadio,info);
        }
    };
}

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.tabsList.count > 0) {
            for (int i = 0; i < self.tabsList.count; i++)
            {
                MediaInfo *channel = self.tabsList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.cv_tabbar cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.cv_tabbar scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.cv_tabbar cellForItemAtIndexPath:indexPath];
            [self.cv_tabbar scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}

-(void)isCrossFocused:(BOOL)isFocused
{
    if (isFocused)
    {
        self.view_cross.backgroundColor = AppColor;
        self.lbl_cross.textColor = [UIColor whiteColor];
    } else
    {
        self.view_cross.backgroundColor = [UIColor whiteColor];
        self.lbl_cross.textColor = AppColor;
    }
}

@end
