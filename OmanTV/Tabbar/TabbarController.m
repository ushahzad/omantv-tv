//
//  TabbarController.m
//  AwaanAppleTV-New
//
//  Created by MacUser on 06/11/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "TabbarController.h"
#import "SearchResultsVC.h"
#import "ShowsVC.h"
#import "CatchupVC.h"
#import "LiveChannelPlayer.h"
#import "CategoriesVC.h"
#import "LiveChannelsVC.h"
#import "SearchResultsVC.h"
#import "SchedulesVC.h"
#import "LoginVC.h"
#import "MyListVC.h"
#import "RightSideMenuVC.h"


@interface TabbarController ()

@end

@implementation TabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabsList = [NSMutableArray new];
    self.menuList = [NSMutableArray new];
    
    
    self.cv_menu.hidden = true;
    
    [self setupTabbarItems];
    [self setupMenuItems];
    
    self.cv_tabbar.tag = 101;
    self.cv_menu.tag = 101;
     
    [StaticData scaleToArabic:self.cv_tabbar];
    [StaticData scaleToArabic:self.cv_menu];
    
    self.cv_tabbar.remembersLastFocusedIndexPath = true;
    
    [self setupTriggerActionOnButtons];
    
    
    [self setupChannelTypeController];
    
    if ([StaticData isUserLogins]) {
        
//        [StaticData shared].userInfo.firstName = @"عمر";
        NSCharacterSet* tSet = [NSCharacterSet characterSetWithCharactersInString:
                                @"abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        NSCharacterSet* invSet = [tSet invertedSet];
        NSString* legalS = [StaticData shared].userInfo.firstName;
        if ([legalS rangeOfCharacterFromSet:invSet].location != NSNotFound) {
            self.lbl_username.text = [NSString stringWithFormat:@"أهلا %@",[StaticData shared].userInfo.firstName];
        }
        else {
            self.lbl_username.text = [NSString stringWithFormat:@"%@ أهلا",[StaticData shared].userInfo.firstName];
        }
        
    } else {
        self.lbl_username.text = @"";
    }
}

-(void)setupTriggerActionOnButtons
{
//    NSArray *buttonsArray = @[self.btn_opeSearch];
//    for (UIButton *sender in buttonsArray)
//    {
//        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
//    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_opeSearch)
        {
            SearchResultsVC *vc = [[StaticData getStoryboard:7] instantiateViewControllerWithIdentifier:@"SearchResultsVC"];
            [self.navigationController pushViewController:vc animated:true];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_menu)
    {
        MediaInfo *info = self.menuList[indexPath.row];
        CGFloat width = (collectionView.frame.size.width - 300) / (self.menuList.count-1);
        return CGSizeMake(info.isSelected ? 300 : width, collectionView.frame.size.height);
    }
    return CGSizeMake(86, 52);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.cv_menu) {
        return self.menuList.count;
    }
    return self.tabsList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == self.cv_menu)
    {
        return [self menuCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    }
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.tabsList[indexPath.row];
    cell.thumbnail.image = [UIImage imageNamed:info.thumbnail];
    
    if ([info.itemType isEqualToString:self.pageType]) {
        cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:AppColor];
    } else {
        cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:[UIColor whiteColor]];
    }
    
    [StaticData scaleToArabic:cell.contentView];
    return cell;
}

-(UICollectionViewCell *)menuCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    MediaInfo *info = self.menuList[indexPath.row];
    cell.lbl_title.text = info.title;
    cell.lbl_subTitle.text = info.title;
    
    cell.view_info.backgroundColor = [UIColor whiteColor];
    
    if ([info.itemType isEqualToString:self.pageType]) {
        cell.view_info.hidden = false;
        cell.view_info.backgroundColor = LightWidthColor
    } else {
        cell.view_info.hidden = true;
    }
    
    cell.thumbnail.image = [StaticData changeImageColor:[UIImage imageNamed:info.thumbnail] withColor:AppColor];
    
    cell.view_info.layer.cornerRadius = 12.0;
    cell.view_info.clipsToBounds = true;
    
    [StaticData scaleToArabic:cell.contentView];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_menu)
    {
        MediaInfo *info = self.menuList[indexPath.row];
        if ([info.itemType isEqualToString:@"home"])
        {
            [StaticData setRootViewController];
        } else if ([info.itemType isEqualToString:@"programs"])
        {
            ShowsVC *vc = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ShowsVC"];
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"catchup"])
        {
            CatchupVC *vc = [[StaticData getStoryboard:2] instantiateViewControllerWithIdentifier:@"CatchupVC"];
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"categories"])
        {
            CategoriesVC *vc = [[StaticData getStoryboard:5] instantiateViewControllerWithIdentifier:@"CategoriesVC"];
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"channels"])
        {
            LiveChannelsVC *vc = [[StaticData getStoryboard:6] instantiateViewControllerWithIdentifier:@"LiveChannelsVC"];
            [self.navigationController pushViewController:vc animated:true];
        } else if ([info.itemType isEqualToString:@"live"])
        {
            LiveChannelPlayer *vc = [[StaticData getStoryboard:4] instantiateViewControllerWithIdentifier:@"LiveChannelPlayer"];
            [self.navigationController pushViewController:vc animated:true];
        }
    } else
    {
        
        RightSideMenuVC *vc = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"RightSideMenuVC"];
        vc.parentVC = self.parentViewController;
        vc.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:vc animated:false completion:nil];
        
//        MediaInfo *info = self.tabsList[indexPath.row];
//        if ([info.itemType isEqualToString:@"home"])
//        {
//            [StaticData setRootViewController];
//        } else if ([info.itemType isEqualToString:@"menu"])
//        {
//            [self shpwHideMenu];
//        } else if ([info.itemType isEqualToString:@"schedule"])
//        {
//            SchedulesVC *vc = [[StaticData getStoryboard:8] instantiateViewControllerWithIdentifier:@"SchedulesVC"];
//            [self.navigationController pushViewController:vc animated:true];
//        } else if ([info.itemType isEqualToString:@"profile"])
//        {
//            if ([StaticData isUserLogins])
//            {
//                MyListVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"MyListVC"];
//                [self.navigationController pushViewController:vc animated:true];
//            } else
//            {
//                LoginVC *vc = [[StaticData getStoryboard:9] instantiateViewControllerWithIdentifier:@"LoginVC"];
//                [self.navigationController pushViewController:vc animated:true];
//            }
//        }
    }
}

-(void)shpwHideMenu
{
    self.cv_menu.hidden = false;
    self.parentHeightConstraint.constant = self.isMenuOpen ? 150.0 : 265.0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.parentViewController.view layoutIfNeeded];
    }];
    self.isMenuOpen = !self.isMenuOpen;
    
    if (!self.isMenuOpen) {
        self.cv_menu.hidden = true;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_tabbar)
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:[UIColor whiteColor]];
            
            //MediaInfo *info = self.tabsList[context.previouslyFocusedIndexPath.row];
            //if (info.isSelected) {
                cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:[UIColor whiteColor]];
            //}
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.thumbnail.image = [StaticData changeImageColor:cell.thumbnail.image withColor:AppColor];
        }
    } else if (collectionView == self.cv_menu)
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
            cell.lbl_title.textColor = [UIColor whiteColor];
            
            MediaInfo *info = self.menuList[context.previouslyFocusedIndexPath.row];
            if (info.isSelected) {
                cell.view_info.backgroundColor = LightWidthColor;
            }
        }
        
        if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
        {
            CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
            cell.lbl_title.textColor = AppColor;
            
            cell.view_info.backgroundColor = [UIColor whiteColor];
        }
    }
    
    if (context.nextFocusedView.superview.tag != 101)
    {
        if (self.isMenuOpen)
        {
            [self shpwHideMenu];
        }
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.nextFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.nextFocusedView;
        if (sender == self.btn_opeSearch) {
            self.img_searchBG.image = [UIImage imageNamed:@"search_field_bg_selected"];
        }
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]]) {
        UIButton *sender = (UIButton *)context.previouslyFocusedView;
         if (sender == self.btn_opeSearch) {
            self.img_searchBG.image = [UIImage imageNamed:@"search_field_bg"];
        }
    }
}

-(void)setupTabbarItems
{
    MediaInfo *info = [MediaInfo new];
    info.itemType = @"menu";
    info.thumbnail = @"tabbar_icon_menu";
    [self.tabsList addObject:info];
    
//    info = [MediaInfo new];
//    info.itemType = @"home";
//    info.thumbnail = @"tabbar_icon_home";
//    [self.tabsList addObject:info];
//
//    info = [MediaInfo new];
//    info.itemType = @"schedule";
//    info.thumbnail = @"tabbar_icon_schedule";
//    [self.tabsList addObject:info];
//
//    info = [MediaInfo new];
//    info.itemType = @"profile";
//    info.thumbnail = @"tabbar_icon_profile";
//    [self.tabsList addObject:info];
    
    BOOL isSelectedFound = false;
    for (MediaInfo *info in self.tabsList)
    {
        if ([info.itemType isEqualToString:self.pageType])
        {
            isSelectedFound = true;
            info.isSelected = true;
        }
    }
    
    if (!isSelectedFound) {
        MediaInfo *info = self.tabsList[0];
        info.isSelected = true;
    }
}

-(void)setupMenuItems
{
    MediaInfo *info = [MediaInfo new];
    info.title = @"الرئيسية";
    info.itemType = @"home";
    info.thumbnail = @"tabbar_icon_home";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البرامج";
    info.itemType = @"programs";
    info.thumbnail = @"menu_icon_programs";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"القنوات";
    info.itemType = @"channels";
    info.thumbnail = @"menu_icon_channels";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"الفئات";
    info.itemType = @"categories";
    info.thumbnail = @"menu_icon_categories";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"لا يفوتك";
    info.itemType = @"catchup";
    info.thumbnail = @"menu_icon_programs";
    [self.menuList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"البث المباشر";
    info.itemType = @"live";
    info.thumbnail = @"menu_icon_channels";
    [self.menuList addObject:info];
    
//    info = [MediaInfo new];
//    info.title = @"اللغات";
//    info.itemType = @"langauge";
//    info.thumbnail = @"tabbar_icon_menu";
//    [self.menuList addObject:info];
//
//    info = [MediaInfo new];
//    info.title = @"إعدادات";
//    info.itemType = @"settings";
//    info.thumbnail = @"tabbar_icon_menu";
//    [self.menuList addObject:info];
    
    for (MediaInfo *info in self.menuList)
    {
        if ([info.itemType isEqualToString:self.pageType])
        {
            info.isSelected = true;
        }
    }
}

-(void)setupChannelTypeController
{
    /*if ([self.pageType isEqualToString:@"schedule"])
    {
        [self setupChannelsTypesController:@"مرئي مباشر" withRadioLiveTitle:@"سمعي مباشر"];
    } else*/
    if ([self.pageType isEqualToString:@"programs"])
    {
        [self setupChannelsTypesController:@"" withRadioLiveTitle:@""];
    }
//    else if ([self.pageType isEqualToString:@"catchup"])
//    {
//        [self setupChannelsTypesController:@"" withRadioLiveTitle:@""];
//    }
    else if ([self.pageType isEqualToString:@"categories"])
    {
        [self setupChannelsTypesController:@"" withRadioLiveTitle:@""];
    } else
    {
        self.collectionWidthConstraing.constant = 1100.0;
    }
}

-(void)setupChannelsTypesController:(NSString *)liveTvTitle withRadioLiveTitle:(NSString *)liveRadioTitle
{
    ChannelsTypeVC *vc_channelsTypeVC = [[StaticData getStoryboard:1] instantiateViewControllerWithIdentifier:@"ChannelsTypeVC"];
    vc_channelsTypeVC.liveTvTitle = liveTvTitle;
    vc_channelsTypeVC.liveRadioTitle = liveRadioTitle;
    CGRect frame = self.view.frame;
    CGFloat height = 70;
    
    [self addChildViewController:vc_channelsTypeVC];
    
    [vc_channelsTypeVC.view setFrame:CGRectMake(0, 0, frame.size.width, height)];

    [self.view_channelsTypes addSubview:vc_channelsTypeVC.view];
    
    [vc_channelsTypeVC didMoveToParentViewController:self];
        
    __weak typeof(self) weakSelf = self;
    [StaticData addConstraingWithParent:self.view_channelsTypes onChild:vc_channelsTypeVC.view];
    
    vc_channelsTypeVC.ChoosedChannelTypeCallBack = ^(BOOL isRadio,MediaInfo *info)
    {
        if (weakSelf.ChooseChannelTypeCallBack)
        {
            weakSelf.ChooseChannelTypeCallBack(isRadio,info);
        }
    };
}

-(CollectionCell *)findSelectedCell
{
    @try {
        if (self.tabsList.count > 0) {
            for (int i = 0; i < self.tabsList.count; i++)
            {
                MediaInfo *channel = self.tabsList[i];
                if (channel.isSelected)
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                    if (indexPath) {
                        CollectionCell *cell = (CollectionCell *)[self.cv_tabbar cellForItemAtIndexPath:indexPath];
                       
                        if (cell) {
                             [self.cv_tabbar scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                            return cell;
                        }
                    } else {
                        return nil;
                    }
                }
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if (indexPath) {
            CollectionCell *cell = (CollectionCell *)[self.cv_tabbar cellForItemAtIndexPath:indexPath];
            [self.cv_tabbar scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
            return cell;
        }
        return nil;
    } @catch (NSException *exception) {
        return nil;
    } @finally {
    }
    return nil;
}
@end
