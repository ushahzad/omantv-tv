//
//  FeaturedShowsHomeInfo.m
//  AwaanAppleTV-New
//
//  Created by Curiologix on 13/09/2019.
//  Copyright © 2019 dotcom. All rights reserved.
//

#import "FeaturedShowsHomeInfo.h"

@interface FeaturedShowsHomeInfo ()

@end

@implementation FeaturedShowsHomeInfo

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.lbl_title.text = info.title;
    //self.lbl_subTitle.text = info.subTitle;
    
    self.view_left_arrow.layer.cornerRadius = 35;
    self.view_left_arrow.clipsToBounds = true;
    
    self.view_right_arrow.layer.cornerRadius = 35;
    self.view_right_arrow.clipsToBounds = true;
    
    self.view_play.layer.cornerRadius = 12.0;
    self.view_play.clipsToBounds = true;
    [self.view_play setBackgroundColor:[StaticData colorFromHexString:@"#808080"]];
    
    [self isRightArrowFoucuse:false];
    [self isLeftArrowFoucuse:false];
    
    [self setupTriggerActionOnButtons];
}

-(void)viewDidAppear:(BOOL)animated
{
    UISwipeGestureRecognizer *upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(takeSwipeGestureToTabbar:)];
    upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:upSwipe];
}

-(void)setupTriggerActionOnButtons
{
    NSArray *buttonsArray = @[self.btn_play];
    for (UIButton *sender in buttonsArray)
    {
        [sender addTarget:self action:@selector(buttonActionTrigger:) forControlEvents:UIControlEventPrimaryActionTriggered];
    }
}

-(void)buttonActionTrigger:(UIButton *)sender
{
    @try {
        if (sender == self.btn_play)
        {
            if (self.selectedShow) {
                [StaticData openShowDetailsPageWith:self withMediaInfo:self.selectedShow];
            }
        } else if (sender == self.btn_left_arrow)
        {
        } else if (sender == self.btn_right_arrow)
        {
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)setupShowDetails:(MediaInfo *)show
{
    @try {
        
        self.lbl_title.text = show.title;
        self.lbl_subTitle.text = show.subTitle;
        self.lbl_subTitle.adjustsFontSizeToFitWidth = true;
        
        self.view_liveChannelShowDetails.hidden = false;
        
        self.isPlayerPause = false;
        
        self.selectedShow = show;

        NSString *encodedCover = [show.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        self.img_channelPreview.contentMode = UIViewContentModeScaleAspectFill;
        //cell.thumbnail.backgroundColor = SelectedColor;
        [self.indicator startAnimating];
        
        // try to load cache image
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:imagePath done:^(UIImage *image, SDImageCacheType cacheType)
        {
            if (image)
            {
                [self.indicator stopAnimating];
                [self makeImagesTransition:image];
            } else
            {
                [self downloadImageWithImagePath:imagePath];
            }
        }];
        
    } @catch (NSException *exception) {
    } @finally {
    }
}


-(void)downloadImageWithImagePath:(NSString *)imagePath
{
    @try {
        self.activeImagePath = imagePath;
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imagePath] options:SDWebImageHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image)
            {
                if ([self.activeImagePath isEqualToString:imageURL.absoluteString])
                {
                    [self makeImagesTransition:image];
                }
            } else {
                [StaticData downloadImageIfNotLoadinedBySdImage:self.img_channelPreview withUrl:imageURL completed:^(UIImage *image)
                {
                    if ([self.activeImagePath isEqualToString:imageURL.absoluteString])
                    {
                        [self makeImagesTransition:image];
                    }
                }];
            }
            [self.indicator stopAnimating];
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)makeImagesTransition:(UIImage *)newImage
{
    [UIView transitionWithView:self.img_channelPreview
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                      self.img_channelPreview.image = newImage;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    
    if (context.previouslyFocusedView == self.btn_play)
    {
        self.view_play.backgroundColor = [StaticData colorFromHexString:@"#808080"];
    }
    
    if (context.nextFocusedView == self.btn_play)
    {
        self.view_play.backgroundColor = AppColor;
    }
    
    if (context.previouslyFocusedView == self.btn_right_arrow)
    {
        [self isRightArrowFoucuse:false];
    }
    
    if (context.nextFocusedView == self.btn_right_arrow)
    {
        [self isLeftArrowFoucuse:false];
        [self isRightArrowFoucuse:true];
    }
    
    if (context.previouslyFocusedView == self.btn_left_arrow)
    {
        [self isLeftArrowFoucuse:false];
    }
    
    if (context.nextFocusedView == self.btn_left_arrow)
    {
        [self isRightArrowFoucuse:false];
        [self isLeftArrowFoucuse:true];
    }
    
}

-(void)isRightArrowFoucuse:(BOOL)isTrue
{
    
    if (isTrue)
    {
        self.view_right_arrow.backgroundColor = AppColor;
        self.img_right_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"right_arrow"] withColor:[UIColor whiteColor]];
    } else
    {
        self.view_right_arrow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
        self.img_right_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"right_arrow"] withColor:[UIColor whiteColor]];
    }
}

-(void)isLeftArrowFoucuse:(BOOL)isTrue
{
    
    if (isTrue)
    {
        self.view_left_arrow.backgroundColor = AppColor;
        self.img_left_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"left_arrow"] withColor:[UIColor whiteColor]];
    } else
    {
        self.view_left_arrow.backgroundColor = [StaticData colorFromHexString:@"#808080"];
        self.img_left_arrow.image = [StaticData changeImageColor:[UIImage imageNamed:@"left_arrow"] withColor:[UIColor whiteColor]];
    }
}

-(void)takeSwipeGestureToTabbar:(UISwipeGestureRecognizer *)swipe
{
    if (self.HomeBannerSwipeUpCallBack) {
        self.HomeBannerSwipeUpCallBack();
    }
}

@end
