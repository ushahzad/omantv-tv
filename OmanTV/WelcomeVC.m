//
//  WelcomeVC.m
//  DubaiTV
//
//  Created by Curiologix on 01/02/2018.
//  Copyright © 2018 Ali Raza. All rights reserved.
//

#import "WelcomeVC.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self startHomeScreen];
    
//    [self checkAppVersion];
//    [self createVideoPlayer];
}

- (void)createVideoPlayer {
    
    @try {
        //NSInteger splash = [[NSUserDefaults standardUserDefaults] integerForKey:@"RandomSplash"];
        NSString *welcome = @"welcome";
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:welcome ofType:@"mp4"];
        NSURL *url = [NSURL fileURLWithPath:filePath];
        
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:url];
        
        self.player = [AVPlayer playerWithPlayerItem:playerItem];
        
        _playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        _playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        _playerLayer.frame = [UIScreen mainScreen].bounds;
        [self.playerView.layer addSublayer:_playerLayer];
        
        [self.player play];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.player.currentItem];
    } @catch (NSException *exception) {
        [self startHomeScreen];
    } @finally {
    }
}

- (void)moviePlayDidEnd:(NSNotification*)notification{
    self.videoPlayingFinished = true;
    if (self.videoPlayingFinished && self.checkingAppUpdateFinished)
    {
        [self startHomeScreen];
    }
}

-(void)checkAppVersion
{
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/general_settings/apps_version?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:@"AppVersion"];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setTimeoutInterval:10.0];
    [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
       [HUD hideUIBlockingIndicator];
       [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
       [self responseReceived:responseObject withCallType:callType];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        @try {
            [HUD hideUIBlockingIndicator];
            
            self.checkingAppUpdateFinished = true;
            [self startAppNow];
            
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
        } @finally {
        }
        
        [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
    }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
  {
    @try {
        if ([callType isEqualToString:@"AppVersion"])
        {
            self.appVersionInfo = responseObject;
            [self checkAppUpdated:responseObject];
        }
    } @catch (NSException *exception) {
        self.checkingAppUpdateFinished = true;
        [self startAppNow];
    } @finally {
    }
}

-(void)checkAppUpdated:(id)responseObject
{
    @try {
        
        NSDictionary *data = [StaticData checkDictionaryNull:responseObject[@"data"]];
        NSString *liveVersion = [StaticData checkStringNull:data[@"apple_tv"]];
        NSString *appVersionStr = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        if ([liveVersion compare:appVersionStr options:NSNumericSearch] == NSOrderedDescending) {
            // appVersionStr is lower than the liveVersion
            self.checkingAppUpdateFinished = false;
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"هناك تحديث متوفر" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:isEnglishLang ? @"OK" : Ok style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                NSURL*url = [[NSURL alloc]initWithString:@"https://apps.apple.com/om/app/ayn/id1576357105" ];
                [[UIApplication sharedApplication] openURL:url];
                
                exit(0);
                
            }];
            [alert addAction:action];
            [self presentViewController:alert animated:true completion:nil];
        } else
        {
            self.checkingAppUpdateFinished = true;
            [self startAppNow];
        }
    } @catch (NSException *exception) {
        self.checkingAppUpdateFinished = true;
        [self startAppNow];
    } @finally {
    }
}

-(void)startHomeScreen
{
    [StaticData setRootViewController];
}

-(void)startAppNow
{
    @try {
        if (self.videoPlayingFinished && self.checkingAppUpdateFinished)
        {
            if (self.VideoPlayingFinishedCallBack){
                self.VideoPlayingFinishedCallBack();
            }
            
            [self startHomeScreen];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

@end
