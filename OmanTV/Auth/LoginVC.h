//
//  LoginVC.h


#import <UIKit/UIKit.h>
#import "ForgotPasswordVC.h"
#import "ScheduleVideosVC.h"

@interface LoginVC : UIViewController <UITextFieldDelegate>
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_qrCode;
@property(nonatomic,strong) IBOutlet UIButton *btn_remember;
@property(nonatomic,strong) IBOutlet UIButton *btn_forgetPassword;
@property(strong,nonatomic)IBOutlet UILabel *lbl_email;
@property(strong,nonatomic)IBOutlet UILabel *lbl_remeber;
@property(strong,nonatomic)IBOutlet UILabel *lbl_password;
@property(strong,nonatomic)IBOutlet UILabel *lbl_descriptions;
@property(strong,nonatomic)IBOutlet UILabel *lbl_signin;
@property(strong,nonatomic)IBOutlet UITextField *txt_email;
@property(strong,nonatomic)IBOutlet UITextField *txt_password;
@property(strong,nonatomic)IBOutlet UITextField *activeTextfield;
@property(strong,nonatomic)IBOutlet UIButton *btn_email;
@property(strong,nonatomic)IBOutlet UIButton *btn_password;
@property(strong,nonatomic)IBOutlet UIButton *btn_back;
@property(strong,nonatomic)IBOutlet UIButton *btn_signin;
@property(strong,nonatomic)IBOutlet UIButton *btn_register;
@property(strong,nonatomic)IBOutlet UIButton *btn_qrCode;
@property(assign,nonatomic) BOOL isNeedToShowBackButton;
@property(strong,nonatomic)IBOutlet UIImageView *img_email;
@property(strong,nonatomic)IBOutlet UIImageView *img_password;
@property(strong,nonatomic)IBOutlet UIImageView *img_forgotPassword;
@property(nonatomic, weak) IBOutlet UIImageView *img_bg;

@property(strong,nonatomic)IBOutlet UILabel *lbl_qr;
@property(strong,nonatomic)IBOutlet UIImageView *img_qr;

@end
