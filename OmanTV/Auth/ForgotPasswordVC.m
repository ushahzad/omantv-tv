//
//  ForgotPasswordVC.m

#import "ForgotPasswordVC.h"
#import "ChangePasswordNow.h"
@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];

    TabbarController *vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"profile"];
    [vc_tabbarVC.view setBackgroundColor:[UIColor clearColor]];
    
    
    UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#ffffff"]];
    [self.btn_send setImage:img forState:UIControlStateNormal];
    
    self.img_email.layer.cornerRadius = 5.0;
    
    [self loadBackgroundImage];
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgetPasswordNow)];
    [self.btn_send addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEmailKeyboard)];
    [self.btn_email addGestureRecognizer:tab];
    
}

-(void)loadBackgroundImage
{
    @try {
        if ([StaticData shared].appSettingsInfo)
        {
            NSString *thumbnailPath = [StaticData checkStringNull:[StaticData shared].appSettingsInfo[@"ch_bg_app"]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:thumbnailPath];
            imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            self.img_bg.contentMode = UIViewContentModeScaleAspectFill;
            
            [self.img_bg sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                 } else {
                     [StaticData downloadImageIfNotLoadinedBySdImage:self.img_bg withUrl:imageURL completed:^(UIImage *image) {
                     }];
                 }
             }];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)openEmailKeyboard
{
    [self.txt_email becomeFirstResponder];
}
-(void)hideKeyboard
{
    [self.txt_email resignFirstResponder];
}

-(void)forgetPasswordNow
{
    if (_txt_email.text.length == 0) {
        [StaticData showAlertView:AllFieldRequird withvc:self];
    }  else if (![StaticData validateEmailWithString:_txt_email.text])
    {
        [StaticData showAlertView:EmailInvalid withvc:self];
    } else
    {
        [_txt_email resignFirstResponder];
        [self changePasswordNow];
    }
}

-(void)changePasswordNow
{
    [HUD showUIBlockingIndicator];
    NSString *path = [NSString stringWithFormat:@"%@/apps/send_forget_password_code?key=%@&user_id=%@",BaseURL,BaseKEY,BaseUID];
    NSDictionary *params = @{@"email":_txt_email.text};
    [self executeApiCallToGetResponse:path withParameters:params withCallType:@"ChangdPassword"];
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [HUD hideUIBlockingIndicator];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        [self responseRecieved:responseObject withCallType:callType];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [HUD hideUIBlockingIndicator];
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData)
        {
            NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            if (serializedData && serializedData[@"error"])
            {
                [StaticData showAlertView:serializedData[@"error"] withvc:self];
            }
        }
    }];
    
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ChangdPassword"])
        {
            if (responseObjct[@"success"] && [responseObjct[@"success"] isEqualToString:@"yes"])
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"يرجى التحقق من بريدك الإلكتروني والحصول على الرمز لإكمال عملية تغيير كلمة المرور" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"حسناً" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    ChangePasswordNow *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordNow"];
                    [self.navigationController pushViewController:vc animated:true];
                    
                    vc.BackToLoginCallBack = ^{
                        [self.navigationController popViewControllerAnimated:false];
                    };
                }];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:true completion:nil];
            }
        }
    } @catch (NSException *exception) {
        [StaticData showAlertView:@"Exception found please try again." withvc:self];
    } @finally {
        
    }
}

-(IBAction)backToVC:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txt_email) {
        self.lbl_email.text = textField.text;
        self.txt_email.text = textField.text;
        if (self.lbl_email.text.length == 0) {
            self.lbl_email.text = @"البريد الإلكتروني";
        }
    }
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.txt_email.hidden = true;
    if (textField == _txt_email) {
        self.img_email.image = [UIImage imageNamed:@"bg_textField"];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_txt_email == textField) {
        self.img_email.image = [UIImage imageNamed:@"bg_textField_white"];
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
  
    if (context.previouslyFocusedView == self.btn_email)
    {
        self.img_email.alpha = 0.2;
        self.img_email.backgroundColor = [UIColor whiteColor];
        self.img_email.image = [UIImage imageNamed:@"bg_field_white"];
    }
    
    if (context.nextFocusedView == self.btn_email) {
        self.img_email.alpha = 1.0;
        self.img_email.backgroundColor = AppColor;
        self.img_email.image = [UIImage imageNamed:@"bg_textField_pink"];
    }
    
    if (context.previouslyFocusedView == self.btn_send)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#ffffff"]];
        [self.btn_send setImage:img forState:UIControlStateNormal];
        self.lbl_signin.textColor = AppColor;
    }
    
    if (context.nextFocusedView == self.btn_send)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:AppColor];
        [self.btn_send setImage:img forState:UIControlStateNormal];
        
        self.lbl_signin.textColor = [UIColor whiteColor];
    }
}

-(NSArray *)preferredFocusEnvironments
{
    return @[];
}

@end

