//
//  QRLoginVC.h
//  OmanTV
//
//  Created by Cenex Studio on 02/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QRLoginVC : UIViewController
@property(strong,nonatomic)IBOutlet UIButton *btn_qrCode;
@property(strong,nonatomic)IBOutlet UIImageView *img_qrCode;
@property(strong,nonatomic)IBOutlet UIButton *btn_login;
@property(strong,nonatomic)IBOutlet UILabel *lbl_login;
@property(strong,nonatomic)IBOutlet UILabel *lbl_picCode;
@property(strong,nonatomic)IBOutlet UILabel *lbl_loginUrl;
@property(strong,nonatomic)IBOutlet UIImageView *img_login;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loginIndicator;
@property (nonatomic, retain) NSTimer *fiveSecondTimer;
@property (nonatomic, retain) NSTimer *tenMinutesTimer;
@property (nonatomic, retain) NSDictionary *qrInfo;
@property (nonatomic, assign) BOOL isViewActive;
@end

NS_ASSUME_NONNULL_END
