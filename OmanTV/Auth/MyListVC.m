//
//  MyListVC.m
//  ADtv
//
//  Created by MacUser on 26/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "MyListVC.h"

@interface MyListVC ()

@end

@implementation MyListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"profile"];
    
    self.typesList = [NSMutableArray new];
    self.contentList = [NSMutableArray new];
    
    self.img_delete.hidden = true;
    self.btn_delete.hidden = true;
    
    [StaticData scaleToArabic:self.cv_types];
    [StaticData scaleToArabic:self.collectionView];
    [self.cv_types remembersLastFocusedIndexPath];
    
    self.typesList = [NSMutableArray new];
    
    //fav show show
    MediaInfo *info = [MediaInfo new];
    
    info = [MediaInfo new];
    info.title = @"البرامج المفضلة";
    info.type = 1;
    info.isSelected = true;
    [self.typesList addObject:info];
    
    self.selectedType = info;
    
    info = [MediaInfo new];
    info.title = @"برامج المشاهدة لاحقاً";
    info.type = 2;
    [self.typesList addObject:info];
    
    info = [MediaInfo new];
    info.title = @"المرئيات المفضلة";
    info.type = 3;
    [self.typesList addObject:info];
    
    //fav watch later show
    info = [MediaInfo new];
    info.title = @"مرئيات المشاهدة لاحقاً";
    info.type = 4;
    [self.typesList addObject:info];
    
    
    //fav audio show
    info = [MediaInfo new];
    info.title = @"الصوتيات المفضلة";
    info.type = 5;
    [self.typesList addObject:info];
    
    //fav audio watch later show
    info = [MediaInfo new];
    info.title = @"الإستماع لاحقاً";
    info.type = 6;
    [self.typesList addObject:info];

    info = [MediaInfo new];
    info.title = @"تسجيل الخروج";
    info.type = 7;
    [self.typesList addObject:info];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.lbl_noResultsFound.hidden = true;
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
    
    if (self.selectedType.type == 1)
    {
        [self loadFavShows];
    }
    else if (self.selectedType.type == 2)
    {
        [self loadWatchLaterShows];
    }
    else if (self.selectedType.type == 3)
    {
        [self loadFavVideos];
    } else if (self.selectedType.type == 4)
    {
        [self loadWatchLaterVideos];
    } else if (self.selectedType.type == 5)
    {
        [self loadFavAudioShows];
    }else if (self.selectedType.type == 6)
    {
        [self loadAudioWatchLaterShows];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableDelete)];
    [self.btn_delete addGestureRecognizer:tab];
}

-(void)enableDelete
{
    if (self.isDeleteAble) {
        self.isDeleteAble = false;
    } else {
        self.isDeleteAble = true;
    }
    [StaticData reloadCollectionView:self.collectionView];
}

-(void)loadFavShows
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=1&need_filter_by_type=yes&is_radio=0&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)loadFavVideos
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_videos?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=1&need_filter_by_type=yes&is_radio=0&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)loadWatchLaterVideos
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_videos?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=0&need_filter_by_type=yes&is_radio=0&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)loadWatchLaterShows
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=0&need_filter_by_type=yes&is_radio=0&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)loadFavAudioShows
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=1&need_filter_by_type=yes&is_radio=1&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)loadAudioWatchLaterShows
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@&app_id=%@&X-API-KEY=%@&fav_type=0&need_filter_by_type=yes&is_radio=1&need_labels=yes&need_top_10=yes",BaseURL,BaseUID,BaseKEY,AppID,[StaticData findLogedUserToken]];
    [self executeApiToGetApiResponse:path withParameters:nil withCallType:[@(self.selectedType.type) stringValue]];
}

-(void)makeShowFav:(MediaInfo *)showInfo
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
    NSDictionary *parms = @{@"category_id":showInfo.ID,@"fav_type":@"1",@"X-API-KEY":[StaticData findLogedUserToken]};
    [self executeApiToGetApiResponse:path withParameters:parms withCallType:@"MakeShowFav"];
    
    if (showInfo.isfav) {
        showInfo.isfav = false;
    } else {
        showInfo.isfav = true;
    }
    [self reloadCollectionView];
}

-(void)makeWatchLaterShow:(MediaInfo *)showInfo
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/endpoint/channel_users/favorite_shows?user_id=%@&key=%@",BaseURL,BaseUID,BaseKEY];
    NSDictionary *parms = @{@"category_id":showInfo.ID,@"fav_type":@"0",@"X-API-KEY":[StaticData findLogedUserToken]};
    [self executeApiToGetApiResponse:path withParameters:parms withCallType:@"MakeShowFav"];
    
    if (showInfo.isfav) {
        showInfo.isfav = false;
    } else {
        showInfo.isfav = true;
    }
    [self reloadCollectionView];
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    if ([callType isEqualToString:@"MakeShowFav"] || [callType isEqualToString:@"ShorterUrl"])
    {
        [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
            [self responseReceived:responseObject withCallType:callType];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        }];
    } else
    {
        [manager GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
            
            [self responseReceived:responseObject withCallType:callType];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self.indicator stopAnimating];
            [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
        }];
    }
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType containsString:[@(self.selectedType.type) stringValue]])
        {
            [self.contentList removeAllObjects];
            for (NSDictionary *dicObj in responseObject[@"data"])
            {
                if ([[StaticData checkStringNull:dicObj[@"publish"]] isEqualToString:@"no"])
                {
                    continue;
                }
                
                MediaInfo *info = [MediaInfo new];
                info.ID = [StaticData checkStringNull:dicObj[@"id"]];
                info.title = [StaticData checkStringNull:dicObj[@"title_ar"]];
                info.thumbnailPath = [StaticData checkStringNull:dicObj[@"img"]];
                
                if ([info.thumbnailPath isEqualToString:@""]) {
                    info.thumbnailPath = [StaticData checkStringNull:dicObj[@"thumbnail"]];
                }
                
                info.type = [callType intValue];
                
                if (info.type == 3 || info.type == 4) {
                    info.isRadio = true;
                }
                
                if (![StaticData isVideoBlockInMyCountry:dicObj isShowGeoStatus:false])
                {
                    [_contentList addObject:info];
                }
            }
            
            [self checkRecordFound];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        [self checkRecordFound];
    } @finally {
    }
}

-(void)checkRecordFound
{
    @try {
        if (_contentList.count == 0) {
            self.lbl_noResultsFound.hidden = false;
            self.img_delete.hidden = true;
            self.btn_delete.hidden = true;
        } else {
            self.img_delete.hidden = false;
            self.btn_delete.hidden = false;
            
            MediaInfo *info = self.contentList[0];
            info.isSelected = true;
        }
        
        [self reloadCollectionView];
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)reloadCollectionView
{
    [StaticData reloadCollectionView:self.collectionView];
    [StaticData reloadCollectionView:self.cv_types];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == _cv_types)
    {
        CGFloat totalCellWidth = 1393;
        CGFloat totalSpacingWidth = 0;
        CGFloat leftInset = (collectionView.bounds.size.width - (totalCellWidth + totalSpacingWidth)) / 2;
        CGFloat rightInset = leftInset;
        UIEdgeInsets sectionInset = UIEdgeInsetsMake(0, leftInset, 0, rightInset);
        return sectionInset;
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CGRect frame = collectionView.frame;
    
    if (collectionView == _cv_types)
    {
        return CGSizeMake(210, frame.size.height);
    }
    
    CGFloat width = (frame.size.width - 100) / 5;
    CGFloat height = width;
    
    if (self.tempImage) {
        CGFloat ratio = self.tempImage.size.height / self.tempImage.size.width;
        height = ratio * width;
    }
    
    return CGSizeMake(width, height + 50);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == _cv_types)
    {
        return _typesList.count;
    }
    return self.contentList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == _cv_types)
    {
        CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        MediaInfo *info = self.typesList[indexPath.row];
        cell.lbl_title.text = info.title;
        
        cell.backgroundColor = [UIColor clearColor];
        cell.view_info.hidden = false;
        cell.alpha = 1.0;
        
        if (info.isSelected) {
            [self setupSelectedState:cell withIndexPath:indexPath];
            cell.backgroundColor = LightWidthColor;
            cell.view_info.hidden = true;
        } else {
            cell.lbl_title.textColor = [UIColor whiteColor];
        }
        
        cell.view_info.backgroundColor = [StaticData colorFromHexString:@"#262626"];
        cell.view_info.layer.cornerRadius = 12.0;
        cell.view_info.layer.masksToBounds = true;
        
        cell.layer.cornerRadius = 12.0;
        cell.layer.masksToBounds = true;
        
        return cell;
    }
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.view_info.hidden = false;
    
    MediaInfo *info = self.contentList[indexPath.row];
    cell.info = info;
    cell.lbl_titleMarquee.text = info.title;
    [StaticData setupMarqureeLabel:cell.lbl_titleMarquee];
    
    cell.view_info.hidden = !self.isDeleteAble;
    
    @try {
        NSString * encodedCover = [info.thumbnailPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *imagePath = [BaseImageURL stringByAppendingString:encodedCover];
        [cell.indicator startAnimating];
        cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"default_logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            if (image)
            {
                if (!self.tempImage) {
                    self.tempImage = image;
                    [StaticData reloadCollectionView:self.collectionView];
                }
                [cell.indicator stopAnimating];
            } else {
                [cell.indicator stopAnimating];
            }
        }];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    cell.DeleteCallBack = ^(CollectionCell * _Nonnull cell, MediaInfo * _Nonnull info) {
        if (info.type == 1 || info.type == 3)
        {
            [self makeShowFav:info];
        } else if (info.type == 2 || info.type == 4)
        {
            [self makeWatchLaterShow:info];
        }
        
        [self.contentList removeObject:info];
        [self reloadCollectionView];
    };
    
    cell.ViewCallBack = ^(CollectionCell * _Nonnull cell, MediaInfo * _Nonnull info) {
        [StaticData openShowDetailsPageWith:self withMediaInfo:info];
    };
    
    cell.thumbnail.layer.cornerRadius = CellRounded;
    cell.thumbnail.layer.masksToBounds = true;

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.cv_types)
    {
        
        for (MediaInfo *info in self.typesList)
        {
            info.isSelected = false;
        }
        
        MediaInfo *info = self.typesList[indexPath.row];
        info.isSelected = true;
        self.selectedType = info;
        
        [self resetContent];
        
        if (info.type == 1)
        {
            [self loadFavShows];
        }
        else if (info.type == 2)
        {
            [self loadWatchLaterShows];
        }
        else if (info.type == 3)
        {
            [self loadFavVideos];
        } else if (info.type == 4)
        {
            [self loadWatchLaterVideos];
        } else if (info.type == 5)
        {
            [self loadFavAudioShows];
        }else if (info.type == 6)
        {
            [self loadAudioWatchLaterShows];
        }
        else if (info.type == 7)
        {
            [StaticData shared].userInfo = nil;
            [StaticData logOutMe];
            [StaticData setRootViewController];
        }
        
    } else {
        MediaInfo *info = self.contentList[indexPath.row];
        if (self.isDeleteAble) {
            
            if (info.type == 1 || info.type == 3)
            {
                [self makeShowFav:info];
            } else if (info.type == 2 || info.type == 4)
            {
                [self makeWatchLaterShow:info];
            }
            
            [self.contentList removeObject:info];
            [self reloadCollectionView];
        } else
        {
            info.itemType = @"profile";
            [StaticData openShowDetailsPageWith:self withMediaInfo:info];
        }
    }
}

-(void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (collectionView == self.cv_types)
    {
        if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] || [context.nextFocusedView isKindOfClass:[CollectionCell class]])
        {
            if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
            {
                CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
                [self setupSelectedState:cell withIndexPath:context.previouslyFocusedIndexPath];
            }
            
            if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
            {
                CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
                cell.view_info.hidden = true;
                cell.backgroundColor =  [UIColor whiteColor];
                cell.lbl_title.textColor = AppColor;
            } else
            {
                [StaticData reloadCollectionView:self.cv_types];
            }
        }
        return;
    }
    
    if ([context.previouslyFocusedView isKindOfClass:[CollectionCell class]] && context.previouslyFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.previouslyFocusedView;
        [cell.lbl_titleMarquee pauseLabel];
        cell.thumbnail.layer.borderColor = [UIColor clearColor].CGColor;
        cell.thumbnail.layer.borderWidth = 0.0;
    }
    
    if ([context.nextFocusedView isKindOfClass:[CollectionCell class]] && context.nextFocusedView.superview == collectionView)
    {
        CollectionCell *cell = (CollectionCell *)context.nextFocusedView;
        [cell.lbl_titleMarquee unpauseLabel];
        
        cell.clipsToBounds = true;
        cell.thumbnail.layer.borderColor = AppColor.CGColor;
        cell.thumbnail.layer.borderWidth = CellBorderWidth;
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.previouslyFocusedView == self.btn_delete) {
        self.img_delete.image = [UIImage imageNamed:@"delete"];
    }
    
    if (context.nextFocusedView == self.btn_delete) {
        self.img_delete.image = [UIImage imageNamed:@"delete_selected"];
    }
}

-(void)setupSelectedState:(CollectionCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.view_info.hidden = false;
    cell.backgroundColor =  [UIColor clearColor];
    cell.lbl_title.textColor = [UIColor whiteColor];
    
    MediaInfo *info = self.typesList[indexPath.row];

    if (info.isSelected) {
        cell.lbl_title.textColor = AppColor;
    }
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}

-(void)resetContent
{
    self.lbl_noResultsFound.hidden = true;
    [self.contentList removeAllObjects];
    [self reloadCollectionView];
}

@end
