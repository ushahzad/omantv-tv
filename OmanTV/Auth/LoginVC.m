//
//  LoginVC.m

#import "LoginVC.h"
//#import "ForgotPasswordVC.h"
@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.vc_tabbarVC = [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"profile"];
    [self.vc_tabbarVC.view setBackgroundColor:[UIColor clearColor]];
    
    self.txt_password.hidden = true;
    self.txt_password.hidden = true;
    
    self.img_email.layer.cornerRadius = 5;
    self.img_password.layer.cornerRadius = 5;
    self.img_forgotPassword.layer.cornerRadius = 5;
    
    UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"unselected_remember_circle"] withColor:[UIColor whiteColor]];
    [self.btn_remember setImage:img forState:UIControlStateNormal];
    
    img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[UIColor whiteColor]];
    [self.btn_signin setImage:img forState:UIControlStateNormal];
    
    img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#ffffff"]];
    [self.btn_signin setImage:img forState:UIControlStateNormal];
    
    _btn_remember.tag = 0;
    [_btn_remember setImage:[UIImage imageNamed:@"unselected_remember_circle"] forState:UIControlStateNormal];
    
    NSDictionary *loginRemeberInfo = [StaticData getMyRememberInfo];
    if (loginRemeberInfo) {
        self.txt_email.text = [StaticData checkStringNull:loginRemeberInfo[@"Email"]];
        self.lbl_email.text = self.txt_email.text;
        self.txt_password.text = [StaticData checkStringNull:loginRemeberInfo[@"Password"]];
        self.lbl_password.text = self.txt_password.text;
        self.txt_password.secureTextEntry = true;
        _btn_remember.tag = 1;
        [_btn_remember setImage:[UIImage imageNamed:@"selected_remember_circle"] forState:UIControlStateNormal];
        
        [self makePasswordSecure:self.txt_password];
    }
    
    [self loadBackgroundImage];
    
//    UIColor *plcColor = [UIColor whiteColor];
//    [StaticData setTextFieldPlaceHolderColor:self.txt_email withColor:plcColor];
//    [StaticData setTextFieldPlaceHolderColor:self.txt_password withColor:plcColor];
    
//    [StaticData buttonUnderLineTitle:self.btn_forgetPassword withColor:[StaticData colorFromHexString:@"#ffffff"]];
    
    [StaticData setupLineSpacing:self.lbl_descriptions];
    self.lbl_descriptions.textAlignment = NSTextAlignmentCenter;
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginAction)];
    [self.btn_signin addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToRegiterVC)];
    [self.btn_register addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotToForgetPasswordVC)];
    [self.btn_forgetPassword addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rememberMe)];
    [self.btn_remember addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEmailKeyboard)];
    [self.btn_email addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPasswordKeyboard)];
    [self.btn_password addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(qrCodeAction)];
    [self.btn_qrCode addGestureRecognizer:tab];
}

-(void)qrCodeAction
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)loadBackgroundImage
{
    @try {
        if ([StaticData shared].appSettingsInfo)
        {
            NSString *thumbnailPath = [StaticData checkStringNull:[StaticData shared].appSettingsInfo[@"apple_tv_bg"]];
            NSString *imagePath = [BaseImageURL stringByAppendingString:thumbnailPath];
            imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            self.img_bg.contentMode = UIViewContentModeScaleAspectFill;
            
            [self.img_bg sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 if (image)
                 {
                 } else {
                     [StaticData downloadImageIfNotLoadinedBySdImage:self.img_bg withUrl:imageURL completed:^(UIImage *image) {
                     }];
                 }
             }];
        }
    } @catch (NSException *exception) {
    } @finally {
    }
}

-(void)openEmailKeyboard
{
    [self.txt_email becomeFirstResponder];
}

-(void)openPasswordKeyboard
{
    [self.txt_password becomeFirstResponder];
}
    
-(void)hideKeyboard
{
    [_txt_email resignFirstResponder];
    [_txt_password resignFirstResponder];
}

-(void)loginAction
{
    if (_txt_email.text.length == 0 || _txt_password.text.length == 0) {
        [StaticData showAlertView:AllFieldRequird withvc:self];
    } else if (![StaticData validateEmailWithString:_txt_email.text])
    {
        [StaticData showAlertView:InvalidEmail withvc:self];
    }
    /*else if (![StaticData isValidPassword:_txt_password.text]) {
        [StaticData showAlertView:PasswordValidation withvc:self];
    }*/
    else
    {
        [self hideKeyboard];
        [self loginUserNow];
    }
}

-(void)loginUserNow
{
    NSString *path = [NSString stringWithFormat:@"%@/login?user_id=%@&key=%@",BaseAuthURL,BaseUID,BaseKEY];
    
    NSDictionary *prams = @{@"email":_txt_email.text,@"password":_txt_password.text};

    [self executeApiCallToGetResponse:path withParameters:prams withCallType:@"login"];
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    [HUD showUIBlockingIndicator];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [HUD hideUIBlockingIndicator];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        [self responseRecieved:responseObject withCallType:callType];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [HUD hideUIBlockingIndicator];
        [StaticData log:[NSString stringWithFormat:@"%@",error.description]];
        @try {
            [StaticData showAlertView:@"هذا البريد الإلكتروني غير مستخدم من قبل" withvc:self];
            NSError* jsonError;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                                 options:kNilOptions
                                                                   error:&jsonError];
            if (!jsonError && json)
            {
                
            }
        } @catch (NSException *exception) {
        } @finally {
        }
    }];
    
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"login"])
        {
            if ([responseObjct isKindOfClass:[NSDictionary class]] && responseObjct[@"error"])
            {
                [StaticData showAlertView:@"خطأ في البريد الإلكتروني أو كلمة المرور" withvc:self];
                
//                NSString *error = [StaticData checkStringNull:responseObjct[@"error"]];
//                if(error.length > 0)
//                {
//                    [StaticData showAlertView:error withvc:self];
//                }
            } else
            {
                if([responseObjct isKindOfClass:[NSDictionary class]])
                {
                    NSString *status = [responseObjct objectForKey:@"success"];
                    if([status isEqualToString:@"yes"])
                    {
                        NSDictionary *userDic = [StaticData checkDictionaryNull:responseObjct[@"data"]];
                        
                        [StaticData saveLoginUserInfo:userDic];
                        [StaticData setupLogedUserProfileInfo];
                        
                        if (_btn_remember.tag == 1)
                        {
                            [StaticData saveMyRememberInfo:@{@"Email":self.txt_email.text,@"Password":self.txt_password.text}];
                        } else
                        {
                            [StaticData saveMyRememberInfo:nil];
                        }
                        
                        [self startAppNow];
                    } else {
                        [self showLoginErrorMessage];
                    }
                } else {
                    [self showLoginErrorMessage];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception found please try again.");
    } @finally {
        
    }
}

-(void)accountCreateSuccessfull:(NSString *)message withVarificationID:(NSString *)vID
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [ac addAction:action];
    [self presentViewController:ac animated:true completion:nil];
}

-(void)showLoginErrorMessage
{
    [StaticData showAlertView:CheckUserAndPassword withvc:self];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.activeTextfield = textField;
    if (textField == self.txt_email) {
        self.lbl_email.text = textField.text;
        self.txt_email.text = textField.text;
        if (self.lbl_email.text.length == 0) {
            self.lbl_email.text = @"البريد الإلكتروني";
        }
    } else if (textField == self.txt_password) {
        [self makePasswordSecure:textField];
        self.txt_password.text = textField.text;
        if (self.txt_password.text.length == 0) {
            self.lbl_password.text = @"كلمة المرور";
        }
    }
    return true;
}

-(void)rememberMe
{
    if (_btn_remember.tag == 0)
    {
        _btn_remember.tag = 1;
        [_btn_remember setImage:[UIImage imageNamed:@"selected_remember_circle"] forState:UIControlStateNormal];
        
    }else if (_btn_remember.tag == 1)
    {
        _btn_remember.tag = 0;
        [_btn_remember setImage:[UIImage imageNamed:@"unselected_remember_circle"] forState:UIControlStateNormal];
    }
}

-(void)goToRegiterVC
{
//    SignUpVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
//    vc.vc_login = self;
//    [self.navigationController pushViewController:vc animated:true];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txt_email) {
    } else if (textField == self.txt_password) {
        if (textField == self.txt_password) {
            self.txt_password.secureTextEntry = true;
        }
    }
    [textField becomeFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.txt_email == textField) {
        self.img_email.image = [UIImage imageNamed:@"bg_textField_white"];
    } else if (self.txt_password == textField) {
        if (textField.text.length == 0) {
            self.txt_password.secureTextEntry = false;
        }
    }
}

-(void)startAppNow
{
    [StaticData setRootViewController];
}

-(void)gotToForgetPasswordVC
{
    ForgotPasswordVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVC"];
    [self.navigationController pushViewController:vc animated:true];
}

-(IBAction)backToView:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if ([context.previouslyFocusedView isKindOfClass:[UIButton class]] || [context.nextFocusedView isKindOfClass:[UIButton class]])
    {
        //context.previouslyFocusedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        //context.nextFocusedView.transform = CGAffineTransformMakeScale(1.1, 1.1);
    }
    
    if (context.previouslyFocusedView == self.btn_email || context.nextFocusedView == self.btn_email || context.previouslyFocusedView == self.btn_password || context.nextFocusedView == self.btn_password || context.nextFocusedView == self.btn_qrCode || context.previouslyFocusedView == self.btn_qrCode)
    {
        if (context.previouslyFocusedView == self.btn_email)
        {
            self.img_email.alpha = 0.2;
            self.img_email.image = [UIImage imageNamed:@"bg_field_white"];
            self.img_email.backgroundColor = [UIColor whiteColor];
        }
        
        if (context.nextFocusedView == self.btn_email) {
            self.img_email.alpha = 1;
            self.img_email.image = [UIImage imageNamed:@"bg_textField_pink"];
            self.img_email.backgroundColor = AppColor;
        }
        
        if (context.previouslyFocusedView == self.btn_password) {
            self.img_password.alpha = 0.2;
            self.img_password.image = [UIImage imageNamed:@"bg_field_white"];
            self.img_password.backgroundColor = [UIColor whiteColor];
        }
        
        if (context.nextFocusedView == self.btn_password) {
            self.img_password.alpha = 1;
            self.img_password.image = [UIImage imageNamed:@"bg_textField_pink"];
            self.img_password.backgroundColor = AppColor;
        }
        
        if (context.nextFocusedView == self.btn_qrCode) {
            self.lbl_qr.textColor = [UIColor whiteColor];
            self.img_qr.image = [StaticData changeImageColor:[UIImage imageNamed:@"btn_qr_code"] withColor:[UIColor whiteColor]];
        }
        
        if (context.previouslyFocusedView == self.btn_qrCode) {
            self.lbl_qr.textColor = AppColor;
            self.img_qr.image = [StaticData changeImageColor:[UIImage imageNamed:@"btn_qr_code"] withColor:AppColor];
            
//            self.view_qrCode.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }
        
        self.btn_email.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.btn_password.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    
    if (context.previouslyFocusedView == self.btn_remember)
    {
        self.lbl_remeber.textColor = [UIColor whiteColor];
        if (_btn_remember.tag == 0)
        {
//            UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"unselected_remember_circle"] withColor:[StaticData colorFromHexString:@"#979797"]];
//            [self.btn_remember setImage:img forState:UIControlStateNormal];
        }
    }
    
    if (context.nextFocusedView == self.btn_remember)
    {
        self.lbl_remeber.textColor = AppColor;
        if (_btn_remember.tag == 0)
        {
//            UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"unselected_remember_circle"] withColor:AppColor];
//            [self.btn_remember setImage:img forState:UIControlStateNormal];
        }
    }
    
    
    if (context.previouslyFocusedView == self.btn_signin)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#ffffff"]];
        [self.btn_signin setImage:img forState:UIControlStateNormal];
        
        self.lbl_signin.textColor = AppColor;
    }
    
    if (context.nextFocusedView == self.btn_signin)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:AppColor];
        [self.btn_signin setImage:img forState:UIControlStateNormal];
        
        self.lbl_signin.textColor = [UIColor whiteColor];
    }
    
    if (context.previouslyFocusedView == self.btn_forgetPassword)
    {
        self.img_forgotPassword.alpha = 0.2;
        self.img_forgotPassword.backgroundColor = [UIColor whiteColor];
        self.img_forgotPassword.image = [UIImage imageNamed:@"bg_field_white"];
//        [StaticData buttonUnderLineTitle:self.btn_forgetPassword withColor:[StaticData colorFromHexString:@"#ffffff"]];
    }
    
    if (context.nextFocusedView == self.btn_forgetPassword)
    {
        self.img_forgotPassword.alpha = 1;
        self.img_forgotPassword.backgroundColor = AppColor;
        self.img_forgotPassword.image = [UIImage imageNamed:@"bg_textField_pink"];
//        [StaticData buttonUnderLineTitle:self.btn_forgetPassword withColor:AppColor];
    }
}

-(void)makePasswordSecure:(UITextField *)textField
{
    NSMutableString *dottedPassword = [NSMutableString new];

    for (int i = 0; i < [textField.text length]; i++)
    {
        [dottedPassword appendString:@"●"]; // BLACK CIRCLE Unicode: U+25CF, UTF-8: E2 97 8F
    }

    self.lbl_password.text = dottedPassword;
}

-(NSArray *)preferredFocusEnvironments
{
    @try {
        if (self.vc_tabbarVC)
        {
            CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
            if (cell) {
                return @[cell];
            }
            return @[self.vc_tabbarVC.cv_tabbar];
        }
        CollectionCell *cell = [self.vc_tabbarVC findSelectedCell];
        if (cell) {
            return @[cell];
        }

        return @[self.vc_tabbarVC.cv_tabbar];
    } @catch (NSException *exception) {
    } @finally {
    }

    return @[];
}

@end
