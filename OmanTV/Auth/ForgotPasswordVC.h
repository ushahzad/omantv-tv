//
//  ForgotPasswordVC.h


#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController <UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_navBarParent;
@property (nonatomic, weak) IBOutlet UITextField *txt_email;
@property (nonatomic, weak) IBOutlet UIImageView *img_email;
@property(strong,nonatomic)IBOutlet UILabel *lbl_signin;
@property(strong,nonatomic)IBOutlet UIButton *btn_email;
@property(strong,nonatomic)IBOutlet UILabel *lbl_email;
@property(strong,nonatomic)IBOutlet UIButton *btn_send;
@property(nonatomic, weak) IBOutlet UIImageView *img_bg;

@end
