//
//  ChangePasswordNow.h
//  FanBox
//
//  Created by MacUser on 19/07/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordNow : UIViewController <UITextFieldDelegate>
@property void (^BackToLoginCallBack)(void);
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_navBarParent;
@property (nonatomic, weak) IBOutlet UITextField *txt_emailCode;

@property (nonatomic, weak) IBOutlet UITextField *txt_password;

@property (nonatomic, weak) IBOutlet UIImageView *img_emailCode;
@property (nonatomic, weak) IBOutlet UIImageView *img_password;

@property(strong,nonatomic)IBOutlet UIButton *btn_emailCode;
@property(strong,nonatomic)IBOutlet UIButton *btn_password;

@property(strong,nonatomic)IBOutlet UILabel *lbl_emailCode;
@property(strong,nonatomic)IBOutlet UILabel *lbl_password;

@property(strong,nonatomic)IBOutlet UIButton *btn_send;

@end

NS_ASSUME_NONNULL_END
