//
//  ChangePasswordNow.m
//  FanBox
//
//  Created by MacUser on 19/07/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import "ChangePasswordNow.h"

@implementation ChangePasswordNow

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [StaticData setupTabbarControllerWithVC:self withChildView:self.view_tabbar withPageType:@"profile"];
    
    UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#979797"]];
    [self.btn_send setImage:img forState:UIControlStateNormal];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changePasswordNow)];
    [self.btn_send addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEmailCodeKeyboard)];
    [self.btn_emailCode addGestureRecognizer:tab];
    
    tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPasswordKeyboard)];
    [self.btn_password addGestureRecognizer:tab];
    
}

-(void)openEmailCodeKeyboard
{
    [self.txt_emailCode becomeFirstResponder];
}

-(void)openPasswordKeyboard
{
    [self.txt_password becomeFirstResponder];
}

-(void)hideKeyboard
{
    [self.txt_emailCode resignFirstResponder];
    [self.txt_password resignFirstResponder];
}

-(void)changePasswordNow
{
    [HUD showUIBlockingIndicator];
    if (self.txt_emailCode.text.length == 0) {
        [StaticData showAlertView:@"الرجاء إدخال رمز البريد الإلكتروني"  withvc:self];
    } else if (self.txt_password.text.length == 0) {
        [StaticData showAlertView:EnterNewPassword withvc:self];
    } else if (self.txt_password.text.length < 8)
    {
        [StaticData showAlertView:PasswordRequiredMoreChar withvc:self];
    } else {
        NSString *path = [NSString stringWithFormat:@"%@/change_password?key=%@&user_id=%@",BaseAuthURL,BaseKEY,BaseUID];
        NSDictionary *prams = @{@"token":self.txt_emailCode.text,@"password":self.txt_password.text};

        [self executeApiCallToGetResponse:path withParameters:prams withCallType:@"ChangePassword"];
    }
}

-(void)executeApiCallToGetResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [HUD hideUIBlockingIndicator];
        [StaticData log:[NSString stringWithFormat:@"callType %@ = %@",callType,responseObject]];
        [self responseRecieved:responseObject withCallType:callType];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [HUD hideUIBlockingIndicator];
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData)
        {
            NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            if (serializedData && serializedData[@"error"])
            {
                [StaticData showAlertView:serializedData[@"error"] withvc:self];
            }
        }
    }];
    
}

-(void)responseRecieved:(id)responseObjct withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"ChangePassword"])
        {
            if ([responseObjct isKindOfClass:[NSDictionary class]] && responseObjct[@"error"])
            {
                NSString *error = [StaticData checkStringNull:responseObjct[@"error"]];
                if(error.length > 0)
                {
                    [StaticData showAlertView:error withvc:self];
                }
            } else
            {
                if([responseObjct isKindOfClass:[NSDictionary class]])
                {
                    NSString *status = [responseObjct objectForKey:@"success"];
                    if([status isEqualToString:@"yes"])
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message: @"تم تغيير كلمة المرور بنجاح" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"حسناً" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self.navigationController popViewControllerAnimated:false];
                            self.BackToLoginCallBack();
                        }];
                        [alert addAction:cancel];
                        
                        [self presentViewController:alert animated:true completion:nil];
                    }
                }
            }
        }
    } @catch (NSException *exception) {
        [StaticData showAlertView:@"Exception found please try again." withvc:self];
    } @finally {
        
    }
}

-(IBAction)backToVC:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txt_emailCode) {
        self.lbl_emailCode.text = textField.text;
        self.txt_emailCode.text = textField.text;
        if (self.lbl_emailCode.text.length == 0) {
            self.lbl_emailCode.text = @"كود البريد الإلكتروني";
        }
    } else if (textField == self.txt_password) {
        [self makePasswordSecure:textField];
        self.txt_password.text = textField.text;
        if (self.lbl_password.text.length == 0) {
            self.lbl_password.text = @"كلمة مرور جديدة";
        }
    }
    return true;
}

-(void)makePasswordSecure:(UITextField *)textField
{
    NSMutableString *dottedPassword = [NSMutableString new];

    for (int i = 0; i < [textField.text length]; i++)
    {
        [dottedPassword appendString:@"●"]; // BLACK CIRCLE Unicode: U+25CF, UTF-8: E2 97 8F
    }

    self.lbl_password.text = dottedPassword;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.txt_emailCode.hidden = true;
    if (textField == _txt_emailCode) {
        self.img_emailCode.image = [UIImage imageNamed:@"bg_textField"];
    } else if (textField == _txt_password) {
        self.img_password.image = [UIImage imageNamed:@"bg_textField"];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_txt_emailCode == textField) {
        self.img_emailCode.image = [UIImage imageNamed:@"bg_textField_white"];
    } else if (_txt_password == textField) {
        self.img_password.image = [UIImage imageNamed:@"bg_textField_white"];
    }
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
  
    if (context.previouslyFocusedView == self.btn_emailCode)
    {
        self.img_emailCode.image = [UIImage imageNamed:@"bg_field_white"];
    }
    
    if (context.nextFocusedView == self.btn_emailCode) {
        self.img_emailCode.image = [UIImage imageNamed:@"bg_textField_pink"];
    }
    
    if (context.previouslyFocusedView == self.btn_password)
    {
        self.img_password.image = [UIImage imageNamed:@"bg_field_white"];
    }
    
    if (context.nextFocusedView == self.btn_password) {
        self.img_password.image = [UIImage imageNamed:@"bg_textField_pink"];
    }
    
    if (context.previouslyFocusedView == self.btn_send)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:[StaticData colorFromHexString:@"#979797"]];
        [self.btn_send setImage:img forState:UIControlStateNormal];
    }
    
    if (context.nextFocusedView == self.btn_send)
    {
        UIImage *img = [StaticData changeImageColor:[UIImage imageNamed:@"btn_send"] withColor:AppColor];
        [self.btn_send setImage:img forState:UIControlStateNormal];
    }
}

-(NSArray *)preferredFocusEnvironments
{
    return @[];
}

@end

