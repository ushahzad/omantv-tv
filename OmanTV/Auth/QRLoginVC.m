//
//  QRLoginVC.m
//  OmanTV
//
//  Created by Cenex Studio on 02/09/2021.
//  Copyright © 2021 MacUser. All rights reserved.
//

#import "QRLoginVC.h"
#import "LoginVC.h"

@interface QRLoginVC ()

@end

@implementation QRLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.isViewActive = false;
    [self inValidateAllTimer];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.img_qrCode.image = nil;
    self.lbl_picCode.text = @"";
    self.lbl_loginUrl.text = @"";
    [self loadQrCodeAndPin];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.isViewActive = true;
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginAction)];
    [self.btn_login addGestureRecognizer:tab];
}

-(void)loadQrCodeAndPin
{
    [self.indicator startAnimating];
    NSString *path = [NSString stringWithFormat:@"%@/apps_rest/generate_login_qr?key=%@&user_id=%@&smartTV=1",BaseURL,BaseKEY,BaseUID];
    [self executeApiToGetApiResponse:path withParameters:@{@"app_id":AppID} withCallType:@"QrInfo"];
}

-(void)callQrValidation
{
    if (self.qrInfo)
    {
        NSString *pin = [StaticData checkStringNull:self.qrInfo[@"pin"]];
        if (pin.length > 0)
        {
            [self.loginIndicator startAnimating];
            NSString *path = [NSString stringWithFormat:@"%@/endpoint/qr_auth/login_by_pin?key=%@&user_id=%@",BaseURL,BaseKEY,BaseUID];
            [self executeApiToGetApiResponse:path withParameters:@{@"pin":pin} withCallType:@"QrValidation"];
        }
    }
}

-(void)executeApiToGetApiResponse:(NSString *)path withParameters:(NSDictionary *)params withCallType:(NSString *)callType
{
    [StaticData log:[NSString stringWithFormat:@"Call Api %@",path]];
    [StaticData log:[NSString stringWithFormat:@"Call Prams %@",params]];
    [StaticData log:[NSString stringWithFormat:@"Call Type %@",callType]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];

    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@" Call Type %@ = %@",callType,responseObject]];
        [self responseReceived:responseObject withCallType:callType];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.indicator stopAnimating];
        [StaticData log:[NSString stringWithFormat:@"Error Message = %@",error.description]];
    }];
}

-(void)responseReceived:(id)responseObject withCallType:(NSString *)callType
{
    @try {
        if ([callType isEqualToString:@"QrInfo"])
        {
            self.qrInfo = responseObject;
            [self loadQrImage:[StaticData checkStringNull:responseObject[@"qr_path"]]];
            self.lbl_picCode.text = [StaticData checkStringNull:responseObject[@"pin"]];
            self.lbl_loginUrl.text = [StaticData checkStringNull:responseObject[@"login_base_url"]];
        } else if ([callType isEqualToString:@"QrValidation"])
        {
            if (!self.isViewActive) return;
            
            NSString *error = [StaticData checkStringNull:responseObject[@"error"]];
            if (error.length == 0)
            {
                NSString *status = [responseObject objectForKey:@"success"];
                if([status isEqualToString:@"yes"])
                {
                    [self.loginIndicator stopAnimating];
                    NSDictionary *userDic = [StaticData checkDictionaryNull:responseObject[@"data"]];
                    
                    [StaticData saveLoginUserInfo:userDic];
                    [StaticData setupLogedUserProfileInfo];
                    
                    
                    [self startAppNow];
                    
                    [self inValidateAllTimer];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    } @finally {
    }
}

-(void)startAppNow
{
    [StaticData setRootViewController];
}


-(void)loadQrImage:(NSString *)qrPath
{
    @try {
        [self.indicator startAnimating];
        NSString *imagePath = [@"https://statres.cdn.mgmlcdn.com/analytics/" stringByAppendingString:qrPath];
        imagePath = [imagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.img_qrCode.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.img_qrCode sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self checkUserLoginOnMobile];
                 [self.indicator stopAnimating];
             } else {
                 [StaticData downloadImageIfNotLoadinedBySdImage:self.img_qrCode withUrl:[NSURL URLWithString:imagePath] completed:^(UIImage *image) {
                     if (image) {
                         [self checkUserLoginOnMobile];
                     }
                     [self.indicator stopAnimating];
                 }];
             }
         }];
        
    } @catch (NSException *exception) {
    } @finally {
    }
}

// here we will call qr validation api after 5 seconds until 10 minutes
-(void)checkUserLoginOnMobile
{
    if ([self.fiveSecondTimer isValid]) {
        [self.fiveSecondTimer invalidate];
    }
    
    self.fiveSecondTimer = [NSTimer scheduledTimerWithTimeInterval:5 repeats:true block:^(NSTimer * _Nonnull timer) {
        [self callQrValidation];
    }];
    
    if ([self.tenMinutesTimer isValid]) {
        [self.tenMinutesTimer invalidate];
    }
    
    self.tenMinutesTimer = [NSTimer scheduledTimerWithTimeInterval:600 repeats:false block:^(NSTimer * _Nonnull timer) {
        if ([self.fiveSecondTimer isValid]) {
            [self.fiveSecondTimer invalidate];
        }
        
        if ([self.tenMinutesTimer isValid]) {
            [self.tenMinutesTimer invalidate];
        }
    }];
    
}

-(void)inValidateAllTimer
{
    self.isViewActive = false;
    if ([self.fiveSecondTimer isValid]) {
        [self.fiveSecondTimer invalidate];
    }
    
    if ([self.tenMinutesTimer isValid]) {
        [self.tenMinutesTimer invalidate];
    }
}

-(void)loginAction
{
    LoginVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:vc animated:true];
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
    if (context.nextFocusedView == self.btn_qrCode)
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.img_qrCode.transform = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:nil];
        
    }
    
    if (context.nextFocusedView == self.btn_login)
    {
        self.lbl_login.textColor = [UIColor whiteColor];
        self.img_login.image = [StaticData changeImageColor:[UIImage imageNamed:@"login_profile_icon"] withColor:[UIColor whiteColor]];
    }
    
    if (context.previouslyFocusedView == self.btn_qrCode)
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.img_qrCode.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } completion:nil];
        
    }
    
    if (context.previouslyFocusedView == self.btn_login)
    {
        self.lbl_login.textColor = AppColor;
        self.img_login.image = [StaticData changeImageColor:[UIImage imageNamed:@"login_profile_icon"] withColor:AppColor];
        
    }
}

@end
