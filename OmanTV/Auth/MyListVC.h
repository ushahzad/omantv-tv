//
//  MyListVC.h
//  ADtv
//
//  Created by MacUser on 26/11/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScheduleVideosVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyListVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;
@property (nonatomic, weak) IBOutlet UIView *view_parent;
@property (nonatomic, weak) IBOutlet UILabel *lbl_noResultsFound;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;

@property (nonatomic, weak) IBOutlet UICollectionView *cv_types;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIButton *btn_delete;
@property (nonatomic, weak) IBOutlet UIImageView *img_delete;
@property (nonatomic, retain) MediaInfo *selectedType;
@property (nonatomic, retain)NSMutableArray *typesList;
@property (nonatomic, retain)NSMutableArray *contentList;
@property (nonatomic, retain) UIImage *tempImage;
@property (nonatomic, assign) BOOL isDeleteAble;
@end

NS_ASSUME_NONNULL_END
