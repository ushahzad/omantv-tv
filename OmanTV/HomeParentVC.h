//
//  MainVC.h
//  Majid
//
//  Created by MacUser on 31/05/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeBanner.h"
#import "LatestEpisodesVC.h"
#import "CustomCarouselsVC.h"
#import "HomeChannelsTypeVC.h"
#import "HomeLiveVC.h"
#import "ResumeWatchingVC.h"
#import "MostWatchedVC.h"
#import "NowOnTVVC.h"
#import "HomeProgramsVC.h"
#import "RecomendtationShowsVC.h"
#import "FeaturedShowsHomeInfo.h"
#import "HomeAudioBooksTabVC.h"
#import "HomeAudioBooksVC.h"
#import "HomeRamadanVC.h"
#import "LatestPodcastsVC.h"
#import "SetupCustomCarousels.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeParentVC : UIViewController{
    dispatch_group_t group;
}
typedef void(^AnimationCompletionBlock)(BOOL isCompleted);
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll_view;
@property (nonatomic, weak) IBOutlet UIView *view_content;
@property (nonatomic, weak) IBOutlet UIView *view_tabbar;

@property (nonatomic, retain) IBOutlet UIView *view_tvDisplay;
@property (nonatomic, retain) FeaturedShowsHomeInfo *vc_featureShowsInfo;
@property (retain, nonatomic) HomeBanner *vc_homeBanner;
@property (retain, nonatomic) HomeChannelsTypeVC *vc_homeChannelsTypeVC;
@property (retain, nonatomic) HomeLiveVC *vc_homeLiveVC;
@property (retain, nonatomic) NowOnTVVC *vc_nowOnTVVC;
@property (retain, nonatomic) HomeRamadanVC *vc_homeRamadanVC;
@property (retain, nonatomic) HomeProgramsVC *vc_homeProgramsVC;
@property (nonatomic, strong) LatestPodcastsVC *vc_latestPodcastsVC;
@property (retain, nonatomic) LatestEpisodesVC *vc_latestEpisodesVC;
@property (retain, nonatomic) RecomendtationShowsVC *vc_recomendtationShowsVC;
@property (retain, nonatomic) MostWatchedVC *vc_mostWatchedVC;
@property (retain, nonatomic) ResumeWatchingVC *vc_resumeWatchingVC;
@property (retain, nonatomic) TabbarController *vc_tabbarVC;
@property (nonatomic, retain) NSLayoutConstraint *featuredViewHeightAnchor;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollTopConstraint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic, retain) NSMutableArray *contentList;
@property (nonatomic, retain) NSMutableArray *customCarouselListVC;
@property (nonatomic, strong) HomeAudioBooksTabVC *vc_homeAudioBooksTabVC;
@property (nonatomic, strong) HomeAudioBooksVC *vc_homeAudioBooksVC;

@property (nonatomic, retain) id homeConfig;
@property (nonatomic, assign) BOOL isSwipeUpCallOnHomeBanner;
@property (nonatomic, assign) BOOL isHomeRadio;
@property (nonatomic, assign) BOOL isAudioPlay;
@property (nonatomic, retain) MediaInfo *activeShow;
@end

NS_ASSUME_NONNULL_END
